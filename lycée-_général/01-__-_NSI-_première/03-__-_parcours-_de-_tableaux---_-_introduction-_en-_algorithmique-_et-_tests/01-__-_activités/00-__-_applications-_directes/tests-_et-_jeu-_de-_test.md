# Tests et jeu de test

+++consignes
<ol start="0">
    <li>Créez vous un dossier du jour en exécutant chacune des lignes ci-dessous sur un terminal. <b>Attention !</b> n'oubliez pas d'adapter les lignes 2 et 3 </li>
  </ol>
  <pre><code class="language-bash hljs"><span class="hljs-comment"># lignes à réadapter et à exécuter dans un terminal</span>
nom_prenom=dupond_cedric
dateDuJour=18_10_21

mkdir /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour} && cd /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
touch jeudetest.py
thonny jeudetest.py
</code></pre>
  <ol start="1">
  <li>Dérouler l'activité et répondre aux questions</li>
<ol>
+++

En cours nous avons vu comment tester la fonction `divisionEntiere` avec un jeu de test de dividendes : 

```python
def divisionEntiere(dividende, diviseur):
  assert isinstance(dividende, int) or isinstance(dividende, float) 
  assert isinstance(diviseur, int) or isinstance(diviseur, float) 
  return int(dividende / diviseur)

dividendes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
for dividende in dividendes:
  resultat = divisionEntiere(dividende, 7)
  assert isinstance(resultat, int)
  print(resultat)
```

+++question write "Quel est le type de la variable dividende ?"

+++spoil "Aide"
  Relire les cours du module 2
spoil+++

+++question write "Dans la ligne 7 est utilisée l'instruction <code>for</code>, rappelez à quoi sert cette instruction"

+++spoil "Aide"
  Relire les cours du module 1
spoil+++

<hr>

Nous souhaitons nous assurer que la fonction `divisionEntiere` fonctionne bien pour un diviseur compris entre -5 (compris) et 5 (compris)

Voici le jeu de test que nous devons soumettre à la fonction : 

- dans mon programme, je veux tester si la fonction `divisionEntiere` fonctionne bien avec le dividende 20 et le diviseur **5**
- dans mon programme, je veux tester si la fonction `divisionEntiere` fonctionne bien avec le dividende 20  et le diviseur **4**
- dans mon programme, je veux tester si la fonction `divisionEntiere` fonctionne bien avec le dividende 20 et le diviseur **3**
- ...
- dans mon programme, je veux tester si la fonction `divisionEntiere` fonctionne bien avec le dividende 20 et le diviseur **-4**
- dans mon programme, je veux tester si la fonction `divisionEntiere` fonctionne bien avec le dividende 20 et le diviseur **-5**

+++question "Modifiez le code dans votre IDE afin de tester la fonction <code>divisionEntiere</code> avec le jeu de test décris ci-dessus"

+++todo une partie des élèves perdus ici, peut être du au mélange fonction + tableau + for + parcours de tableau . 2 solutions : ajouter des étapes intermédiaires dans cet exercice + prendre en amont davantage de temps sur les parties fonction + tableau + for + parcours de tableau

+++question write "Suite à la question précédente, une erreur apparaît dans le terminal lors de l'exécution du programme. En analysant le message d'erreur, écrivez le cas de test qui provoque cette erreur"

<hr>

Comme la fonction `divisionEntiere` prend deux paramètres en entrée (le dividende et le diviseur), il serait par exemple intéressant de tester tous les cas de figures possibles des combinaisons de dividende et diviseurs compris entre 10 et 20. 

Le jeu de tests serait : 

- dans mon programme, je veux tester si la fonction `divisionEntiere` fonctionne bien avec le dividende **10** et le diviseur **10**
- dans mon programme, je veux tester si la fonction `divisionEntiere` fonctionne bien avec le dividende **10** et le diviseur **11**
- ...
- dans mon programme, je veux tester si la fonction `divisionEntiere` fonctionne bien avec le dividende **11** et le diviseur **10**
- dans mon programme, je veux tester si la fonction `divisionEntiere` fonctionne bien avec le dividende **11** et le diviseur **11**
- ...
- dans mon programme, je veux tester si la fonction `divisionEntiere` fonctionne bien avec le dividende **20** et le diviseur **19** 
- dans mon programme, je veux tester si la fonction `divisionEntiere` fonctionne bien avec le dividende **20** et le diviseur **20**

+++question write "Combien de tests contient ce jeu de tests ?"

<hr>

Pour information, voici le code qui permet de tester toutes les combinaisons de dividende et de diviseurs allant de 10 (compris) à 20 (compris)

Pendant l'année nous aurons l'occasion de nous intéresser plus en détail sur les boucles imbriquées en programmation. 

widget python
def divisionEntiere(dividende, diviseur):
  assert isinstance(dividende, int) or isinstance(dividende, float) 
  assert isinstance(diviseur, int) or isinstance(diviseur, float) 
  return int(dividende / diviseur)

dividendes = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
diviseurs = dividendes

for dividende in dividendes:
  for diviseur in diviseurs:
    resultat = divisionEntiere(dividende, diviseur)
    assert isinstance(resultat, int)
    print(resultat)
widget

<hr>

Pour les personnes en avance, reprenez le premier exemple du cours : 

```python
def attaque(perso1, perso2, degat):
  print(perso1 + ' attaque ' + perso2 + ' et inflige ' + str(degat) + ' dégat(s) !')

personnageJoueur = 'genos'
attaque(personnageJoueur, 'saitama', 1)
```

Définissez un jeu de test pour tester la fonction `attaque` et implémentez le dans un fichier python



## Réponses

<ol start="2">
  <li>
+++spoil
widget python
def divisionEntiere(dividende, diviseur):
  assert isinstance(dividende, int) or isinstance(dividende, float) 
  assert isinstance(diviseur, int) or isinstance(diviseur, float) 
  return int(dividende / diviseur)

dividendes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
for dividende in dividendes:
  resultat = divisionEntiere(dividende, 7)
  assert isinstance(resultat, int)
  print(resultat)
  
diviseurs = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
for diviseur in diviseurs:
  resultat = divisionEntiere(20, diviseur)
  assert isinstance(resultat, int)
  print(resultat)
widget
spoil+++
  </li>
  <li>
+++spoil
L'erreur est : <span class="text-red">"ZeroDivisionError: division by zero"</span>. Le cas de test qui provoque cette erreur est : 
- dans mon programme, je veux tester si la fonction `divisionEntiere` fonctionne bien avec le dividende 20 et le diviseur 0

spoil+++
  </li>
  <li>
+++spoil
L'erreur est : <span class="text-red">"ZeroDivisionError: division by zero"</span>. Le cas de test qui provoque cette erreur est : 
- dans mon programme, je veux tester si la fonction `divisionEntiere` fonctionne bien avec le dividende 20 et le diviseur 0
spoil+++
  </li>
</ol></ol></ol>