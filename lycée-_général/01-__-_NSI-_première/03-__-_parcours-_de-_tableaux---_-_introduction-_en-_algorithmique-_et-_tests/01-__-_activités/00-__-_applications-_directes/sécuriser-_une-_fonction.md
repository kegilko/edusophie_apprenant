# Sécuriser une fonction

+++consignes
<ol start="0">
    <li>Créez vous un dossier du jour en exécutant chacune des lignes ci-dessous sur un terminal. <b>Attention !</b> n'oubliez pas d'adapter les lignes 2 et 3 </li>
  </ol>
  <pre><code class="language-bash hljs"><span class="hljs-comment"># lignes à réadapter et à exécuter dans un terminal</span>
nom_prenom=dupond_cedric
dateDuJour=18_10_21

mkdir /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour} && cd /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
touch securiserUneFonction.py
thonny securiserUneFonction.py
</code></pre>
  <ol start="1">
  <li>Dérouler l'activité et répondre aux questions</li>
<ol>
+++

La plupart des langages informatiques mettent nativement à disposition la fonction `abs()`. Cette fonction prend un paramètre qui est un entier et renvoie en sortie la valeur `abs`olue de cet entier.

Le but de cette courte activité est de tester si cette fonction fournit par python fait bien le travail attendu. 

+++question write "Écrivez un exemple simple d'utilisation de la fonction <code>abs()</code> en précisant ce qu'affiche comme résultat votre exemple"

+++spoil "Aide"
Aidez vous d'une recherche sur Internet pour voir comment utiliser cette fonction
spoil+++

+++question write "Quels tests peut on faire sur le résultat retourné par la fonction <code>abs()</code> ?"

+++question "Traduisez l'exemple que vous avez écris en question 0 en python dans le fichier 'securiserUneFonction'. Ajoutez dans le programme les tests que vous avez énoncés dans la question 1 "

## Résultats

<ol>
  <li>
    +++spoil
widget python
nombre = -17
nombre = abs(nombre)
print(nombre)
widget
    spoil+++
  </li>
  <li>
    +++spoil
<ul>
  <li>Nous pouvons vérifier si le résultat retourné est un nombre entier (+++flag-us <code>int</code>)</li>
  <li>Nous pouvons vérifier que le résultat soit supérieur ou égal à 0</li>
</ul>
    spoil+++
  </li>
  <li>
    +++spoil
widget python
nombre = -17
nombre = abs(nombre)

assert isinstance(nombre, int) and nombre >= 0

print(nombre)
widget

+++bulle matthieu
  remarque, nous pouvons utiliser l'instruction <code>if</code> pour effectuer des tests
+++

widget python
nombre = -17
nombre = abs(nombre)

if(isinstance(nombre, int) and nombre > 0):
  print(nombre)
else:
  raise ValueError("Erreur ! soit nombre n'est pas un entier soit nombre est inférieur à zéro")
widget

    spoil+++
    
    
  </li>
</ol></ol></ol>