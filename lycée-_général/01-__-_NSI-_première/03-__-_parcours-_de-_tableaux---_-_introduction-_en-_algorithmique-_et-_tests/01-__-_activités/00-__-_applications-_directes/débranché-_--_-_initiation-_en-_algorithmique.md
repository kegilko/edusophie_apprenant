# Initiation en algorithmique

+++consigne
  Suivre le début de l'activité en classe entière
+++

Dans cette activité, vous allez rédiger une suite d'étapes à suivre pour effectuer une tâche. La vidéo ci-dessous illustre un bon exemple de rédaction et de lecture d'une consigne :

+++video "https://www.youtube.com/watch?v=KY2z0YSw0Lk" "challenge 'consignes exactes'"

## Rédaction d'une notice pour enfant

Imaginons un jeu de carte composé de cartes dont les valeurs peuvent aller de 0 (compris) à 9 (compris). Lors d'une partie, un joueur peut être amené à piocher une carte et à l'ajouter dans sa main. 

Dans le jeu en cours, il est pratique de rassembler dans sa main les cartes possédant le même chiffre. 

Par exemple si dans ma main j'ai, de gauche à droite, les cartes suivantes : 

<style>
  .carte{
    color:white;
    padding:3rem 2rem;
    font-size:2rem;
    border-radius: 0.25rem !important;
    font-weight: bold;
    margin-bottom:1rem;
    text-align:center;
    background-color:green;
    border:1px solid black;
  }
</style>

<div class="d-flex flex-wrap justify-content-evenly mb-2">
  <div class="carte">4</div>
  <div class="carte">7</div>
  <div class="carte">6</div>
  <div class="carte">2</div>
  <div class="carte">0</div>
</div>

Et si je pioche ensuite la carte portant le <span class="p-1 m-1 border border-primary border-2">numéro 6</span>, je la place alors à côté de l'autre carte numéro 6 que je possède déjà : 

<div class="d-flex flex-wrap justify-content-evenly mb-2">
  <div class="carte">4</div>
  <div class="carte">7</div>
  <div class="carte">6</div>
  <div class="carte border-primary border border-3">6</div>
  <div class="carte">2</div>
  <div class="carte">0</div>
</div>

Prenez une feuille A4 et remplissez la comme suit : 

+++bulle matthieu
vous pouvez utiliser aussi à la place un vrai jeu de carte, à condition qu'il contienne des cartes avec un chiffre dessus et en plusieurs exemplaires
+++

+++image "/images/md/EFG967B9F3A6ED6FG652G2C2888AEF"
<p class="text-center">le +++fichier "/fichiers/44399AGC6BF2A4EB4B9B5288AEB998" "lien vers le fichier format png"  et le +++fichier "/fichiers/7287D8F77C8F96284GBGF785BC5586" "lien vers le fichier gimp au format xcf" </p>

+++prof lire la consigne avec les élèves et faire une démo avec un jeu de carte

<br>
+++consignes
Lisez toute la consigne avec votre groupe, et ensuite suivez les étapes en respectant le temps indiqué <i class="fas fa-stopwatch"></i> (chronométrez vous)
<br><br>
<ol start="0">
  <li>(<i class="fas fa-stopwatch"></i> 5 minutes max) Chacun de votre côté  et sans interagir avec le reste du groupe, écrivez la marche à suivre (comme si vous deviez l'expliquer à un enfant) pour ajouter une nouvelle carte dans votre main de gauche en respectant les règles suivantes : <ul>
    <li>La nouvelle carte doit être insérée à côté d'une autre carte similaire dans la main gauche. Si il n'y a pas une autre carte similaire déjà en main, gérez ce cas de figure comme vous le souhaitez</li>
    <li class="text-red"><i class="fas fa-eye"></i> Le joueur ne peut visualiser qu'une seule carte à la fois dans la main gauche </li>
  </ul></li>
  <li>Échangez les notices dans votre groupe de telle sorte que chaque membre ait une notice dont il n'est pas le rédacteur</li>
  <li>(<i class="fas fa-stopwatch"></i> 3 minutes max) Chacun de votre côté, prenez en main gauche 6 cartes (celles que vous voulez) et piochez une carte (main droite). Suivez <b>scrupuleusement</b> la notice qu'on vous a donnée. (comme dans la vidéo de début d'activité). Notez : <ul>
    <li>Si vous avez obtenu le résultat attendu ou, si il y a eu un problème et l'étape qui a posé problème</li>
    <li>Le nombre de comparaisons de cartes que vous avez effectués pour ajouter la carte en lisant la notice</li>
  </ul></li>
  <li>Comparez les notices du groupe, lesquelles ont fonctionné ? si une notice n'a pas fonctionné, déterminez avec le groupe pourquoi et corrigez la notice en conséquence.</li>
  <li>Une fois terminé, continuez l'activité</li>
</ol>
+++


<hr>
Le contenu de la notice influe sur le nombre de comparaisons de cartes que va devoir faire un joueur pour insérer la carte qui vient d'être piochée. 
+++question write "Quels autres paramètres peuvent faire varier le nombre de comparaisons de cartes que le joueur doit effectuer lorsqu'il pioche une nouvelle carte ?"

+++question write "La main gauche peut contenir un ensemble fini de cartes. En programmation, quel est le type de variable qui est le plus adapté pour contenir les données des cartes qu'a un joueur virtuel dans sa main gauche ?"

<hr>

Voici ci-dessous un exemple de notice : 

<div class="p-2 border border-dark rounded mb-3">
  <p>Étant donné une main remplie avec X cartes, lorsque vous piochez une carte : </p>
  <ol start="0">
    <li>Pour chaque cartes en main<ul>
      <li>Comparez la carte courante de la main et la carte piochée, si les deux cartes sont identiques, placez la carte piochée à droite de la carte de la main et c'est terminé</li>
    </ul></li>
    <li>Placez la carte piochée à la fin de la main et c'est terminé</li>
  </ol>
</div>

Dans le meilleur des cas de figure, le première carte de la main est identique à celle qui vient d'être piochée. Il y a donc au total qu'une seule comparaison de faîte en suivant la notice ci-dessus.

+++question write "Quel serait le pire des cas de figure ?"

+++question write "Dans le pire des cas, combien de comparaisons sont effectuées lorsque le joueur à 6 cartes en main ? et lorsqu'il a 9 cartes en mains ? et lorsqu'il a 1000 cartes en mains ?"

+++question write "À raison de 0.1 seconde pour visualiser et comparer deux cartes entre elles, combien de temps faudrait il dans le pire des cas pour insérer une carte qui vient d'être piochée dans une main qui contient 1000 cartes ?"

+++bulle matthieu
    pour les groupes en avance, réfléchissez ensemble à une notice qui explique comment ranger dans l'ordre les cartes dans la main gauche en respectant toujours les mêmes contraintes
+++

+++bulle matthieu droite
    ce type d'opération appartient aux algorithmes de tri que nous verrons plus tard dans l'année :)
+++

<hr>

Partons du principe que le joueur a en main les cartes triées dans l'ordre, de la plus petite valeur à la plus grande, un exemple de main possible avant de piocher peut être : 

<div class="d-flex flex-wrap justify-content-evenly mb-2">
  <div class="carte">0</div>
  <div class="carte">2</div>
  <div class="carte">4</div>
  <div class="carte">6</div>
  <div class="carte">6</div>
  <div class="carte">7</div>
</div>

Le fait que les cartes soient triées dans l'ordre permet d'insérer une carte de manière plus efficace. Plus efficace dans le sens où moins de comparaisons vont être nécessaires pour insérer une nouvelle carte.

+++consignes
  <ul>
    <li>(<i class="fas fa-stopwatch"></i> 15 minutes max) En binome, réfléchissez à une nouvelle notice pour insérer une carte piochée dans une main triée. Cette nouvelle notice doit, dans le pire des cas, faire effectuer moins de comparaisons que dans la première notice. Rédigez ensuite cette nouvelle notice qui explique à un joueur comment insérer une carte piochée dans sa main triée</li>
    <li>Une fois la notice rédigée : <ul>
      <li>Trouvez un autre groupe qui a terminé, échangez vos notices et suivez la nouvelle notice <b>scrupuleusement</b></li>
      <li>Échangez avec l'autre groupe : est-ce que la notice permet bien d'insérer une nouvelle carte dans la main ? Sur quel(s) point(s) l'exemple du dictionnaire a influencé le contenu de la notice ?</li>
    </ul></li>
    <li>Une fois terminé, continuez l'activité</li>
  </ul>
+++

+++todo cette phase est un peu longue : peut être proposer une correction collective et corriger ensemble les dernières questions 

+++question write "Avec cette nouvelle notice et dans le pire des cas, combien de comparaisons sont effectuées lorsque le joueur à 6 cartes en main ? et lorsqu'il a 9 cartes en mains ? et lorsqu'il a 1000 cartes en mains ?"

+++question write "Avec cette nouvelle notice, à raison de 0.1 seconde pour visualiser et comparer deux cartes entre elles, combien de temps faudrait il dans le pire des cas pour insérer une carte qui vient d'être piochée dans une main qui contient 1000 cartes ?"

+++bulle matthieu
    vous attendez qu'un groupe finisse sa notice ou vous avez pu tester votre notice ?
+++

+++bulle matthieu droite
  continuez à réfléchir ensemble à une notice qui explique comment ranger dans l'ordre les cartes dans la main gauche en respectant toujours les mêmes contraintes
+++

<div class="d-none">

Ci-dessous se trouve un programme python qui simule une partie de carte. Le but va être d'analyser le code à travers quelques questions, puis de rédiger une notice depuis 

widget python
import random

main = []

# Début de partie : on pioche une main de 5 cartes

# On génère 5 fois un nombre aléatoire entre 0 et 9, et on l'insère dans la main


def chercheUneCarteDansLaMain(main, numero):
  position = 0
  for carte in main:
    if carte == numero:
      return position
    position = position + 1
  return -1

position = chercheUneCarteDansLaMain(main, carteDeLaPioche)

if(position != -1):
  print("Je possède déjà un exemplaire dans ma main de la carte que je viens de piocher.\n")
  main.insert(position, carteDeLaPioche)
else:
  print("Je ne possède pas d'exemplaire dans ma main de la carte que je viens de piocher. J'ajoute cette dernière en première position.\n")
  main.insert(0, carteDeLaPioche)
  
print("Voici ma nouvelle main : ")
print(main)
widget

widget python
main = [4, 7, 6, 6, 2, 0]
carteDeLaPioche = 2

def chercheUneCarteDansLaMain(main, numero):
  position = 0
  for carte in main:
    if carte == numero:
      return position
    position = position + 1
  return -1

position = chercheUneCarteDansLaMain(main, carteDeLaPioche)
widget

+++question write "Rappelez à quoi sert l'instruction <code>def</code> en python et qui se trouve à la ligne 4"

+++spoil "Aide"
  Re-parcourir le cours sur les fonctions
spoil+++

+++question write "En rappelant à quoi sert l'instruction <code>for</code> en programmation, que fait d'après vous la ligne 6 ?"

+++spoil "Aide"
  Vous ne vous souvenez plus à quoi sert l'instruction <code>for</code> ? re-parcourez le cours sur les instructions itératives
spoil+++

<hr>

Pour rappel, l'instruction `return` stoppe l'exécution de la fonction en cours et renvoie vers le programme principal la donnée qui se trouve à droite de l'instruction `return`. Par exemple, si la ligne 10 est exécutée, l'exécution de la fonction `chercheUneCarteDansLaMain` se stoppe et renvoie vers le programme principal la valeur `-1`. 

+++question write "Expliquez en quelques mots ce que fait la fonction <code>chercheUneCarteDansLaMain</code>. Que prend-elle en paramètre et que retourne-t-elle ?"

+++spoil "Aide"
  Testez le programme en visualisant le contenu des variables. Ajoutez par exemple un print pour afficher la valeur de <code>position</code>. Vous pouvez vous aider aussi du mode déboguer de votre IDE.
spoil+++

<hr>

Le programme actuel n'affiche rien lorsqu'il est exécuté.

+++question "Sans modifier le code actuel, ajoutez des lignes après la ligne 12 pour que le programme affiche un message à la fin de son exécution. Ce message indique si le joueur possède déjà dans sa main la carte qu'il vient de piocher"

+++question "En recherchant sur Internet comment insérer un élément dans un tableau, modifiez le code pour que le programme ajoute dans la main la carte qui vient d'être piochée"

</div>

## Réponses

+++spoil "Exemples de notice" text-green
<div class="p-2 border border-dark rounded mb-3">
  <p>Étant donné une main gauche remplie avec 6 cartes, lorsque vous piochez une carte : </p>
  <ol start="0">
    <li>Regardez si la première carte est la même que celle piochée.<ul>
      <li>Si oui, placez la carte piochée à droite de la première carte et c'est terminé</li>
    </ul></li>
    <li>Regardez si la deuxième carte est la même que celle piochée.<ul>
      <li>Si oui, placez la carte piochée à droite de la deuxième carte et c'est terminé</li>
    </ul></li>
    <li>Regardez si la troisième carte est la même que celle piochée.<ul>
      <li>Si oui, placez la carte piochée à droite de la troisième carte et c'est terminé</li>
    </ul></li>
    <li>Regardez si la quatrième carte est la même que celle piochée.<ul>
      <li>Si oui, placez la carte piochée à droite de la quatrième carte et c'est terminé</li>
    </ul></li>
    <li>Regardez si la cinquième carte est la même que celle piochée.<ul>
      <li>Si oui, placez la carte piochée à droite de la cinquième carte et c'est terminé</li>
    </ul></li>
    <li>Placez la carte piochée à droite de la sixième carte et c'est terminé</li>
  </ol>
</div>

<p>et sa version plus générique</p>

<div class="p-2 border border-dark rounded mb-3">
  <p>Étant donné une main gauche remplie avec X cartes, lorsque vous piochez une carte : </p>
  <ol start="0">
    <li>Pour chaque cartes en main<ul>
      <li>Comparez la carte courante de la main et la carte piochée, si les deux cartes sont identiques, placez la carte piochée à droite de la carte de la main et c'est terminé</li>
    </ul></li>
    <li>Placez la carte piochée à la fin de la main et c'est terminé</li>
  </ol>
</div>

spoil+++

<ol start="0">
  <li>
    +++spoil
      Le nombre de comparaisons pour insérer dans la main une carte qui vient d'être piochée peut varier en fonction : 
      <ul>
        <li>Du nombre de cartes déjà en main</li>
        <li>Des numéros et de l'ordre des cartes en main</li>
      </ul>
    spoil+++
  </li>
  <li>
    +++spoil
      Le type de variable pouvant contenir un ensemble de données est un tableau (en Python, nous parlons de liste)
    spoil+++
  </li>
  <li>
    +++spoil
      Le pire des cas serait qu'il n'y ait pas dans la main gauche un autre exemplaire de la carte que le joueur vient de piocher. Dans ce cas-la, le joueur va comparer toutes les cartes une à une avec celle qui vient d'être piochée avant d'insérer la nouvelle carte.
    spoil+++
  </li>
  <li>
    +++spoil
      <ul>
        <li>Avec 6 cartes : il faut effectuer 6 comparaisons</li>
        <li>Avec 9 cartes : il faut effectuer 9 comparaisons</li>
        <li>Avec 1000 cartes : il faut effectuer 1000 comparaisons</li>
      </ul>
    spoil+++
  </li>
  <li>
    +++spoil
      Avec 1000 cartes, 1000 comparaisons sont effectuées. Donc il faudrait 1000 comparaisons x 0.1 seconde = 100 secondes, soit 1 minute 30 entre le moment de piocher une nouvelle carte et l'insérer dans la main (dans le pire des cas)
    spoil+++
  </li>
</ol>

+++spoil "Exemple de notice utilisant une recherche par dichotomie" text-green
<div class="p-2 border border-dark rounded mb-3">
  <p>Étant donné une main gauche remplie avec X cartes triées dans l'ordre. Lorsque vous piochez une carte : </p>
  <ol start="0">
    <li>Choisissez la carte au milieu de votre main gauche : <ul>
      <li>Si la carte choisie est identique à la carte piochée, placez la carte piochée à droite de la carte choisie. Si des cartes ont été posées sur la table lors du déroulement de cette notice, reprenez les en main en faisant attention à l'ordre et c'est terminé</li>
      <li>Sinon, si la carte choisie à une valeur inférieure à la carte piochée, posez sur la table la première moitié dans votre main gauche et revenir à l'étape 0</li>
      <li>Sinon, si la carte choisie à une valeur supérieure à la carte piochée, posez sur la table la deuxième moitié dans votre main gauche et revenir à l'étape 0</li>
      <li>Sinon, si il ne reste plus qu'une carte dans votre main gauche, placez la carte piochée à droite (resp. à gauche) de la carte choisie si la valeur de la carte piochée est inférieure (resp. supérieure) à celle de la carte choisie. Reprenez en main les cartes écartées sur la table en faisant attention à l'ordre et c'est terminé</li>
    </ul></li>
  </ol>
</div>

spoil+++

<ol start="5">
  <li>
    +++spoil
      <ul>
        <li>Avec 6 cartes : il faut effectuer 3 comparaisons</li>
        <li>Avec 9 cartes : il faut effectuer 4 comparaisons</li>
        <li>Avec 1000 cartes : il faut effectuer 10 comparaisons</li>
      </ul>
      Ces chiffres peuvent être trouvés à la main. Une méthode possible est de diviser par 2 le nombre total de carte jusqu'à ce qu'on atteint un nombre inférieur à 1, il suffit de compter le nombre de divisions par 2 qui ont été effectuées, par exemple avec 9 cartes : 
      <ul>
        <li>9 / 2 = 4.5</li>
        <li>4.5 / 2 = 2.25</li>
        <li>2.25 / 2 = 1.125</li>
        <li>1.125 / 2 = 0.5625</li>
      </ul>
      Il a fallu 4 divisions par 2 pour obtenir un nombre inférieur à 1 et donc, dans le pire des cas, il faut 4 comparaisons dans une main composée de 9 cartes pour trouver si oui ou non il y a parmi elles un même exemplaire que celle qui vient d'être piochée
    spoil+++
  </li>
  <li>
    +++spoil
      Avec 1000 cartes, 10 comparaisons sont effectuées. Donc il faudrait 10 comparaisons x 0.1 seconde = 1 seconde entre le moment de piocher une nouvelle carte et l'insérer dans la main (dans le pire des cas)
    spoil+++
  </li>
  <div class="d-none">
  <li>
    +++spoil
      La fonction <code>chercheUneCarteDansLaMain</code> permet de rechercher dans une main virtuelle si le joueur possède déjà ou non une carte. Si il possède déjà la carte donnée en paramètre (<code>numero</code>) alors la fonction retourne la position à laquelle a été trouvée la carte recherchée (<code>return position</code>). Sinon, la fonction renvoie <code>-1</code> (<code>return -1</code>)
    spoil+++
  </li>
  <li>
    +++spoil
      La ligne 6 permet de parcourir un tableau. Le tableau ici est <code>main</code> et chaque tour de boucle permet de passer d'un élément du tableau à l'autre
    spoil+++
  </li>
  <li>
    +++spoil
      La fonction <code>chercheUneCarteDansLaMain</code> permet de rechercher dans une main virtuelle si le joueur possède déjà ou non une carte. Si il possède déjà la carte donnée en paramètre (<code>numero</code>) alors la fonction retourne la position à laquelle a été trouvée la carte recherchée (<code>return position</code>). Sinon, la fonction renvoie <code>-1</code> (<code>return -1</code>)
    spoil+++
  </li>
  <li>
    +++spoil
widget python
main = [4, 7, 6, 6, 2, 0]
carteDeLaPioche = 2

def chercheUneCarteDansLaMain(main, numero):
  position = 0
  for carte in main:
    if carte == numero:
      return position
    position = position + 1
  return -1

position = chercheUneCarteDansLaMain(main, carteDeLaPioche)

if(position != -1):
  print("Je possède déjà un exemplaire dans ma main de la carte que je viens de piocher")
else:
  print("Je ne possède pas d'exemplaire dans ma main de la carte que je viens de piocher")
widget
    spoil+++
  </li>
  <li>
    +++spoil
widget python
main = [4, 7, 6, 6, 2, 0]
carteDeLaPioche = 2

def chercheUneCarteDansLaMain(main, numero):
  position = 0
  for carte in main:
    if carte == numero:
      return position
    position = position + 1
  return -1

position = chercheUneCarteDansLaMain(main, carteDeLaPioche)

if(position != -1):
  print("Je possède déjà un exemplaire dans ma main de la carte que je viens de piocher.\n")
  main.insert(position, carteDeLaPioche)
else:
  print("Je ne possède pas d'exemplaire dans ma main de la carte que je viens de piocher. J'ajoute cette dernière en première position.\n")
  main.insert(0, carteDeLaPioche)
  
print("Voici ma nouvelle main : ")
print(main)
widget
    spoil+++
  </li>
  </div>
</ol>