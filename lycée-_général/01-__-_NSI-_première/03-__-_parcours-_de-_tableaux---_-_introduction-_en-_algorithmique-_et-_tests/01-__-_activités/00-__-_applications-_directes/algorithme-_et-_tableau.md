# Algorithme et tableau

+++consignes
  <ol start="0">
    <li>Créez vous un dossier du jour en exécutant chacune des lignes ci-dessous sur un terminal. <b>Attention !</b> n'oubliez pas d'adapter les lignes 2 et 3 </li>
  </ol>
  <pre><code class="language-bash hljs"><span class="hljs-comment"># lignes à réadapter et à exécuter dans un terminal</span>
nom_prenom=dupond_cedric
dateDuJour=18_10_21

mkdir /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour} && cd /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
touch parcoursDeTableauSimple.py
thonny parcoursDeTableauSimple.py
</code></pre>
  <ol start="1">
    <li>Parcourir l'activité et répondre aux questions</li>
  </ol>
+++

+++question write "Dans le code bash ci-dessus, expliquez ce que font les commandes de la ligne 6 et 7"

<hr>



+++question write "Rappelez la principale différence qu'il y a entre une variable de type tableau (+++flag-us array) et une variable avec un type simple (char, int, string, ...)"

<hr>

Quelque soit le langage de programmation, nous sommes souvent amené à devoir manipuler l'ensemble (ou une partie) des éléments contenus dans une variable de type tableau ( = liste en python ). Il est possible de manipuler un à un les éléments du tableau, on dit qu'on **parcours le tableau**.

Il est possible de parcourir manuellement un tableau (ou liste en python) comme suit : 

widget python
uneListe = ['a', 'b', 'c'] # uneListe est une liste de char

# Je parcours manuellement chaque élément du tableau uneListe et affiche chacun des éléments
print(uneListe[0])
print(uneListe[1])
print(uneListe[2])
widget

Cependant, ce parcours manuel devient rapidement très peu pratique si le tableau contient un nombre important d'élément et/ou si le nombre d'éléments contenus dans le tableau peut varier en cours d'exécution du programme.

+++question write "Après quelques recherches sur Internet et quelques tests, écrivez un exemple en python qui montre comment parcourir un à un les éléments d'un tableau en utilisant une instruction itérative"

<hr>

Ci-dessous est rédigé un algorithme avec un pseudo-code proche du langage naturel : 

- Étant donné un portefeuille contenant des euros en pièces, je souhaite compter le nombre de pièces de 2 euros qui se trouvent dedans
- Je garde en tête le nombre total de pièces de 2 euros qui est pour l'instant égal à 0
- Pour chaque piece dans le portefeuille :
    - Si la pièce que j'observe est une pièce de deux euros, je rajoute 1 au nombre total de pièces de 2 euros
- Le compte total correspond au nombre de pièces de 2 euros qui se trouvent dans mon portefeuille

Voici sa version avec un pseudo-code plus proche de la machine

<pre class="ps-2 py-2">portefeuille ← [2, 0.5, 1, 1, 0.10, 2, 2] 
compteur ← 0 

Pour chaque pièce dans le portefeuille
  Si piece = 2 Alors
    compteur ← compteur + 1
  Fin Si
Fin Pour
    
Afficher "je possède " + compteur + " pièce(s) de 2 euros"
</pre>

+++question "Implémentez l'algorithme en langage python dans le fichier parcoursDeTableauSimple.py"

<hr>

La partie ci-dessous fait suite à l'activité +++lien "/apprenant/01-__-_NSI-_premiere/02-__-_introduction-_aux-_tableaux-_et-_programmation-_Web-_cote-_client/01-__-_activites/00-__-_applications-_directes/premiers-_pas-_avec-_les-_tableaux.md" "'Premiers pas avec les tableaux'" . 


Pour rappel, avant d'aborder les tableaux, voici le programme qui permettait de connaître le niveau de notre personnage virtuel en fonction de son expérience : 

widget python
def getLevel(xp): # fonction utilisée dans le programme principal
  assert isinstance(xp, int)

  niveau1 = 1
  niveau2 = 4
  niveau3 = 10
  niveau4 = 24
  niveau5 = 54
  niveau6 = 121
  niveau7 = 269
  niveau8 = 601
  niveau9 = 1338
  niveau10 = 2980

  if(xp < niveau1):
    return 1
  elif(xp < niveau2):
    return 2
  elif(xp < niveau3):
    return 3
  elif(xp < niveau4):
    return 4
  elif(xp < niveau5):
    return 5
  elif(xp < niveau6):
    return 6
  elif(xp < niveau7):
    return 7
  elif(xp < niveau8):
    return 8
  elif(xp < niveau9):
    return 9
  else:
    return 10

# v programme principal v

mu_xp = 300
level = getLevel(mu_xp)
print(level)
widget


Ensuite, vous avez appris à déclarer (= initialiser) un tableau en python et à lui affecter des valeurs lors de sa création. Ce qui a permis de réduire les lignes 4 à 13 du code ci-dessus à une seule ligne (la ligne 4 dans le code ci-dessous) en adaptant aussi les instructions conditionnelles '`if ... elif ... else`' dans le reste de la fonction `'getLevel`'. 

widget python
def getLevel(xp): # fonction utilisée dans le programme principal
  assert isinstance(xp, int)

  niveaux = [1,4,10,24,54,121,269,601,1338,2980]

  if(xp < niveaux[0]):
    return 1
  elif(xp < niveaux[1]):
    return 2
  elif(xp < niveaux[2]):
    return 3
  elif(xp < niveaux[3]):
    return 4
  elif(xp < niveaux[4]):
    return 5
  elif(xp < niveaux[5]):
    return 6
  elif(xp < niveaux[6]):
    return 7
  elif(xp < niveaux[7]):
    return 8
  elif(xp < niveaux[8]):
    return 9
  else:
    return 10

# v programme principal v

mu_xp = 300
level = getLevel(mu_xp)
print(level)
widget

+++question write "En vous appuyant sur les réponses aux questions 2 et 3, rédigez un algorithme avec du pseudo-code qui décrit comment parcourir un tableau d'expérience et retourner le niveau correspondant à l'expérience donnée en entrée"

<hr>

Cet algorithme va permettre de réduire considérablement le contenu de la fonction `getLevel`.

+++question "Créez un nouveau script python (un nouveau fichier .py). Recopiez dedans le code qui se trouve dans l'éditeur python #2 ci-dessus. Modifiez le contenu de la fonction <code>getLevel</code> en implémentant dedans l'algorithme rédigé dans la question 4"

## Réponse

<ol start="0">
  <li>
+++spoil
  <p>La commande <code>touch</code> permet de créer un fichier. Cette commande prend un paramètre en entrée qui est le nom du fichier que l'on souhaite créer. Donc la commande <code>touch parcoursDeTableauSimple.py</code> crée le fichier nommé 'parcoursDeTableauSimple.py' dans le dossier dans lequel est notre terminal</p>
  <p>La commande <code>thonny</code> permet de démarrer l'IDE thonny (si il est installé sur l'ordinateur). Cette commande peut prendre un paramètre en entrée qui est le nom du fichier que l'on souhaite ouvrir dans thonny. Donc la commande <code>thonny parcoursDeTableauSimple.py</code> démarre l'IDE thonny et ouvre automatiquement le fichier 'parcoursDeTableauSimple.py'</p>
spoil+++
  </li>
  <li>
+++spoil
  <p>Une variable de type simple (string, int, char, boolean, float, ...) ne peut contenir qu'une seule donnée. À l'inverse, une variable de type tableau permet de stocker un ensemble de données</p>
spoil+++
  </li>
  <li>
+++spoil 
widget python
inventaire = ['épée', 'hache', 'potion']

for item in inventaire:
  print('je possède un(e) ' + item + ' !')
widget

<p>Il est possible d'utiliser l'instruction <code>while</code> à la place de <code>for</code></p>

widget python
inventaire = ['épée', 'hache', 'potion']
compteur = 0

while(compteur < len(inventaire)):
  print('je possède un(e) ' + inventaire[compteur] + ' !')
  compteur += 1
widget

spoil+++
  </li>
  <li>
+++spoil 
widget python
portefeuille = [2, 0.5, 1, 1, 0.10, 2, 2]
compteur = 0

for piece in portefeuille:
  if(piece == 2):
    compteur += 1

print("je possède " + str(compteur) + " pièce(s) de 2 euros")
widget
spoil+++
  </li>
  <li>
+++spoil 

<p>Une version plutôt 'haut niveau' ( = proche du langage naturel)</p>

<ul>
  <li>Étant donné une table contenant les paliers d'expériences correspondant à des niveaux, les paliers sont triés par ordre croissant</li>
  <li>Étant donné un certain nombre de points d'expérience</li>
  <li>Pour chaque pallier d'expérience : <ul>
    <li>Si le nombre de points d'expérience est en dessous du pallier courant Alors je retourne le numéro du pallier</li>
  </ul></li>
</ul>
<br>

<p> Une version plutôt 'bas niveau' ( = proche du langage machine)</p>

<pre class="ps-2 py-2">table d'expérience ← [1, 4, 10, 24, 54, 121, 269, 601, 1338, 2980] 
expérience ← 300
niveau ← 1

Pour chaque pallier dans la table d'expérience
  Si expérience < pallier Alors
    retourner le numéro du pallier
  Fin Si
  niveau ← niveau + 1
Fin Pour
</pre>

spoil+++
  </li>
  <li>
+++spoil
  widget python
def getLevel(xp): # fonction utilisée dans le programme principal
  assert isinstance(xp, int)

  niveaux = [1,4,10,24,54,121,269,601,1338,2980]
  compteur = 1

  for pallier in niveaux:
    if(xp < pallier):
      return compteur
    compteur += 1

# v programme principal v

mu_xp = 300
level = getLevel(mu_xp)
print(level)
  widget
  
<p>Ci-dessous une version n'utilisant pas de variable compteur</p>

  widget python
def getLevel(xp): # fonction utilisée dans le programme principal
  assert isinstance(xp, int)

  niveaux = [1,4,10,24,54,121,269,601,1338,2980]

  for numero, pallier in enumerate(niveaux):
    if(xp < pallier):
      return numero + 1

# v programme principal v

mu_xp = 300
level = getLevel(mu_xp)
print(level)
  widget

spoil+++
  </li>
</ol>
