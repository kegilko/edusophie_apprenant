# Introduction en algorithmique et parcours de tableaux

+++programme
  Écrire un algorithme de recherche d’une occurrence sur des valeurs de type quelconque
  Itérer sur les éléments d’un tableau
  
  Définir ce qu'est l'algorithmique et ce qu'est un algorithme
  Parcourir un tableau de données avec Python
+++

+++sommaire 2

## Introduction

+++bulle matthieu
  qu'est-ce que c'est pour vous un algorithme ?
+++

+++bulle matthieu droite
  et dans quel contexte en avez vous entendu parlé ?
+++

Avant de se jeter sur son clavier pour concevoir un programme, il est **parfois** bon de rédiger sommairement les étapes de notre programme sur un papier. On dit qu'on est dans une phase de conception.

+++bulle matthieu
  dans des projets informatiques complexes, les phases de conception peuvent occuper un temps très important
+++

Un **algorithme**, qui est rédigé pendant la phase de conception, décrit la manière dont un programme doit se comporter. Le vocabulaire et l'agencement des étapes sont rédigés avec du **pseudo-code**.

Prenons l'exemple d'une recette de cuisine pour faire des cookies : 

<div class="d-flex justify-content-evenly">
  +++image "/images/md/H9525G2EG87F8665EHC6BA2DE697HB" col-5
  <div class="col-5 align-self-center">
+++bulle matthieu
  la recette d'un cookie pour 6 personnes est un <b>algorithme</b>
+++ col-12
+++bulle matthieu droite
  cet <b>algorithme</b> a besoin de certaines données de départ pour pouvoir être exécuté (le nombre d'œufs, le nombre de gramme de farine, etc... )
+++ col-12
+++bulle matthieu
  et la recette décrit les étapes à suivre. Les données et les étapes correspondent au <b>pseudo-code</b>
+++ col-12
  </div>
</div>

### Enjeux de l'algorithmique

Les algorithmes ne sont pas simplement là pour décrire le comportement des programmes. Ce **domaine fondamental** de l'informatique a pour but de concevoir les programmes les moins coûteux possibles : en temps et en ressource machine (espace disque, RAM, processeur, ...)

+++bulle matthieu
au lycée, nous nous intéresserons principalement au temps
+++

+++activité binome 50 ../01-__-_activites/00-__-_applications-_directes/debranche-_--_-_initiation-_en-_algorithmique.md "Initiation en algorithmique"

### Résumons +++emoji-write 

- Un algorithme décrit le comportement d'un programme.
- Un programme peut être conçu de différentes manières. L'algorithmique consiste à chercher la manière la plus optimale en terme de temps d'exécution et/ou d'utilisation de ressources machines (disque dur, RAM, processeur...)
- L'efficacité d'un algorithme dépend de son contenu et également de comment sont structurées les données qu'il traite (exemple : la recherche d'un mot dans un dictionnaire est très efficace grâce au fait que les mots soit triés par ordre alphabétique)

## Pseudo-code : la frontière entre la machine et l'humain

Il n'existe pas de format normalisé pour l'écriture du pseudo code : 
- plus le pseudo-code est proche du langage humain plus il est facile de le partager

+++bulle matthieu
  à privilégier si vous souhaitez le partager dans un groupe par exemple
+++

- plus le pseudo-code est proche du langage machine plus il est facile à traduire en un langage de programmation

+++bulle matthieu
  à privilégier si vous êtes seul face à un devoir surveillé par exemple
+++

+++activité binome 40 ../01-__-_activites/00-__-_applications-_directes/algorithme-_et-_tableau.md "Algorithme et tableau"


## Résumons +++emoji-write 

- Le comportement d'un algorithme peut être rédigé en pseudo-code : une écriture plus ou moins proche du langage naturel (le français pour nous)
- En programmation, pour travailler sur chacun des éléments d'un tableau, nous utilisons les instructions `for` ou `while`, voici quelques exemples : **( vous n'êtes pas obligé de les recopier )**

widget python
# je déclare un tableau et lui affecte quelques valeurs
sequence = ['c', 'g', 'a', 't', 't']

# j'affiche chaque éléments du tableau 
for nucléique in sequence:
  print(nucléique)

widget

widget python
# je déclare les notes de mon trimestre
mesNotes = [10, 12, 16, 9]

# je calcule la moyenne de mon trimestre
somme = 0
for note in mesNotes:
  somme = somme + note

moyenne = somme / len(mesNotes) # len() est une fonction qui retourne le nombre d'éléments dans un tableau
print('Ma moyenne est de : ' + str(moyenne))

widget

widget python
achats = [17.50, 199.99, 35,10, 99.99]
argentDisponible = 250
cumul = 0
i = 0

while(argentDisponible > cumul):
  cumul += achats[i] # on ajoute achats[i] à cumul
  i += 1 # on incrémente i de 1

if(argentDisponible < cumul):
  raise ValueError("le plafond du compte en banque a été dépassé à cause de l'achat n°" + str(i) + " (" + str(achats[i]) + "€)")
else:
  argentDisponible = argentDisponible - cumul
  print('argent restant sur le compte : ' + str(argentDisponible))

widget