# Utilisation de la table d'encodage ASCII

+++consigne
Laissez vous guider par l'activité et répondez aux questions
+++

Voici ci-dessous le contenu d'une partie de la mémoire d'une machine : 

`1000010 1101111 1101110 1101010 1101111 1110101 1110010 100000 100001`

Ce contenu représente un texte encodé en ASCII.

+++question write "En vous aidant de recherches sur le Web, retrouvez le texte qui correspond à cette suite de nombres binaires"

+++spoil "Aide"
  Il vous faut utiliser une table d'encodage ASCII pour pouvoir faire les correspondances entre les octets et les caractères. Vous pouvez trouver une table ASCII en fouillant dans le Web
spoil+++

+++question write "Encodez en binaire, en utilisant la table d'encodage ASCII, le texte ci-dessous : "

`Le nombre 42.`

<hr>

Dans sa première version, la table d'encodage ASCII utilise 7 bits pour représenter un caractère. Par exemple, le caractère 'a' est représenté par les 7 bits '1100001'

+++question write "Combien de caractères peut on encoder avec la première version de la table d'encodage ASCII ?"

+++question write "En déduire pour quelle raison le contenu de mon fichier texte ci-dessous, utilisant l'encodage ASCII, affiche des caractères bizarres"

+++image "/images/md/646F98F3H777G45H7H4459AHHD46H6" col-8 mx-auto


## Réponses

<ol start="0">
  <li>
  +++spoil
    Bonjour !
  spoil+++
  </li>
  <li>
  +++spoil
01001100 01100101 00100000 01101110 01101111 01101101 01100010 01110010 01100101 00100000 00110100 00110010 00101110
  spoil+++
  </li>
  <li>
  +++spoil
<ul>
  <li>Avec 1 bit, nous avons 2 possibilités : 0 ou 1)</li>
  <li>Avec 2 bits, nous avons 4 (calcul : 2 * 2) possibilitées : 00 ou 01 ou 10 ou 11</li>
  <li>Avec 3 bits, nous avons 8 (calcul : 2 * 2 * 2) possibilitées : 000 ou 001 ou 010 ou ... ou 101 ou 110 ou 111</li>
  <li>...</li>
  <li>Avec 7 bits, nous avons 2<sup>7</sup> (calcul : 2 * 2 * 2 * 2 * 2 * 2 * 2) possibilitées, soit 128 numbres binaires possibles et donc 128 caractères peuvent être gérés à travers la table d'encodage ASCII</li>
</ul>  
  spoil+++
  </li>
  <li>
  +++spoil
La table ASCII, qui ne peut représenter que 127 caractères différents, ne prends pas en charge les carractères accentués. Ce qui provoque des bugs d'affichages lorsqu'on souhaite afficher du texte, contenant des accents, avec le système d'encodage ASCII.
  spoil+++
  </li>
</ol>