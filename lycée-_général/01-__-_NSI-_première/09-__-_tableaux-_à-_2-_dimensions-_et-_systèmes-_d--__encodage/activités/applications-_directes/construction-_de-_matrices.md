# Construction de matrices

+++consignes
  <ol start="0">
    <li>Créez vous un dossier du jour en exécutant chacune des lignes ci-dessous sur un terminal. <b>Attention !</b> n'oubliez pas d'adapter les lignes 2 et 3 </li>
  </ol>
  <pre><code class="language-bash"># lignes à ré-adapter et à exécuter dans un terminal
  nom_prenom=dupond_cedric
  dateDuJour=18_10_21

  mkdir /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
  cd /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
  touch matrices.py
  thonny matrices.py
  </code></pre>
  <ol start="1">
    <li>Parcourez l'activité et répondez aux questions</li>
  </ol>
+++

Dans cet exercice, vous créez des tableaux de tableaux en python. Le but est de vous familiariser avec ce type de structure de données. Retranscrivez les données des énoncés ci-dessous dans une variable python.

<hr>

Voici deux exemples :

**Exemple énoncé #0 :**

Une établissement scolaire est composé de plusieurs salles, et chaque salle contient diverses équipements. Par exemple la salle 100 contient 16 bureaux, 16 chaises et un vidéo-projecteur

**Retranscription des données de l'énoncé dans une variable python :**

widget python
salles = [
  ['100', '16 bureaux', '16 chaises', '1 tableau classique', '1 vidéo-projecteur'],
  ['101', '24 bureaux', '24 chaises', '1 tableau classique'],
  ['102', '19 bureaux', '22 chaises']
]
widget

**Exemple énoncé #1 :**

Ma trousse contient plusieurs objets ; un stylo 4 couleurs, une gomme, une petite règle, ...

**Retranscription des données de l'énoncé dans une variable python :**

widget python
trousse = ['style 4 couleurs', 'gomme', 'petite règle', 'effaceur', 'crayon gris', 'surligneur jaune']
widget

+++bulle matthieu
  il n'est pas toujours nécessaire de créer un tableaux de 2 dimensions selon les données que l'on manipule.
+++

+++bulle matthieu droite
  dans les énoncés ci-dessous, stockées les données décrites dans une variable avec une structure adéquate (un tableau de dimension 1 ou un tableau de dimension 2)
+++

## Énoncés

**Énoncé #0 :**

Un peintre souhaite partager ses créations sur Internet. Le peintre souhaite que le visiteur sur le site puisse rechercher une oeuvre en fonction des principales couleurs qui ont été utilisées. Par exemple, le tableau 'nature morte 2021' contient les couleurs noir, vert, marron et jaune.

+++question "Créez une variable python nommée 'creations_artistiques' dans la structure est adaptée pour contenir le type de données décrites dans l'énoncé"

<hr>

**Énoncé #1 :**

Une université est composée d'un ensemble de laboratoires de recherches et chaque laboratoires de recherches est dirigé par un directeur de recherche, des chercheurs et des doctorants (un chercheur en phase de formation)

+++question "Créez une variable python nommée 'universite' dans la structure est adaptée pour contenir le type de données décrites dans l'énoncé"

<hr>

**Énoncé #2 :**

La matin quand je pars au lycée, je prépare mon sac en mettant dedans les affaires dont j'aurai besoin pendant la journée. Le sac contient divers objets ; classeurs, livres, trousse, ...

+++question "Créez une variable python nommée 'sac' dans la structure est adaptée pour contenir le type de données décrites dans l'énoncé"

<hr>

**Énoncé #3 :**

Un échiquier est une grille carrée de 8 cases de côté. Chaque case peut vide ou alors occupée par une pièce (pion, cavalier, fou, ...) 

+++question "Créez une variable python nommée 'echiquier' dans la structure est adaptée pour contenir le type de données décrites dans l'énoncé"

## Aller plus loin

**Énoncé #4 :**

Nous souhaitons stocker dans une variable l'état d'un rubik cube 3x3. Pour rappel, un rubik cude est composé de 6 faces, chaque face est une grille carrée de 3 cases de côté (et chaque carré peut contenir une couleur parmis les suivantes : bleu, rouge, jaune, blanc, orange, vert)

+++spoil "Aide"
  Un tableau à deux dimensions ne suffira pas...
spoil+++

+++question "Créez une variable python nommée 'rubikcude' dans la structure est adaptée pour contenir le type de données décrites dans l'énoncé"

## Réponses

<ol start="0">
  <li>
  +++spoil
Un tableau à deux dimensions sera le plus adapté dans ce cas car le peintre possède un ensemble d'oeuvres et chaque oeuvre possède un emsemble de couleur  

widget python
creations_artistiques = [
  ['nature morte avec des fruits', 'noir', 'vert', 'marron', 'jaune'],
  ['peinture impressionniste représentant un port', 'gris', 'bleu', 'blanc'],
  ['portait fait à la peinture à l\'huile', 'jaune', 'orange', 'noir', 'blanc']
]
widget
  spoil+++
  </li>
  <li value="2">
  +++spoil
Dans cet énoncé nous pourrions être tenté de donner plus d'informations pour chaque objets, par exemple, un classeur peut lui même contenir des feuilles, des intercalaires, une trousse peut contenir un stylo, une gomme, etc.. Dans ce cas, une matrice serait plus adaptée.
<br>
<br>
Cependant, l'énoncé ne le précisant pas, il vaut mieux se focaliser sur les données dont nous disposons. Nous utilisons alors la structure la plus simple possible, et donc un tableau contenant une seul dimension

widget python
sac = ['trousse', 'classeur de français', 'livre de français', 'calculatrice', 'pochette de SVT']
widget
  spoil+++
  </li>
  <li value="4">
  +++spoil
Un tableau à deux dimensions permettrait de représenter une des faces du rubik cude : 

widget python
face_1 = [
  ['blanc', 'vert', 'jaune'],
  ['vert', 'vert', 'rouge'],
  ['blanc', 'bleu', 'bleu'],
]
widget

Pour stocker toutes les données d'un rubik cude (et donc toutes ces faces), il faut alors utiliser non pas un tableau de tableau, mais un tableau de tableau de tableau (un tableau à trois dimensions)

widget python
rubikcude = [
  [ # face 1
    ['vert', 'vert', 'vert'],
    ['vert', 'vert', 'vert'],
    ['vert', 'bleu', 'bleu']
  ], 
  [ # face 2
    ['rouge', 'bleu', 'jaune'],
    ['orange', 'jaune', 'vert'],
    ['jaune', 'rouge', 'rouge']
  ], 
  [ # face 3
    ['bleu', 'bleu', 'bleu'],
    ['orange', 'bleu', 'jaune'],
    ['orange', 'jaune', 'jaune']
  ], 
  [ # face 4
    ['blanc', 'blanc', 'blanc'],
    ['blanc', 'blanc', 'blanc'],
    ['blanc', 'blanc', 'blanc']
  ], 
  [ # face 5
    ['rouge', 'rouge', 'rouge'],
    ['rouge', 'rouge', 'jaune'],
    ['jaune', 'rouge', 'bleu']
  ], 
  [ # face 6
    ['orange', 'orange', 'orange'],
    ['bleu', 'orange', 'orange'],
    ['vert', 'jaune', 'orange']
  ], 
]
widget

  spoil+++
  </li>
</ol>


