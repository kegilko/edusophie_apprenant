# Manipulations de tables d'encodages

+++consigne
  Suivez le déroulement de l'activité
+++

Il existe de nombreux sites collaboratifs sur Internet qui proposent de créer et d'échanger des fichiers permettant de sous-titrer les séries et les films.

Un des formats de fichier standard utilisé est le format .srt.

+++question "Cherchez un site Web proposant le téléchargement de sous titres de séries"

+++question "Téléchargez un fichier sous-titre en espagnol"

+++spoil "Aide"
  si vous avez du mal à trouver, vous pouvez télécharger +++fichier "/fichiers/FEE9G25886D3AGB63FFG8877AE7834" "le fichier suivant"
spoil+++

+++question write "Quelle commande bash permet d'afficher le contenu d'un fichier textuel ?"

+++question write "Affichez le contenu du fichier de sous titre dans votre terminal ou depuis le logiciel Office Writer, que remarquez vous ?"

<hr>

Il existe de nombreuse manières de modifier la table d'encodage pour lire le contenu d'un fichier. Cela peut se faire à travers des logiciels spécialisés, des sites Web ou des commandes bash

Nous allons nous intéresser à l'approche bash avec deux commande :

<ul>
  <li>La commande <code>file</code> qui permet, <b>avec le bon paramètre</b>, de détecter l'encodage utilisé par un fichier texte</li>
  <li>la commande <code>iconv</code> qui permet, <b>avec le bon paramètre</b>, de lister les tables d'encodage disponibles sur le système d'exploitation</li>
  <li>la commande <code>iconv</code> toujours qui permet, <b>avec les bons paramètres</b>, d'afficher le contenu d'un fichier textuel en précisant la table d'encodage à utiliser</li>
</ul>

+++question write "Affichez la table d'encodage utilisée par le fichier de sous titre que vous avez téléchargez. Quelle ligne de commande avez vous utilisé ?"

+++question write "Affichez les tables d'encodages que met à disposition votre système d'exploitation. Quelle ligne de commande avez vous utilisé ?"

+++question write "Affichez le contenu du fichier des sous-titres en utilisant une table d'encodage permettant d'afficher tous les caractères correctement. Quelle ligne de commande avez vous utilisé ?"

+++bulle matthieu
  vous pouvez recherchez sur Internet comment utiliser ces commandes pour les besoins de l'exercice
+++

+++bulle matthieu droite
  je vous rappelle également qu'il existe la commande 'man' qui permet d'afficher le <b>man</b>uel. Elle s'utilise très simplement, par exemple : <code>man file</code>
+++

## Réponses

<ol start="4">
  <li>
  +++spoil
    <code>file --mime-encoding fichiersoustitre.str</code>
  spoil+++
  </li>
  <li>
  +++spoil
    <code>iconv -l</code>
  spoil+++
  </li>
  <li>
  +++spoil
    <code>iconv -f ISO-8859-1 -t utf-8 fichiersoustitre.str</code>
    Attention, 'ISO-8859-1' peut varier en fonction de ce que vous avez trouvé dans la question 4
  spoil+++
  </li>
</ol>