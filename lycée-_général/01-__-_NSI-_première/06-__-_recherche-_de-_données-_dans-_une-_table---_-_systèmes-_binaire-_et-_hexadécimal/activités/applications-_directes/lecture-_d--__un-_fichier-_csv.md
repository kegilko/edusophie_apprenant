# Lecture d'un fichier csv

+++question write "Dans le dossier du jours, créez un fichier nommé <b>achats.csv</b>. Quelle commande bash permet de créer un fichier ?"

---

Ouvrez le fichier <b>achats.csv</b> avec un éditeur de texte. Vous pouvez utiliser la commande `gedit achats.csv`

+++question "Copiez/collez les lignes suivantes dans le fichier <b>achats.csv</b>. Sauvegardez et fermez gedit"

```markdown
id,produit,prix,quantité
0,fauteuil,499,1
1,souris,60,1
2,écran,70,3
3,clavier,99,1
4,rubik cube,20,1
```

---

+++question write "Créez le fichier <b>initiation_csv_python.py</b> dans le même dossier que celui qui contient achats.csv puis ouvrez le fichier avec thonny. Quelles commandes bash permettent de créer un fichier, puis, de l'ouvrir avec thonny ?"

+++question write "Dans le fichier <b>initiation_csv_python.py</b>, écrire un programme qui affiche tout le contenu du fichier csv dans la console"

Nous nous intéressons aux types des variables qui sont utilisées dans le programme. 

+++question write "Quelles sont les variables utilisées dans votre programme ?"

---

Pour rappel, la fonction <code>type</code> permet de connaître le type d'une variable donnée en paramètre. Par exemple, le programme suivant : 

```python
a = 17
b = '20'
c = 'onze'
d = [a, b, c] # contient du coup : 17, '20', 'onze'

print(type(a))
print(type(b))
print(type(c))
print(type(d))
```

affiche dans la console : 

<div class="border border-dark p-2 d-inline-block mb-3">
\<class 'int'=""><br>
\<class 'str'=""><br>
\<class 'str'=""><br>
\<class 'list'="">
  
</class></class></class></class></div>

+++question write "En vous aidant de la fonction <code>type</code>, quelles sont les types des variables que vous avez recensé dans la question 5 ?"

---

Nous souhaitons afficher seulement le nom du produit et non la ligne entière du fichier csv. Actuellement, chaque ligne du fichier csv est une chaîne de caractères.

Pour pouvoir accéder seulement à la deuxième donnée de chaque ligne...

<div class="border border-dark p-1 mb-3 bg-grey d-inline-block">
id,<b>produit</b>,prix,quantité<br>
0,<b>fauteuil</b>,499,1<br>
1,<b>souris</b>,60,1<br>
2,<b>écran</b>,70,3<br>
3,<b>clavier</b>,99,1<br>
4,<b>rubik cube</b>,20,1
</div>

...il faut convertir à chaque tour de boucle <code>for</code> la ligne courante en un tableau python. Une fois la ligne convertie en tableau, il suffira simplement d'un <code>print(ligne[1])</code> pour afficher le nom du produit.

La fonction <code>split</code> permet de convertir une chaîne de caractère en un tableau

+++question write "Après quelques recherches sur Internet, donner un exemple simple d'utilisation de la fonction <code>split</code> sur une chaîne de caractère contenant des données séparées par des virgules"

+++question "Améliorez votre programme afin d'afficher seulement les produits contenus dans le fichier csv. Le résultat attendu est :"

```markdown
produit
fauteuil
souris
écran
clavier
rubik cube
```

---

Votre programme affiche les nom des produits, vous remarquez que le mot 'produit' s'affiche dans votre console. Ce qui est normal, le deuxième élément de la première ligne du fichier .csv "id,produit,prix,quantité" est 'produit'.

+++question "Améliorez votre programme afin d'afficher toutes les lignes du fichier csv <b>sauf</b> la première ligne. Le résultat attendu est :"

```markdown
fauteuil
souris
écran
clavier
rubik cube
```

+++spoil "Aide"
  Une première approche consiste à utiliser une variable et un <code>if</code>. Une autre approche consiste à utiliser la fonction <code>next</code>, il est possible de voir comment elle fonctionne après quelques recherches sur Internet
spoil+++

+++question "Modifiez le contenu de la boucle <code>for</code> afin d'afficher seulement les produits dont le coût est strictement supérieur à 70 euros"

+++question "Améliorez votre programme afin qu'il affiche à la fin de son exécution le coût total de tous les produits achetés"



## Réponses

<ol start="0">
  <li>
  +++spoil
    La commande <code>touch</code>, par exemple : <code>touch achats.csv</code>
  spoil+++
  </li>
  <li value="2">
  +++spoil
    Pour créer un fichier et l'ouvrir par la suite avec thonny, il suffit d'exécuter la commande suivante : <code>touch initiation_csv_python.py && thonny initiation_csv_python.py</code>
  spoil+++
  </li>
  <li>
  +++spoil

```python
achats = open('achats.csv','r',encoding='utf8')

for achat in achats:
  print(achat)
```
  
  spoil+++
  </li>
  <li>
  +++spoil
Les variables utilisées sont : achats et achat
  spoil+++
  </li>
  <li>
  +++spoil

En exécutant le code suivant : 

```python
achats = open('achats.csv','r',encoding='utf8')
print(type(achats))

for achat in achats:
  print(achat)
  print(type(achat))

```

La console nous affiche : <br>

<div class="border border-dark p-2 d-inline-block mb-3">
\<class '_io.textiowrapper'=""><br>
...<br>
\<class 'str'=""><br>
</class></class></div>
  
<p>la variable <code>achats</code> est de type <code>_io.TextIOWrapper</code></p>
<p>la variable <code>achat</code> est de type <code>str</code></p>
  
+++bulle matthieu
  <code>_io.TextIOWrapper</code> est un type créé par python. Ce type n'est pas à connaître
+++

+++bulle matthieu droite
  ce type fonctionne comme un tableau
+++

  spoil+++
  </li>
  <li>
  +++spoil

```python
ingredients = "pomme,poire,cerise,mangue"
print(type(ingredients)) # affiche 'str'

ingredients = ingredients.split(",")
print(type(ingredients)) # affiche 'list'

print(ingredients[2]) # affiche 'cerise'
```
  
  spoil+++
  </li>
  <li>
  +++spoil

```python
achats = open('achats.csv','r',encoding='utf8')

for achat in achats:
  achat = achat.split(',')
  print(achat[1])
```
  
  spoil+++
  </li>
  <li>
  +++spoil

Voici quelques approches possibles : 

Approche 1 :

```python
achats = open('achats.csv','r',encoding='utf8')
premiereLigne = True

for achat in achats:
  if(premiereLigne):
    premiereLigne = False
  else:
    achat = achat.split(',')
    print(achat[1])
```

Approche 2 :

```python
achats = open('achats.csv','r',encoding='utf8')
compteur = 0

for achat in achats:
  if(compteur != 0):
    achat = achat.split(',')
    print(achat[1])
  compteur = compteur + 1
```

Approche 3 :

```python
achats = open('achats.csv','r',encoding='utf8')
next(achats) # On saute la première ligne

for achat in achats:
  achat = achat.split(',')
  print(achat[1])
```

  spoil+++
  </li>
  <li>
  +++spoil

Approche 1 :

```python
achats = open('achats.csv','r',encoding='utf8')
premiereLigne = True

for achat in achats:
  if(premiereLigne):
    premiereLigne = False
  else:
    achat = achat.split(',')
    if(int(achat[2]) > 70):
      print(achat[1])
```

Approche 2 :

```python
achats = open('achats.csv','r',encoding='utf8')
compteur = 0

for achat in achats:
  if(compteur != 0):
    achat = achat.split(',')
    if(int(achat[2]) > 70):
      print(achat[1])
  compteur = compteur + 1
```

Approche 3 :

```python
achats = open('achats.csv','r',encoding='utf8')
next(achats) # On saute la première ligne

for achat in achats:
  achat = achat.split(',')
  if(int(achat[2]) > 70):
      print(achat[1])
```

  spoil+++
  </li>
  <li>
  +++spoil

```python
achats = open('achats.csv','r',encoding='utf8')
next(achats) # On saute la première ligne
cumul = 0

for achat in achats:
  achat = achat.split(',')
  if(int(achat[2]) > 70):
      print(achat[1])
  cumul += int(achat[2]) * int(achat[3])

print("Le coût total est de : " + str(cumul) + " euros")
```

  spoil+++
  </li>
</ol>