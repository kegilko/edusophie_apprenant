# Étude du protocole du 'bit alterné'

+++consignes
<ul>
  <li>Suivre l'activité et répondre aux questions</li>
  <li>Pensez à répartir les rôles dans votre binôme</li>
</ul>
+++

Le protocole du bit alterné est un protocole de réseau fonctionnant au niveau de la couche liaison. Ce protocole permet de retransmettre un paquet perdu ou corrompu.

Lorsqu'un paquet circule dans un réseau, certains nœuds du réseaux peuvent tomber en panne ou même les liaisons entre les nœuds. Le paquet, qui en cours de route, se retrouve alors supprimé si il se retrouve au mauvais endroit au mauvais moment.

## Description du protocole du bit alterné

Nous pouvons représenter le fonctionnement d'un protocole par un échange entre deux personnes : 

- Étant donné deux personnes: Julie et Romain
- Julie souhaite envoyer un message à Romain avec des pigeons voyageurs
- Le message étant assez long, Julie découpe son message en plusieurs paquets et donc fera appel à autant de pigeon voyageur qu'il y a de paquet.
- Romain devra ensuite rassembler les paquets pour obtenir le message de Julie

L'algorithme de ce protocole est le suivant : 

- concernant l'émetteur (Julie ici) : 
    0. je défini **FLAG_1** et lui assigne la valeur 0
    1. je revoie toutes les 10 secondes le paquet X tant que je ne reçois pas un **a**ccusé de **r**éception (**AR**) dont l'en-tête est égale à **FLAG_1**
    2. je répète l'étape 1 pour chacun des paquets que je dois envoyer
    3. quand je reçoit un **a**ccusé de **r**éception, j'inverse la valeur de **FLAG_1**

+++bulle matthieu
  inverser la valeur du <b>FLAG</b> signifie changer 0 en 1 ou changer 1 en 0
+++

<ul><ol start="4">
<li>si j'ai envoyé tout mes paquets et que j'ai reçu tous les accusés réceptions des paquets qui compose mon message, je m'arrête</li>
</ol></ul>

- concernant le destinataire (Romain ici) : 
    0. je défini **FLAG_2** et lui assigne la valeur 0
    1. Si la valeur **FLAG_1** de l'entête du paquet que je reçois est égale à **FLAG_2**, alors j'accepte la paquet. Puis j'inverse la valeur de **FLAG_2**. Puis toutes les 10 secondes, j'envoie à l'émetteur un **a**ccusé de **r**éception (**AR**) avec comme en-tête la valeur **FLAG_2** tant que je ne reçois pas de paquet avec en en-tête la valeur **FLAG_2**.  

## Déroulement du protocole

Ci-dessous Julie envoie un message à Romain, le message est découpé en un paquet et donc un seul pigeon voyageur est nécessaire.

+++diagramme
  sequenceDiagram
    autonumber
    Julie->>+Romain: paquet 1 (header 0)
    Romain->>+Julie: AR (header 1)
    Note left of Julie: Je n'ai plus de<br> paquet à envoyer !
    Note right of Romain: Je ne reçois plus de paquet<br> donc j'ai tout reçu !
+++

+++bulle matthieu
  ce type de diagramme ci-dessus est un diagramme de séquence
+++

Ci-dessous se trouve le tableau affichant les valeurs des FLAGs pendant le déroulement du protocole représenté dans le diagramme de séquence ci-dessus

<table class="table">
  <thead class="text-center">
    <tr><th colspan="3">Évolution des FLAGs</th></tr>
  </thead>
  <tbody>
    <tr class="text-center">
      <td></td>
      <td><b>FLAG_1</b></td>
      <td><b>FLAG_2</b></td>
    </tr>
    <tr>
      <td>Initialisation</td>
      <td class="text-center">0</td>
      <td class="text-center">0</td>
    </tr>
    <tr>
      <td>Fin de l'étape 1</td>
      <td class="text-center">0</td>
      <td class="text-center">0</td>
    </tr>
    <tr>
      <td>Fin de l'étape 2</td>
      <td class="text-center">0</td>
      <td class="text-center">1</td>
    </tr>
    <tr>
      <td>Fin de l'échange</td>
      <td class="text-center">1</td>
      <td class="text-center">1</td>
    </tr>
  </tbody>
</table>

+++question write "Dessinez le diagramme de séquence et complétez un tableau d'évolution des FLAGs pour les cas suivants : "

- cas 1 : lors du premier envoie du paquet 1 de l'émetteur vers le destinataire, celui ci est perdu
- cas 2 : le message est découpé en deux paquets
- cas 3 : le message est découpé en deux paquets, un paquet est perdu pendant les échanges
- cas 4 : les pigeons voyageurs ne voyagent pas à la même vitesse
- cas 5 : les pigeons voyageurs ne voyagent pas à la même vitesse et un paquet est perdu pendant les échanges 
- cas 6 : les pigeons voyageurs ne voyagent pas à la même vitesse et au moins 3 paquets sont perdus pendant les échanges