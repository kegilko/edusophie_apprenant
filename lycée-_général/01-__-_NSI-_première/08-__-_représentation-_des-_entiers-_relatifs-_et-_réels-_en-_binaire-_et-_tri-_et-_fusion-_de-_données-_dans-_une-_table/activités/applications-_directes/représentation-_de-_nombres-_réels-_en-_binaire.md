# Représentation de nombres réels en binaire

+++consigne
  Sortez votre crayon et une feuille et laissez vous guider par l'activité !
+++

Supposons que nous utilisons une application quelconque (un logiciel de bureautique, un jeux vidéo, un site Web, ...) dans lequel nous sommes amenés à saisir sur notre écran le nombre 23,375 (en base 10)

L'application a besoin de sauvegarder ce nombre dans sa mémoire, mémoire qui ne sait traiter qu'avec des 0 et des 1. L'ordinateur a donc besoin de convertir ce nombre décimal en binaire.

## Conversion réel => binaire 

Pour traduire un nombre réel en binaire nous utilisons la méthode suivante : 
- **Étape 0** : d'abord nous convertissons la partie entière du nombre en binaire (ici 23)
- **Étape 1** : ensuite, nous utilisons des multiplications successives par 2 sur la partie décimale (ici 0,375) pour trouver son équivalent en binaire

Détaillons l'opération avec le nombre <b class="text-red">23</b>,<b class="text-green">375</b><sub>10</sub>

####**Étape 0**
<p><b class="text-red">23</b><sub>10</sub> = <b class="text-red">10111</b><sub>2</sub></p>

####**Étape 1**

<table class="text-center">
  <thead>
    <tr>
      <th class="px-1">Multiplication par 2</th>
      <th class="px-1">Bit retenu</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0.375 * 2 = <b class="text-blue">0</b>.75</td>
      <td><b class="text-blue">0</b></td>
    </tr>
    <tr>
      <td>0.75 * 2 = <b class="text-blue">1</b>.5</td>
      <td><b class="text-blue">1</b></td>
    </tr>
    <tr>
      <td>0.5 * 2 = <b class="text-orange">1</b></td>
      <td><b class="text-blue">1</b></td>
    </tr>
  </tbody>
</table>

Nous arrêtons de multiplier par 2 lorsque le résultat est exactement égal à <b class="text-orange">1</b>.

En lisant **de bas en haut** les bits retenus lors des multiplications successives, nous en déduisons que <b class="text-green">0,375</b><sub>10</sub> = <b class="text-blue">0,011</b><sub>2</sub> (sur un octet)

Nous pouvons donc en déduire que : <b>23</b>,<b class="text-green">375</b><sub>10</sub> = <b>10111</b>,<b class="text-blue">011</b><sub>2</sub>

+++question write "En suivant la méthode d'au dessus, convertissez les nombres réels suivants en nombres binaires (attention, certaines multiplications successives se déroulent indéfiniment...)"

- 18,125<sub>10</sub>
- 27,15625<sub>10</sub>
- 63,40<sub>10</sub>
- 0,2<sub>10</sub>
- 0,1<sub>10</sub>

+++bulle matthieu
  pour vérifier vos réponses, vous pouvez vous aider de convertisseur en ligne comme <a href="https://fr.planetcalc.com/862/" target="_blank">celui-ci par exemple</a>
+++

<hr>

+++question write "En déduire pour quelle raison 0,1 + 0,2 aboutit à un résultat erroné sur un ordinateur"

+++question write "D'après vous, quelle méthode pourrait être utilisée pour passer d'un nombre binaire flottant (base 2) à un nombre réel décimal (base 10) ?"

## Qu'en est il de la position de la virgule ? (partie hors programme)

Cette partie est là pour compléter très légèrement le programme de première. Son contenu ne sera pas évalué.

La virgule en tant que telle n'existe pas dans les composants électroniques utilisés par l'ordinateur. Nous pourrions naïvement ajouter dans la mémoire un nombre binaire qui représente la position de la virgule. Cependant cette approche peut s'avérer rapidement coûteuse en terme de ressources machine, nous préférons alors utiliser une approche plus efficace. 

N'importe quel nombre mathématique réel peut s'écrire sous la forme : 1,x * 10<sup>y</sup>. Par exemple, 23,375<sub>10</sub> peut s'écrire sous la forme 2,3375 * <b>10</b><sup>1</sup>

Nous pouvons aussi réécrire le nombre binaire en utilisant cette notation, Ainsi,  10111,011<sub>2</sub> peut s'écrire sous la forme 1,0111011 * <b>2</b><sup>4</sup>. Cette notation de nombre binaire est appelée **notation en virgule flottante**. Grâce à cette notation, nous partons du principe que <u>la virgule se positionnera toujours après le premier bit de gauche</u>. <u>Cette information-la</u> qui est invariable est bien moins coûteuse en ressources machine que l'approche naïve évoquée en premier lieu

## Vous êtes en avance ?

+++question "Codez en python un programme d'entraînement à la conversion d'un nombre réel vers un nombre binaire"

Le programme fonctionne de la manière suivante :

- Un nombre décimale à virgule est générés, il est affiché dans la console
    - L'utilisateur calcule de son côté sur un papier la conversion
- Si l'utilisateur appuie ensuite sur la touche entrée, la console affiche le résultat de la conversion
    - L'utilisateur peut alors vérifier si sa conversion est bonne
    
+++question "Ajoutez un système de score sommaire à votre programme"


## Réponses

<ol start="0">
  <li>
  +++spoil
    <ul>
      <li>18,125<sub>10</sub> = 10010,001<sub>2</sub></li>
      <li>27,15625<sub>10</sub> = 11011,00101<sub>2</sub></li>
      <li>63,40<sub>10</sub> = 111111,0110011001100...<sub>2</sub></li>
      <li>0,2<sub>10</sub> = 0,001100110011...<sub>2</sub></li>
      <li>0,1<sub>10</sub> = 0,0001100110011...<sub>2</sub></li>
    </ul>
  spoil+++
  </li>
  <li>
  +++spoil
    <p>Les représentations binaires des nombres 0,2<sub>10</sub> et 0,1<sub>10</sub> contiennent un nombre infini de chiffre. La machine ne pouvant traiter avec des des éléments infini (car elle utilise des ressources finies), elle représente approximativement ces nombres en binaires, et donc, le résultat de l'addition devient approximatif</p>
  spoil+++
  </li>
  <li>
  +++spoil
    <p>Pour cela, il faut utiliser des puissances de 2 négatives en fonction de la place des digits '1' après la virgule, prenons par exemple le nombre 0,00101<sub>2</sub> ( = 0,15625<sub>10</sub>) :</p><ul>
        <li>0,<b class="text-red">00</b><b class="text-black">1</b><b class="text-red">0</b><b class="text-blue">1</b><sub>2</sub> => 2<sup>-1</sup> * <b class="text-red">0</b> + 2<sup>-1</sup> * <b class="text-red">0</b> + 2<sup>-2</sup> * <b class="text-black">1</b> + 2<sup>-3</sup> * <b class="text-red">0</b> + 2<sup>-4</sup> * <b class="text-blue">1</b> = 0,15625</li>
      </ul>
    <p></p>
  spoil+++
  </li>
</ol>

