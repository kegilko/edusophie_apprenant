# Représentation d'un binaire relatif : méthode du complément à 2

+++consigne
  Sortez votre crayon et une feuille et laissez vous guider par l'activité !
+++

Pour obtenir l'équivalent négatif d'un nombre binaire, la méthode naïve consistait à modifier le bit le plus à droite du nombre binaire par 1.

À travers les étapes ci-dessous, vous allez apprendre à utiliser une autre méthode (nommée méthode du complément à 2) qui permet d'obtenir une représentation négative d'un nombre binaire. Cette représentation permettra d'additionner les nombres binaires entre eux.

## Représenter un nombre binaire négatif via la méthode du complément à 2

Voici la règle de la méthode du complément à 2 :

- Les nombres positifs ont le bit le plus à gauche qui vaut forcément 0
- Les nombres négatifs sont obtenus en deux étapes :
    - **Étape 0 :** on inverse les bits de l'écriture binaire de sa valeur absolue
    - **Étape 1 :** on ajoute 1 au résultat
    
Par exemple, le nombre 17<sub>10</sub> vaut sur un octet 00010001<sub>2</sub>. Puis, en suivant la règle du complément à 2 :
- **Étape 0 :** avec 00010001<sub>2</sub>, nous obtenons 11101110<sub>2</sub>
- **Étape 1 :** avec 11101110<sub>2</sub>, nous obtenons 11101111<sub>2</sub>

Donc, en utilisant la méthode du complément à 2 : -17<sub>10</sub> = 11101111<sub>2</sub>

+++question write "Entraînez vous à convertir les nombres ci-dessous et à déterminer les représentations binaires négatives en utilisant la méthode du complément à 2. Nous travaillerons ici sur une mémoire de <b>deux octets</b>"

<table class="table table-sm text-center">
  <thead>
    <tr>
      <th>Nombre en base 10</th>
      <th>Nombre en base 2</th>
      <th>Nombre négatif en base 2</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>17<sub>10</sub></td>
      <td>0000 0000 0001 0001<sub>2</sub></td>
      <td>1111 1111 1110 1111<sub>2</sub></td>
    </tr>
    <tr>
      <td>54<sub>10</sub></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>255<sub>10</sub></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>1023<sub>10</sub></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>4912<sub>10</sub></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>32768<sub>10</sub></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

+++question write "Que remarque-t-on pour la dernière valeur ?"

+++question write "Sur deux octets, combien de nombres binaires positifs et négatifs peut on représenter ?"

## Additionner des nombres binaires relatifs 

Comme on l'a vu avec la méthode naïve du cours, il n'est pas possible en l'appliquant d'additionner des nombres binaires entre eux. Les résultats des calculs sont tout simplement faux.

+++question write "Dans une mémoire de 1 octet et en utilisant la méthode du complément à 2, calculez : (17<sub>10</sub>)<sub>2</sub> + (-17<sub>10</sub>)<sub>2</sub>"

+++question write "Même question dans une mémoire de deux octets"

+++question write "Est-ce que les résultats des questions 3 et 4 sont cohérents ?"

## Entraînement

+++question write "En considérant que nous utilisons une mémoire de <u><b>1 octet</b></u>, calculez les additions suivantes"

- (17<sub>10</sub>)<sub>2</sub> + (-19<sub>10</sub>)<sub>2</sub>
- (42<sub>10</sub>)<sub>2</sub> + (-23<sub>10</sub>)<sub>2</sub>
- (-12<sub>10</sub>)<sub>2</sub> + (96<sub>10</sub>)<sub>2</sub>
- (-66<sub>10</sub>)<sub>2</sub> + (-32<sub>10</sub>)<sub>2</sub>
- (-111<sub>10</sub>)<sub>2</sub> + (-110<sub>10</sub>)<sub>2</sub>

## Vous êtes en avance ?

+++question "Écrivez un programme python d'entraînement à la méthode du complément à 2"

Le programme fonctionne de la manière suivante :
- 2 nombres décimaux compris entre -127 et 127 sont générés, ils sont affichés dans la console
    - L'utilisateur calcule de son côté sur un papier les équivalents binaires des deux nombres générés
- Si l'utilisateur appuie ensuite une première fois sur la touche entrée, la console affiche les équivalents binaires des deux nombres générés. L'utilisateur peut vérifier si ses conversions sont bonnes
    - L'utilisateur calcule ensuite l'addition entre les deux nombres générés
- Si l'utilisateur appuie ensuite une deuxième fois sur la touche entrée, la console affiche le résultat de l'addition des deux nombres initialement générés. L'utilisateur peut vérifier si ses calculs sont bons


## Réponses

<ol start="0">
  <li>
  +++spoil
  <table class="table table-sm text-center">
    <thead>
      <tr>
        <th>Nombre en base 10</th>
        <th>Nombre en base 2</th>
        <th>Nombre négatif en base 2</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>17<sub>10</sub></td>
        <td>0000 0000 0001 0001<sub>2</sub></td>
        <td>1111 1111 1110 1111<sub>2</sub></td>
      </tr>
      <tr>
        <td>54<sub>10</sub></td>
        <td class="text-green">0000 0000 0011 0110<sub>2</sub></td>
        <td class="text-green">1111 1111 1100 1010<sub>2</sub></td>
      </tr>
      <tr>
        <td>255<sub>10</sub></td>
        <td class="text-green">0000 0000 1111 1111<sub>2</sub></td>
        <td class="text-green">1111 1111 0000 0001<sub>2</sub></td>
      </tr>
      <tr>
        <td>1023<sub>10</sub></td>
        <td class="text-green">0000 0011 1111 1111<sub>2</sub></td>
        <td class="text-green">1111 1100 0000 0001<sub>2</sub></td>
      </tr>
      <tr>
        <td>4912<sub>10</sub></td>
        <td class="text-green">0001 0011 0011 0000<sub>2</sub></td>
        <td class="text-green">1110 1100 1101 0000<sub>2</sub></td>
      </tr>
      <tr>
        <td>32768<sub>10</sub></td>
        <td class="text-green">1000 0000 0000 0000<sub>2</sub></td>
        <td class="text-red">impossible sur 2 octets</td>
      </tr>
    </tbody>
  </table>
  spoil+++
  </li>
  <li>
  +++spoil
    Sur 2 octets, 32768<sub>10</sub> vaut 1000 0000 0000 0000<sub>2</sub>. Or, pour pouvoir utiliser la méthode du complément à 2, un nombre positif doit commencer tout à gauche par un bit égal à 0. Ce qui n'est pas le cas ici. Il faudrait alors utiliser une mémoire plus grande pour pouvoir prendre en compte ce nombre
  spoil+++
  </li>
  <li>
  +++spoil
    Deux octets contiennent 8 bits. Le premier bit de gauche est réservé pour le signe du nombre, il reste donc 7 bits. Combien de combinaisons possibles de nombres peut on donc avoir sur 7 bits ?
    <ul>
      <li>Dans 1 bit, nous avons 2 nombres possibles: 0 ou 1</li>
      <li>Dans 2 bits, nous avons (2 * 2) 4 nombres possibles: 00, 01, 10 ou 11</li>
      <li>Dans 3 bits, nous avons (2 * 2 * 2) 8 nombres possibles: 000, 001, 010, ..., ou 111</li>
      <li>...</li>
      <li>Dans 7 bits, nous avons (2<sup>7</sup>) 128 nombres possibles</li>
    </ul>
    Comme le bit de gauche vaut 0 si le nombre est positif ou 1 si le nombre est négatif. Nous obtenons donc un total de (128 * 2) 256 nombres décimaux représentables sur un octet allant de -127 à 127.
  spoil+++
  </li>
  <li>
  +++spoil
    <ul>
      <li>17<sub>10</sub> = 0001 0001<sub>2</sub></li>
      <li>Via la méthode du complément à 2 : -17<sub>10</sub> = 1110 1111<sub>2</sub></li>
    </ul>
    <p>En posant l'addition 0001 0001<sub>2</sub> + 1110 1111<sub>2</sub>, nous obtenons 1 0000 0000. Or, comme nous calculons depuis une mémoire de 1 octet (donc de 8 bits), le bit 1 tout à gauche est ignoré par la mémoire. Ainsi sur un octet, 0001 0001<sub>2</sub> + 1110 1111<sub>2</sub> = 0000 0000<sub>2</sub></p>
  spoil+++
  </li>
  <li>
  +++spoil
    Sur deux octets : 
    <ul>
      <li>17<sub>10</sub> = 0000 0000 0001 0001<sub>2</sub></li>
      <li>Via la méthode du complément à 2 : -17<sub>10</sub> = 1111 1111 1110 1111<sub>2</sub></li>
    </ul>
    <p>En posant l'addition 0000 0000 0001 0001<sub>2</sub> + 1111 1111 1110 1111<sub>2</sub>, nous obtenons 1 0000 0000 0000 0000. Or, comme nous calculons depuis une mémoire de 2 octets (donc de 16 bits), le bit 1 tout à gauche est ignoré par la mémoire. Ainsi sur un octet, 0000 0000 0001 0001<sub>2</sub> + 1111 1111 1110 1111<sub>2</sub> = 0000 0000 0000 0000<sub>2</sub></p>
  spoil+++
  </li>
  <li>
  +++spoil
    Que l'on soit sur 1 octet ou sur deux octets (et sur 3 octets, sur 4 octets, etc ...), (17<sub>10</sub>)<sub>2</sub> + (-17<sub>10</sub>)<sub>2</sub> est bien égal à 0.
  spoil+++
  </li>
  <li value="7">
  +++spoil
<p>Pour développez ce programme, il vaut mieux procéder par étapes. Dans cette première étape, créons, sans rentrer dans les détails le corps principal du programme : </p>

<p><b>Étape 0 : version sans commentaire</b></p>

widget python
import random

a = random.randint(-127, 127)
b = random.randint(-127, 127)

input("afficher les nombres en binaire ? pressez entrée")

a_en_binaire = decimalToBinary(a)
b_en_binaire = decimalToBinary(b)

print(str(a) + " : " + a_en_binaire)
print(str(b) + " : " + b_en_binaire)

input("afficher le résultat de l'addition binaire ? pressez entrée")

resultat = additionBinaires(a_en_binaire + b_en_binaire)

print(resultat)
widget

<p><b>Étape 0 : version avec commentaires</b></p>

widget python
import random # nécessaire juste après pour générer des nombres aléatoires

# deux nombres décimaux aléatoires sont crées
a = random.randint(-127, 127)
b = random.randint(-127, 127)

# nous mettons en pause le programme tant que l'utilisateur n'appuie pas sur la touche entrée
input("afficher les nombres en binaire ? pressez entrée")

# l'ordinateur calcul et affiche les équivalents binaires des nombres a et b
a_en_binaire = decimalVersBinaire(a)
b_en_binaire = decimalVersBinaire(b)

print(str(a) + " : " + a_en_binaire)
print(str(b) + " : " + b_en_binaire)

# nous mettons en pause le programme tant que l'utilisateur n'appuie pas sur la touche entrée
input("afficher le résultat de l'addition binaire ? pressez entrée")

# l'ordinateur calcul l'addition entre a_en_binaire et b_en_binaire et affiche le résultat
resultat = additionBinaires(a_en_binaire + b_en_binaire)

print(resultat)
widget

<p>Les fonctions <code>decimalVersBinaire</code> et <code>additionBinaires</code> n'existent pas. Elle doivent donc être définies ainsi que que leur contenu</p>
<p>Commençons par la fonction <code>decimalVersBinaire</code>. Cette fonction prend en entrée un nombre entier et renvoie son équivalent en binaire. Pour se faciliter la tâche, l'équivalent binaire sera stocké dans un <code>string</code>.</p>
<p>Le nombre donné en entré est soit positif (le cas où la conversion est le plus simple), soit négatif (où là nous devons appliquer la méthode du complément à 2). Ne prenons en compte pour l'instant que les nombres positifs avec la fonction <code>decimalVersBinaire</code> : </p>

widget python
def decimalVersBinaire(nombre):
  assert isinstance(nombre, int)

  # nous appliquons les divisions successives par 2 tant que le quotient n'est pas égal à zéro

  quotient = nombre
  resultat = ""

  while(quotient != 0):
    reste = quotient % 2 # '%' permet d'obtenir le reste de la division entre quotient et 2 
    quotient = quotient // 2 # '//' permet de faire une division et de ne conserver que la partie entière du résultat
    resultat = str(reste) + resultat
  
  return resultat

# exemple d'utilisation de la fonction
print(decimalVersBinaire(18))

widget

<p>Améliorons la fonction <code>decimalVersBinaire</code> pour qu'elle puisse gérer les conversions de nombres négatifs. Dans le cas où le nombre à convertir est négatif, alors nous devons appliquer la méthode du complément à 2. Cette partie-la devient un peu complexe si on la programme de A à Z, accrochez vous ! </p>

widget python
def decimalVersBinaire(nombre):
  assert isinstance(nombre, int)
  
  # nous appliquons les divisions successives par 2 tant que le quotient n'est pas égal à zéro

  quotient = abs(nombre) # si nombre est négatif, la boucle while va s'exécuter indéfiniment. Donc j'utilise la fonction abs (absolue) pour être sur de travailler au début avec un nombre positif
  resultat = ""

  while(quotient != 0):
    reste = quotient % 2 # '%' permet d'obtenir le reste de la division entre quotient et 2 
    quotient = quotient // 2 # '//' permet de faire une division et de ne conserver que la partie entière du résultat
    resultat = str(reste) + resultat
  
  if(nombre < 0): # nous devons appliquer la méthode du complément à 2 si le nombre est négatif
    # je dois parcourir chacun des bits et les inverser
    bits_inverses = ""
    for bit in resultat:
      if(bit == '0'):
        bits_inverses += "1"
      else:
        bits_inverses += "0"
    
    # ensuite, je dois ajouter 1. Pour me faciliter le travail, je renverse l'ordre des bits pour parcourir mon nombre de droite vers la gauche. La fonction reversed est fournie par défaut par python
    resultat_negatif = ""
    retenue = "1"
    for bit in reversed(bits_inverses):
      if(bit == "1" and retenue == "1"):
        resultat_negatif = "0" + resultat_negatif 
        retenue = "0"
      elif(bit == "0" and retenue == "1"):
        resultat_negatif = "1" + resultat_negatif
        retenue = "0"
      else:
        resultat_negatif = bit + resultat_negatif
    resultat = resultat_negatif
  
  
  return resultat

# exemple d'utilisation de la fonction
print(decimalVersBinaire(17))
print(decimalVersBinaire(-17))

widget

<p>La rédaction de la fonction 'decimalVersBinaire' est maintenant terminée ! il ne reste plus qu'à s'occuper de la fonction <code>additionBinaires</code></p>

  spoil+++
  </li>
</ol>