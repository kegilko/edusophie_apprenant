# Tri et fusion de données dans une table

+++programme
  Trier une table suivant une colonne
  Construire une nouvelle table en combinant les données de deux tables

  Ajouter des données dans un fichier CSV
  Trier un fichier CSV à l'aide de la bibliothèque python panda 
  Fusionner plusieurs fichiers CSV à l'aide de la bibliothèque python panda 
+++

+++sommaire

## Quelques rappels

+++bulle matthieu
  qu'est-ce qu'un protocole en informatique ? 
+++

+++spoil
  Un protocole est un ensemble de règles établies qui permettent à deux machines de dialoguer entres elles
spoil+++

<hr>

Si je souhaite que deux machines échangent des données, ces données doivent être **idéalement** structurées sous le même format. (utiliser le même protocole est nécessaire mais ne suffit pas)

+++bulle matthieu
  quels formats de fichiers de données structurées connaissez vous ? 
+++

+++spoil

  <ul>
    <li>Les fichiers CSV : </li>
  </ul>
  <pre><code class="csv language-csv hljs">animal;age;durée de la vidéo (en secondes)
oiseau;14;124
poisson;4;99
cheval;17;64
  </code></pre>

  <p>Dans le cas d'un fichier au format CSV, on dit que les données sont disposées en table</p>

  <ul>
    <li>Les fichiers JSON : </li>
  </ul>
  <pre class="col-lg-12 mx-auto col-print-12"><code class="hljs text language-json">[
    {
      "animal": "oiseau",
      "age": 14,
      "durée de la vidéo (en seconde)": 124
    },
    {
      "animal": "poisson",
      "age": 4,
      "durée de la vidéo (en seconde)": 99
    },
    {
      "animal": "cheval",
      "age": 17,
      "durée de la vidéo (en seconde)": 64
    }
  ]
  </code></pre>

spoil+++

<hr>

Des logiciels permettent de manipuler des fichiers CSV (logiciel tableur) tels que : excel (microsoft), calc (libreoffice) ou encore la solution Web google sheet

+++bulle matthieu
  quelles limites y a-t-il à utiliser ses logiciels ? 
+++

+++spoil
  <ul>
    <li>Ces logiciels ne peuvent pas traiter de fichiers de données volumineux (lorsque le fichier de données commencent à peser plusieurs centaines de mega-octets)</li>
    <li>Nous sommes dépendants des fonctionnalités mises à disposition par ces logiciels. Autrement dit, nous ne pouvons pas faire ce que nous voulons avec les données</li>
    <li>Il est difficile voir impossible d'automatiser des actions sur nos données : ajout automatique de données, suppression automatique, tri automatique, etc... Pour ces actions, il faut qu'un humain les effectue manuellement</li>
  </ul>
spoil+++

## Amorce du prochain projet

Le but du prochain projet est de créer un mini-site Web affichant des éléments contenus dans un fichier CSV. Le fichier CSV sera lu côté serveur.

Voici quelques idées de projets : 

- Créer un moteur de recherche Web
- Créer un chan de discussion Web
- Créer une page de produits d'un site ecommerce
- Créer un évaluateur de mot de passe

Le mini-site devra proposer au visiteur la possibilité d'afficher de trier les données affichées (selon les critères de votre choix)

+++bulle matthieu
  vous pouvez bien sur proposer vos propres idées
+++

+++bulle matthieu droite
  seuls impératifs: le projet doit consister au développement d'un mini-site web dont le contenu provient d'un fichier CSV
+++

## Trier et fusionner des données disposées en table 

+++activité binome 20 ../activites/applications-_directes/tri-_de-_donnees-_en-_table.md "Tri des données en table"

À travers une approche from scratch (= partir de zéro), plusieurs dizaines de lignes de code python sont nécessaires pour pouvoir trier des données structurées en table.

Fort heureusement, et c'est là l'une des puissances du domaine de l'informatique, nous pouvons utiliser des bibliothèques prêtes à l'emploi qui permettent de réduire considérablement le nombre de ligne d'un programme et d'en faciliter sa lecture.

### Découverte de la bibliothèque python pandas

La bibliothèque **python** **pandas** met à disposition un ensemble de fonctions prêtes à l'emploi permettant de manipuler facilement des données structurées.

+++activité binome 70 ../activites/applications-_directes/tri-_et-_fusion-_de-_donnees-_en-_table-_avec-_pandas.md "tri et fusion de données en table avec pandas"

+++bulle matthieu
  vous avez terminé l'activité et votre binôme aussi ? 
+++

+++bulle matthieu droite
  choisissez un des sujets plus bas (ou proposez le votre), ils sont classés par ordre de difficulté
+++

### +++emoji-write Résumons

- (Rappel) En programmation, une bibliothèque regroupe un ensemble de fonctions prêtes à l'emploi
    - Cela permet de simplifier le code en limitant le nombre de ligne et en le rendant plus lisible
- La bibliothèque <code>pandas</code> (et également <code>csv</code>) permet de manipuler facilement des données structurées en table :
    - Pour enregistrer le contenu d'un fichier CSV dans une variable, à l'aide de <code>pandas</code>, il faut utiliser la fonction <code>read_csv</code>. Cette fonction stocke le contenu d'un fichier CSV dans une variable de type <code>dataframe</code>
    - Un <code>dataframe</code> est une sorte de super-variable créée de toute pièce par la bibliothèque <code>pandas</code>. En programmation nous appelons cela un objet (concept qui sera exploré en terminale NSI)
    - Il est possible d'appliquer des fonctions directement sur des objets via la syntaxe : <code>monObject.leNomDeLaFonction(...)</code>
    - Il est possible d'enregistrer le contenu d'un dataframe dans un fichier CSV à l'aide de la fonction <code>to_csv</code>, exemple : <code>monDataframe.to_csv('nomDuFichierCSV.csv')</code>
    - La bibliothèque <code>pandas</code> permet de concaténer ou de fusionner facilement des fichiers CSV à l'aide de fonctions prêtes à l'emploi : <code>concat</code>, <code>merge</code>, ...
        - Le comportement de ces fonctions est décrit +++lien "https://pandas.pydata.org/pandas-docs/stable/user_guide/merging.html" "dans la documentation officielle"
    - La bibliothèque <code>pandas</code> permet de trier facilement le contenu d'un fichier CSV à l'aide de la fonction <code>sort_values</code>

## Sujets des projets

- Créer une page de produits d'un site ecommerce
- Créer un évaluateur de mot de passe
- Créer un moteur de recherche Web
- Créer un chan de discussion Web

+++bulle matthieu
  pour le sujet choisi, créez un fichier CSV
+++

- produits d'un site ecommerce
    - le fichier CSV doit contenir des informations sur des produits (nom, prix, description, ...)
- Créer un évaluateur de mot de passe
    - créez un fichier CSV contenant les 500 mots de passes les plus utilisés en vous basant +++lien "https://github.com/danielmiessler/SecLists/blob/master/Passwords/Common-Credentials/10-million-password-list-top-500.txt" "sur ce lien"
- Créer un moteur de recherche Web
    - créer un fichier CSV qui contient les informations de vos 5 sites Web préférés (nom du site, url, descriptions, mots-clés...)
- Créer un chan de discussion Web
    - créer un fichier CSV qui contient deux colonnes : pseudo et message. Ajoutez quelques lignes dans le fichier CSV
    
+++bulle matthieu
  pour terminer (et c'est la première étape du projet), créer une page Web (à l'aide de flask)
+++

+++bulle matthieu droite
  le fichier CSV doit être lu côté serveur et la page Web générée doit afficher tous le contenu du fichier CSV
+++


