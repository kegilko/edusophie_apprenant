# HTML et javascript

Dans cette activité, vous devez analyser le code de cette <a href="/fichiers/EFDAC52D344DCG9HF232A47EGCG9B2" target="_blank">page Internet</a> :

- Créez vous un dossier du jour

Pour rappel pour créer votre dossier du jour et y accéder, utilisez les commandes `mkdir` et `cd`. Utilisez la touche `tabulation` pour compléter les chemins dans vos commandes.

- Ouvrez la page avec votre navigateur Internet préféré
- Sauvegardez la page dans votre dossier du jour (raccourcis `ctrl + s`)
- Depuis votre terminal, ouvrez le fichiez .html avec la commande `gedit`

Vous allez devoir remplir 2 objectifs  en priorité : 
- remplir la partie 'javascript' du cheatsheet donnée en cours au fur et à mesure de l'activité
- lister les évènements utilisés par javascript (plus d'explication ci-dessous)

## Javascript et les événements

Javascript permet de déclencher certaines parties du code en fonction des actions effectuées par le visiteur sur la page. 

Intéressons nous à l'événement 'mouseover'. Dans le premier bloc de la page, vous allez pouvoir tester ce type d'événement : 

![](/images/md/87BHH64922D62CAAHD64C4CH84CBAA)

- suivez les instructions écrites dans ce bloc qui permettent de tester l'événement 'mouseover'
- ensuite, répondez aux questions suivantes en analysant le code de la page

+++question write "Donner le nom des balises qui contiennent l'attribut <code>onmouseover</code>"

+++bulle matthieu
  le nom de l'événement est bien <code>mouseover</code>
+++

+++bulle matthieu droite
  le <code>on</code> de <code><b>on</b>mouseover</code> indique juste au navigateur "attention ! un événement est déclenché ici !"
+++

+++question write "Dans ces balises, la valeur de l'attribut 'onmouseover' est de la forme '<code>nom()</code>' (<code>survol1()</code> et <code>survol2()</code>). À quel type d'instruction python vous fait penser cette syntaxe ?"

- Analysez le code qui est exécuté lorsque vous survolez avec votre souris les éléments <b>ici !</b>
- Remplissez la feuille cheatsheet python/javascript" par rapport au code que vous avez analysé et qui concerne l'événement 'mouseover'

Pour la suite, analysez le reste du code de la page et remplissez petit à petit le tableau ci-dessous ( +++emoji-writing_hand ) et complétez le cheatsheet python/javascript.

<table class="table">
  <thead>
    <tr>
      <th>Nom de l'événement</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>onmouseover</td>
      <td class="col-12">est déclenché lorsque la souris survole l'élément HTML</td>
    </tr>
    <tr>
      <td>...</td>
      <td>...</td>
    </tr>
  </tbody>
</table>

## Réponses 

<ol start="0">
  <li>
+++spoil
  <p>deux balises <code>b</code> contiennent l'attribut <code>onmouseover</code> :</p>
  <img class="col-12 border border-dark" src="/images/md/89GH9CF98HGHHGC77F95HEBG3A4H2E">
spoil+++
  </li>
  <li>
+++spoil
  <p>Cette syntaxe est la même que celle utilisée en python lorsqu'on souhaite appeler ( = exécuter) une fonction. Les fonctions <code>survol1()</code> et <code>survol2()</code> sont définies dans la balise script située dans la balise header au tout début du code : </p>
  
  <img class="col-12 border border-dark" src="/images/md/AF3GC4GGGH4GF3BB6D5EAB4G929847">
  
spoil+++
  </li>
</ol>

    <p>Le tableau "python/javascript cheatsheet" complété</p>
  +++spoil
<br><br><ul><p>Liens vers le tableau : </p>
  <li><a href="/fichiers/356DF864465EG3CC7948HD96GAF528" target="_blank">version pdf</a></li>
  <li><a href="/fichiers/EG3F68636F2A6535329B8EE27FFC3B" target="_blank">version éditable (.odt)</a></li>
</ul>
  spoil+++
  
    <p>Le tableau "Événements Javascripts"complété</p>
  +++spoil
<table class="table">
  <thead>
    <tr>
      <th>Nom de l'événement</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>onmouseover</td>
      <td class="col-12">est déclenché lorsque la souris survole l'élément HTML</td>
    </tr>
    <tr>
      <td>onload</td>
      <td class="col-12">est déclenché lorsque la page a fini de se charger</td>
    </tr>
    <tr>
      <td>onclick</td>
      <td class="col-12">est déclenché lorsqu'on clique sur l'élément avec la souris</td>
    </tr>
    <tr>
      <td>onkeydown</td>
      <td class="col-12">est déclenché lorsqu'on appuie sur une touche du clavier</td>
    </tr>
  </tbody>
</table>
  spoil+++
  
