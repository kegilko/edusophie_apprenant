# Les limites des variables de type de base

Le but secondaire de cette activité est de continuer à s'entraîner à programmer. Et le but principal est de se confronter à un problème dans lequel l'utilisation de variables de type de base sera inadaptée.

Enfilez vos casquettes de développeur(se)s de jeux vidéo. Je vous présente Mu, le héros principal de votre histoire. Voici les informations reliées à cette entité venue d'ailleurs ! 

<style>
  .perso{
    text-align: center;
    border: 4px solid #0d6efd !important;
    border-radius: 0.25rem !important;
    margin-right: auto !important;
    margin-left: auto !important;
    margin-bottom: 2rem !important;
    background-image: url('/images/md/9F1DCC54391C9EB542FEEA84G32AD4');
    background-size: cover;
    flex: 0 0 auto;
    width: 50%;
  }
  .fperso h2{
    margin:0 0 2rem 0 !important;
    padding:1rem 0 !important;
    border-bottom: 2px solid black !important;
    color: black !important;
  }
  .fperso h3{
    color: black !important;
  }
  .fperso table{
    width:90%;
    margin-right: auto !important;
    margin-left: auto !important;
    margin-bottom: 1rem !important;
  }
  .fperso .table1 tr td:first-child {
    text-align: left;
    padding-left:1rem;
  }
  .fperso img{
    width: 100%;
    border:1px solid;
    border-radius:50%;
    background-color: #ffffff52;
  }
  .fperso .text-red{
    color:red;
  }
</style>

<div class="fperso">
  <h2 class="w-100 bg-primary">Feuille de personnage</h2>
  <div class="image col-5 mx-auto">
    <img src="/images/md/H7BABDH4GEDF7GH9B5BE7DE94CGD7G">
  </div>
  <h3>Mu</h3>
  
  <table class="table1">
    <thead>
      <tr><th colspan="2">Caractéristiques</th></tr>
    </thead>
    <tbody>
      <tr>
        <td><b>race</b></td>
        <td>poulet dimensionnel</td>
      </tr>
      <tr>
        <td><b>points d'expérience</b></td>
        <td>3</td>
      </tr>
      <tr>
        <td><b>niveau</b></td>
        <td class="text-red">?</td>
      </tr>
    </tbody>
  </table>
  <table>
    <thead>
      <tr><th>Capacités</th></tr>
    </thead>
    <tbody>
      <tr>
        <td>retour vers le futur mineur</td>
      </tr>
      <tr>
        <td>coup de bec quantique léger</td>
      </tr>
    </tbody>
  </table>
</div>

Ce personnage, contrôlé par le joueur, acquiert de l'expérience après diverses actions (tuer un monstre, terminer une quêtes, etc..) puis gagne des niveaux lorsqu'un certain nombre de points d'expériences est atteint.

- Créez un script python nommé 'pouletRPG.py' puis ouvrez le fichier avec thonny à l'aide de la commande bash suivante 

```bash
# assurez vous d'être dans votre dossier du jour depuis votre terminal puis exécutez : 
touch pouletRPG.py && thonny pouletRPG.py
```

+++question write "Rappelez ce que fait la commande bash <code>touch</code>"

+++question write "Dans la commande ci-dessus, que permet de faire <code>&&</code> ?"

Avant d'aller faire combattre votre poulet dimensionnel, il faut d'abord déterminer quel est le niveau de notre héros à plumes en fonction de son nombre de points d'expériences.

Vous allez donc créer pas à pas une fonction qui permet de répondre à ce besoin.

## Déclaration de la fonction 'getLevel'

0. Déclarez une fonction nommée 'getLevel'
1. Cette fonction doit prendre un paramètre en entrée, nommez ce paramètre 'xp'
2. 'xp' doit être un nombre entier, vérifier **dès le tout début de la fonction 'getLevel'** que la valeur de 'xp' soit bien un entier

+++spoil "Aide pour l'étape 2"
  Pour tester le type d'une variable, il faut utiliser l'instruction <code>assert</code> avec la fonction <code>isinstance</code>
spoil+++

+++question write "Quelles lignes de code avez vous du écrire pour répondre aux 3 étapes ci-dessus ?"

## Contenu de la fonction 'getLevel'

### Initialisation des données nécessaires

En mathématique, la fonction exponentielle (e) est parfaite pour décrire le nombre de points d'expériences par niveau. 

+++diagramme col-6 mx-auto
  flowchart TB
  A[<img src="/images/md/946CHCCFDD65F557BB299AEA5H675E">]
  B[<img src="/images/md/D9A756F72HBB321G6BC55EDG8E989B">]
  A-->B
+++
<div class="col-8 mx-auto text-center">Pour les curieux et curieuses, le code pour générer ces données <a href="/fichiers/FG91FH3389E3AAHE3G9E39BDCD3AG9" target="_blank">se trouve ici</a><a></a></div><a>

+++question "En utilisant les données du graphique ci-dessus, étoffez le contenu de la fonction 'getLevel' comme suit : "

- Déclarez une variable par niveau (niveau1, niveau2, ...) 
- Affecter à chacune de ces variables le nombre de points d'expériences correspondant (1 point d'xp pour le niveau 1, 4 points d'xp pour le niveau 2, etc..)

### Retour de la fonction

Avant la fin de la déclaration de la fonction 'getLevel', ajoutez les lignes qui correspondent à ces commentaires :

```python
# SI la valeur de la variable xp est inférieure à la valeur de la variable niveau2
#    ALORS retourner 1 
```

+++question write "Quel est le code python correspondant à ces commentaires ?"

Vous allez tester maintenant si votre fonction marche bien pour un nombre de points d'expériences inférieur à 4. Pour cela, ajoutez les lignes qui correspondent à ces commentaires **après** la déclaration de la fonction 'getLevel' : 

```python
# 0. déclarez la variable mu_xp et affectez à cette variable la valeur 3
# 1. appelez la fonction getLevel en lui soumettant comme paramètre la variable gmu_xp
# 2. capturez le résultat de la fonction 'getLevel' dans une variable nommée level
# 3. affichez la valeur de la variable level
```

+++question write "Quel est le code python correspondant à ces commentaires ?"

+++bulle matthieu
  Si vous avez respecté toutes les consignes, le programme devrait afficher 1 dans la console lorsque vous l'exécutez ! 
+++

### Compléter le contenu de la fonction 'getLevel'

Pour l'instant la fonction ne gère que le cas où l'expérience envoyée en paramètre est inférieure à 4. 

- Compléter la fin de la fonction avec des instructions <code>elif</code> et une instruction <code>else</code> afin de gérer tous les cas du niveau 1 au niveau 10

+++spoil "Aide"
Programmez et testez étapes après étapes, ne faîtes pas tout d'un coup :

<ul>
  <li>D'abords, gérez le cas où l'expérience peut être inférieures à 4 ou lorsqu'elle est inférieure à 10</li>
  <li>Ensuite, exécutez votre code en changeant la valeur du paramètre passé dans l'appel de la fonction 'getLevel' et vérifiez que votre programme affiche bien le résultat que vous attendez</li>
  <li>Et ainsi de suite, améliorez petit à petit votre programme pour gérer tous les niveaux </li>
</ul>
spoil+++

## Les limites des variables de type de base

Votre programme fonctionne, vous souhaitez maintenant rajouter une centaine de niveaux dans votre programme

+++question write "Estimer le nombre de lignes que vous devez ajouter dans votre programme pour gérer 100 niveaux (soit 90 niveaux de plus qu'actuellement)"

+++bulle matthieu
et le problème est bien là, en utilisant une variable par données le nombre de ligne dans le programme va rapidement exploser
+++

+++bulle matthieu droite
et plus il y a de lignes dans un programme, plus il devient lourd et compliqué à faire évoluer, plus il y a de risque d'insérer une erreur involontaire, etc..
+++

## Réponses

<ol start="0">
  <li>
+++spoil
  En bash, la commande <code>touch</code> permet de créer un fichier
spoil+++
  </li>
  <li>
+++spoil
  En bash, <code>&&</code> permet d'exécuter plusieurs commandes en une seule ligne :
  <ul>
    <li>'<code>touch pouletRPG.py</code>' est d'abord exécuté</li>
    <li>et ensuite, grâce à '<code>&&</code>', la commande suivante '<code>thonny pouletRPG.py</code>' est exécutée</li>
  </ul>
spoil+++
  </li>
  <li>
+++spoil
  <pre><code>def tableDeNiveaux(xp):
  assert insinstance(xp, int)</code></pre>
spoil+++
  </li>
  <li>
+++spoil
  <pre><code>def tableDeNiveaux(xp):
  assert insinstance(xp, int)
  
  niveau1 = 1
  niveau2 = 4
  niveau3 = 10
  niveau4 = 24
  niveau5 = 54
  niveau6 = 121
  niveau7 = 269
  niveau8 = 601
  niveau9 = 1338
  niveau10 = 2980</code></pre>
spoil+++
  </li>
  <li>
+++spoil
  <pre><code class="python language-python">def getLevel(xp):
  assert isinstance(xp, int)
  
  niveau1 = 1
  niveau2 = 4
  niveau3 = 10
  niveau4 = 24
  niveau5 = 54
  niveau6 = 121
  niveau7 = 269
  niveau8 = 601
  niveau9 = 1338
  niveau10 = 2980
  
  # SI la valeur de la variable xp est inférieure à la valeur de la variable niveau2
  if(xp < niveau2):
    # ALORS retourner 1 
    return 1</code></pre>
spoil+++
  </li>
  <li>
+++spoil
  <pre><code class="python language-python">def getLevel(xp):
  assert isinstance(xp, int)
  
  niveau1 = 1
  niveau2 = 4
  niveau3 = 10
  niveau4 = 24
  niveau5 = 54
  niveau6 = 121
  niveau7 = 269
  niveau8 = 601
  niveau9 = 1338
  niveau10 = 2980
  
  # SI la valeur de la variable xp est inférieure à la valeur de la variable niveau2
  if(xp < niveau2):
      # ALORS retourner 1 
      return 1

# Déclarez la variable mu_xp et affectez à cette variable la valeur 3
mu_xp = 3

# Appelez la fonction getLevel en lui soumettant comme paramètre la variable gmu_xp
# et
# Capturez le résultat de la fonction 'getLevel' dans une variable nommée level
level = getLevel(mu_xp)

# Affichez la valeur de la variable level
print(level)
</code></pre>
spoil+++
  </li>
  <li>
+++spoil
  À raison d'une ligne pour déclarer une variable et de deux lignes de tests à faire supplémentaire (l'instruction <code>elif</code> plus le <code>return</code>). Passer de 10 niveau à 100 niveau implique l'ajout de 90 * 3 (270) nouvelles lignes.
  <br><br>
  +++bulle matthieu
    <b>note au passage</b>, au lycée si le code de votre programme dépasse la centaine de ligne, il est fort probable que vous fassiez fausse route quelque part 
  +++
  
spoil+++
  </li>
</ol>


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br></a>