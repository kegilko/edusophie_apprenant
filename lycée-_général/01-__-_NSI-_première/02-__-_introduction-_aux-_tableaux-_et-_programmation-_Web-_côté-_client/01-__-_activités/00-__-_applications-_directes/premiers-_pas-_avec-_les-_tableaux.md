# Premiers pas avec les tableaux

Cette activité fait suite à l'activité "<a href="les-_limites-_des-_variables-_de-_type-_de-_base.md" target="_blank">Les limites des variables de type de base</a>" du cours sur les tableaux.

À la fin de la précédente activité, le dernier code produit est celui-ci : 

```python
def getLevel(xp):
  assert isinstance(xp, int)

  niveau1 = 1
  niveau2 = 4
  niveau3 = 10
  niveau4 = 24
  niveau5 = 54
  niveau6 = 121
  niveau7 = 269
  niveau8 = 601
  niveau9 = 1338
  niveau10 = 2980

  if(xp < niveau2):
      return 1
  elif(xp < niveau3):
      return 2
  elif(xp < niveau4):
      return 3
  elif(xp < niveau5):
      return 4
  elif(xp < niveau6):
      return 5
  elif(xp < niveau7):
      return 6
  elif(xp < niveau8):
      return 7
  elif(xp < niveau9):
      return 8
  elif(xp < niveau10):
      return 9
  else:
      return 10

mu_xp = 300
level = getLevel(mu_xp)
print(level)
```

En cours, vous avez appris un nouveau type de variable qui permet de stocker un ensemble de données.

+++question "Remplacez les variables déclarées au début de la fonction getLevel par une seule variable nommée 'tableaux' qui est de type tableau"

SI vous exécutez le code, votre **IDE** vous sort une erreur

+++question write "Expliquez l'erreur remontée par votre IDE"

+++question "Corrigez dans votre programme les lignes qui provoquent ce type d'erreur"

+++bulle matthieu
grâce à un tableau, vous avez pu déjà réduire toutes les déclarations de variables en une seule ligne
+++

+++bulle matthieu droite
cependant, le nombre de lignes utilisées par les instructions conditionnelles reste conséquent
+++

Pour réduire le bloc de code <code>if / elif / elif / ... / else</code>, il va falloir apprendre à parcourir un tableau à l'aide d'une instruction itérative (<code>for</code> ou <code>while</code>). Ce qui sera l'objet d'un prochain cours. Pour les personnes en avance, plongez vous dans Internet pour trouver comment parcourir une liste en Python :)

## Résultats

<ol>
  <li>
+++spoil

<pre><code class="python language-python">def getLevel(xp):
  assert isinstance(xp, int)

  niveaux = [1,4,10,24,54,121,269,601,1338,2980]

  if(xp < niveau2):
      return 1
  elif(xp < niveau3):
      return 2
  elif(xp < niveau4):
      return 3
  elif(xp < niveau5):
      return 4
  elif(xp < niveau6):
      return 5
  elif(xp < niveau7):
      return 6
  elif(xp < niveau8):
      return 7
  elif(xp < niveau9):
      return 8
  elif(xp < niveau10):
      return 9
  else:
      return 10

mu_xp = 300
level = getLevel(mu_xp)
print(level)
</code></pre>

spoil+++
  </li>
  <li>
+++spoil
Une erreur est remontée au niveau de l'instruction <code>if</code> car la variable utilisée (niveau2) dans cette instruction n'existe plus
spoil+++
  </li>
  <li>
+++spoil

<pre><code class="python language-python">def getLevel(xp):
  assert isinstance(xp, int)

  niveaux = [1,4,10,24,54,121,269,601,1338,2980]

  if(xp < niveaux[1]):
      return 1
  elif(xp < niveaux[2]):
      return 2
  elif(xp < niveaux[3]):
      return 3
  elif(xp < niveaux[4]):
      return 4
  elif(xp < niveaux[5]):
      return 5
  elif(xp < niveaux[6]):
      return 6
  elif(xp < niveaux[7]):
      return 7
  elif(xp < niveaux[8]):
      return 8
  elif(xp < niveaux[9]):
      return 9
  else:
      return 10

mu_xp = 300
level = getLevel(mu_xp)
print(level)
</code></pre>

spoil+++
  </li>
</ol>
