# Projet "Galerie de photos"

+++sommaire 1

## Sujet du projet

Un client, nommé Lucas, vous demande de développer un site Internet affichant une galerie de photos. Ci-dessous, voici les principaux échanges qui ont eu lieu pendant la réunion : 

+++bulle lucas
  je ne peux pas vous fournir pour l'instant les photos !
+++

+++bulle matthieu droite
  pas de problème ! nous pouvons travailler dès maintenant en utilisant des photos factices en attendant ! 
+++

+++bulle lucas
  super !
+++

<br>

+++bulle lucas
  j'aimerai que les photos soient disposées dans une grille sous forme de card contenant quelques informations
+++

+++bulle lucas droite
  voici ce que ça donne si je griffonne ça sur un tableau
+++

<div class="d-flex justify-content-around">
  
  <div class="col-5">
    <img class="w-100" src="/images/md/9939CFC962A299HB3B8EB78CEG9AC7">
    <div class="text-center">vue générale</div>
  </div>
  
  <div class="col-5">
    <img class="w-100" src="/images/md/E435BD285BABCBH482A57HEHG55HHC">
    <div class="text-center">détaillé d'une 'card'</div>
  </div>

</div>

<br>

Durant le reste de la réunion, Lucas décrit les fonctionnalités qu'il aimerai avoir sur son site. Fonctionnalités qui sont négociées avec Matthieu, le chef de projet, en fonction du temps et de l'argent dont dispose Lucas. 

Le chef de projet rédige ensuite les fonctionnalités retenues dans le bloc "Spécifications fonctionnelles" qui se trouvent ci-dessous. 

## Spécifications fonctionnelles

Les spécifications fonctionnelles liste les fonctionnalités attendues de l'application ou du site Internet. Cette partie du document doit être compréhensible par n'importe quelle personne, qu'elle soit informaticienne ou non.

0. Lorsqu'un visiteur accède à la page Internet, il doit pouvoir voir l'ensemble des photos de Lucas
1. Sur chacune des photos, le visiteur peut lire un titre, une description ainsi qu'un mot-clé
2. Lorsque l'utilisateur clique sur une photo, la photo doit être zoomée (et ainsi permettre au visiteur de regarder en détail la photo)
3. (optionnel) si le temps le permet, l'aspect visuel de la page Internet peut être amélioré (ajout d'animations css et/ou javascript, prise en charge de l'affichage sur smartphone et sur ordinateur, ...)

## Spécifications techniques

Les spécifications techniques décrivent comment, techniquement, les développeurs doivent programmer l'application ou le site Internet afin de satisfaire les spécifications fonctionnelles. Cette partie est en général rédigée par un développeur expérimenté.

### Architecture générale 

<img src="/images/md/B7B9245795EE2EEG8CGDE9994BD7GE">

<br>
#### Répartition des rôles 


+++imageFloat /images/md/C5G49G6BHB43DA5C639H36EH9H34FF
Un <b>développeur backend</b> s'occupera du serveur qui doit fournir les données des photos
+++

+++imageFloat /images/md/FHE5DD822G3G39D7FA8E842E336F7A droite
Un <b>développeur frontend</b> s'occupera de la mise en forme de la page Internet index.html
+++