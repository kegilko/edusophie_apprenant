# Analyse d'un programme utilisant une double boucle

Ci-dessous est définie la fonction 'inconnue' : 

widget python 
def inconnue(w):
  v = w.split(' ')
  x = False
  
  for y in v:
    for z in y:
      if(z == 'a'):
        x = True
    if(x == True):
      print(y)
      x = False

phrase = input("veuillez saisir une phrase")
inconnue(phrase)

widget

Le but de cet exercice est de déterminer à travers quelques questions le but de la fonction 'inconnue' et de déterminer ensuite sa terminaison.

+++question write "Exécutez plusieurs fois le code avec différentes phrases, que fait d'après ce programme ?"

+++question write "Certaines variables utilisées dans le programme n'ont pas de nom très parlant. Analyser le code et proposez des noms plus explicites pour les variables <code>v</code>, <code>w</code>, <code>x</code>, <code>y</code> et <code>z</code>"

+++spoil "Aide 1 : à quoi sert la fonction <code>split</code> ?"

La fonction <code>split</code> permet de découper une chaîne de caractère selon un motif. Voici deux exemples d'utilisation de cette fonction : 

widget python
# exemple 1 : 
phrase = "Un processeur est un composant électronique permettant à un ordinateur d'effectuer des calculs"
mots = phrase.split(' ') # je découpe la phrase en fonction des espaces
print(mots) # affiche le contenu du tableau 'mots', chaque élément du tableau correspond à un mot de la phrase

# exemple 2 : 
loto = "42,75,23,21,66,70,7"
nombres = loto.split(',') # je découpe en fonction des virgules
print(nombres) # affiche le contenu du tableau 'nombres', chaque élément du tableau correspond à un nombre du loto
widget

spoil+++

+++spoil "Aide 2 : je ne comprend pas vraiment ce que fait la ligne 6"

Le langage python permet de boucler sur chaque lettre d'une chaîne de caractère à l'aide d'une boucle <code>for</code>. Ainsi le code suivant affiche chaque lettre contenue dans une chaîne de caractères : 

widget python
alphabet = "abcdefghijklmnopqrstuvwxyz"
for lettre in alphabet:
  print(lettre)
widget

spoil+++

+++question write "Justifiez la terminaison de ce programme"