# Préparation python pour les algorithmes de tri

Le but de cette activité préliminaire est de faire quelques quelques rappels qui vont directement servir dans la suite du cours. Vous allez également apprendre à chronométrer le temps d'exécution de vos programmes python.

+++consignes
  <ol start="0">
    <li>Créez vous un dossier du jour en exécutant chacune des lignes ci-dessous sur un terminal. <b>Attention !</b> n'oubliez pas d'adapter les lignes 2 et 3 </li>
  </ol>
  <pre><code class="language-bash"># lignes à réadapter et à exécuter dans un terminal
  nom_prenom=dupond_cedric
  dateDuJour=18_10_21

  mkdir /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
  cd /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
  touch algorithmeDeTri.py
  thonny algorithmeDeTri.py
  </code></pre>
  <ol start="1">
    <li>Parcourez l'activité et répondez aux questions</li>
  </ol>
+++

## Rappels 

Pour les questions ci-dessous, vous pouvez vous aider de votre IDE pour vérifier vos réponses

### Instructions itératives <code>for</code> et <code>while</code>

+++question write "À l'aide de l'instruction itérative <code>for</code>, écrivez le code python permettant d'afficher tous les nombres allant de 0 à 10"

+++spoil "Aide"
  vous pouvez vous aider de recherches sur Internet ou alors  +++lien "/apprenant/01-__-_NSI-_premiere/00-__-_rappels-_en-_programmation-_et-_systeme-_d--__exploitation-_Linux/01-__-_activites/00-__-_applications-_directes/les-_boucles-_while-_et-_for.md" "d'une des toutes premières activités" que nous avons fait en début d'année
spoil+++

+++question write "À l'aide de l'instruction itérative <code>for</code>, écrivez le code python permettant d'afficher tous les nombres allant de 1 à 10"

+++question write "Écrivez le code python permettant de créer une liste (ou tableau) contenant 10 entiers choisis par vous"

+++question write "À l'aide de l'instruction itérative <code>for</code>, écrivez le code python permettant d'afficher dans la console chacun des éléments de votre liste"

+++spoil "Aide"
  vous pouvez vous aider de recherches sur Internet ou alors +++lien "/apprenant/01-__-_NSI-_premiere/03-__-_parcours-_de-_tableaux---_-_introduction-_en-_algorithmique-_et-_tests/00-__-_cours/00-__-_introduction-_en-_algorithmique---_-_parcours-_de-_tableaux.md#introduction" "du premier cours" dans lequel nous avons abordé le parcours de tableaux
spoil+++

+++question write "En utilisant l'instruction itérative <code>while</code>, écrivez le code python permettant d'afficher dans la console chacun des éléments de votre liste"

### Création d'une fonction

+++question write "Écrivez en python la fonction affiche qui prend en paramètre une liste et qui affiche chaque élément de la liste"

+++spoil "Aide"
  vous pouvez vous aider de recherches sur Internet ou alors +++lien "/apprenant/01-__-_NSI-_premiere/00-__-_rappels-_en-_programmation-_et-_systeme-_d--__exploitation-_Linux/00-__-_cours/3-__-_programmation-_--_-_les-_fonctions.md" "du cours" dans lequel nous avons abordé les fonctions
spoil+++

+++question write "Écrivez un exemple d'utilisation de la fonction affiche en utilisant la liste créée à la question #2"

+++question write "En utilisant l'instruction <code>for</code>, écrivez en python la fonction taille qui prend en paramètre une liste et qui <b>retourne</b> le nombre d'éléments contenus dans la liste"

+++question write "Écrivez un exemple d'utilisation de la fonction taille en utilisant la liste créée à la question #2"

### Complexité 

+++question write "Combien d'étapes effectue la fonction taille avec une liste contenant 10 éléments ? et contenant 50 éléments ? et contenant n éléments ?"

+++question write "Quel est la complexité de la fonction taille ?"

## Chronométrer l'exécution de son code

Pour étudier l'efficacité algorithmique d'un programme, nous pouvons chronométrer le temps d'exécution de ce dernier. Divers modules python permettent cela, nous allons utiliser le module 'timeit'. En voici un exemple : 

widget python 
import timeit

start = timeit.default_timer()

# ** signifie 'puissance'
a = 68976987698686 ** 300

end = timeit.default_timer()
difference = end - start
tempsEnMilliSeconde = int(round(1000 * difference))

print("Le temps total d'exécution du programme est de " + str(tempsEnMilliSeconde) + " ms")

widget

+++question "Après avoir ajouté dans le fichier algorithmeDeTri.py la fonction taille, modifiez cette dernière afin de connaître son temps d'exécution lorsque vous exécutez votre programme"

<hr>

Le code suivant permet de générer une liste contenant 10 éléments, tous égal à 0 : 

```python
import random

liste = []
n = 10 ** 5 # correspond à 10^5 en python
for i in range(n):
    liste.append(0)

```

+++question "Ajouter le code ci-dessus dans votre programme afin de pouvoir générer par la suite des listes contenant n éléments"

+++question "Exécutez plusieurs fois votre programme en modifiant le nombre d'éléments contenus dans la liste et remplissez (<i class="emojiGenerator em em-writing_hand"></i>) le tableau ci-dessous : "

+++bulle matthieu
  <b>attention</b> ! vous devez chronométrer l'exécution de la fonction taille et pas le temps d'exécution total de votre programme !
+++

+++bulle matthieu droite
  exécutez plusieurs fois votre programme pour un n donné afin d'établir une moyenne grossière du temps d'exécution de la fonction taille
+++

<div class="col-8 mx-auto my-3">
<table class="table table-sm text-center">
  <thead>
    <tr>
      <th>n</th>
      <th>temps d'exécution de la fonction taille (en ms)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>10<sup>5</sup></td>
      <td></td>
    </tr>
    <tr>
      <td>10<sup>6</sup></td>
      <td></td>
    </tr>
    <tr>
      <td>10<sup>7</sup></td>
      <td></td>
    </tr>
    <tr>
      <td>10<sup>8</sup></td>
      <td></td>
    </tr>
    
  </tbody>
</table>
</div>

+++question write "Tracez grossièrement dans un graphe une courbe passant par les points décrits dans le tableau de la question précédente (l'abscisse du graphe est n et l'ordonnée est le temps d'exécution)"

+++question write "Est-ce que l'évolution de la courbe de votre graphe correspond bien à la complexité algorithmique trouvée à la question #10 ?"

## Réponses

<ol start="0">
  <li>
  +++spoil
widget python
for i in range(11):
  print(i)
widget
  spoil+++
  </li>
  <li>
  +++spoil
widget python
for i in range(1, 11):
  print(i)
widget
  spoil+++
  </li>
  <li>
  +++spoil
widget python
liste = [0, 1, 2, 3, 4, 5 ,6 ,7 ,8 ,9]
widget
  spoil+++
  </li>
  <li>
  +++spoil
widget python (definition de la liste:3)
for i in liste:
  print(i)
widget
  spoil+++
  </li>
  <li>
  +++spoil
widget python (definition de la liste:3)
compteur = 0
while(compteur < 10):
  print(liste[compteur])
  compteur = compteur + 1

widget
  spoil+++
  </li>
  <li>
  +++spoil
<p>Version avec for</p>

widget python
def affiche(liste):
  for i in liste:
    print(i)
widget

<p>Version avec while</p>

widget python
def affiche(liste):
  compteur = 0
  while(compteur < 10):
    print(liste[compteur])
    compteur = compteur + 1
widget

  spoil+++
  </li>
  <li>
  +++spoil

widget python (définition de la liste:3) (définition de la fonction affiche:7)
affiche(liste)
widget

  spoil+++
  </li>
  <li>
  +++spoil
widget python
def taille(liste):
  compteur = 0
  for i in liste:
    compteur = compteur + 1
  return compteur
widget
  spoil+++
  </li>
  <li>
  +++spoil
widget python (définition de la liste:3) (définition de la fonction taille:9)
print(taille(liste))

# ou alors, de manière plus verbeuse
# taille = taille(liste)
# print(taille)
widget
  spoil+++
  </li>
  <li>
  +++spoil
<ul>
  <li>Si le nombre d'élément de la liste est égal à 10, la fonction taille effectuera 1 + 10 x 2 étapes : 
  
  <table class="table table-sm table-borderless text-green mt-2 mb-3">
    <tbody>
      <tr>
        <td colspan="2">1 +</td>
        <td>(affectation)</td>
      </tr>
      <tr>
        <td colspan="2">10 multiplié par</td>
        <td>(car la boucle for se répète autant de fois qu'il y a d'élément dans la liste)</td>
      </tr>
      <tr>
        <td></td>
        <td>2</td>
        <td>(un calcul <b>puis</b> une affectation)</td>
      </tr>
    </tbody>
  </table>
  </li>
  
  <li>Si le nombre d'élément de la liste est égal à 50, la fonction taille effectuera 1 + 50 x 2 étapes</li>
  <li>Si le nombre d'élément de la liste est egal à n, la fonction taille effectuera 1 + n x 2 étapes</li>
</ul>
  spoil+++
  </li>
  <li>
  +++spoil
La complexité de l'algorithme de la fonction taille est : 
<ul>
  <li>O(1 + n x 2), qui est égal à O(n) car en algorithmique nous  nous soucions seulement de l'ordre de grandeur.</li>
</ul>
La fonction taille décrit donc un algorithme de complexité linéaire
  spoil+++
  </li>
  <li>
  +++spoil
Le programme doit rassembler à ceci : 

widget python
import timeit
    
def taille(liste):
  start = timeit.default_timer()
  compteur = 0
  for i in liste:
    compteur = compteur + 1
  end = timeit.default_timer()
  difference = end - start
  tempsEnMilliSeconde = int(round(1000 * difference))
  print("Le temps total d'exécution de la fonction taille est de " + str(tempsEnMilliSeconde) + " ms")

  return compteur

liste = [0, 1, 2, 3, 4, 5 ,6 ,7 ,8 ,9]

print(taille(liste))
widget
  spoil+++
  </li>
  <li>
  +++spoil
widget python
import timeit
    
def taille(liste):
  start = timeit.default_timer()
  compteur = 0
  for i in liste:
    compteur = compteur + 1
  end = timeit.default_timer()
  difference = end - start
  tempsEnMilliSeconde = int(round(1000 * difference))
  print("Le temps total d'exécution de la fonction taille est de " + str(tempsEnMilliSeconde) + " ms")
  
  return compteur

liste = []
n = 10 ** 5
for i in range(n):
    liste.append(0)

print(taille(liste))
widget
  spoil+++
  </li>
  <li>
  +++spoil
Voici un exemple de temps d'exécutions moyens relevés depuis un ordinateur portable : 

<div class="col-8 mx-auto my-3">
<table class="table table-sm text-center">
  <thead>
    <tr>
      <th>n</th>
      <th>temps d'exécution de la fonction taille (en ms)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>10<sup>5</sup></td>
      <td>5</td>
    </tr>
    <tr>
      <td>10<sup>6</sup></td>
      <td>47</td>
    </tr>
    <tr>
      <td>10<sup>7</sup></td>
      <td>490</td>
    </tr>
    <tr>
      <td>10<sup>8</sup></td>
      <td>4759</td>
    </tr>
    
  </tbody>
</table>
</div>

  spoil+++
  </li>
  <li>
  +++spoil
En utilisant +++lien "https://www.geogebra.org/graphing" "l'outil suivant", voici à quoi ressemble la courbe que j'obtiens : 

+++image "/images/md/HHD528BGGAD45EBAHCE8345F7528A6"

  spoil+++
  </li>
  <li>
  +++spoil
La courbe décrite par les différentes mesures évolue de manière linéaire. Ce comportement correspond bien à la complexité de l'algorithme de la fonction taille établit à la question #10  
  spoil+++
  </li>
</ol>
