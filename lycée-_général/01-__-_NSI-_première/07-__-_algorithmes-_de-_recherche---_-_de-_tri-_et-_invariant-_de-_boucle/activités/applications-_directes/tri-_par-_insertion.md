# Tri par insertion

+++consignes
  <ol start="0">
    <li>Levez vous et répondez avec votre groupe à la première question de l'activité en vous aidant des cartes</li>
    <li>Répondez aux autres questions à l'aide d'un ordinateur. L'entre aide entre membres du groupe est bien sur toujours encouragée</li>
  </ol>
+++

## Prise en main de l'algorithme

Le tri par insertion peut être implémenté dans la plupart des langages de programmation. Voici le pseudo code de cet algorithme mis à disposition +++lien "https://fr.wikipedia.org/wiki/Tri_par_insertion" "par la page wikipédia 'Tri par insertion'" (et très légèrement amélioré) : 

<pre class="p-2" style="background-color:#eeeeee">0.  fonction tri_insertion(tableau T)
1.    POUR i allant de 1 à taille(T) - 1 (compris)
2.        # mémoriser T[i] dans n
3.        n ← T[i]                            
4.        # décaler les éléments T[0]..T[i-1] qui sont plus grands que n, en partant de T[i-1]
5.        j ← i                               
6.        TANT QUE j > 0 et T[j - 1] > n
7.            T[j] ← T[j - 1]
8.            j ← j - 1
9.        fin TANT QUE
10.       # placer n dans le "trou" laissé par le décalage
11.       T[j] ← n
12.    fin POUR
13.    retourner T
14.  fin fonction
</pre>


Voici quelques ressources Web qui peuvent vous aider à comprendre comment appliquer cet algorithme 

<b>Une animation :</b>

+++image "/images/md/DC7D33D4G858AAA5754G4B47ADB2A6" col-8 mx-auto

<b>Une vidéo de 'danse algorithmique':</b> 

+++video "https://www.youtube.com/watch?v=ROalU379l3U" "Insert-sort with Romanian folk dance"

+++question "Reproduisez l'algorithme du tri par insertion avec un paquet de cartes mélangées"

<hr>

Pour rappel la fonction len, fournie de base par python, permet d'obtenir le nombre d'élément dans un tableau.

+++question "Implémentez en python l'algorithme du tri par insertion. Vérifiez que le résultat du programme python donne le même résultat que l'application de l'algorithme avec un jeu de cartes"

## Étude de la complexité

Tout comme l'algorithme de tri par sélection, le tri par insertion contient deux boucles imbriquées l'une dans l'autre. Nous allons donc de nouveau nous focaliser sur le nombre total de tour de boucle de l'algorithme.

Dans le pseudo-code, <b class="text-red">la première boucle</b> démarre à la ligne 1 et <b class="text-green">la seconde boucle</b> à la ligne 6.

<pre class="p-2" style="background-color:#eeeeee">0.  procédure tri_insertion(tableau T)
1.    <b class="text-red">pour i de 1 à taille(T) - 1 (compris)</b>
2.        # mémoriser T[i] dans n
3.        n ← T[i]                            
4.        # décaler les éléments T[0]..T[i-1] qui sont plus grands que n, en partant de T[i-1]
5.        j ← i                               
6.        <b class="text-green">tant que j > 0 et T[j - 1] > n</b>
7.            T[j] ← T[j - 1]
8.            j ← j - 1
9.        # placer n dans le "trou" laissé par le décalage
10.       T[j] ← n
11.   retourner T
12. fin procédure
</pre>

Dans l'algorithme, n correspond au nombre total de cartes qu'il y a dans votre paquet.

+++question write "Supposons que le paquet contient 5 cartes au total, combien de tour va effectuer <b class="text-red">la première boucle</b> ?"

+++question write "Donnez un exemple de paquet de 5 cartes avec lequel l'algorithme sera dans le pire des cas d'exécution possibles"

+++bulle matthieu
  aidez vous d'un paquet de 5 cartes, et essayez de trouver l'ordre initial de carte qui fera exécuter le plus d'étape possible à l'algorithme
+++

<hr>

+++question write "En supposant que le paquet contient toujours 5 cartes au total, pour chaque tour de <b class="text-red">la première boucle</b>, combien de tour effectue <b>dans le pire des cas</b> <b class="text-green">la seconde boucle</b> en fonction de n ? Complétez le tableau ci-dessous :"

<table class="table table-sm text-center">
  <thead>
    <tr>
      <th>Tour de <b class="text-red">la première boucle</b>, valeur de <b class="text-red">i</b></th>
      <th>Nombre de tours effectués par <b class="text-green">la seconde boucle</b> <u>dans le pire des cas</u></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td></td>
    </tr>
    <tr>
      <td>2</td>
      <td></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td></td>
    </tr>
  </tbody>
</table>
  

+++question write "Soit n le nombre de cartes dans le paquet, combien de tour va effectuer <b class="text-red">la première boucle</b> en fonction de n ?"

+++question write "En supposant que le paquet contient toujours n cartes au total, <b>pour chaque tour</b> de <b class="text-red"> la première boucle</b>, combien de tour effectue <b class="text-green">la seconde boucle</b> ? Complétez le tableau ci-dessous :"

<table class="table table-sm text-center">
  <thead>
    <tr>
      <th>Tour de <b class="text-red">la première boucle</b>, valeur de <b class="text-red">i</b></th>
      <th>Nombre de tours effectués par <b class="text-green">la seconde boucle</b></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>2</td>
      <td>2</td>
    </tr>
    <tr>
      <td>3</td>
      <td>3</td>
    </tr>
    <tr>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <td>n - 4</td>
      <td></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td> </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td> </td>
    </tr>
  </tbody>
</table>

+++question write "En déduire que le nombre <b>total</b> de tours de la seconde boucle est égal à : ( ( n - 1 ) * n ) /2"

+++question write "En déduire que la complexité de l'algorithme est en O(n²)"

### Représentation graphique de la complexité

+++question "Améliorer le programme python afin de chronométrer le temps d'exécution de la fonction de tri"

+++bulle matthieu
  pour vous aider, +++lien "preparation-_python-_pour-_les-_algorithmes-_de-_tri.md" "voici le lien" vers l'activité dans laquelle nous nous sommes intéressés au chronométrage du temps d'exécution de code python
+++

<hr>

La fonction ci-dessous, qui va servir par la suite, permet de générer une liste de n entiers aléatoires compris entre 0 et 100

```python
import random

def genererListeEntiers(n):
  liste = []
  for i in range(n):
    liste.append(random.randint(0, 100))
  return liste
```

+++question "Ajouter la fonction genererListeEntiers dans votre programme afin de pouvoir générer par la suite des listes contenant n éléments aléatoires"

+++question "Complétez le tableau ci-dessous"

+++bulle matthieu
  <b>attention !</b> si dans votre programme vous affichiez toutes les cartes, enlevez de votre code ces affichages (les fonctions print) car la nombre de cartes va être très important et l'affichage va alors saturer votre IDE et par conséquent votre ordinateur
+++

<div class="col-8 mx-auto my-3">
<table class="table table-sm text-center">
  <thead>
    <tr>
      <th>n</th>
      <th>temps d'exécution de la fonction de tri (en ms)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>300</td>
      <td></td>
    </tr>
    <tr>
      <td>3000</td>
      <td></td>
    </tr>
    <tr>
      <td>30000</td>
      <td></td>
    </tr>
  </tbody>
</table>
</div>

+++question write "Tracez grossièrement dans un graphe une courbe passant par les points décrits dans le tableau de la question précédente (l'abscisse du graphe est n et l'ordonnée est le temps d'exécution)"

+++question write "Est-ce que l'évolution de la courbe de votre graphe correspond bien à la complexité algorithmique donnée à la question #8 ?"

## Réponses

<ol start="1">
  <li>
  +++spoil
widget python
def tri_insertion(T):
  for i in range(1, len(T)):
      n = T[i]
      j = i
      while(j > 0 and T[j-1] > n):
          T[j] = T[j-1]
          j = j - 1
      T[j] = n
  return T

cartes = [4, 7, 0, 3, 3]
cartes = tri_insertion(cartes)
print(cartes)
widget
  spoil+++
  </li>
  <li>
  +++spoil
La première boucle démarre à i = 1 et se termine à i = n - 1, où n est égal à 5. Donc i prendra les valeurs 1 2 3 et 4. La première boucle effectue donc 4 tours
  spoil+++
  </li>
  <li>
  +++spoil
Il faut ranger les cartes par ordre décroissant. Voici un exemple de paquet initial:

  10 9 8 7 6 

+++bulle matthieu
  rien ne vaut la pratique, pour comprendre cette réponse, prenez 5 cartes dans cet ordre et appliquez l'algorithme du tri par insertion
+++

+++bulle matthieu droite
  vous verrez qu'on ne peut pas trouver pire configuration pour cet algorithme
+++

  spoil+++
  </li>
  
  <li>
  +++spoil
<table class="table table-sm text-center">
  <thead>
    <tr>
      <th>Tour de <b class="text-red">la première boucle</b>, valeur de <b class="text-red">i</b></th>
      <th>Nombre de tours effectués par <b class="text-green">la seconde boucle</b> <u>dans le pire des cas</u></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>2</td>
      <td>2</td>
    </tr>
    <tr>
      <td>3</td>
      <td>3</td>
    </tr>
    <tr>
      <td>4</td>
      <td>4</td>
    </tr>
  </tbody>
</table>
  spoil+++
  </li>
  
  <li>
  +++spoil
La première boucle démarre à i = 1 et se termine à i = n - 1. Donc i prendra les valeurs 1 2 ... n-3 n-2 n-1. La première boucle effectue donc n-1 tours
  spoil+++
  </li>
  
  <li>
    +++spoil
<table class="table table-sm text-center">
  <thead>
    <tr>
      <th>Tour de <b class="text-red">la première boucle</b>, valeur de <b class="text-red">i</b></th>
      <th>Nombre de tours effectués par <b class="text-green">la seconde boucle</b></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>2</td>
      <td>2</td>
    </tr>
    <tr>
      <td>3</td>
      <td>3</td>
    </tr>
    <tr>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <td>n - 3</td>
      <td>n - 3</td>
    </tr>
    <tr>
      <td>n - 2</td>
      <td>n - 2</td>
    </tr>
    <tr>
      <td>n - 1</td>
      <td>n - 1</td>
    </tr>
  </tbody>
</table>
    spoil+++
  </li>
  <li>
    +++spoil

<ul>
  <li>le nombre de tours de <b class="text-red">la première boucle</b> est : <b class="text-blue">n-1</b></li>
  <li>à travers tous les tours de <b class="text-red">la première boucle</b>, <b class="text-green">la deuxième boucle</b> s'exécutera un nombre total de fois de : 1 + 2 + 3 + ... + (n-3) + (n-2) + (n-1)</li>
  <li>donc, le nombre total de tours de la seconde boucle est de : <ul>
    <li class="text-black">( <b class="text-blue">( n - 1 )</b> x <b class="text-green">n</b> ) / 2</li>
  </ul></li>
</ul>

+++bulle matthieu
  une version très proche, et plus détaillée, de ce calcul se trouve dans la réponse #7 de l'activité "<a href="tri-_par-_selection.md" target="_blank">tri par sélection</a>" 
+++
    spoil+++
  </li>
  
  <li>
    +++spoil
<p>Développons la formule : ( ( n - 1 ) * n ) /2</p>
<p>Nous obtenons alors :  n² / 2 - n / 2</p>

<p>En algorithmique, nous ne nous intéressons qu'aux ordres de grandeur. Si n atteint une valeur infiniment grande, la partie la plus importante dans la formule est n² (le reste devient négligeable).</p>

<p>L'algorithme du tri par insertion est donc de complexité <b>O(n²)</b></p>

+++bulle matthieu
  on dit que l'algorithme est de complexité <b>quadratique</b>
+++
    spoil+++
  </li>

  <li>
    +++spoil
widget python
import timeit

def tri_insertion(T):
    for i in range(1, len(T)):
        n = T[i]
        j = i
        while(j > 0 and T[j-1] > n):
            T[j] = T[j-1]
            j = j - 1
        T[j] = n
    return T


start = timeit.default_timer()

cartes = [4, 7, 0, 3, 3]
cartes = tri_insertion(cartes)

end = timeit.default_timer()
difference = end - start
tempsEnMilliSeconde = int(round(1000 * difference))
print("Le temps total d'exécution de la fonction de tri est de " + str(tempsEnMilliSeconde) + " ms")
widget
    spoil+++
  </li>

  <li>
    +++spoil
widget python
import timeit
import random

def genererListeEntiers(n):
  liste = []
  for i in range(n):
    liste.append(random.randint(0, 100))
  return liste

def tri_insertion(T):
    for i in range(1, len(T)):
        n = T[i]
        j = i
        while(j > 0 and T[j-1] > n):
            T[j] = T[j-1]
            j = j - 1
        T[j] = n
    return T


start = timeit.default_timer()

cartes = genererListeEntiers(10)
cartes = tri_insertion(cartes)

end = timeit.default_timer()
difference = end - start
tempsEnMilliSeconde = int(round(1000 * difference))
print("Le temps total d'exécution de la fonction de tri est de " + str(tempsEnMilliSeconde) + " ms")
widget
    spoil+++
  </li>
  
  <li>
    +++spoil
Voici un exemple de temps d'exécutions moyens relevés depuis un ordinateur portable

<div class="col-8 mx-auto my-3">
<table class="table table-sm text-center">
  <thead>
    <tr>
      <th>n</th>
      <th>temps d'exécution de la fonction de tri (en ms)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>300</td>
      <td>5</td>
    </tr>
    <tr>
      <td>3000</td>
      <td>651</td>
    </tr>
    <tr>
      <td>30000</td>
      <td>58259</td>
    </tr>
  </tbody>
</table>
</div>
    spoil+++
  </li>
  
  <li>
  +++spoil
En utilisant +++lien "https://www.geogebra.org/graphing" "l'outil suivant", voici à quoi ressemble la courbe que j'obtiens : 

+++image "/images/md/8AG6D3CHB3662D5267A3C3E3292EG8"

  spoil+++
  </li>
  <li>
  +++spoil
La courbe décrite par les différentes mesures évolue de manière très semblable à la fonction x². Ce comportement correspond bien à la complexité de l'algorithme de la fonction taille établit à la question #8
  spoil+++
  </li>
  
</ol>