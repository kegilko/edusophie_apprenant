# Algorithmes min, max et moyenne

+++consignes
  <ol start="0">
    <li>Créez vous un dossier du jour en exécutant chacune des lignes ci-dessous sur un terminal. <b>Attention !</b> n'oubliez pas d'adapter les lignes 2 et 3 </li>
  </ol>
  <pre><code class="language-bash"># lignes à réadapter et à exécuter dans un terminal
  nom_prenom=dupond_cedric
  dateDuJour=18_10_21

  mkdir /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
  cd /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
  touch algo.py
  thonny algo.py
  </code></pre>
  <ol start="1">
    <li>Choisissez un des trois algorithmes : min, max ou moyenne</li>
    <li>Répondez aux questions de l'algorithme choisi</li>
    <li>En fin de séance, des personnes à tour de rôle présenterons les différents algorithmes</li>
  </ol>
+++


+++sommaire

## Algorithme 'min'

L'algorithme min permet de trouver la valeur la plus petite dans un ensemble de valeur données. Ci-dessous, l'algorithme min écrit en pseudo-code : 

```text
tableau ← [3, 6, 2, 8, 7, 2, 2, 1, 16, 8]
min ← tableau[0]

Pour chaque nombre dans tableau
  Si min > nombre 
    min = nombre
  Fin Si
Fin Pour

Afficher min
```
+++question write "Si le tableau contient n éléments aléatoires, combien de tour de boucle fera l'instruction Pour ?"

+++question write "En fonction de n, combien d'étapes <b>au maximum</b> (le pire des cas) sont effectuées par l'algorithme ?"

+++question write "Pourquoi l'algorithme est en O(n) ?"

+++question "Programmez l'algorithme en python et testez le"

+++question "Programmez l'algorithme en javascript ou en bash"

+++spoil "Aide javascript 1"
  Pour rappel : pour écrire un programme en javascript, vous pouvez utiliser un éditeur de texte simple en écrivant dans une balise \<script>...\</script>. Enregistrez votre fichier avec l'extension .html et ouvrez le avec un navigateur Web
spoil+++

+++spoil "Aide javascript 2"
  Aidez vous si besoin de <code>console.log()</code> pour afficher des messages dans la console du navigateur
spoil+++

+++spoil "Aide bash"
  Vous devez créer un fichier avec l'extension .sh (via par exemple la commande <code>touch algo.sh</code>  ). Ouvrez ensuite le fichier avec gedit par exemple et écrivez votre programme en bash dans le fichier. Vous devrez sans doute vous aider d'Internet pour savoir comment créer une variable en bash ou encore utiliser une boucle. Pour tester votre programme bash, il suffit depuis le terminal d'exécuter la commande <code>bash algo.sh</code>
spoil+++

## Algorithme 'max'

L'algorithme min permet de trouver la valeur la plus grande dans un ensemble de valeur données. Ci-dessous, l'algorithme max écrit en pseudo-code : 

```text
tableau ← [3, 34, 2, 3, 43, 12 ,10, 5, 87, 6]
max ← tableau[0]

Pour chaque nombre dans tableau
  Si max < nombre 
    max = nombre
  Fin Si
Fin Pour

Afficher max
```
+++question write "Si le tableau contient n éléments aléatoires, combien de tour de boucle fera l'instruction Pour ?"

+++question write "En fonction de n, combien d'étapes <b>au maximum</b> (le pire des cas) sont effectuées par l'algorithme ?"

+++question write "Pourquoi l'algorithme est en O(n) ?"

+++question "Programmez l'algorithme en python et testez le"

+++question "Programmez l'algorithme en javascript ou en bash"

+++spoil "Aide javascript 1"
  Pour rappel : pour écrire un programme en javascript, vous pouvez utiliser un éditeur de texte simple en écrivant dans une balise \<script>...\</script>. Enregistrez votre fichier avec l'extension .html et ouvrez le avec un navigateur Web
spoil+++

+++spoil "Aide javascript 2"
  Aidez vous si besoin de <code>console.log()</code> pour afficher des messages dans la console du navigateur
spoil+++

+++spoil "Aide bash"
  Vous devez créer un fichier avec l'extension .sh (via par exemple la commande <code>touch algo.sh</code>  ). Ouvrez ensuite le fichier avec gedit par exemple et écrivez votre programme en bash dans le fichier. Vous devrez sans doute vous aider d'Internet pour savoir comment créer une variable en bash ou encore utiliser une boucle. Pour tester votre programme bash, il suffit depuis le terminal d'exécuter la commande <code>bash algo.sh</code>
spoil+++

## Algorithme 'moyenne'

L'algorithme moyenne permet de calculer la moyenne d'un ensemble de valeur. Ci-dessous, l'algorithme moyenne écrit en pseudo-code : 

```text
tableau ← [12, 2, 4, 6, 6, 30, 12, 5, 22, 17]
somme ← 0
nbr_elements ← 0

Pour chaque nombre dans tableau
  somme ← somme + nombre
  nbr_elements ← nbr_elements + 1
Fin Pour

moyenne ← somme / nbr_elements

Afficher moyenne
```

+++question write "Si le tableau contient n éléments aléatoires, combien de tour de boucle fera l'instruction Pour ?"

+++question write "En fonction de n, combien d'étapes sont effectuées par l'algorithme ?"

+++question write "Pourquoi l'algorithme est en O(n) ?"

+++question "Programmez l'algorithme en python et testez le"

+++question "Programmez l'algorithme en javascript ou en bash"

+++spoil "Aide javascript 1"
  Pour rappel : pour écrire un programme en javascript, vous pouvez utiliser un éditeur de texte simple en écrivant dans une balise \<script>...\</script>. Enregistrez votre fichier avec l'extension .html et ouvrez le avec un navigateur Web
spoil+++

+++spoil "Aide javascript 2"
  Aidez vous si besoin de <code>console.log()</code> pour afficher des messages dans la console du navigateur
spoil+++

+++spoil "Aide bash"
  Vous devez créer un fichier avec l'extension .sh (via par exemple la commande <code>touch algo.sh</code>  ). Ouvrez ensuite le fichier avec gedit par exemple et écrivez votre programme en bash dans le fichier. Vous devrez sans doute vous aider d'Internet pour savoir comment créer une variable en bash ou encore utiliser une boucle. Pour tester votre programme bash, il suffit depuis le terminal d'exécuter la commande <code>bash algo.sh</code>
spoil+++

## Réponses

Questions 0, 5 et 10 :

+++spoil
L'instruction Pour boucle autant de fois qu'il y a d'élément dans la variable tableau. La boucle fera donc n tours de boucle, n étant le nombre d'élément du tableau
spoil+++

Questions 1, 6 et 11 :

+++todo prendre en compte les affectations posent soucis dans la complexité (faut il prendre en compte les affectations faites par la boucle for par exemple ?). Il serait peut être judicieux de ne compter que les comparaison et les calcul, a voir avec le reste de l'année !

+++spoil
<br><p class="mt-4 mb-2"><b>Algorithmes min et max :</b></p>

Avant la boucle Pour se trouvent deux affectations. Dans la boucle Pour il y a une condition et dedans éventuellement (si la condition est vrai) une affectation. On compte le nombre d'étapes maximum qu'il peut y avoir (le pire des cas) en fonction de la taille du tableau (n), nous obtenons donc : 2 + n * 2

<br><p class="mt-4 mb-2"><b>Algorithme moyenne :</b></p>

Avant la boucle Pour se trouvent deux affectations. Dans la boucle Pour il y a un calcul puis une affection et de nouveau un calcul plus une affectation. Le nombre d'étapes qu'il y a en fonction de la taille du tableau (n) est : 2 + n * 4

spoil+++

Questions 2, 7 et 12 :

+++spoil

<br><p class="mt-4 mb-2"><b>Algorithmes min et max :</b></p>

Le nombre d'étapes dans l'algorithme est de 2 + n * 1 . Cette formule correspond à une fonction linéaire et augmente donc de manière linéaire en fonction de n. L'algorithme est donc de complexité linéaire, ce qui correspond à O(n)

<br><p class="mt-4 mb-2"><b>Algorithme moyenne :</b></p>

Le nombre d'étapes dans l'algorithme est de 2 + n * 4 . Cette formule correspond à une fonction linéaire et augmente donc de manière linéaire en fonction de n. L'algorithme est donc de complexité linéaire, ce qui correspond à O(n)

spoil+++

Questions 3, 8 et 13 :

+++spoil

<br><p class="mt-4 mb-2"><b>Algorithme min :</b></p>

widget python
tableau = [3, 6, 2, 8, 7, 2, 2, 1, 16, 8]
min = tableau[0]

for nombre in tableau:
    if(min > nombre):
        min = nombre

print(min)
widget

<br><p class="mt-4 mb-2"><b>Algorithme max :</b></p>

widget python
tableau = [3, 34, 2, 3, 43, 12 ,10, 5, 87, 6]
max = tableau[0]

for nombre in tableau:
    if(max < nombre):
        max = nombre

print(max)
widget 

<br><p class="mt-4 mb-2"><b>Algorithme moyenne :</b></p>

widget python
tableau = [12, 2, 4, 6, 6, 30, 12, 5, 22, 17]
somme = 0
nbr_elements = 0

for nombre in tableau:
    somme += nombre
    nbr_elements += 1

moyenne = somme / nbr_elements

print(moyenne)
widget

spoil+++

Questions 4, 9 et 14 :

<br><p class="mt-4 mb-2"><b>Algorithmes min :</b></p>

version javascript : 

<pre>\<script>
let tableau = [3, 6, 2, 8, 7, 2, 2, 1, 16, 8]
min = tableau[0]

for (const nombre of tableau){
	if(min > nombre){
		min = nombre
	}
}

console.log(min)
\</script>
</pre>

<br><p class="mt-4 mb-2"><b>Algorithmes max :</b></p>

version javascript : 

<pre>\<script>
let tableau = [3, 34, 2, 3, 43, 12 ,10, 5, 87, 6]
max = tableau[0]

for (const nombre of tableau){
	if(max < nombre){
		max = nombre
	}
}

console.log(max)
\</script>
</pre>

<br><p class="mt-4 mb-2"><b>Algorithmes moyenne :</b></p>

version javascript : 

<pre>\<script>
let tableau = [12, 2, 4, 6, 6, 30, 12, 5, 22, 17]
somme = 0
nbr_elements = 0

for (const nombre of tableau){
	somme = somme + nombre
	nbr_elements = nbr_elements + 1
}

moyenne = somme / nbr_elements

console.log(moyenne)
\</script>
</pre>