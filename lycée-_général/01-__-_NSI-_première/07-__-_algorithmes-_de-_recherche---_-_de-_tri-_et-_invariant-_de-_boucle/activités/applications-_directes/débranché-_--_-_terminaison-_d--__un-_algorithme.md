# Terminaison d'un algorithme

+++consigne
  Sortez votre crayon et une feuille et laissez vous guider par l'activité !
+++

## Algorithme de tri par insertion

En programmation, certaines boucles s'exécuter indéfiniment. La plupart du temps, cet effet n'est pas voulu. Déterminons à travers quelques étapes si l'algorithme du tri par sélection exécute un nombre fini d'instructions.

<pre class="p-2" style="background-color:#eeeeee">0.  procédure tri_insertion(tableau T)
1.    <b class="text-red">pour i de 1 à taille(T) - 1 (compris)</b>
2.        # mémoriser T[i] dans n
3.        n ← T[i]                            
4.        # décaler les éléments T[0]..T[i-1] qui sont plus grands que n, en partant de T[i-1]
5.        j ← i                               
6.        <b class="text-green">tant que j > 0 et T[j - 1] > n</b>
7.            T[j] ← T[j - 1]
8.            j ← j - 1
9.        # placer n dans le "trou" laissé par le décalage
10.       T[j] ← n
11.   retourner T
12. fin procédure
</pre>

Il faut alors analyser les boucles contenues dans l'algorithme, il y en a 2 : 
- Une boucle 'pour' qui démarre à la ligne 1
- Une boucle 'tant que' qui démarre à la ligne 6

Nous savons que, de fait, une boucle pour s'exécutera toujours un nombre fini de fois. Nous pouvons donc dès maintenant écarter la première boucle de notre analyse. 

La deuxième boucle, qui est une boucle conditionnelle, peut par contre s'exécuter indéfiniment.

+++question write "Quels sont les variants utilisés dans la boucle 'tant que' ? "

+++question write "Est-ce que ces variants changent de valeurs à chaque tour de boucle ?"

+++question write "Justifier que tôt ou tard, la condition de la boucle 'tant que' sera fausse, et que donc, la boucle arrêtera de s'exécuter"

## Algorithme de tri par sélection

<pre class="p-2" style="background-color:#eeeeee">0.  procédure tri_selection(tableau t)
1.     n ← longueur(t) 
2.     <b class="text-red">pour i de 0 à n - 2 (compris)</b>
3.         min ← i       
4.         <b class="text-green">pour j de i + 1 à n - 1 (compris)</b>
5.             si t[j] < t[min], alors min ← j
6.         fin pour
7.         si min ≠ i, alors échanger t[i] et t[min]
8.     fin pour
9.     retourner t
10.  fin procédure
</pre>

+++question write "justifiez la terminaison de l'algorithme de tri par sélection"

## Autres programmes

Voici deux programmes dont on prétend qu'ils calculent le nombre de chiffres d'un nombre positif saisi par l'utilisateur : 

**Programme 1**

```python
a = int(input("veuillez saisir un nombre"))
i = 0
while( a > 0):
  a = a // 10 # // est une division entière, par exemple 10 // 3 = 3
  i += 1
print(i)
```

**Programme 2**

```python
a = int(input("veuillez saisir un nombre"))
k = 1
i = 0
while( k < a + 1 ):
  k = k * 10
  i += 1
print(i)
```

+++question write "Justifiez la terminaison de ces deux programmes en utilisant la technique du variant"

+++question write "Que se passe-t-il si on remplace dans le premier programme a > 0 par a != 0 ?"

+++question write "Que se passe-t-il si on remplace dans le second programme k < a + 1 par k != a + 1 ?"