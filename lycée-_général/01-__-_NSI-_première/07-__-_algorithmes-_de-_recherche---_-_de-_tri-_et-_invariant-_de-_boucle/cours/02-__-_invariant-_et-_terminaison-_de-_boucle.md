#Invariant et terminaison de boucle

+++programme
  Décrire un invariant de boucle qui prouve la correction des tris par insertion, par sélection
  
  Prouver qu'un programme produit bien le résultat qu'on attend de lui
  Détecter les boucles infinies dans un programme
  Justifier qu'un programme se termine en un nombre fini d'étape
+++

+++sommaire

## Introduction

Lors de la conception/programmation d'un algorithme, certaines questions sont récurrentes : 
- est-ce que je suis certain qu'il me renverra toujours le bon résultat quelques soit les données utilisées ? 
- est-ce que les variables utilisées par le programme sont correctement initialisées ? 
- est-ce que le nombre de boucle est bon ? 
- est-ce que le programme se termine bien ? (il n'y a pas de boucle infini)

+++bulle matthieu
  concevoir un algorithme (et créer un programme par la suite) peut présenter deux principaux problèmes
+++

+++bulle matthieu droite
  le programme peut produire un résultat inattendu / incorrect, le programme s'exécute de manière infini sans jamais s'arrêter
+++

Dans le cas d'un algorithme simple, sans boucle, il est aisé de déterminer si ce dernier est valide ( = correct) et se termine bien : 

widget python
a = input('veuillez entrer a')
b = input('veuillez entrer b')

somme = int(a) + int(b)
print(a + " + " + b + " = " + str(somme))
widget

+++bulle matthieu
  que fait le programme suivant ?
+++

+++bulle matthieu droite
  est-ce ce programme s'exécute un nombre fini de fois ? produit il le résultat attendu quelque soit les données utilisées ? 
+++

+++spoil
  Le but du programme semble être de calculer la <b>somme</b> entre deux nombres. Sans boucle, le programme exécute les instructions de haut en bas et se termine. Le programme cependant ne produit pas forcément le résultat attendu si l'utilisateur saisit du texte pour les valeurs des variables a et b.
spoil+++

Déterminer si un algorithme est valide ( = correct) et qu'il se termine bien se complique lorsque ce dernier utilise une boucle. Pour prouver la validité et la terminaison, nous disposons de la notion d'invariant de boucle

## Notion d'invariant de boucle

Un invariant de boucle est une propriété qui est vraie :
0. **avant l'entrée** dans une boucle (**initialisation**)
1. **à chaque passage** dans cette boucle (**hérédité**)
2. et **à la sortie** de cette boucle (**conclusion**)

### Cas pratique

Voici un algorithme de calcul avec une boucle conditionnelle et deux variables a et b, a étant un entier naturel : 

```text
a ← un entier naturel
b ← un entier naturel

m ← 0
p ← 0

tant que m < a 
  m ← m + 1
  p ← p + b
fin du tant que

afficher p
```

+++bulle matthieu
  exécutons cet algorithme au tableau ! quel est le but de cet algorithme ?
+++

+++spoil
  cet algorithme calcule le résultat de la multiplication entre a et b.
spoil+++

<hr>

nous allons vérifier si la propriété "p = m * b" est un invariant de la boucle "tant que".

#### **Étape 0 "initialisation"**

Dans cette étape, on doit montrer que la proposition est vraie avant la première itération de la boucle

+++bulle matthieu
  avant d'entrer dans la boucle "tant que", est-ce que p = m * b ?
+++

+++spoil
  Oui, car m = 0 et p = 0, donc p = m * b est vraie
spoil+++

<hr>

#### **Étape 1 "hérédité"**

Dans cette étape, on doit montrer que si la proposition est vraie dans un passage de la boucle, elle reste vraie au passage suivant de la boucle. 

Donc, <b class="text-blue">supposons que p = m * b est vrai</b> après un passage dans la boucle. Lors du passage suivant dans la boucle : 
- m' = m + 1
- p' = p + b

+++bulle matthieu
  est-ce que p' = m' x b ? dit autrement, est-ce que l'invariant est toujours vrai avec les nouvelles valeurs de m et de p ?
+++

+++spoil

<b class="text-blue">Nous avons supposé que p = m * b est vraie</b>, donc : 

<ul>
  <li>m' = m + 1</li>
  <li><u>p'</u> = p + b = m * b + b = ( m + 1 ) x b = <u>m' * b</u> </li>
</ul>

<p>Donc à partir de la supposition '<b class="text-blue">p = m * b est vraie</b>' nous avons démontré qu'au tour de boucle suivant, <u>p' = m' * b</u></p>
spoil+++

<hr>

#### **Étape 2 "conclusion"**

<p>p = m * b est vrai avant et pendant la boucle. Nous pouvons en conclure que la proposition 'p = m * b' est un invariant de la boucle "tant que" de notre algorithme. Et puisqu'à la sortie la variable m est égale à la valeur de la variable a, alors nous obtenons bien le produit p = a * b. L'algorithme est donc valide.</p>

<hr>

+++activité binome 30 ../activites/applications-_directes/debranche-_--_-_validite-_d--__un-_algorithme.md "Validité d'un algorithme"

## +++emoji-write Résumons

- Un algorithme est considéré comme **valide (ou correct)** lorsqu'il **produit le résultat attendu** quelques soient les données fournies en entrée
- Dans le cas où l'algorithme contient au moins une boucle conditionnelle, nous devons nous assurer que chaque boucle produit bien le résultat attendu
- Nous déterminons pour chaque boucle un invariant de boucle
    - nous déterminons une propriété qui est une expression soit vraie soit fausse
    - si cette propriété est vraie avant et pendant la boucle , alors cette propriété est un invariant de boucle. Cela permet de prouver que la boucle produira toujours le résultat qu'on attends d'elle

## Terminaison d'un algorithme

Lors de la conception d'un algorithme, puis d'un programme, nous devons nous assurer que ce dernier ne provoquera pas de boucle infini non voulue. Dans le cas où l'algorithme utilise une boucle non conditionnelle (le <code>for</code> en python), le nombre d'étapes de la boucle est déjà déterminé 

widget python
for i in range(10):
  print(i)
widget

Afin de prouver la terminaison d'un algorithme contenant une boucle conditionnelle (le <code>while</code> en python), nous raisonnons en utilisant la technique du **variant**.

+++bulle matthieu
  reprenons l'algorithme du calcul de la multiplication et intéressons nous à la terminaison de l'unique boucle contenu dedans
+++

```text
a ← un entier naturel
b ← un entier naturel

m ← 0
p ← 0

tant que m < a 
  m ← m + 1
  p ← p + b
fin du tant que

afficher p
```

+++bulle matthieu
  'variant' sonne comme 'variable'
+++

+++bulle matthieu droite
  quelles variables sont utilisées dans la condition de la boucle ?
+++

+++spoil
  À la ligne 7, qui défini l'unique boucle conditionnelle de l'algorithme, les variables m et a sont utilisées dans la condition de la boucle.
spoil+++

+++bulle matthieu
  est-ce que ces variables sont modifiées lors des tours de la boucle ?
+++

+++spoil
  Seule la variable m est modifiée à chaque tour de la boucle
spoil+++

+++bulle matthieu
  compte tenu de la condition de la boucle de l'algorithme et du fait qu'elle contient une variable qui change de valeur à chaque tour de boucle
+++

+++bulle matthieu droite
  est-ce que <b><i>tôt ou tard</i></b> la condition de la boucle deviendra fausse ? (ce qui permettra d'arrêter l'exécution de la boucle)
+++

+++spoil
  Oui car a ne changeant pas de valeur, m est incrémenté de 1 à chaque tour de boucle. Donc à partir d'un certain moment, la condition de la boucle 'm < a' deviendra fausse et donc, la boucle arrêtera de s'exécuter et le programme continuera à exécuter les lignes présentent après la fin de la boucle
spoil+++

<hr>

+++activité binome 30 ../activites/applications-_directes/debranche-_--_-_terminaison-_d--__un-_algorithme.md "Terminaison d'un algorithme"

## +++emoji-write Résumons

- Une boucle non conditionnelle (for) s'exécute toujours un nombre fini de fois
- Lorsque nous utilisons une boucle conditionnelle dans un algorithme, nous nous exposons au risque d'obtenir un programme qui s'exécute indéfiniment.
- Pour déterminer si un algorithme se termine bien, nous utilisons la technique du variant 
    - nous analysons la condition utilisée par la boucle en question et nous déterminons quelles sont les variables qui sont utilisées dans la condition
    - nous regardons si ces variables sont modifiées dans le corps de la boucle (= à chaque tour de boucle)
    - nous vérifions avec un raisonnement simple si tôt ou tard, la condition de la boucle deviendra fausse. Si ce n'est pas le cas, la boucle va s'exécuter sans jamais s'arrêter (ce qui n'est en général pas le comportement attendu)
    
    