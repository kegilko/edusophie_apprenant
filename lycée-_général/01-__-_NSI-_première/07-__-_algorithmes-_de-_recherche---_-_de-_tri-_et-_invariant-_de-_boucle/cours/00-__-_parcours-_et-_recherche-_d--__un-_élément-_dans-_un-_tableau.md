# Parcours et recherche d'un élément dans un tableau

+++todo trouver un meilleur titre de cours car on fait aussi lalgo de la moyenne dans ce cours

+++programme
  Écrire un algorithme de recherche d’un extremum, de calcul d’une moyenne
  
  Rédiger un algorithme en pseudo code
  Déterminer la complexité d'un algorithme
  Rechercher une valeur minimale maximale dans un tableau
  Calculer la moyenne de valeurs contenues dans un tableau
+++

+++sommaire

## Quelques rappels

+++bulle matthieu
  qu'est-ce qu'un algorithme ?
+++

+++spoil nolabel
+++citation /images/md/5E4525H25H28BBFC6CACH24884G2CH gauche
  Un algorithme est une suite finie et non ambiguë d'instructions et d’opérations permettant de résoudre une classe de problèmes
+++wikipedia https://fr.wikipedia.org/wiki/Algorithme

+++bulle matthieu droite
  une recette de cuisine est un algorithme
+++

+++bulle matthieu droite
  les programmes que nous créons suivent de fait un algorithme
+++

<hr>

spoil+++

+++bulle matthieu
  qu'est-ce que l'algorithmique ?
+++

+++spoil nolabel
+++citation /images/md/5E4525H25H28BBFC6CACH24884G2CH gauche
  L'algorithmique est l'étude et la production de règles et techniques qui sont impliquées dans la définition et la conception d'algorithmes
+++wikipedia https://fr.wikipedia.org/wiki/Algorithmique
spoil+++

+++bulle matthieu
  qu'est-ce qu'un bon algorithme ?
+++

+++spoil nolabel
Un bon algorithme est un algorithme efficace en temps et en ressources machine utilisés. Plus un algorithme est efficace, plus il s'exécute rapidement et moins il consomme de ressources de la machine
spoil+++

## Notion de complexité

En informatique, le problème de la **complexité** d'un algorithme est fondamental. Au lycée, nous nous focalisons sur la complexité temporelle d'un algorithme, c'est à dire, le temps que met un programme pour s'exécuter. 

+++bulle matthieu
  avant les années <b class="h4">~</b>1990, la matériel informatique était rare et donc (très) onéreux. La conception d'algorithmes efficaces était alors essentielle dans le travail d'un développeur.
+++

+++bulle matthieu droite
  aujourd'hui, concevoir des algorithmes les plus efficaces possibles reste nécessaire pour certaines industries informatiques. Et l'algorithmique s'invite de plus en plus dans les questions écologiques.
+++

### Déterminer la complexité d'un algorithme

Le temps d'exécution d'un algorithme dépend directement du nombre d'étapes effectuées dans ce dernier.  Nous comptabilisons uniquement les trois types d'étapes suivantes :

- Une **affectation** d'une nouvelle valeur à une variable avec le signe ← ( qui compte donc pour une étape )
- Une **comparaison** ( <, >, <=, >=, == ) entre deux données ( qui compte donc pour une étape )
- Un **calcul** ( +, -, *, /, % ) entre deux nombres ( qui compte donc pour une étape )

---

+++bulle matthieu
  combien d'étapes contiennent les algorithmes suivants ? 
+++

```text
a ← 10
b ← 20
c ← a + b
Afficher c
```

+++spoil
  Il y a 4 étapes au total

  <ul>
    <li><code>a ← 10</code> : une affectation</li>
    <li><code>b ← 20</code> : une affectation</li>
    <li><code>c ← a + b</code> : un calcul <b>puis</b> une affectation</li>
    <li><code>Afficher c</code> : 'Afficher' ne compte pas pour une étape</li>
  </ul>
  
spoil+++

```text
a ← 12
b ← 20
Si a > b
  c ← a
FIn Si
```

+++spoil
  Il y a 3 étapes au total
  
  <ul>
    <li><code>a ← 12</code> : une affectation</li>
    <li><code>b ← 20</code> : une affectation</li>
    <li><code>Si a > b</code> : une comparaison</li>
  </ul>
  
spoil+++

---

+++bulle matthieu
  les appels de fonction ne sont pas comptabilisés mais leur contenu doit être pris en compte
+++

+++bulle matthieu droite
  combien d'étapes contiennent l'algorithme suivant ? 
+++

```text

Fonction somme(a, b)
  Retourner a + b
Fin Fonction

Afficher somme(10, 17)
Afficher somme(1, 22)
```

+++spoil
  Il y a 2 étapes au total
  
  <ul>
    <li><code>Afficher somme(10, 17)</code> : cette ligne appelle la fonction somme dans laquelle un calcul est fait, ce qui compte donc pour une étape</li>
    <li><code>Afficher somme(1, 22)</code> : cette ligne appelle la fonction somme dans laquelle un calcul est fait, ce qui compte donc pour une étape</li>
    <li>L'instruction '<code>Retourner</code>' ne compte pas pour une étape</li>
  </ul>
  
spoil+++

+++activité individuel 30 ../activites/applications-_directes/debranche-_--_-_analyse-_d--__algorithmes-_simples.md "Analyse d'algorithmes simples"

### +++emoji-write Résumons

- Un algorithme est composés d'un ensemble d'étapes
- En algorithmique, pour déterminer la complexité d'un algorithme, nous nous focalisons sur les <b class="text-blue">étapes</b> suivantes : les <span class="text-blue">affectations</span>, les <span class="text-blue">calculs</span> et les <span class="text-blue">comparaisons</span>
- Un algorithme efficace (ou dit autrement, avec une bonne complexité) est un algorithme qui résout un problème en utilisant le moins d'<b class="text-blue">étapes</b> possibles
- Les instructions itératives <code>for</code> et <code>while</code> répètent un certain nombre de fois un bloc d'instruction. Les <b class="text-blue">étapes</b> contenues dans ces blocs d'instruction doivent être prises en compte dans le calcul de la complexité d'un algorithme.

## Notation grand O 

Dans des algorithmes simples, le nombre d'étape sera souvent constant. Analysons l'algorithme suivant : 

```text
joueur ← "Elminster"
pnj ← "forgeron Sam"

Afficher pnj + " : bonjour " + joueur + " ! que puis je faire pour vous ?"
```

+++bulle matthieu droite
  de quelle(s) donnée(s) a besoin cet algorithme pour fonctionner ?
+++

+++spoil
  L'algorithme a besoin du prénom du joueur et du nom du pnj
spoil+++


+++bulle matthieu droite
  combien d'étapes sont effectuées dans cet algorithme ?
+++

+++spoil
  L'algorithme effectue 2 affectations
spoil+++

+++bulle matthieu
  est-ce que le nombre d'étape(s) effectué par l'algorithme change en fonction des données en entrée ?
+++

+++spoil
  Si la chaîne de caractère contenue dans les variables joueur ou pnj grandit, cela n'impacte pas la complexité de l'algorithme
spoil+++


On dit que l'algorithme est de **complexité constante**, cela se note **O(1)**.

---

Analysons l'algorithme suivant : 

```text
nombre ← 5
resultat ← 0

Pour chaque chiffre allant de 1 (inclu) à nombre (inclu)
  resultat ← resultat + chiffre
Fin Pour

Afficher resultat
```

+++bulle matthieu
  que vaut résultat à la fin de l'algorithme ? quel est le but de cet algorithme ?
+++

+++spoil
  Résultat vaut 1 + 2 + 3 + 4 + 5, soit 15. Cet algorithme calcul la somme de tous les entiers positifs compris entre 1 et nombre
spoil+++

+++bulle matthieu droite
  de quelle(s) donnée(s) a besoin cet algorithme pour fonctionner ?
+++

+++spoil
  L'algorithme a besoin d'un nombre en entrée pour pouvoir travailler
spoil+++

+++bulle matthieu
  est-ce que le nombre d'étape(s) effectué par l'algorithme change en fonction des données en entrée ?
+++

+++spoil
  <p>Oui ! plus nombre est grand, plus la boucle Pour effectuera des tours, Et à chaque tour de boucle, 2 étapes sont effectuées.</p>
spoil+++

+++spoil "Suite" nolabel
<p>Soit n le nombre de tour que va effectuer la boucle Pour. La ligne 5 de l'algorithme contient 2 étapes (un calcul puis une affectation). Donc les lignes 4 à 6 vont effectuer 2 x n étapes.</p>

<p>Avant la boucle Pour, deux affectations ont lieu lignes 1 et 2</p>
<p>L'algorithme effectue donc 2 + 2 x n étapes. Cette formule décrit une courbe linéaire dans un graphe, on dit alors que l'algorithme est de complexité linéaire et on note cela O(n)</p>
spoil+++

---

Une dernière analyse avant de continuer : 

```text
pioche ← 7
main ← [2, 7, 5, 1, 9]

Pour chaque carte dans la main
  Si carte = pioche 
    Afficher "je possède déjà cette carte"
    Retourner Vrai
  Fin Si
Fin Pour

Retourner Faux
```

+++bulle matthieu
  quel est le but de cet algorithme ?
+++

+++spoil
  Cet algorithme vérifie si un autre exemplaire d'une carte, qui vient d'être piochée, se trouve déjà dans la main. Si oui, l'algorithme affiche un message et termine son exécution en renvoyant Vrai, sinon l'algorithme renvoie Faux.
spoil+++

+++bulle matthieu droite
  de quelle(s) donnée(s) a besoin cet algorithme pour fonctionner ?
+++

+++spoil
  L'algorithme a besoin du numéro de la carte piochée, et de l'ensemble de cartes actuellement en main
spoil+++

+++bulle matthieu
  Avec les données actuelles, combien d'étape(s) effectue l'algorithme ?
+++

+++spoil
  <p>L'algorithme effectue deux affectations (lignes 1 et 2). La boucle Pour va faire 2 tours de boucles et dans chaque tour de boucle, une comparaison est faîte (ligne 5). L'algorithme effectue donc au total 2 + 2 * 2 étapes, soit 6 étapes au total</p>
spoil+++

+++bulle matthieu droite
  quel est le pire des cas que peut rencontrer cet algorithme ? combien d'étape(s) devra effectuer l'algorithme dans le pire des cas ?
+++

+++spoil
  <p>Le pire des cas serait que la main ne possède aucun exemplaire similaire à la carte piochée</p>
  <p>Le nombre d'étape dans l'algorithme va donc varier en fonction du nombre de carte en main</p>
spoil+++

+++spoil "Suite" nolabel
  Soit n, le nombre de carte dans la main. Dans le pire des cas, la boucle Pour va effectuer n tour de boucle, Dans chaque tour de boucle, une comparaison est faite, donc la boucle effectuera n * 1 étapes. En début d'algorithme, deux affectations sont effectuées. Le nombre total d'étape dans l'algorithme est de 2 + n. 
  
+++bulle matthieu
  est-ce que 2 + n correspond à une courbe linéaire ? 
+++
  
  +++spoil
    Oui ! cet algorithme est en O(n), c'est à dire de complexité linéaire
  spoil+++
  
spoil+++


## Algorithmes courants

Dans les applications, site Web, logiciels, etc... certains algorithmes sont très récurrents et font, du coup, partie des algorithmes "cas d'école" étudiés pendant les études informatiques. Cette année nous voyons :  

- **la recherche d'un élément dans un tableau** : ( +++lien "/apprenant/01-__-_NSI-_premiere/03-__-_parcours-_de-_tableaux---_-_introduction-_en-_algorithmique-_et-_tests/01-__-_activites/00-__-_applications-_directes/debranche-_--_-_initiation-_en-_algorithmique.md" "activité insertion de carte dans une main" )
    - recherche d'un contact dans un logiciel de messagerie instantanée
    - recherche d'un item dans un inventaire d'un personnage de jeux vidéo
- **le calcul d'une moyenne**
    - calcul du temps moyen effectué par un service Web de création d'itinéraire
    - calcul de la moyenne de la classe sur ÉcoleDirecte ou Pronote
- **la recherche d'un extremum dans un tableau**
    - mettre en surbrillance le prix le moins cher d'une nuit d'hôtel parmi une liste pour un voyage
    - rechercher la température la plus élevée de l'année
- **le tri de tableau** : séance future

+++activité groupe 40 ../activites/applications-_directes/algorithmes-_min---_-_max-_et-_moyenne.md "Algorithmes min, max et moyenne"

## +++emoji-write Résumons

- pour mesurer la complexité d'un algorithme, nous utilisons la notation O (prononcé 'grand O')
- un algorithme est de complexité constante ( noté O(1) ) lorsque le nombre d'étape (affectation, calcul, comparaison) effectué en son sein ne varie pas en fonction des données en entrée
- un algorithme est de complexité linéaire ( noté O(n) ) lorsque le nombre d'étape (affectation, calcul, comparaison) effectué en son sein varie linéairement en fonction des données en entrée
- les algorithmes min, max et moyenne sont à connaître par +++emoji-heart 

