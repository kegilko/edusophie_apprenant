# Algorithmes de tri

+++prof il faut compter au moins deux séances pour ce cours

+++programme /images/md/2GE328HCGCA7B7FGF23G5BGF9426DB
  Écrire un algorithme de tri (insertion et sélection)
  Écrire une fonction renvoyant un p-uplet de valeurs

  Implémenter un algorithme de tri connu
  Retourner un ensemble de valeur à l'issue d'une fonction
+++

+++sommaire

## Introduction 

Nous trions tous des données régulièrement :

- tri des photos
- rangement de feuilles de cours
- tri de messages
- rangement de livres dans une étagère
- ... 

En informatique, il s'agit d'une opération essentielle pour la majeure partie des sites/applications Web, logiciels et systèmes d'exploitations.

+++bulle matthieu
  quelques exemples connaissez vous de tris effectués par des sites Web ? des logiciels ? des systèmes d'exploitation ?
+++

Nous allons étudier deux des algorithmes de tri les plus connus (mais pas forcement les plus efficaces ! )

Avant cela, commençons par une courte activité activité de rappel sur Python (avec une petite nouveauté)

+++activité binome 45 ../activites/applications-_directes/preparation-_python-_pour-_les-_algorithmes-_de-_tri.md "Préparation python pour les algorithmes de tri"

### Résumons

- Le parcours de tableau est souvent utilisé en algorithmique
- [Rappel] En python, pour parcourir un tableau, nous pouvons utiliser l'instruction itérative <code>for</code> ou <code>while</code> : 
```python
animaux = ['chat', 'chien', 'souris']
for animal in animaux:
  print(animal)
```

```python
animaux = ['chat', 'chien', 'souris']
compteur = 0
while(compteur < 3):
  print(animal[compteur])
  compteur = compteur + 1
```
- [Rappel] Au lycée, nous nous focalisons sur le pire des cas que peut rencontrer un algorithme et sur le temps que peut mettre ce dernier à s'exécuter
- [Rappel] Un algorithme dont les étapes évoluent, dans le pire des cas, de manière linéaires en fonction des données est de complexité linéaire. Cela est noté O(n) (grand O de n)

+++prof pour les activités suivantes, il faut imprimer des paquets de carte pour chaque groupe (deux paquets de 5 cartes par groupe par exemple). Le fichier pdf se trouve +++fichier "/fichiers/5HCF5494C54434976E6B84CD6DFDGE" "à l'adresse suivante"

## Tri par sélection

Les algorithmes de tri permettent de trier un tableau de données dans un ordre spécifique. Par convention en algorithmique, nous trions des tableaux d'éléments par rapport à leur nombre et de manière croissante. Ainsi, à la fin d'un tri, un tableau de données doit être trié du plus petit élément au plus grand.

+++activité groupe 50 ../activites/applications-_directes/tri-_par-_selection.md "Tri par sélection"
  
## Tri par insertion

+++activité groupe 45 ../activites/applications-_directes/tri-_par-_insertion.md "Tri par insertion"

## Comparaison de complexités

Nous avons vu des algorithmes à complexité constante ( O(1) ), à complexité linéaire ( O(n) ) et à complexité quadratique ( O(n²) )

Il existe d'autres types de complexités, voici un aperçu général des complexités abordées dans les programmes de <b class="text-green">première NSI</b> et de <b class="text-blue">terminale NSI</b>:

<table class="table table-sm">
  <thead>
    <tr class="align-middle text-center">
      <th rowspan="2">Complexité</th>
      <th rowspan="2">Notation</th>
      <th colspan="6" class="text-center">Temps approximatif d'exécution d'un programme contenant un nombre d'étapes e</th>
      <th rowspan="2">Algorithmes vus en NSI</th>
    </tr>
    <tr class="align-middle text-center">
      <th><span class="text-nowrap">e</span></th>
      <th><span class="text-nowrap">20 x e</span></th>
      <th><span class="text-nowrap">50 x e</span></th>
      <th><span class="text-nowrap">1 000 x e</span></th>
      <th><span class="text-nowrap">10 000 x e</span></th>
      <th><span class="text-nowrap">1 000 000 x e</span></th>
    </tr>
  </thead>
  <tbody>
    <tr class="align-middle">
      <td>constante</td>
      <td>O(1)</td>
      <td class="text-nowrap">10 ns</td>
      <td class="text-nowrap">10 ns</td>
      <td class="text-nowrap">10 ns</td>
      <td class="text-nowrap">10 ns</td>
      <td class="text-nowrap">10 ns</td>
      <td class="text-nowrap">10 ns</td>
      <td class="text-green">accès à une cellule de tableau</td>
    </tr>
    <tr class="align-middle">
      <td>logarithmique</td>
      <td>O(log<sub>2</sub>(n))</td>
      <td class="text-nowrap">10 ns</td>
      <td class="text-nowrap">10 ns</td>
      <td class="text-nowrap">20 ns</td>
      <td class="text-nowrap">30 ns</td>
      <td class="text-nowrap">40 ns</td>
      <td class="text-nowrap">60 ns</td>
      <td class="text-green">recherche dichotomique</td>
    </tr>
    <tr class="align-middle">
      <td>linéaire</td>
      <td>O(n)</td>
      <td class="text-nowrap">10 ns</td>
      <td class="text-nowrap">20 ns</td>
      <td class="text-nowrap">50 ns</td>
      <td class="text-nowrap">10 µs</td>
      <td class="text-nowrap">100 µs</td>
      <td class="text-nowrap">10 ms</td>
      <td class="text-green">parcours de liste</td>
    </tr>
    <tr class="align-middle">
      <td>linéarithmique</td>
      <td class="text-nowrap">O(n log<sub>2</sub>(n))</td>
      <td class="text-nowrap">10 ns</td>
      <td class="text-nowrap">864 ns</td>
      <td class="text-nowrap">2,8 µs</td>
      <td class="text-nowrap">100 µs</td>
      <td class="text-nowrap">1,3 ms</td>
      <td class="text-nowrap">199 ms</td>
      <td class="text-blue">tri par fusion</td>
    </tr>
    <tr class="align-middle">
      <td>quadratique</td>
      <td>O(n²)</td>
      <td class="text-nowrap">10 ns</td>
      <td class="text-nowrap">4 µs</td>
      <td class="text-nowrap">25 µs</td>
      <td class="text-nowrap">10 ms</td>
      <td class="text-nowrap"><b style="color:yellow">1 s</b></td>
      <td class="text-nowrap"><b style="color:orange">2h47mn</b></td>
      <td class="text-green">tri par sélection, tri par insertion</td>
    </tr>
    <tr class="align-middle">
      <td>exponentielle</td>
      <td>O(2<sup>n</sup>)</td>
      <td class="text-nowrap">10 ns</td>
      <td class="text-nowrap">10,5 ms</td>
      <td class="text-nowrap"><b class="text-red">130 jours</b></td>
      <td class="text-nowrap"><b class="text-red">10<sup>283</sup> siècles</b></td>
      <td class="text-nowrap text-center">-</td>
      <td class="text-nowrap text-center">-</td>
      <td class="text-green">problèmes du sac à dos, problème du rendu de monnaie</td>
    </tr>
    <tr class="align-middle">
      <td>factorielle</td>
      <td>O(n!)</td>
      <td class="text-nowrap">10 ns</td>
      <td class="text-nowrap"><b class="text-red">8 siècles</b></td>
      <td class="text-nowrap"><b class="text-red">10<sup>46</sup> siècles</b></td>
      <td class="text-nowrap text-center">-</td>
      <td class="text-nowrap text-center">-</td>
      <td class="text-nowrap text-center">-</td>
      <td class="text-blue">problème du voyageur de commerce</td>
    </tr>
  </tbody>
</table>
  
+++image "/images/md/HGAFGE5C45E945GH36D9EH4673EA8A" mx-auto col-10

## Résumons 

- la complexité, dans le pire des cas, des algorithmes de tri par sélection et insertion est quadratique, c'est à dire en O(n²). Cela est principalement du à l'imbrication des deux instructions itératives 
- plus la complexité est faible, moins l'algorithme prendra de temps à être exécuté