# Algorithmes d'apprentissage et algorithmes gloutons

+++programme
  Écrire un algorithme qui prédit la classe d’un élément en fonction de la classe majoritaire de ses k plus proches voisin
  Résoudre un problème grâce à un algorithme glouton
  
  Déterminer les principaux enjeux techniques graviants autour de l'intelligence artificielle
  Faire connaissance avec les algorithmes gloutons
+++

## Introduction

+++bulle matthieu
  (rappel) qu'est-ce qu'est-ce qu´un algorithme ?
+++

+++bulle matthieu droite
  quelle est la différence entre un bon et un mauvais algorithme ?
+++

## Bref détour sur le machin learning

+++bulle matthieu
  qu'est-ce qu'une intelligence artificielle ?
+++

+++bulle matthieu droite
  qu'est-ce que le machin learning ?
+++

+++video "https://www.youtube.com/watch?v=RC7GTAKoFGA" "Comprendre le Machine Learning en 5min" 00:36

Quelques domaines (parmis tant d'autres) friands des technologies liées aux intelligences artificielles : 

<ul>
  <li>l'industrie automobile</li>
  <li>le marketing</li>
  <li>les moteurs de recherches</li>
  <li>les réseaux sociaux</li>
  <li>le secteur médical</li>
  <li>les villes connectées</li>
  <li>etc etc...</li>
</ul>

+++bulle matthieu
  d'une manière générale, quand il y a des données en jeu et qu'il y a un besoin de projection sur le futur
+++

+++bulle matthieu droite
  nous entrons en plein dans le domaine informatique de l'intelligence artificielle
+++

## Initiation à un premier algorithme d'apprentissage automatique

+++activité binome 30 ../activites/applications-_directes/reconnaissance-_de-_plantes-_depuis-_une-_application-_mobile.md "Reconnaissance de plantes depuis une application mobile"

## Présentation des algorithmes gloutons

+++imageFloat /images/md/GB9383995DDBDGEE7BC5AHC65E87E9
Les algorithmes peuvent être rangés dans différentes familles. Certains algorithmes sont qualifiés de <b>glouton</b>. Ces algorithmes ont la particularité suivante : à chaque étape d'exécution de l'algorithme, celui-ci cherche la meilleure solution au problème dans l'espoir d'obtenir à la fin la meilleure solution. 
+++

Voici ci-dessous la représentation d'un réseau routier entre différentes villes.

+++image "/images/md/93BDC2H9832E9E257G62H94EAD94C8" col-10 mx-auto

+++bulle matthieu
  en partant de Montpellier, quelle règle je peux suivre pour essayer de trouver le plus court chemin d'aller à Valance ?
+++

+++spoil

```text
Se placer dans la ville de départ
Tant que la ville d'arrivée n'est pas atteinte
  Depuis la ville actuelle, aller dans la ville voisine (non visités encore) la plus proche
```

À chaque étape, l'algorithme choisit la solution qui semble être la meilleure, il s'agit bien d'un algorithme glouton. 

spoil+++

+++bulle matthieu
  Si je souhaite aller de Montpellier à Saint-Étienne en utilisant l'algorithme glouton, par quelle(s) ville(s) vais-je passer ? combien de kilomètres au total je vais parcourir ?
+++

+++bulle matthieu
  quelle limite peut on trouver à notre algorithme ?
+++

Dans la famille des algorithmes gloutons se trouvent deux très connus algorithmes :

- <b>le rendu de pièce de monnaie</b> : comment rendre le moins de pièces et billets possible suite à un achat
- <b>la gestion d'un sac à dos</b> : prendre le plus d'objets possible dans un sac à dos en fonction du poid des objets

+++bulle matthieu
  recherchez sur Internet une implémantation python ou javascript d'un de ces deux algorithmes et faîte la fonctionner
+++

+++bulle matthieu droite
  analysez le code, modifiez le, testez le !
+++
