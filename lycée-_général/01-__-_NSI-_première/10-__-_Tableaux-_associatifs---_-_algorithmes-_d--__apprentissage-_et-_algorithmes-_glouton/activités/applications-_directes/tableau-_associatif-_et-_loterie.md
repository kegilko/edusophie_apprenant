# Tableau associatif et loterie

+++consignes
  <ol start="0">
    <li>Créez vous un dossier du jour en exécutant chacune des lignes ci-dessous sur un terminal. <b>Attention !</b> n'oubliez pas d'adapter les lignes 2 et 3 </li>
  </ol>
  <pre><code class="language-bash"># lignes à ré-adapter et à exécuter dans un terminal
  nom_prenom=dupond_cedric
  dateDuJour=18_10_21

  mkdir /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
  cd /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
  touch loterie.py
  thonny loterie.py
  </code></pre>
  <ol start="1">
    <li>Le but de cette activité est de reprendre le programme 'loterie' détaillé dans le cours et de le modifier pour utiliser un dictionnaire à la place d'un tableau séquentiel</li>
  </ol>
+++

Voici la dernière version du programme utilisée par Alexandre pour sa loterie lors de ses lives : 

```python

#V-- imports --V#

import random # bibliothèque qui sera nécessaire pour la loterie

#V-- variables générales --V#

viewers = [ # les 'False' servent dans le programme à savoir si un utilisateur a déjà modifié son numéro ou non
  ['hellfire@optonline.net', '7', False],
  ['jeffcovey@outlook.com','1', False],
  ['smeier@icloud.com','1', False],
  ['eurohack@sbcglobal.net','4', False],
  ['duncand@yahoo.ca','9', False],
  ['drjlaw@gmail.com','8', False],
  ['paina@live.com','1', False]
]

#V-- définition des fonctions nécessaires --V#

def modifier(viewers, email, choix, nouvelle_valeur):
  index = 0
  for viewer in viewers:
    if(viewer[0] == email):
      if(choix == 'email'):
        viewers[index][0] = nouvelle_valeur
      else:
        if(viewers[index][2] == True): # si le viewer a déjà modifié son numéro une fois...
          print('Erreur ! ce viewer a déjà modifié son numéro une fois !')
        else: # sinon, nous modifions son numéro et enregistrons dans viewer cette information en index 2
          viewers[index][2] = True
          viewers[index][1] = nouvelle_valeur
    index += 1
  return viewers

def afficher(viewers): # affiche les informations de tous les viewers
  for viewer in viewers:
    print("'" + viewer[0] + "' a choisi le numéro " + viewer[1] + " !", end=' ')
    if(viewer[2] == False):
      print("Il peut modifier son numéro")
    else:
      print("Il ne peut plus modifier son numéro")

def supprimer(viewers, email_a_supprimer): # supprime un email
  viewers_a_jour = []
  for viewer in viewers:
    if(viewer[0] != email_a_supprimer):
      viewers_a_jour.append(viewer)
  return viewers_a_jour

def loterie(viewers, nombre_max): # déclenche une loterie
  nombre_gagnant = random.randint(0, int(nombre_max))
  print('le bon numéro est ' + str(nombre_gagnant))
  
  for viewer in viewers:
    if(viewer[1] == str(nombre_gagnant)):
        print("'" + viewer[0] + "' a gagné un lot ! ")
      

#V-- coeur du programme --V#

nombre_max = input('Renseigner le nombre maximal que peut générer une loterie : ')

while(True): # boucle infinie
  choix = input('Choisir une action : afficher | supprimer | modifier | loterie\n => ')
  if(choix == 'afficher'):
    afficher(viewers)
  elif(choix == 'supprimer'):
    email_a_supprimer = input('quel email supprimer ? ')
    viewers = supprimer(viewers, email_a_supprimer)
  elif(choix == 'loterie'):
    loterie(viewers, nombre_max)
  elif(choix == 'modifier'):
    viewers = modifier(viewers, input("quel est l'email de l'utilisateur concerné ? "), input('Quelle information modifier ? ( Choisir une action : email | numero ) '), input('Quelle est la nouvelle valeur à enregistrer ? '))
  else:
    print("ce choix n'existe pas ! fin du programme")
    break # on sort de la boucle infinie

```

Le code est complet et fonctionne !

+++question "Recopiez le code dans votre fichier 'loterie.py', analysez le, modifiez le, testez le pendant quelques minutes"

+++bulle matthieu
  le but est de comprendre le programme et son code dans les grandes lignes
+++

<hr>

Actuellement la variable <code>viewers</code> est un tableau séquentiel. En cours aidant du cours, vous allez modifier ce tableau et (nécessairement) modifier le reste du programme pas à pas. 

+++question "Modifiez la structure de la variable <code>viewers</code> (lignes 7 - 15) afin d'utiliser un tableau associatif (à la place de l'actuel tableau séquentiel)"

+++spoil "Aide : quelle clé utiliser ?"
  Parmi les informations qui concernent un viewer, quelle information est unique ? c'est cette information qui doit être utilisée en clé dans le dictionnaire !
spoil+++

+++spoil "Aide : que stocker dans la valeur de chaque clé ?"
  Un viewers possède trois informations : son mail, son numéro, et si il a déjà modifié ou non son numéro (<code>True</code> ou <code>False</code>). La clé est le mail (car il est unique d'un viewer à l'autre), la valeur doit donc stocker le numéro et l'information qui permet de savoir si ce numéro a été modifié ou non (<code>True</code> ou <code>False</code>). Quel est le type de donnée le plus adapté pour contenir un ensemble d'informations séquentielles ? c'est ce type qu'il faut alors utiliser dans les valeurs de votre dictionnaire !
spoil+++

<hr>

La variable contenant les données principales du programme ayant été mofifiée, il faut maintenant modifier le reste du programme pour l'adapter par rapport à la manipulation du tableau associatif <code>viewers</code> !

Parcourir le programme de haut en bas et modifier les lignes les unes après les autres n'est pas la meilleure des approches. Une bonne approche dans la modification d'un programme est de modifier ses fonctionnalités les unes après les autres. Après avoir modifié une fonctionnalité, vous pouvez ainsi la tester et voir rapidement si vos modifications sont valides. 

Le programme 'loterie' propose plusieurs fonctionnalités : 

0. afficher le contenu de la variable <code>viewers</code>
1. modifier les informations d'un viewer
2. supprimer un viewer de la liste des viewers
3. déclencher une loterie

+++question "Modifiez le code qui ne concerne que l'affchage des informations des viewers en l'adaptant à la nouvelle structure utilisée par la variable <code>viewers</code>"

+++bulle matthieu
  testez votre code et assurez vous que la fonctionnalité 'afficher' fonctionne bien avant de passer à la suite !
+++

<hr>

+++question "Modifiez le code qui ne concerne que la modification des informations des viewers en l'adaptant à la nouvelle structure utilisée par la variable <code>viewers</code>"

+++bulle matthieu
  testez votre code et assurez vous que la fonctionnalité 'modifier' fonctionne bien avant de passer à la suite !
+++

<hr>

+++question "Modifiez le code qui ne concerne que la suppression d'un viewer donné en l'adaptant à la nouvelle structure utilisée par la variable <code>viewers</code>"

+++bulle matthieu
  testez votre code et assurez vous que la fonctionnalité 'modifier' fonctionne bien avant de passer à la suite !
+++

<hr>

+++question "Pour terminer, modifiez le code qui concerne que le déclenchement de la loterie en l'adaptant à la nouvelle structure utilisée par la variable <code>viewers</code>"

## Vous êtes en avance ? 

Recréez le programme mais en Javascript (ou tout autre langage de programmation de votre choix) ! vous devrez au préalable vous renseigner sur la manipulation des tableaux associatifs sur le langage choisi

## Réponses

<ol start="1">
  <li>
  +++spoil

```python

viewers = {
  'hellfire@optonline.net' : ['7', False],
  'jeffcovey@outlook.com' : ['1', False],
  'smeier@icloud.com' : ['1', False],
  'eurohack@sbcglobal.net' : ['4', False],
  'duncand@yahoo.ca' : ['9', False],
  'drjlaw@gmail.com' : ['8', False],
  'paina@live.com' : ['1' False]
}

```

  spoil+++
  </li>
</ol>