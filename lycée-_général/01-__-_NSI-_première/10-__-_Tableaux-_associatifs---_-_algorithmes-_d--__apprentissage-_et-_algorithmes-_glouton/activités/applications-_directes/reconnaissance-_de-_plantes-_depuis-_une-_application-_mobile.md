# Reconnaissance de plantes depuis une application mobile

+++consignes
  <ol start="0">
    <li>Créez vous un dossier du jour en exécutant chacune des lignes ci-dessous sur un terminal. <b>Attention !</b> n'oubliez pas d'adapter les lignes 2 et 3 </li>
  </ol>
  <pre><code class="language-bash"># lignes à ré-adapter et à exécuter dans un terminal
  nom_prenom=dupond_cedric
  dateDuJour=18_10_21

  mkdir /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
  cd /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
  touch iris.py
  thonny iris.py
  </code></pre>
  <ol start="1">
    <li>Parcourez l'activité et répondez aux questions</li>
  </ol>
+++

Imaginez vous à la place d'un biologiste qui travail dans un bureau d'étude dont le but est de déterminer si un terrain peut devenir constructible ou non. Si le terrain habrite des espèces animales ou végétales protégées, le biologiste en fat rapport et le terrain ne pourra alors pas devenir constructible.

Notre amis biologiste part donc sur le terrain pour y recenser les différentes espèces animales comme végétales. Dans son travail, il s'aide d'une application mobile lui permettant d'identifier si besoin une espère et de noter ses observations.

+++question "Pour son bon fonctionnement, quel lien pourrait on trouver entre cette application et le machin learning ?"

<hr>

Dans cette activité, nous nous intéressons à la reconnaissance de la fleur d'iris qui se décline en 3 différentes sous-espèces : 

- l'iris virginica
- l'iris versicolor
- l'iris setosa

+++image "/images/md/5A88G28E5F5E3887AG2GG37ED7E5E9" col-10 mx-auto

Nous supposons que l'application dispose de données sur ces 3 plantes et que ces données sont stockées <a href="/d/nsi_1ere/iris.csv">dans ce fichier csv</a>. L'application utilise le code suivant : 

```python
import pandas
import matplotlib.pyplot as plt

# Importation des données
iris = pandas.read_csv("IRIS.csv")

x = iris.loc[:,"petal_length"]
y = iris.loc[:,"petal_width"]
lab = iris.loc[:,"species"]

# Affichage des données
for e, c in [('setosa','#00FF00'), ('virginica', '#FF0000'), ('versicolor', '#0000FF')]:
    plt.scatter(x[lab == e], y[lab == e], color = c, label = e)

plt.legend()
fig = plt.gcf()
plt.show()
```

+++question "Recopiez le code dans votre fichier iris.py puis exécutez le et résolvez les erreurs qui se déclenchent"

<hr>

Une fois que le code fonctionne, vous devriez avoir ce graphique qui s'affiche sur votre ordinateur : 

+++image "/images/md/A63D8F74CCH86FA26F665GDGEDH4F9" mx-auto col-6

+++question write "En analysant le code, à quoi correspondent l'abscisse et l'ordonnée de ce graphique ?"

+++question write "Qu'observez vous sur le graphique par rapport aux 3 espèces d'iris ?"

+++question "(bonus) Si je veux modifier les couleurs des points sur le graphique, quelle(s) modification(s) dois-je apporter dans le code ?"

<hr>

Depuis son smartphone, notre biologiste prends en photo un iris. Un sous-programme d'analyse d'image détermine que la largeur moyenne des pétales de la fleur fait 0.6 cm et la longeur fait 2 cm

+++question write "À quelle espèce semble faire partie l'iris qui vient d'être prise en photo ?"

<hr>

Dans certains cas, il est facile de déterminer à quelle sous-espèce d'iris nous avons affaire. Mais parfois, cela est beaucoup moins évident ! nous pouvons prendre par exemple le cas où la longueur moyenne fait 5,3cm et la largeur moyenne fait 1,6 cm. 

+++question "En vous aidant de recherches sur Internet sur la bibliothèque <b>python</b> <b>matplotlib</b>, complétez le code python afin d'ajouter un point dans le graphique aux coordonnées (5,3;1,6)"

<hr>

Dans de tel cas, il devient nécessaire de déterminer via un algorithme à quelle sous espèce appartient une fleur. Un algorithme, appartenant à la famille des algorithmes de machin learning, prévu dans notre cas précis est l'<b>algorithme des k plus proches voisins</b>. Dans les grandes lignes, voici ce que fait l'algorithme : 

- il calcule la distance entre le point représentant la fleur qui vient d'être prise en photo et chaque autres points situés dans le graphique
- il sélectionne uniquement les <b>k</b> distances les plus petites (autrement dit, les <b>k</b> plus proches voisins)
- parmi les <b>k</b> plus proches voisins, il détermine quelle est l’espèce majoritaire.

+++bulle matthieu
  dans cette activité nous nous nous focalisons sur deux données, la largeur et la longueur moyenne d'une pétale
+++

+++bulle matthieu droite
  dans un cas réel, il arrive parfois qu'il faille appliquer l'algorithme avec une dizaine (voir des dizaines !) de paramètres différents !
+++

Voici une implémentation 'python-way' de cet algorithme : 

```python
import pandas
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier

# Importation des données
iris = pandas.read_csv("datas/iris.csv")
x = iris.loc[:,"petal_length"]
y = iris.loc[:,"petal_width"]
lab = iris.loc[:,"species"]

# Affichage des données
for e, c in [('setosa','g'), ('virginica', 'r'), ('versicolor', 'b')]:
    plt.scatter(x[lab == e], y[lab == e], color = c, label = e)
plt.legend()
fig = plt.gcf()

# Caractéristiques de l'Iris mystère
longueur = 
largeur = 
plt.scatter(longueur, largeur, color='k', marker="x")


# Algorithme des k plus proches voisins
k = 3
d = list(zip(x,y)) # Regroupement en couples de coordonnées (x, y)
model = KNeighborsClassifier(n_neighbors = k)
model.fit(d, lab)
prediction = model.predict([[longueur,largeur]])


# Affichage des résultats
plt.text(3,0.1, "Résultat : " + prediction[0], fontsize=12)


plt.show()
```

+++question "Créez un nouveau fichier python nommé 'iris2.py' (dans le même dossier que celui qui contient 'iris.py') et copiez dedans le code ci-dessus"

+++question "Exécutez le code (vous aurez besoin de le compléter) et trouvez ainsi la sous-espèce de l'iris dont la longueur moyenne de pétale fait 5,3cm et la largeur moyenne fait 1,6 cm"

## Réponses

<ol start="0">
  <li>
  +++spoil
  Le machin learning peut permettre à l'application d'identifier des espèces en les prenant en photo. Avec une base de données de photos d'espèces importante et les bon algorithmes, l'application est en mesure d'identifier une espèce à partir d'une photo
  spoil+++
  </li>
  <li>
  +++spoil
  Pour que le code fonctionne, il faut que le chemin vers le fichier 'iris.csv' de la ligne 5 soit correct. Il faut que le nom du fichier soit le bon(attention : les majuscules et les minuscules sont différentes en programmation). Il vous faudra peut être télécharger sur thonny la bibliothèque <code>matplotlib</code>
  
  Voici un exemple de code corrigé : 
  
```python
import pandas
import matplotlib.pyplot as plt

# Importation des données
# le fichier CSV doit se trouver dans un dossier nommé 'datas'
# le dossier datas doit se retrouver au même niveau que le fichier python iris.py
iris = pandas.read_csv("datas/iris.csv") 

x = iris.loc[:,"petal_length"]
y = iris.loc[:,"petal_width"]
lab = iris.loc[:,"species"]

# Affichage des données
for e, c in [('setosa','#00FF00'), ('virginica', '#FF0000'), ('versicolor', '#0000FF')]:
    plt.scatter(x[lab == e], y[lab == e], color = c, label = e)

plt.legend()
fig = plt.gcf()
plt.show()
```

  spoil+++
  </li>
  <li>
  +++spoil
  Nous pouvons deviner à partir des lignes 7 et 8 que l'abscisse représente la longueur ( +++flag-us length ) de la pétale de la fleur mesurée dans le fichier CSV. L'ordonnée représente elle la largeur de la pétale
  spoil+++
  </li>
  <li>
  +++spoil
  Le graphique regroupe les données des trois espèces dans 3 endroits relativement bien séparés
  spoil+++
  </li>
  <li value="5">
  +++spoil
  En s'aidant du graphique, nous pouvons être à peu près certain que la fleur prise en photo fait partie de la sous-espèce setosa
  
  +++image "/images/md/GE254GHFDEAG4D95D4H5DDG25CDE76" col-8 mx-auto
  spoil+++
  </li>
  <li>
    +++spoil
      Il suffit d'ajouter par exemple la ligne <code>plt.plot([5.3], [1.6], marker="x", markersize=5)</code> avant l'appel de la fonction <code>plot</code> de plt
    spoil+++
  </li>
  <li value="8">
    +++spoil
      Il faut probablement installer la bibliothèque python 'sklearn' et compléter les lignes 18 et 19 du programme pour y renseigner les données de l'iris dont nous souhaitons déterminer la sous-espèce.
      
```python
# ...
# lignes 18 et 19
longueur = 5.3
largeur = 1.6
# ...
```
      
    spoil+++
  </li>
</ol>
