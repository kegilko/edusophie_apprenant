# comment fonctionne un cookie

+++consigne
  Cette activité se fait en classe entière. Nous allons visualiser une vidéo qui parle de cookie et répondre à quelques questions
+++

+++video "https://www.youtube.com/watch?v=QWw7Wd2gUJk" "Comment les cookies vous tracent ? (2018)"

+++question write "[1:16] Pourquoi les cookies ont été initialement créés ?"

+++spoil
  Pour pouvoir conserver certaines données côté Client et ainsi soulager le travail du Serveur
spoil+++

+++question write "[1:53] HTTP est un protocole stateless ( +++flag-fr sans état ), qu'est-ce que ça signifie ?"

+++spoil
  Cela signifie que chaque requête est indépendante. La requête ne fait référence à aucune autre requête passée. Chaque requête est effectuée à partir de rien, comme si c'était la première fois qu'elle était émise.
spoil+++

<hr>

[2:26] La vidéo présente <u>**un exemple**</u> de contenu de cookie (servant à authentifier un internaute). La valeur du cookie présenté contient une chaîne de caractères composée d'environ 170 caractères pouvant prendre les valeurs 0 à 9 ou 'a' à 'f'

+++question write "Combien de valeurs possibles peut prendre un caractère ?"

+++spoil
  Sachant que sur l'exemple de la vidéo, un caractère peut varier de 0 à 9 ou de 'a' à 'f', un caractère peut prendre 16 valeurs possibles 
spoil+++

+++question write "Même question avec deux caractères (combien de combinaisons possibles ?)"

+++spoil
  Le nombre total de combinaisons possibles avec deux caractères s'élève à 16². soit 256
spoil+++

+++question write "Même question avec 170 caractères"

+++spoil
  Le nombre total de combinaisons possibles avec deux caractères s'élève à 16<sup>170</sup>, soit environ 5 * 10<sup>204</sup> combinaisons possibles
spoil+++

<hr> 

+++bulle matthieu
  [2.56] Schématisons les interactions entre un internaute et deux sites Web de deux domaines différents !
+++

<hr> 

Chaque requête émise par un client embarque automatiquement certaines informations comme le nom de domaine d'où provient la requête.

+++question write "En déduire comment un site, tel que facebook, arrive à suivre la navigation d'un utilisateur en dehors du site"

+++spoil
  <p>Un internaute consulte un site A, le site A ajoute un cookie dans le navigateur de l'internaute qui contient une chaîne de caractère permettant d'identifier l'internaute.</p>

  <p>Plus tard, l'internaute consulte un site B qui contient un élément Web généré par le site A (comme le bouton j'aime de facebook par exemple). Si cet élément permet de générer une requête vers le site A, alors le serveur du site A va recevoir tous les cookies en lien avec le site A et également (<b>et surtout</b>) le domaine du site B.</p>
  
  <p>Ainsi, le site A qui reçoit la requête identifie l'internaute grâce au cookie et sait dans quel site se trouve actuellement l'internaute</p>
spoil+++

<hr> 

+++bulle matthieu
  [4:32] savez vous ce qu'est google analytic ?
+++

+++bulle matthieu
  que pensez vous de l'affirmation : "si vous n'avez rien à vous reprocher, vous n'avez rien à cacher"
+++