# formulaire de type POST

+++consignes
  Cette activité fait suite à l'activité +++lien "/apprenant/01-__-_NSI-_premiere/05-__-_programmation-_Web-_et-_architecture-_client-_serveur/01-__-_activites/applications-_directes/formulaire-_de-_type-_GET.md" "Formulaire de type GET"
  <ol start="0">
    <li>Dans le dossier du jour, créez un fichier nommé 'formulairePOST.html'</li>
    <li>Ouvrez le fichier avec gedit (éditeur de texte par défaut)</li>
    <li>Parcourir l'activité et répondre aux questions</li>
  </ol>
+++

+++question "Copiez le code source +++lien "/fichiers/AG91FH3389E3AAHE3G9E39BDCD3AG2" "contenu dans cette page" et collez le dans votre fichier 'formulairePOST.html'"

+++question "Modifiez le formulaire contenu dans le fichier 'formulairePOST.html' afin que ce dernier utilise la méthode POST au lieu de GET"

+++question "Ouvrez le fichier 'formulairePost.html' sur votre navigateur préféré. Une fois la page chargée, remplissez le formulaire et validez le"

+++question write "Que remarquez vous dans la barre URL du navigateur maintenant que vous utilisez un formulaire de type POST ?"

+++question "Validez de nouveau le formulaire depuis votre navigateur en saisissant dans le champs de présentation tout le texte présent +++lien "/fichiers/CG91FH3389E3AAHE3G9E39BDCD3AG0" "dans cette page" "

+++question write "Que constatez vous ?"

## Réponses

<ol start="3">
  <li>
  +++spoil
    Contrairement au formulaire de type GET, les informations saisies dans le formulaire (qui est ici de type POST) n'apparaissent plus dans la barre url du navigateur
  spoil+++
  </li>
  <li value="5">
  +++spoil
    Contrairement au formulaire de type GET, saisir des informations volumineuses dans un formulaire type POST ne provoque plus d'erreur
  spoil+++
  </li>
</ol>