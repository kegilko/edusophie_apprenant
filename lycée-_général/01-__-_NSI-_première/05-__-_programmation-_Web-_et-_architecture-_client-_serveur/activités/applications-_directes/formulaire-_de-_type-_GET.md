# formulaire de type GET



+++consignes


  <ol start="0">


    <li>Dans le dossier du jour, créez un fichier nommé 'formulaireGet.html'</li>


    <li>Ouvrez le fichier avec gedit (éditeur de texte par défaut)</li>


    <li>Parcourir l'activité et répondre aux questions</li>


  </ol>


+++



Dans cette activité, vous allez analyser et améliorer un formulaire de création de compte sur un site Web factice. Le formulaire de base se trouve +++lien "/fichiers/DG91FH3389E3AAHE3G9E39BDCD3AG7" "dans ce lien".



+++question "Copiez le code source de la page contenant le formulaire et collez le dans votre fichier 'formulaireGet.html'"



+++question "Ouvrez le fichier 'formulaireGet.html' sur votre navigateur préféré. Une fois la page chargée, remplissez le formulaire et validez le"



Vous devriez obtenir une page ressemblant à ceci : 



+++image "/images/md/H9E2256HB6B76AHD5EGA2B3A2G9BC3"



+++bulle matthieu 


votre formulaire à bien été reçu et traité par le mini-serveur hébergé sur le site edusophie ! 


+++



+++question write "Rangez dans l'ordre les étapes 1 à 9 suivantes qui décrivent ce qui vient de se produire"



<table class="table table-sm">


  <thead>


    <tr>


      <th class="text-nowrap">n° de l'étape</th>


      <th class="col-12">description de l'étape</th>


    </tr>


  </thead>


  <tbody>


    <tr><td class="text-center"><b>00</b></td><td><b>[Début]</b> Le navigateur ouvre le fichier 'formulaireGet.html'</td></tr>


    <tr><td class="text-center">01</td><td>Le client reçoit la réponse du serveur, le navigateur lit le contenu de la réponse du serveur edusophie</td></tr>


    <tr><td class="text-center">02</td><td>Le serveur edusophie envoie la page Web générée au client</td></tr>


    <tr><td class="text-center">03</td><td>Le navigateur prépare une requête HTTPS dans laquelle il met les informations du formulaire qui vient d'être rempli</td></tr>


    <tr><td class="text-center">04</td><td>Le serveur edusophie reçoit la requête de votre navigateur</td></tr>


    <tr><td class="text-center">05</td><td>L'utilisateur valide le formulaire</td></tr>


    <tr><td class="text-center">06</td><td>L'utilisateur remplit le formulaire</td></tr>


    <tr><td class="text-center">07</td><td>Le navigateur envoie à l'url 'https://www.edusophie.com/mini-server' les informations du formulaire rempli</td></tr>


    <tr><td class="text-center">08</td><td>Le serveur edusophie analyse la requête qu'il vient de recevoir. Du contenu de la requête, le serveur extrait les informations du formulaire et également les informations liées au client qui sont automatiquement envoyées avec la requête</td></tr>


    <tr><td class="text-center">09</td><td>Le serveur edusophie génère une page Web en incluant dedans les informations qu'il a extraites</td></tr>


    <tr><td class="text-center"><b>10</b></td><td><b>[Fin]</b> Le navigateur affiche la page Web générée par le serveur edusophie</td></tr>


  </tbody>


</table>


  


<table class="table table-sm">


  <thead>


    <tr>


      <th colspan="11" class="text-center">ordonnancement des étapes</th>


    </tr>


  </thead>


  <tbody>


    <tr class="text-center">


      <td class=""><b>00</b></td>


      <td>??</td>


      <td>??</td>


      <td>??</td>


      <td>??</td>


      <td>??</td>


      <td>??</td>


      <td>??</td>


      <td>??</td>


      <td>??</td>


      <td><b>10</b></td>


    </tr>


  </tbody>


</table>


  


  


+++question write "Après avoir validé le formulaire et reçu la réponse du serveur, quelles informations en rapport avec le formulaire se trouvent dans la barre url de votre navigateur ? comment est 'traduit' la caractère '@' ?"



+++question write "En analysant le code HTML de votre fichier 'formulaireGet.html', expliquez à quoi sert la balise <span class="text-nowrap"><​textarea></span> ?"



+++question write "En analysant le code HTML de votre fichier 'formulaireGet.html', expliquez à quoi sert l'attribut 'name' qui se trouve dans certaines balises"



+++spoil "Aide"


  Modifiez le contenu de cet attribut et validez le formulaire pour essayer de voir les différences


spoil+++



+++question "Validez de nouveau le formulaire depuis votre navigateur en saisissant dans le champs de présentation tout le texte présent +++lien "/fichiers/CG91FH3389E3AAHE3G9E39BDCD3AG0" "dans cette page" "



+++question write "Que constatez vous ?"



<hr>



Mettons de côté le champ permettant de rédiger sa présentation. Remplissez par la suite ce champ avec un texte court.



Actuellement, le formulaire permet à l'utilisateur de saisir son pseudo et sa présentation. Nous souhaitons maintenant permettre à l'utilisateur de saisir en plus un mot de passe à travers le formulaire (en règle générale, lorsqu'on créé un compte en ligne, on doit renseigner un mot de passe).



+++question write "Ajoutez un input de type password dans le formulaire. Quel code HTML avez vous du ajouter ?"



+++spoil "Aide"


  Aidez vous +++lien "/apprenant/01-__-_NSI-_premiere/01-__-_Introduction-_en-_informatique-_fondamentale-_et-_introduction-_en-_programmation-_Web-_cote-_client/00-__-_cours/01-__-_Introduction-_en-_programmation-_Web-_cote-_client.md" "du premier cours de rappel sur le langage HTML" et/ou de recherches sur Internet


spoil+++



Voici un exemple de résultat attendu où on voit l'ajout du champ password 



+++image "/images/md/7GB3GGGBG35HH584FD662B34G3E298" col-6 mx-auto



+++question "Depuis votre navigateur, remplissez tous les champs du formulaire et validez le"



+++question write "Suite à l'ajout du champ permettant de saisir son mot de passe, en quoi le contenu de la barre url de votre navigateur peut poser des problèmes de sécurité après avoir validé le formulaire ?"



## Réponses



<ol start="2">


  <li>


  +++spoil


    Le bon ordre est le suivant :


    


    <table class="table table-sm my-4">


      <thead>


        <tr>


          <th class="text-nowrap">n° de l'étape</th>


          <th class="col-12">description de l'étape</th>


        </tr>


      </thead>


      <tbody>


        <tr><td class="text-center"><b>00</b></td><td><b>[Début]</b> Le navigateur ouvre le fichier 'formulaireGet.html'</td></tr>


        <tr><td class="text-center">06</td><td>L'utilisateur remplit le formulaire</td></tr>


        <tr><td class="text-center">05</td><td>L'utilisateur valide le formulaire</td></tr>


        <tr><td class="text-center">03</td><td>Le navigateur prépare une requête HTTPS dans laquelle il met les informations du formulaire qui vient d'être rempli</td></tr>


        <tr><td class="text-center">07</td><td>Le navigateur envoie à l'url 'https://www.edusophie.com/mini-server' les informations du formulaire rempli</td></tr>


        <tr><td class="text-center">04</td><td>Le serveur edusophie reçoit la requête de votre navigateur</td></tr>


        <tr><td class="text-center">08</td><td>Le serveur edusophie analyse la requête qu'il vient de recevoir. Du contenu de la requête, le serveur extrait les informations du formulaire et également les informations liées au client qui sont automatiquement envoyées avec la requête</td></tr>


        <tr><td class="text-center">09</td><td>Le serveur edusophie génère une page Web en incluant dedans les informations qu'il a extraites</td></tr>


        <tr><td class="text-center">02</td><td>Le serveur edusophie envoie la page Web générée au client</td></tr>


        <tr><td class="text-center">01</td><td>Le client reçoit la réponse du serveur, le navigateur lit le contenu de la réponse du serveur edusophie</td></tr>


        <tr><td class="text-center"><b>10</b></td><td><b>[Fin]</b> Le navigateur affiche la page Web générée par le serveur edusophie</td></tr>


      </tbody>


    </table>



    <table class="table table-sm">


      <thead>


        <tr>


          <th colspan="11" class="text-center">ordonnancement des étapes</th>


        </tr>


      </thead>


      <tbody>


        <tr class="text-center">


          <td class=""><b>00</b></td>


          <td>06</td>


          <td>05</td>


          <td>03</td>


          <td>07</td>


          <td>04</td>


          <td>08</td>


          <td>09</td>


          <td>02</td>


          <td>01</td>


          <td><b>10</b></td>


        </tr>


      </tbody>


    </table>



  spoil+++


  </li>


  <li>


  +++spoil


    Dans la barre URL se trouve les données saisies auparavant dans le formulaire. Le caractère @ de l'adresse email est traduit par %40 dans la barre url.


  spoil+++


  </li>


  <li>


  +++spoil


    La balise HTML textarea génère sur le navigateur un champ dans lequel nous pouvons saisir du texte sur plusieurs lignes


  spoil+++


  </li>


  <li>


  +++spoil


    La valeur de l'attribut permet du côté du serveur d'identifier chacun des champs du formulaire. Il est donc important de renseigner cet attribut dans les éléments du formulaire afin d'envoyer correctement les données aux serveur après validation du formulaire.


  spoil+++


  </li>


  <li value="7">


  +++spoil


    La page ne se charge pas, une erreur semble avoir été déclenchée


  spoil+++


  </li>


  <li>


  +++spoil


    Voici un exemple de code permettant d'ajouter un champ de saisi de mot de passe dans un formulaire : 


      <pre><code><​input type='password' placeholder='ajoutez un mot de passe'></code></pre>


  spoil+++


  </li>


  <li value="10">


  +++spoil


    Le mot de passe saisi par l'utilisateur se retrouve en clair, et à la vue de tous, dans la barre url du navigateur. Une personne malveillante peut donc d'un simple coup d'œil voir le mot de passe utilisé par l'utilisateur lors de la création de son compte.


  spoil+++


  </li>


</ol>