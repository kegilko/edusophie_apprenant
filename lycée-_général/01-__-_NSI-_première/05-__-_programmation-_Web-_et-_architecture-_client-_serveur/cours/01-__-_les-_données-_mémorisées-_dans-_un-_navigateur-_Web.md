# Les données mémorisées dans un navigateur Web

+++programme
  Distinguer ce qui est mémorisé dans le client et retransmis au serveur
  Distinguer ce qui est exécuté sur le client ou sur le serveur et dans quel ordre
  
  Créer et supprimer manuellement des cookies
  Utiliser le localstorage d'un navigateur
+++

+++sommaire 1

## Introduction

Lors des interactions entre un client et un serveur **des données peuvent être enregistrées** du côté sur **serveur** (bases de données) et d'autres du côté du **client** (navigateur Web).

Dans cette séance, nous analysons deux manières d'enregistrer des données côté client (donc côté navigateur), dont la première qui utilise un objet informatique très connu : **les cookies**

## Les cookies

+++bulle matthieu
qu'est-ce qu'un cookie ?
+++

+++bulle matthieu droite
comment un cookie permet de tracer les comportements d'un internaute ?
+++

+++activité classe 30 ../01-__-_activites/applications-_directes/comment-_fonctionne-_un-_cookie.md "Comment fonctionne un cookie"

### +++emoji-write Résumons 

- Un cookie est une information enregistrée dans un navigateur Web
- Les cookies permettent d'authentifier un internaute tout au long de sa navigation sur un site
- Les cookies permettent de sauvegarder des données côté client et ainsi soulager le réseau et le travail du serveur (exemple : lorsqu'un utilisateur ajoute des produits dans un panier dans un site e-commerce)
- Les cookies permettent de tracer les navigations des utilisateurs afin d'analyser leur comportement. Cela permet par exemple de générer des publicité ciblées.

### Manipuler des cookies sur son navigateur

La plupart des navigateurs Web mettent à disposition un Débogueur (le raccourcis clavier en général est F12 ou fn + F12). Dedans, nous pouvons facilement visualiser et modifier  les cookies que notre navigateur a enregistré   

+++bulle matthieu
avant de démarrer l'activité qui suit, accordons nous sur le navigateur que l'on va utiliser tous ensemble
+++

+++activité binome 40 ../01-__-_activites/applications-_directes/creation-_et-_suppression-_de-_cookie.md "Création et suppression de cookie"

### +++emoji-write Résumons 

<ul> 
  <li>Le débogueur du navigateur permet de visualiser les données enregistrées sur notre navigateur Web</li>
  <li>Plusieurs types de données peuvent être enregistrées comme par exemple sous forme de cookie ou dans le localstorage</li>
  <li>Le localstorage n'est jamais envoyé au serveur (contrairement au cookie), ce qui présente un inconvénient et plusieurs avantages : <ul>
    <li class="text-red">il ne permet donc pas d'identifier un utilisateur au fil de sa navigation dans un même site</li>
    <li class="text-green">comme le localstorage n'est jamais transmis au serveur, cela soulage le réseau et les traitements du serveur</li>
    <li class="text-green">de plus, toujours dû au fait que le localstorage ne peut être envoyé au serveur, son contenu ne peut donc pas être utilisé pour tracer la navigation d'un utilisateur à travers le Web contrairement au cookie</li>
  </ul> 
  </li><li>Le localstorage sert à sauvegarder des données liées aux préférences de l'internaute : le panier d'achat dans un site e-commerce, le thème light ou night choisi par l'internaute, etc ...</li>
</ul> 