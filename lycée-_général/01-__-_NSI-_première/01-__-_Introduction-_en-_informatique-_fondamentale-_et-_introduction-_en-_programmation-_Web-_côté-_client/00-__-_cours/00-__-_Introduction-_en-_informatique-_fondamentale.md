# Introduction en informatique fondamentale

+++programme
  Distinguer les rôles et les caractéristiques des différents constituants d’une machine
  Dresser la table d’une expression booléenne
  
  Connaître l´architecture que la plupart des ordinateurs (au sens large) utilisent aujourd´hui
  Utiliser les expressions logiques et (and), ou (or), non (not) 
+++

+++sommaire

## Introduction

+++imageFloat https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/EDVAC.png/185px-EDVAC.png
  En 1945 est conçu EDVAC (<b>E</b>lectronic <b>D</b>iscrete <b>V</b>ariable <b>A</b>utomatic <b>C</b>omputer) un des premier ordinateurs électronique livré quelques années plus tard à un centre de recherche militaire américain. 
+++

+++imageFloat https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/JohnvonNeumann-LosAlamos.jpg/185px-JohnvonNeumann-LosAlamos.jpg droite
  John von Neumann (1903-1957), mathématicien et physicien, apporta au projet une architecture informatique nouvelle pour organiser les composants de l'ordinateur
+++

### L'architecture Von Neumann

<style>
  .cblue{font-weight:bold; color: blue}
  .cred{font-weight:bold; color: red}
  .cgreen{font-weight:bold; color: green}
  .corange{font-weight:bold; color: orange}
</style>

L'architecture proposée par von Neumann décrit un ensemble de composants interconnectés. <span class="cgreen">Une unité centrale (UC, ou CPU pour Central Processing Unit) chargée de contrôler et de traiter/calculer des données</span>. L'unité centrale travaille avec des données, <span class="cred">données qui sont en partie stockées dans une mémoire</span>. Le traitement de l'unité centrale est amorcé par <span class="cblue">un dispositif d'entrée</span>, le résultat du traitement et ensuite transmis à un <span class="corange">dispositif de sortie</span>.

+++diagramme col-8 mx-auto
  flowchart LR
    subgraph AA[" "]
      direction TB
  
      P["Unité centrale de contrôle<br> et de calcul"]
      
      M["Mémoire"]
      P-->M
      M-->P
      
    end
    A["Entrée"]
    A-->AA
    B["Sortie"]
    AA-->B
    
    style A stroke:blue,stroke-width:5px
    style P stroke:green,stroke-width:5px
    style M stroke:red,stroke-width:5px
    style B stroke:orange,stroke-width:5px
    
+++

+++bulle matthieu
  Et malgré toutes les révolutions et avancées technologiques, cette architecture reste toujours très largement utilisée aujourd'hui ! 
+++

+++diagramme col-8 mx-auto
  flowchart LR
    subgraph O[" "]
      direction TB
  
      P[Processeur<br><img src="https://cdn.pixabay.com/photo/2020/06/02/12/20/amd-ryzen-cpu-5250770_1280.jpg" width="150px">]
      
      M[Disque dur, mémoire vive, ...<br><img src="https://upload.wikimedia.org/wikipedia/commons/0/0a/Plain-wrap_Hard_disk_drive_and_Memory.jpg" width="150px">]
      P-->M
      M-->P
      
    end
    A[Clavier, souris, ...<br><img src="https://cdn.pixabay.com/photo/2018/07/07/16/42/keyboard-3522460_1280.jpg" width="150px">]
    A-->O
    B[Écran<br><img src="https://cdn.pixabay.com/photo/2013/07/13/01/20/monitor-155565_1280.png" width="150px">]
    O-->B
    
    style A stroke:red,stroke-width:5px
    style P stroke:blue,stroke-width:5px
    style M stroke:green,stroke-width:5px
    style B stroke:orange,stroke-width:5px
+++

+++bulle matthieu
  Quels autres dispositifs d'entrée et de sortie connaissez vous ? 
+++

+++bulle matthieu droite
  Quelle(s) serait(ent) la/les différence(s) avec un smartphone ? 
+++


<br>
### Résumons +++emoji-writing_hand 

- L'architecture von Neumann décrit les principaux composants (ainsi que leurs interactions) qui composent les ordinateurs (au sens large)
- Cette architecture est toujours aujourd'hui largement utilisée dans la plupart des ordinateurs (au sens large : ordinateur fixe, ordinateur portable, serveur, consoles de jeux, appareils électroménager, ...) 
- Dans une architecture von Neumann : 
    - Les dispositifs entrée-sortie permettent de communiquer avec le monde extérieur
    - L'unité centrale (le processeur ou CPU sur ordinateur) est le coeur de l'architecture. Ce composant contrôle et calcul des traitements
    - Un ensemble de composants "mémoire" permet de stocker des données qui seront utilisées par le CPU

## Au cœur des processeurs

![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/Intel_Pentium_Processor_%28backside%29_with_heat_sink.jpg/475px-Intel_Pentium_Processor_%28backside%29_with_heat_sink.jpg)

L'unité centrale de contrôle et de calcul de l'architecture von Neumann est, de nos jours, incarnée dans un composant électronique nommé 'processeur'. L'invention du processeur est le fruit de recherches et d'inventions provenant de domaines variées et de différentes époques. Ci-dessous, les **principales** étapes de ce long chemin d'évolutions technologiques :

+++imageFloat https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/George_Boole_color.jpg/180px-George_Boole_color.jpg
  George Boole (1815-1864), mathématicien et philosophe, établit une manière de traduire des idées et des concepts en expressions et équations. Il fonda un nouveau domaine des Mathématiques : la logique moderne.
+++
  
+++imageFloat https://upload.wikimedia.org/wikipedia/commons/b/bf/Replica-of-first-transistor.jpg droite
  En 1947, une équipe de chercheurs physiciens (John Bardeen, William Shockley et Walter Brattain) créa un nouveau composant électronique : le transistor
+++

+++imageFloat https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Integrated_circuit_on_microchip.jpg/320px-Integrated_circuit_on_microchip.jpg
Les transistors peuvent être combinés au sein d'une puce électronique afin de générer en sortie un flux électrique qui dépend des flux électriques entrant
+++

Les transistors permettent alors de mettre en application les travaux de George Boole. En d'autres termes, les transistors permettent d'exprimer des idées et des concepts à travers des courants électriques.

<div class="imageFloatGenerator align-items-center d-flex mb-3 mt-4 col-12 col-lg-6 col-print-8 col-xxl-6">
  <div class="imageBlock">
    <iframe src="https://www.youtube-nocookie.com/embed/Fxv3JoS1uY8" title="YouTube video player" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" width="200" height="" frameborder="0"></iframe>
  </div>
  <div>
    <div class="mx-3 mb-0 content">Les processeurs sont des composants électroniques contenant un ensemble de transistors. Et grâce aux nanosciences, les processeurs peuvent contenir des millions de transistors !</div>
    <div class="authorAndSource ms-2"></div>
  </div>
</div>

+++activité binome 40 ../01-__-_activites/00-__-_applications-_directes/algebre-_booleenne-_et-_tables-_de-_verite.md "Algèbre booléenne et tables de vérité"
    <ul>
        <li>Faire l'exercice mis en lien</li>
        <li>Si l'exercice est terminé en avance, dissolvez votre groupe. Chaque membre du groupe vient en aide auprès d'un autre binôme</li>
        <ul>
            <li><b class="text-red">Attention :</b> avant dissolution, chaque membre du groupe doit être capable de faire l'exercice seul</li>
            <li><b class="text-red">Attention :</b> respectez bien <b>la charte d'entraide</b></li>
        </ul>
    </ul>
+++

### Résumons +++emoji-writing_hand 

- L'algèbre de Boole défini des opérations qui s'appliquent sur des nombres binaires (0 ou 1)
- Les principales opérations de l'algèbre de Boole sont : not, and, or
- Les tables de vérités de ces opérations sont celles-ci : 

<div class="d-flex">
<table class="table table-fit">
  <thead>
    <tr class="text-center">
      <th colspan="2">Table de vérité "not"</th>
    </tr>
    <tr>
      <th>A</th>
      <th>not A</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
    </tr>
  </tbody>
</table>

<table class="table table-fit">
  <thead>
    <tr class="text-center">
      <th colspan="3">Table de vérité "or"</th>
    </tr>
    <tr>
      <th>A</th>
      <th>B</th>
      <th>A or B</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>0</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>1</td>
      <td>1</td>
    </tr>
  </tbody>
</table>

<table class="table table-fit">
  <thead>
    <tr class="text-center">
      <th colspan="3">Table de vérité "and"</th>
    </tr>
    <tr>
      <th>A</th>
      <th>B</th>
      <th>A and B</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>1</td>
      <td>1</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>