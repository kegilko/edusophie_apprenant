# Le Web, rappels de seconde

<div class="text-center">
<iframe src="https://player.vimeo.com/video/138623558?h=0e8bb44791&color=b50067&title=0&byline=0&portrait=0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen="" width="640" height="360" frameborder="0"></iframe>
</div>

<br>
+++question write "Après avoir visionné la vidéo ci-dessus et en vous aidant éventuellement de recherches supplémentaires sur Internet, répondez aux questions suivantes :"
0. Qu'est-ce qu'un protocole en informatique ?
1. Qu'est-ce qu'un serveur en informatique ?
2. Quel type de logiciel, qui permet de consulter des pages Web, est utilisé du côté de l'ordinateur client ?
3. Quels sont les deux protocoles utilisés nativement par ces logiciels ?

+++question write "Recopiez grossièrement le schéma ci-dessous (sans les images ;) ) et remplaces les '???' avec les phrases suivantes :"
- 'navigateur Internet'
- '0: je clic sur un lien'
- '1: envoie une requête HTTP ou HTTPS'
- '2: construit une réponse'
- '3: envoie la réponse'
- '4: le navigateur Internet interprète la réponse et l'affiche à l'écran'

<div class="text-center fs-5"><b>Architecture Client-Serveur dans un contexte Web</b></div>

+++diagramme
  flowchart LR

    subgraph S[Serveur]
      H[serveur applicatif]
    end
    
    subgraph C[Client]
      N[<img src="https://i.ibb.co/1RYy30C/browsers-1265309-640.png" width="150"><br>???<br><br>Étape ???<br>Étape ???]
    end
    
    S-->|Étape ???|C
    C-->|Étape ???|S
    H-->|Étape ???|H
+++


## Résultat

<ol start="0">
  <li>
  +++spoil
    <ol>
      <li>Un protocole informatique est un ensemble de règles qui permet à deux ordinateurs (au sens large) de pouvoir échanger des données, autrement dit, de pouvoir communiquer</li>
      <li>Un serveur en informatique est un ordinateur qui est configuré pour se mettre à l'écoute de requêtes clientes. Lorsque qu'une requête est reçu, l'ordinateur serveur la traite et revoit un résultat à l'ordinateur client</li>
      <li>Du côté de l'ordinateur client, un navigateur Internet permet de dialoguer avec des serveurs et permet d'afficher le résultat envoyé par l'ordinateur serveur</li>
      <li>Un protocole informatique est un ensemble de règles qui permet à deux ordinateurs (au sens large) de pouvoir échanger des données, autrement dit, de pouvoir communiquer</li>
    </ol>
  spoil+++
  </li>
  <li>
  +++spoil
+++diagramme
  flowchart LR

    subgraph S[Serveur]
      H[serveur applicatif]
    end
    
    subgraph C[Client]
      N[<img src="https://i.ibb.co/1RYy30C/browsers-1265309-640.png" width="150"><br>navigateur Internet<br><br>Étape 0. je clic sur un lien<br>Étape 4. le navigateur Internet interprète la réponse et l'affiche à l'écran]
    end
    
    C-->|Étape 1: envoie une requête HTTP ou HTTPS|S
    H-->|Étape 2: construit une réponse|H
    S-->|Étape 3: envoie la réponse|C
+++
  spoil+++
  </li>
</ol>
