# La balise interactive input

Le but de cette activité est de découvrir ensemble une documentation Internet qui décrit comment utiliser la balise `<input>`. 

Avant cela, vous devez apprendre à créer votre propre page Web pour pouvoir ensuite tester les différents comportement possible de la balise `<input>`.

## Préparer son environnement de travail

Pour rappel, un navigateur Web sait nativement lire un fichier texte. Il suffit donc, lorsqu'on débute en programmation Web, d'ouvrir un éditeur de texte basique. 

- Sur windows, je vous conseille d'installer et d'utiliser le logiciel libre notepad++ [téléchargeable ici](https://notepad-plus-plus.org/downloads/)
- Sur Mac, je vous conseille d'utiliser le logiciel libre TextMate [téléchargeable ici](https://macromates.com)
- Sur Linux, je vous conseille d'utiliser l'éditeur de texte installé de base qui est `gedit` (nommé aussi 'Éditeur de texte')

+++bulle matthieu
  n'utilisez surtout pas des logiciels tels que microsoft word ou encore open office write qui ne sont pas du tout adaptés pour faire de la programmation Web !
+++

Ouvrez votre éditeur de texte et recopiez dedans le squelette HTML suivant : 

<img class="rounded text-center col-6" src="https://i.ibb.co/KGXLRFm/carbon-1.png">

+++bulle matthieu
  Faîtes bien attention aux fermetures des balises !
+++

Une fois le code recopié, sauvegardez le fichier sur votre ordinateur en précisant qu'il s'agit d'un fichier .html

+++bulle matthieu
  par convention, la page principale d'un site Web est nommée 'index.html', donc sauvegardez votre fichier avec ce nom :)
+++

Une fois le fichier enregistré sur votre disque, vous remarquerez que la coloration syntaxique s'est activée sur votre code. Comme vous avez sauvegardé votre fichier en précisant qu'il s'agit du format html, votre éditeur a automatiquement activité la coloration syntaxique adaptée pour du code html.

Maintenant, testez si votre page Web fonctionne bien ! Il vous suffit d'ouvrir le dossier où est enregistré votre fichier 'index.html' et de cliquer puis déplacer le fichier sur la fenêtre de votre navigateur Web. Votre navigateur Web va ouvrir et lire le code contenu dans votre fichier et afficher sur votre écran :

<img class="rounded text-center col-6" src="https://i.ibb.co/wrS6RbB/Screenshot-from-2021-09-30-13-49-35.png">

## Plongeon dans la documentation

Il existe une multitude de site Internet proposant de la documentation, plus ou moins exhaustive, sur le langage HTML. Un des sites les mieux fournis est le site https://developer.mozilla.org . Dirigez vous [sur la page qui documente la balise `<input>`](https://developer.mozilla.org/fr/docs/Web/HTML/Element/Input).

+++bulle matthieu
  Ne prenez pas peur sur la longueur de la page, nous n'allons n'en exploiter qu'une petite partie dans cette activité ;)
+++
<br>

La partie qui nous intéresse dans la documentation est "Les différents types de champs `<input>`". Voici ce qu'on peut y lire (et qui reprends ce qui a été marqué dans le cours): 

<img class="rounded text-center" src="https://i.ibb.co/z7ZGMqD/Screenshot-from-2021-09-30-14-00-44.png">

Et en dessous de ce paragraphe est listé l'ensemble des valeurs possibles que l'attribut 'type' peut prendre dans la balise input. Vous pouvez cliquer sur ces éléments pour voir des exemples de code HTML qui les utilisent. 

+++bulle matthieu
vous verrez sans doute dans les exemples des balises nouvelles que vous n'avez encore jamais vu
+++

+++bulle matthieu droite
tester les codes, modifiez les balises, recherchez si besoin dans la documentation ou sur Internet à quoi elles correspondent :)
+++


Ainsi, si je veux ajouter dans ma page Web un champs de saisie 'age' et un bouton pour valider l'âge que je saisis, il suffit que j'y ajoute deux balises `<input>` avec le bon type dans la balise body de ma page, le code de votre page devrait ressembler à ceci : 

<img class="rounded text-center col-6" src="https://i.ibb.co/8YkYksB/carbon-2.png">

Une fois votre code mis à jour, rechargez la page de votre navigateur Web, il devrait vous afficher ceci : 

<img class="rounded text-center col-6" src="https://i.ibb.co/hchQtvL/Screenshot-from-2021-09-30-14-22-52.png">

+++bulle matthieu
cliquer sur le bouton 'Valider' n'a aucun effet pour l'instant. Nous nous intéresserons dans un cours futur sur comment configurer l'envoi d'informations vers un serveur
+++

+++bulle matthieu droite
pour l'instant, nous ne nous préoccupons pas de cette partie
+++

## À vous de jouer ! 

En vous aidant de la documentation (**et de recherches sur Internet si besoin**), améliorez le code de votre page en implémentant les fonctionnalités suivantes afin que votre page ressemble à ceci : 

- fonctionnalité 1 : ajoutez dans votre page un champ 'input' avec le bon type pour permettre à l'utilisateur de saisir un mot de passe

+++bulle matthieu
  <b>attention</b>. lorsque l'utilisateur saisit un mot de passe, les caractères ne doivent pas être visibles en clair sur l'écran
+++

+++bulle matthieu droite
  ceci pour éviter qu'une autre personne qui passe derrière l'écran puisse lire le mot de passe
+++

+++question write "Quel est le type de 'input' qui permet de faire ça ?""

- fonctionnalité 2 : ajoutez dans votre page un champ 'input' avec le bon type pour permettre à l'utilisateur de cocher, dans une petite liste de hobbies proposés, les hobbies qu'il possède

+++question write "Quel est le type de 'input' qui permet de faire ça ?""

- fonctionnalité 3 : ajoutez dans votre page un champ 'input' avec le bon type pour permettre à l'utilisateur de sélectionner, dans une liste de niveaux de classe proposés, le niveau de classe dans lequel est l'utilisateur (par exemple, donner le choix entre : seconde, première ou terminal)

+++bulle matthieu
  <b>attention</b>. l'utilisateur ne doit pouvoir sélectionner qu'un seul niveau
+++

+++question write "Quel est le type de 'input' qui permet de faire ça ?"

## Réponses

<ol start="0">
  <li>
  +++spoil
    Il faut utiliser une balise input avec 'password' dans l'attribut 'type': <br>
      <pre><code><input type="password"></code></pre>
      
    Voici un exemple de code qui contient le strict minimum nécessaire : 
    <img class="rounded text-center col-8 offset-2" src="https://i.ibb.co/CsvYXpc/carbon-9.png">
  spoil+++
  
  </li>
  <li>
  +++spoil
    Il faut utiliser une balise input avec 'checkbox' dans l'attribut 'type': <br>
      <pre><code><input type="checkbox"></code></pre>
    Les exemples de la documentation utilisent en plus la balise 'label' qui permet d'ajouter un texte avec la checkbox
    
    Voici un exemple de code qui contient le strict minimum nécessaire : 
    <img class="rounded text-center col-8 offset-2" src="https://i.ibb.co/XzHJMK0/carbon-10.png">
  spoil+++
  </li>
  <li>
  +++spoil
    Il faut utiliser une balise input avec 'radio' dans l'attribut 'type': <br>
      <pre><code><input type="radio"></code></pre>
    
    <b>Attention !</b> pour lier plusieurs boutons 'radio' entre eux, il faut qu'ils utilisent l'attribut 'name' avec la même valeur.
    
    Par exemple, en utilisant l'attribut 'name' avec des valeurs différentes entre les boutons 'radio', je peux sélectionner les 3 à la fois : (comportement non voulu)
    <br><br>
    <input type="radio" name="toto"><label>seconde</label>
    <input type="radio" name="tata"><label>premiere</label>
    <input type="radio" name="titi"><label>terminale</label>
    <br>
    Le code : <br>
    <img class="rounded offset-1 col-10" src="https://i.ibb.co/XJ1H9Rt/carbon-12.png">
    
    <br><br>
    Par contre, si j'utilise le même 'name' entre 3 boutons 'radio', en cocher un décochera les autres : 
        <br><br>
    <input type="radio" name="radio_niveau"><label>seconde</label>
    <input type="radio" name="radio_niveau"><label>premiere</label>
    <input type="radio" name="radio_niveau"><label>terminale</label>
    <br>
    Le code : <br> 
    <img class="rounded offset-1 col-10" src="https://i.ibb.co/8r2SBMB/carbon-11.png">
    
  spoil+++
  </li>
</ol>

+++spoil "Code final de la page"
  <br>
  Voici un exemple de code de page qui ne contient que le strict nécessaire : 
  <img class="rounded offset-1 col-10" src="https://i.ibb.co/xgRZvxz/carbon-13.png">
spoil+++