# S'entraîner sur les tables de vérité

Ce document liste une série d'exercices rapides consacrés à l'utilisation des tables de vérités. La difficulté des exercices augmente au fur et à mesure.

Certains exercices exposent du code python. Exécutez le code dans votre IDE vous donne la réponse de l'exercice, ainsi, <span class="text-red"><b>n'exécutez pas le code python tant que vous n'avez pas travaillé l'exercice</b></span>. Exécutez le code dans votre IDE si vous souhaitez vérifier votre réponse.

+++question "Complétez la table de vérité suivante : not (A or B)"

<table class="table table-fit text-center">
  <thead>
    <tr>
      <th class="text-red">A</th>
      <th class="text-green">B</th>
      <th style="color:#A4A452">A or B</th>
      <th><u>not (<span style="color:#A4A452">A or B</span>)</u></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>0</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>0</td>
      <td>1</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>1</td>
      <td>1</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

+++spoil "Aide"
  
  Voici ci-dessous la première ligne du tableau remplie : 
  
<table class="table table-fit text-center">
  <thead>
    <tr>
      <th class="text-red">A</th>
      <th class="text-green">B</th>
      <th style="color:#A4A452">A or B</th>
      <th><u>not (<span style="color:#A4A452">A or B</span>)</u></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
  
  <br>
  <p><b>Attention ! </b> la table de vérité '<span style="color:#A4A452">A or B</span>' est à connaître pour le contrôle ! (en plus des tables 'A and B' et 'not A')</p>
  <p>Ensuite 'not (<span style="color:#A4A452">A or B</span>)' retourne l'inverse de '<span style="color:#A4A452">A or B</span>'. Puisque '<span style="color:#A4A452">A or B</span>' donne <b>0</b> (pour <span class="text-red">A</span> = 0 et <span class="text-green">B</span> = 0) alors 'not (<span style="color:#A4A452">A or B</span>)' retourne l'inverse de <b>0</b>, c'est à dire <b>1</b></p>
  
spoil+++

+++question "En vous aidant de la table de vérité ci-dessus, déterminez quel message sera affiché dans la console après l'exécution du programme ci-dessous (rappel : <b>n'exécutez le code dans un IDE que pour vérifier votre résultat, pas avant</b>)"

```python
# création d'une commande
plat_principal = True
dessert = False

if(not(plat_principal or dessert)):
  print("commande annulée !")
else:
  print("commande effectuée !")
```

+++spoil "Aide"
  
  <p>Pour rappel : 
    </p><ul>
      <li>l'élément 0 correspond à une absence de signal électrique et l'élément 1 correspond à la présence d'un signal électrique</li>
      <li>En programmation, 0 correspond donc à <code>false</code> (<code><b>F</b>alse</code> en python) et 1 à <code>true</code> (<code><b>T</b>rue</code> en python). Et lors d'une instruction</li>
      <li>Pour terminer, pour rentrer dans un <code>if</code>, un <code>elif</code>, un <code>while</code> ou encore un <code>for</code>, il faut que la condition utilisée par l'instruction (<code>if</code> dans cet exercice) soit vraie (= True)</li>
    </ul>
  <p></p>
  
spoil+++

+++question "Complétez la table de vérité suivante : not A and not B"

<table class="table table-fit text-center">
  <thead>
    <tr>
      <th class="text-blue">A</th>
      <th class="text-red">B</th>
      <th style="color:#A4A452">not A</th>
      <th style="color:#50A7B0">not B</th>
      <th><u><span style="color:#A4A452">not A</span> and <span style="color:#50A7B0">not B</span></u></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>0</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>0</td>
      <td>1</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>1</td>
      <td>1</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

+++spoil "Aide"
  
  Voici ci-dessous la première ligne du tableau remplie : 
  
<table class="table table-fit text-center">
  <thead>
    <tr>
      <th class="text-blue">A</th>
      <th class="text-red">B</th>
      <th style="color:#A4A452">not A</th>
      <th style="color:#50A7B0">not B</th>
      <th><u><span style="color:#A4A452">not A</span> and <span style="color:#50A7B0">not B</span></u></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
    </tr>
  </tbody>
</table><br>

  <p>Dans la première ligne du tableau, <span class="text-blue">A</span> est égal à 0 et <span class="text-red">B</span> est égal à 0 (aucun courant dans les deux entrées). Donc '<span style="color:#A4A452">not A</span>' devient '<span style="color:#A4A452">not 0</span>' et dans la table de vérité 'not', 'not 0' est égal à 1. Pareil pour '<span style="color:#50A7B0">not B</span>', '<span style="color:#50A7B0">not B</span>' devient '<span style="color:#50A7B0">not 0</span>' et donc retourne l'inverse de 0, c'est à dire 1</p>
  
  <p>Pour terminer, concernant la dernière colonne du tableau '<span style="color:#A4A452">not A</span> and <span style="color:#50A7B0">not B</span>', puisque '<span style="color:#A4A452">not A</span>' retourne 1 et '<span style="color:#50A7B0">not B</span>' retourne 1, alors '<span style="color:#A4A452">not A</span> and <span style="color:#50A7B0">not B</span>' devient '<span style="color:#A4A452">1</span> and <span style="color:#50A7B0">1</span>', et '<span style="color:#A4A452">1</span> and <span style="color:#50A7B0">1</span>' retourne 1 d'après la table de vérité 'and'</p>
  
spoil+++

+++spoil "Aide 2"
  
  Si vous voulez vérifier les réponses que vous avez écrites dans le tableau, vous pouvez directement tester l'équivalent de '<span style="color:#A4A452">not A</span> and <span style="color:#50A7B0">not B</span>)' avec des transistors à travers l'outil vu en cours (<a href="https://logic.ly/demo/" target="_blank">https://logic.ly/demo/</a>), '<span style="color:#A4A452">not A</span> and <span style="color:#50A7B0">not B</span>' correspond à : <br>
  
  <img class="mx-auto text-center d-block" src="/images/md/EF74CC98F6AB87D5C887BCDDG3CD7G">
  
  <br> <b>Attention !</b> cela vous permet de vérifier vos réponses dans le tableau, l'outil ne sera pas disponible pendant le contrôle ;)
  
spoil+++

+++question "Deux tables de vérités sont équivalentes si et seulement si les résultats obtenus avec ces deux tables sont égaux. D'après les résultats des questions 0 et 2, est-ce que la table 'not (A or B)' est équivalente à la table 'not A and B' ?"

Question posée autrement : est-ce qu'avec les mêmes valeurs en entrée (les entrées A et B) on obtient les mêmes résultats sur les deux tables ? 

+++question "Complétez la table de vérité suivante : C and (A or B)"

<table class="table table-fit text-center">
  <thead>
    <tr>
      <th class="text-blue">A</th>
      <th class="text-red">B</th>
      <th class="text-green">C</th>
      <th style="color:#A4A452">A or B</th>
      <th><u><span class="text-green">C</span> and (<span style="color:#A4A452">A or B</span>)</u></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

+++spoil "Aide"
  
  Voici ci-dessous la deuxième et la quatrième ligne du tableau remplie : 
  
<table class="table table-fit text-center">
  <thead>
    <tr>
      <th class="text-blue">A</th>
      <th class="text-red">B</th>
      <th class="text-green">C</th>
      <th style="color:#A4A452">A or B</th>
      <th><u><span class="text-green">C</span> and (<span style="color:#A4A452">A or B</span>)</u></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
  
spoil+++

<hr>

Ci-dessous figure un programme que <b>vous pouvez</b> si vous le souhaitez <b>exécuter dès le début de l'exercice</b> (pour comprendre son fonctionnement)

widget python
import random

niveauDeFaim = 5
niveauDeSoif = 7
morduParUnSerpent = False

while(niveauDeFaim > 0 and niveauDeSoif > 0 and not morduParUnSerpent):
  
  print("le jeu continue...")

  niveauDeFaim -= 1 # strictement équivalent à : niveauDeFaim = niveauDeFaim - 1
  
  niveauDeSoif -= 2 # strictement équivalent à : niveauDeSoif = niveauDeSoif - 2
  
  morduParUnSerpent = random.choice([True, False]) # random.choice([True, False]) permet d'obtenir une valeur aléatoire parmi les valeurs contenues dans le tableau passé en paramètre de la fonction 'choice', les valeurs renseignées ici sont True ou False

print("votre personnage est mort, vous avez perdu la partie !")
widget

- soit A la proposition 'niveauDeFaim > 0'
- soit B la proposition 'niveauDeSoif > 0'
- soit C la proposition 'morduParUnSerpent'

+++question "En utilisant les propositions A B et C, formulez la condition que doit respecter la boucle <code>while</code> pour être exécutée"

+++question "Rédigez la table de vérité de la condition que vous venez de rédigez"

+++spoil "Aide"
  L'entête de la table de vérité utilisée dans la condition de l'instruction while est : 
  
  <table class="table table-fit text-center">
    <thead>
      <tr>
        <th class="text-blue">A</th>
        <th class="text-red">B</th>
        <th class="text-green">C</th>
        <th style="color:#A4A452">A and B</th>
        <th class="text-violet">not C</th>
        <th><u><span style="color:#A4A452">A and B</span> and <span class="text-violet">not C</span></u></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>0</td>
        <td>0</td>
        <td>0</td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td>0</td>
        <td>0</td>
        <td>1</td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td>0</td>
        <td>1</td>
        <td>0</td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td>0</td>
        <td>1</td>
        <td>1</td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td>1</td>
        <td>0</td>
        <td>0</td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td>1</td>
        <td>0</td>
        <td>1</td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td>1</td>
        <td>1</td>
        <td>0</td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td>1</td>
        <td>1</td>
        <td>1</td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  
  à vous de compléter le reste de la table de vérité :)
spoil+++