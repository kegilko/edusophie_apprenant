# Ressources Web : droits et sécurité

+++programme /images/md/EEB9G94F8ACEF6355H5HB76DC3GCH8
  Mener une analyse critique des résultats fournis par un moteur de recherche
  Comprendre les enjeux de la publication d’informations
  Connaître certaines notions juridiques (licence, droit d’auteur, droit d’usage, valeur d’un bien)
  Sécuriser sa navigation en ligne et analyser les pages et fichiers
  
  Comparer différents moteurs de recherches Web
  Utiliser les licences "Creative Commons"
  Naviguer sur le Web de manière sécurisée
+++


+++sommaire

## Introduction 

+++bulle r3do
  (rappel) quelle est la différence entre un navigateur Web et un moteur de recherche ?
+++

+++video "https://www.youtube.com/watch?v=0A5fQER40Wg" "le moteur de recherche" 00:10

--- 

Les moteurs de recherches modernes reposent sur 3 mécanismes fondamentaux : l'**exploration**, l'**indexation** et la **recherche**

+++question-video 02:46 write "À quoi correspondent chacun de ces 3 mécanimes ?"

0. **Exploration**

+++spoil
  L'exploration ( +++flag-us crawling ) des moteurs de recherches consiste à trouver les sites Web et à enregistrer leur url dans un fichier
spoil+++

1. **Indexation**

+++spoil
  Une fois les sites enregistrés, ils sont triés en fonction de plusieurs critères : thématique du site (les mots-clés important contenus dans les pages Web du site), popularité du site, ...
  
  <div class="mt-3">
  +++image "/images/md/5AE368FCE7DF85HA9DC734AC44BADH" col-8 mx-auto mb-0
  <p class="pt-0 text-center">un des critères de mesure de la popularité d'un site</p>
  </div>

spoil+++

2. **Recherche**

+++spoil
  Le moteur de recherche utilise les données qu'il a récoltées et triées afin de pouvoir proposer des sites Webs en fonction de la recherche d'un utilisateur 
spoil+++


<hr>

  +++bulle r3do
    la phase d'exploration n'est bien sur pas effectuée par des êtres humains, mais par des programmes qu'on appelle "bot". Chaque bot peut très rapidement naviguer de sites Web en sites Web grâce aux <a href="">liens hypertextes</a>
  +++
  
  +++bulle r3do droite
    le fichier dans lequel sont enregistrées les url des sites devient vite très volumineux, on utilise donc un type de fichier spécialisé dans la manipulation de données volumineuses; <b>une base de données</b> 
  +++

## Des moteurs de recherches pour tous les goûts 

Des moteurs de recherche Web, il en existe des dizaines ! Voici ci-dessous des parts de marchés occupées en France en 2022 : 

<table class="text-center">
  <thead>
    <tr>
      <th class="px-2">moteur</th>
      <th class="px-2">taux d'utilisation sur smartphone</th>
      <th class="px-2">taux d'utilisation sur ordinateur</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="px-2">Google</td>
      <td class="px-2">80,24%</td>
      <td class="px-2">97,01%</td>
    </tr>
    <tr>
      <td class="px-2">Bing</td>
      <td class="px-2">12,46%</td>
      <td class="px-2">0,45%</td>
    </tr>
    <tr>
      <td class="px-2">Yahoo</td>
      <td class="px-2">2,65%</td>
      <td class="px-2">0,42%</td>
    </tr>
    <tr>
      <td class="px-2">Ecosia</td>
      <td class="px-2">1,60%</td>
      <td class="px-2">0,77%</td>
    </tr>
    <tr>
      <td class="px-2">Qwant</td>
      <td class="px-2">1,82%</td>
      <td class="px-2">0,49%</td>
    </tr>
    <tr>
      <td class="px-2">DuckDuckGo</td>
      <td class="px-2">0,69%</td>
      <td class="px-2">0,43%</td>
    </tr>
  </tbody>
</table>
<p class="text-center">données provenant du site +++lien "https://gs.statcounter.com/" "statcounter")</p>

---

+++bulle r3do
  quelles différences y-a-t'il entre les différents moteurs de recherches ? qu'est-ce qui démarque un moteur de recherche par rapport aux autres ? répartissons nous le travail !
+++

+++bulle r3do droite
  chaque groupe prends un moteur de recherche et cherche quel est le principal avantage et le principal défaut en s'aidant de recherches sur Internet
+++

<table style="width:100%">
  <thead>
    <tr class="text-center">
      <th class="px-2">Moteur</th>
      <th class="px-2 text-green">le principal avantage</th>
      <th class="px-2 text-red">le principal inconvénient</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-center">Google</td>
      <td><textarea class="w-100"></textarea></td>
      <td><textarea class="w-100"></textarea></td>
    </tr>
    <tr>
      <td class="text-center">Bing</td>
      <td><textarea class="w-100"></textarea></td>
      <td><textarea class="w-100"></textarea></td>
    </tr>
    <tr>
      <td class="text-center">Yahoo</td>
      <td><textarea class="w-100"></textarea></td>
      <td><textarea class="w-100"></textarea></td>
    </tr>
    <tr>
      <td class="text-center">Ecosia</td>
      <td><textarea class="w-100"></textarea></td>
      <td><textarea class="w-100"></textarea></td>
    </tr>
    <tr>
      <td class="text-center">Qwant</td>
      <td><textarea class="w-100"></textarea></td>
      <td><textarea class="w-100"></textarea></td>
    </tr>
    <tr>
      <td class="text-center">DuckDuckGo</td>
      <td><textarea class="w-100"></textarea></td>
      <td><textarea class="w-100"></textarea></td>
    </tr>
  </tbody>
</table>
  
+++question write "Quels sont les principaux critères qui se dégagent pour choisir un moteur de recherche Web ?"

<textarea class="w-100 mb-2" rows="5"></textarea>

+++bulle r3do
  prenons quelques minutes pour débattre autour de ces critères
+++

## Notions juridiques liées au Web

Tout ce qu’on trouve sur internet (textes, images, photos, etc) est la propriété de son auteur et ne peut être utilisé sans son autorisation. Il existe les droits d’auteur en France, ou le copyright aux États-Unis. Mais la dimension mondiale du Web et l’impossibilité de contrôler le respect des différentes législations a donné naissance à de nouvelles pratiques, comme les licences **Creatives Commons**.

+++video "https://www.youtube.com/watch?v=yop1jJScPXU" "Les licences Creative Commons : comment ça marche ?"

+++bulle r3do
  aujourd'hui le système creative commons est surtout utilisé pour les logiciels et également pour les contenus numériques (photos, œuvre artistique, enregistrement audio, vidéo, ...)
+++

Le tableau ci-dessous regroupe les principaux pictogrammes qui permettent de qualifier la licence sous laquelle on souhaite mettre un média quelconque

+++image "/images/md/CBG6CDH93D9B2DC5963B8573DH7EFA" mx-auto col-10

+++question write "Entraînez vous !"

<div class="col-9 mx-auto border border-2 border-dark rounded p-3 text-center d-print-avoid-break mb-3">
<p>Sur Internet j'ai trouvé une photo présentée avec cette licence</p>

  <div class="py-2 d-inline-block rounded border border-dark" style="background-color:#EEEEEE">
    <i class="mx-1 fab fa-creative-commons-nc-eu fa-3x"></i>
    <i class="mx-1 fab fa-creative-commons-sa fa-3x"></i>
  </div>
  
  <hr>
  
  <div class="text-start">
    <p class="mb-2 p-1" style="color:#00f;font-size: 1.1rem;">
      <b>[A]</b> Je ne peux pas la modifier
    </p> 
    <p class="mb-2 p-1" style="color:#00f;font-size: 1.1rem;">
      <b>[B]</b> Je peux la revendre
    </p> 
    <p class="mb-2 p-1" style="color:#00f;font-size: 1.1rem;">
      <b>[C]</b> Je peux la modifier et la re-partager
    </p> 
    <p class="mb-2 p-1" style="color:#00f;font-size: 1.1rem;">
      <b>[D]</b> Je dois citer l'auteur si je repartage cette photo
    </p> 
    +++spoil "Réponse(s)" text-center nolabel edu-ds d-print-none
        Réponse <b>C</b>
    spoil+++
  </div>
  
</div>

<div class="col-9 mx-auto border border-2 border-dark rounded p-3 text-center d-print-avoid-break mb-3">
<p>J'aimerai partager sur mon réseau social préféré une vidéo présentée avec cette licence</p>

  <div class="py-2 d-inline-block rounded border border-dark" style="background-color:#EEEEEE">
    <i class="mx-1 fab fa-creative-commons-by fa-3x"></i>
    <i class="mx-1 fab fa-creative-commons-nd fa-3x"></i>
  </div>
  
  <hr>
  
  <div class="text-start">
    <p class="mb-2 p-1" style="color:#00f;font-size: 1.1rem;">
      <b>[A]</b> Je dois citer l'auteur de la vidéo lors de mon partage
    </p> 
    <p class="mb-2 p-1" style="color:#00f;font-size: 1.1rem;">
      <b>[B]</b> Avant le partage, je peux modifier la vidéo à l'aide d'un logiciel de montage
    </p> 
    <p class="mb-2 p-1" style="color:#00f;font-size: 1.1rem;">
      <b>[C]</b> Je peux décider de revendre la vidéo au lieu de la partager gratuitement
    </p> 
    <p class="mb-2 p-1" style="color:#00f;font-size: 1.1rem;">
      <b>[D]</b> Je n'ai pas besoin de citer l'auteur si je repartage cette vidéo
    </p> 
    +++spoil "Réponse(s)" text-center nolabel edu-ds d-print-none
        Réponses <b>A</b> et <b>C</b>
    spoil+++
  </div>
  
</div>

<div class="col-9 mx-auto border border-2 border-dark rounded p-3 text-center d-print-avoid-break mb-3">
<p>J'ai téléchargé une musique sur une plateforme de streaming musical, la musique est présentée avec cette licence</p>

  <div class="py-2 d-inline-block rounded border border-dark" style="background-color:#EEEEEE">
    <i class="mx-1 fab fa-creative-commons-by fa-3x"></i>
    <i class="mx-1 fab fa-creative-commons-sa fa-3x"></i>
    <i class="mx-1 fab fa-creative-commons-nd fa-3x"></i>
  </div>
  
  <hr>
  
  <div class="text-start">
    <p class="mb-2 p-1" style="color:#00f;font-size: 1.1rem;">
      <b>[A]</b> Si je la partage, elle doit conserver la même licence ou une licence garantissant les mêmes libertés que la licence originale
    </p> 
    <p class="mb-2 p-1" style="color:#00f;font-size: 1.1rem;">
      <b>[B]</b> Je peux modifier la musique avec un logiciel de remix
    </p> 
    <p class="mb-2 p-1" style="color:#00f;font-size: 1.1rem;">
      <b>[C]</b> Je ne dois pas citer l'auteur de la musique et cas de repartage
    </p> 
    <p class="mb-2 p-1" style="color:#00f;font-size: 1.1rem;">
      <b>[D]</b> Je peux revendre la musique
    </p> 
    +++spoil "Réponse(s)" text-center nolabel edu-ds d-print-none
        Réponses <b>A</b> et <b>D</b>
    spoil+++
  </div>
  
</div>

+++bulle r3do
  pour le cas ci-dessous, vous devez recherchez la signification du pictogramme sur Internet
+++col-12 col-lg-6 col-print-8 col-xxl-6 my-3 mx-auto


<div class="col-9 mx-auto border border-2 border-dark rounded p-3 text-center d-print-avoid-break mb-3">
<p>J'ai téléchargé un logiciel présenté avec cette licence</p>

  <div class="py-2 d-inline-block rounded border border-dark" style="background-color:#EEEEEE">
    <i class="mx-1 fab fa-creative-commons-zero fa-3x"></i>
  </div>

  <hr>
  
  <div class="text-start">
    <p class="mb-2 p-1" style="color:#00f;font-size: 1.1rem;">
      <b>[A]</b> Je peux modifier le logiciel
    </p> 
    <p class="mb-2 p-1" style="color:#00f;font-size: 1.1rem;">
      <b>[B]</b> Je peux revendre le logiciel
    </p> 
    <p class="mb-2 p-1" style="color:#00f;font-size: 1.1rem;">
      <b>[C]</b> Je dois citer l'auteur si je repartage le logiciel
    </p> 
    <p class="mb-2 p-1" style="color:#00f;font-size: 1.1rem;">
      <b>[D]</b> Je peux repartager le logiciel sous une nouvelle licence
    </p> 
    +++spoil "Réponse(s)" text-center nolabel edu-ds d-print-none
        Réponses <b>A</b>, <b>B</b> et <b>D</b>
    spoil+++
  </div>
  
</div>

<hr>

+++bulle r3do
  L'innovation et les droits d'auteurs sont parfois mis en opposition
+++

## Sécuriser sa navigation en ligne

+++bulle r3do
  la sécurité informatique est un vaste domaine 
+++

Dans le cadre du web, deux des principaux enjeux de sécurités sont les suivants :

0. la diffusion ou l'exploitation de données personnelles à notre insu
1. se faire pirater un de ses comptes sur un site et/ou adresse email 


+++bulle r3do
  connaissez vous des exemples de cas d'exploitation abusif de données personnelles ?
+++

+++bulle r3do droite
  quelles sont les bonnes pratiques pour sécuriser sa navigation en ligne ?
+++

{{textarea}}
