# Le Web : introduction

+++programme /images/md/H8H57C387457B4DC94F49E8F2CCF6H 60
  Connaître les étapes du développement du Web
  Reconnaître les pages sécurisées
  Décomposer l’URL d’une page
  Décomposer le contenu d’une requête HTTP et identifier les paramètres passés
  
  Différencier le protocole HTTP au protocole HTTPS
  Analyser le contenu d'une URL
  Différencier un navigateur Web d'un moteur de recherche
  Trouver des images libres de droit sur le Web
  Analyser le contenu d'une requête HTTP ou HTTPS envoyée depuis un client vers un serveur
+++

+++pagebreak

+++sommaire

## Introduction

### Bref historique

+++video "https://www.youtube.com/watch?v=YVn7jrDYjUQ" "L'histoire du Web" 00:04

+++question write "En quelle année peut on dire que le Web est né ?"

+++spoil
  Une première version, minimaliste, du Web a été créée au CERN en 1989 afin de permettre aux chercheurs d'échanger à distance facilement des données. 

  +++bulle r3do
    <span class="text-black">Le CERN est l'<b>O</b>rganisation <b>E</b>uropéenne pour la <b>R</b>echerche <b>N</b>ucléaire</span>
  +++
spoil+++

### Fonctionnement général du Web

+++video "https://www.youtube.com/watch?v=g1rvdtOUWA4" "le Web" 00:00

<p>Pour qu'un navigateur Web puisse s'entendre avec un serveur, possiblement situé de l'autre côté de la planète, ils doivent utiliser le même protocole. Les protocoles possibles sont le <b class="text-red">HTTP</b> (créé en 1990) et le <b class="text-red">HTTPS</b> (créé en 1994).</p>

<p class="mb-0">L'utilisation du protocole <b class="text-red">HTTPS</b> permet de protéger <b class="text-green">la communication client/serveur</b> d'une <b class="text-blue">attaque informatique connue nommée MITM</b> (<b>M</b>an <b>I</b>n <b>T</b>he <b>M</b>iddle)</p><p>
+++image "/images/md/6F9DH9GHBEDDE9HEG9D76A7C7EAC96" col-8 mx-auto

</p>Le principe d'une attaque MITM est qu'une personne malveillante peut, <span class="text-green">à travers le réseau que votre ordinateur utilise</span>,<span class="text-blue"> intercepter les informations </span>échangées entre le client (votre ordinateur par exemple) et le serveur (un serveur qui héberge un site e-commerce par exemple). En <span class="text-red">HTTP</span>, le hacker pourra alors par exemple collecter votre login et votre mot de passe lorsque vous vous authentifiez sur le site Web en question.<p>


+++question-video 00:51 write "Comment le protocole HTTP<u>S</u> protège contre une attaque MITM ?"

+++spoil
  Les échanges en HTTPS sont chiffrés (contrairement au HTTP). Le hacker peut toujours intercepter les communications entre client et serveur, mais ne peut tout simplement pas en comprendre le contenu.

+++image "/images/md/EC6AGF2B38B52G6D8C6B57GE28B64G" col-8 mx-auto d-print-none-2

spoil+++

+++question-video 00:51 write "Comment voit on si un site utilise le protocole HTTPS ?"

+++spoil
  Il suffit de vérifier que le début de l'url du site commence par "http<b>s</b>://" et non "http://"
spoil+++

</p><hr>

Les urls se décomposent en trois parties principales : 

<b class="text-red">protocole</b>://<b class="text-green">nomDeDmaine</b>/<b class="text-blue">chemin/vers/la/ressource/du/serveur</b>

Analysons par exemple l'url pour accéder à une page du wikipédia français : 

<p class="mb-2 text-center"><b><a href="https://fr.wikipedia.org/wiki/WEB" target="_blank">https://fr.wikipedia.org/wiki/WEB</a></b></p>


- <b class="text-red">https</b> : protocole utilisé entre votre client (votre navigateur Web) et le serveur (machine qui héberge le site Wikipédia fr)
- <b class="text-green">fr.wikipedia.org</b> : nom de domaine du site Wikipédia fr
- <b class="text-blue">wiki/WEB</b> : chemin de la ressource que le client demande au serveur 

+++question-video 01:08 write "Décomposez les urls ci-dessous :"

<table class="text-center">
  <thead>
    <tr>
      <th colspan="3" class="pb-2"><a href="http://info.cern.ch/hypertext/WWW/TheProject.html" target="_blank" style="text-decoration: underline;
   text-underline-offset: 4px;">http://info.cern.ch/hypertext/WWW/TheProject.html</a></th>
    </tr>
    <tr>
      <th class="px-2">protocole</th>
      <th class="px-2">nom de domaine</th>
      <th class="px-2">ressource serveur demandée</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
      +++spoil nolabel
        http
      spoil+++
      </td>
      <td>
      +++spoil nolabel
        info.cern.ch
      spoil+++
      </td>
      <td>
      +++spoil nolabel
        hypertext/WWW/TheProject.html
      spoil+++
      </td>
    </tr>
  </tbody>
</table>

<table class="text-center">
  <thead>
    <tr>
      <th colspan="3" class="pb-2"><a href="https://www.laquadrature.net/donnees_perso" target="_blank" style="text-decoration: underline;
   text-underline-offset: 4px;">https://www.laquadrature.net/donnees_perso</a></th>
    </tr>
    <tr>
      <th class="px-2">protocole</th>
      <th class="px-2">nom de domaine</th>
      <th class="px-2">ressource serveur demandée</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
      +++spoil nolabel
        https
      spoil+++
      </td>
      <td>
      +++spoil nolabel
        www.laquadrature.net
      spoil+++
      </td>
      <td>
      +++spoil nolabel
        donnees_perso
      spoil+++
      </td>
    </tr>
  </tbody>
</table>

<table class="text-center">
  <thead>
    <tr>
      <th colspan="3" class="pb-2"><a href="https://www.youtube.com/watch?v=UrG6n15yS0E" target="_blank" style="text-decoration: underline;
   text-underline-offset: 4px;">https://www.youtube.com/watch?v=UrG6n15yS0E</a></th>
    </tr>
    <tr>
      <th class="px-2">protocole</th>
      <th class="px-2">nom de domaine</th>
      <th class="px-2">ressource serveur demandée</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
      +++spoil nolabel
        https
      spoil+++
      </td>
      <td>
      +++spoil nolabel
        www.youtube.com
      spoil+++
      </td>
      <td>
      +++spoil nolabel
        watch?v=UrG6n15yS0E
      spoil+++
      </td>
    </tr>
  </tbody>
</table>

<hr>

+++question-video 02:14 write "Comment un moteur de recherche indexe les sites Web présents sur Internet ?"

+++bulle r3do
  "indexer" dans le sens "faire l'inventaire" / "répertorier"
+++

+++spoil
  Les développeurs du moteur de recherche créent des robots virtuels qui vont se promener de page Web en page Web grâce <a href="">aux liens hypertextes</a> <i class="text-black far fa-hand-pointer"></i>. Pendant leur promenade, les robots répertorient les sites qu'ils traversent (url du site, titre principal, etc). 
  
  +++bulle r3do
    les robots permettent aussi, en se promenant, de cartographier le Web
  +++col-12 col-lg-6 col-print-8 col-xxl-6 mb-3 mx-auto d-print-none-2 text-black
  
  +++bulle r3do droite
    voici un exemple de carte du Web : <a href="http://internet-map.net/" target="_blank">http://internet-map.net</a>
  +++col-12 col-lg-6 col-print-8 col-xxl-6 mb-3 mx-auto d-print-none-2 text-black
  
spoil+++

<hr>

Nous confondons souvent moteur de recherche et navigateur Web. Pour beaucoup de personnes, Google est un navigateur Web alors que Google est le nom de l'entreprise qui a développé son propre navigateur Web

+++bulle r3do
  quel est le nom du navigateur développé par Google ?
+++

+++question-video 02:14 write "Quelle est la différence entre un navigateur Web et un moteur de recherche ?"

+++spoil
   un navigateur est un logiciel qui affiche des pages web, alors qu'un moteur de recherche est un site web qui permet aux utilisateurs de rechercher des sites web à l'aide de mots-clés 
spoil+++

<hr>

Les navigateurs Web sont des logiciels nativement capables de lire du texte enrichi (hypertexte) et d'interpréter le code contenu dedans (HTML, CSS et Javascript) afin d'afficher sur l'écran des pages Web esthétiques et dynamiques.

+++question-video 03:14 write "À quoi sert le langage HTML ?"

+++spoil
  Le langage HTML permet de créer et de structurer le contenu d'une page Web (texte, lien ,image, vidéo, etc...)
spoil+++

+++question-video 03:14 write "À quoi sert le langage CSS ?"

+++spoil
  Le langage CSS permet de mettre en forme le contenu d'une page Web, autrement dit, le langage CSS s'occupe de la décoration d'une page Web (titre en bleu, texte en gras, image de fond, etc...)
spoil+++

<hr>

Une licence, en informatique, désigne un ensemble de règles qui définissent comment une création numérique (une musique, une vidéo, un article, un logiciel, ...) peut être utilisée, diffusée et modifiée. 

Ce concept est important dans la création d'un site Web. En effet, il n'est par exemple pas autorisé d'utiliser dans son site Web des images qui ne nous appartiennent pas et qui sont protégées par la propriété intellectuelle.

+++question-video 04:19 write "Comment trouver des images libres de droit ?"

+++spoil
  Le Web regorge de sites mettant à disposition des banques d'images libres de droit. Il suffit d'utiliser un moteur de recherche Web avec, par exemple, les mots-clés "<a href="https://www.google.com/search?q=images+libres+de+droit">images libres de droit</a>"
spoil+++

### Résumons


- Sur le Web, pour qu'on client et un serveur communiquent ensemble, ils utilisent le protocole HTTP ou le protocole HTTPS
- Le protocole HTTPS permet de chiffrer la communication entre le client et le serveur. Une personne malveillante dans le réseau ne peut alors pas comprendre les données qui sont chiffrées
- Une URL est composée de trois parties : <b class="text-red">le protocole utilisé</b>, <b class="text-green">le nom de domaine</b> et <b class="text-blue">la ressource serveur</b> qui est demandée par un client
    - par exemple : <b class="text-red">https</b>://<b class="text-green">fr.wikipedia.org</b>/<b class="text-blue">wiki/Ada_Lovelace</b>
    - l'url commence par https, le site est donc plus sécurisé qu'un site utilisant le protocole HTTP
- Les pages Web sont reliées par des <a href="">liens hypertextes</a><a> <i class="text-black far fa-hand-pointer"></i>
- Ces liens sont parcourus de pages en pages par des robots virtuels ( +++flag-us crawler ) afin d'analyser et d'indexer les sites Web. C'est grâce à cette opération que les moteurs de recherches existent, ils puisent leurs informations grâce aux données récoltés par les robots virtuels 
- Il ne faut pas confondre navigateur Web et moteur de recherche : un navigateur est un logiciel qui affiche des pages web, alors qu'un moteur de recherche est un site web qui permet aux utilisateurs de rechercher des sites web à l'aide de mots-clés 


## Analyse d'une requête HTTP

Dans le chapitre dédié à Internet, nous nous sommes intéressés aux paquets TCP qui circulent à travers le réseau. Une requête HTTP ou le résultat d'une requête HTTP est découpé et placé dans plusieurs paquets TCP qui sont ensuite envoyés vers un destinataire à travers le réseau Internet. Nous allons voir à quoi ressemble le contenu d'une requête HTTP. Cela marche exactement de la même manière que pour une requête HTTPS.

+++bulle r3do
  La plupart des navigateurs Web mettent à disposition des outils pour aider les développeurs dans la création de site Web
+++

+++bulle r3do droite
  Vous allez utiliser un de ses outils et, grâce à lui, analyser le contenu d'une requête HTTPS
+++

+++consigne
Suivez les étapes 9 à 13 et répondre à la question 14
+++

+++question "Depuis un ordinateur, ouvrez le navigateur Web Firefox <i class="text-orangered fab fa-firefox-browser"></i>"

+++question "Une fois le navigateur ouvert, appuyez sur la touche <b>F12</b> du clavier, cela va ouvrir une fenêtre nommée '<b>Outils de développement</b>' (voir image ci-dessous)"

+++image "/images/md/52FH3EE5C27DGB5HGBBCDHBE9D5E35" col-8 mx-auto

+++question "Cliquez sur l'<b>onglet "réseau"</b> et gardez la fenêtre '<b>Outils de développement</b>' ouverte  (voir image ci-dessous)"

+++image "/images/md/FGG547C2B9A2EC6G887475F22363HC" col-8 mx-auto

+++question "Sur la fenêtre de votre navigateur Web, <b>saisissez et validez l'url 'https://www.edusophie.com/d/pageWebMinimaliste.html'</b>. Une fois la page chargée, la fenêtre d'outils de développement devrait afficher une ligne. Cliquez sur cette ligne pour la sélectionner  (voir image ci-dessous)"

+++image "/images/md/G9G3DBF3G3837FC795FBBE9D9G7CD3" col-8 mx-auto

+++bulle r3do
  après avoir saisi l'url dans votre navigateur, celui-ci envoie une requête HTTPS au serveur qui héberge le site édusophie
+++

+++bulle r3do droite
  le site édusophie renvoie ensuite au navigateur le contenu de la page Web 'pageWebMinimaliste.html'
+++

+++question "Sur la fenêtre '<b>Outils de développement</b>' cliquez sur le bouton 'texte brut' situé à droite de 'En-têtes de la requête', vous devriez pouvoir observer les données brutes de la requête qui a été au serveur hébergeant le site edusophie (voir image ci-dessous)"

+++image "/images/md/282AB96724EDAFCGE3H66BH8245A49" col-8 mx-auto

+++bulle r3do
  afficher le texte brut de l'en-tête de la requête permet d'observer le contenu de la requête qui a été envoyée au serveur qui héberge le site édusophie
+++

+++question write "Quelles informations se trouvent dans l'entête de la requête ? (entourez les bons blocs ci-dessous)"

<div class="d-flex justify-content-evenly flex-wrap randomizer randomizer-bloc">
  <div class="border border-1 rounded p-1 mb-2"> le nom de domaine de l'hébergeur</div>
  <div class="border border-1 rounded p-1 mb-2"> le navigateur Web utilisé</div>
  <div class="border border-1 rounded p-1 mb-2"> le système d'exploitation utilisé</div>
  <div class="border border-1 rounded p-1 mb-2"> la ressource serveur demandée</div>
  <div class="border border-1 rounded p-1 mb-2"> la date du jour</div>
  <div class="border border-1 rounded p-1 mb-2"> le débit Internet</div>
  <div class="border border-1 rounded p-1 mb-2"> l'IP du client</div>
  <div class="border border-1 rounded p-1 mb-2"> l'IP du serveur</div>
</div>

<hr>

+++bulle r3do
  vous êtes en avance ? vous avez le choix entre : 
+++

<ul>
  <li>Analyser les requêtes envoyées sur d'autres sites que vous connaissez</li>
  <li>(sujet du prochain cours) Analyser et modifier le code HTML de la page 'pageWebMinimaliste.html'<ul>
    <li>Pour cela, cliquez sur l'onglet 'Inspecteur' de la fenêtre 'Outils de développement'</li>
    <li>L'onglet affiche le code HTML contenu dans la page Web</li>
    <li>Vous pouvez survoler avec la souris et double cliquez sur des éléments dans le code HTML pour pouvoir les modifier (et regarder le résultat de vos modifications en directe sur votre navigateur Web)</li>
  </ul>
  </li><li>Explorer les fonctionnalités proposées par la fenêtre 'Outils de développement', naviguer dans les autres onglets, essayer de comprendre les données qui sont affichées par la fenêtre 'Outils de développement', etc...</li>
</ul>

### Résumons

- Le contenu d'une requête HTTP(s) est un texte structuré qui contient diverses informations, les principales sont : 
    - le <b style="color:red">nom de domaine de l'hébergeur</b> et la <b style="color:orangered">ressource serveur sollicitée par le client</b>
    - le <b style="color:green">nom du navigateur Web</b> qui envoie la requête ainsi que <b style="color:darkolivegreen">sa version</b>
    - le <b style="color:blue">nom du système d'exploitation</b> de la machine cliente ainsi que <b style="color:blueviolet">sa version</b>
- Voici un exemple de contenu d'une requête HTTP : 
<ul>
  <div>
<pre class="border border-1 p-2 d-inline-block">GET <b style="color:orangered">/d/pageWebMinimaliste.html</b> HTTP/1.1
Host: <b style="color:red">www.edusophie.com</b>
User-Agent: Mozilla/5.0 (<b style="color:blue">Windows</b> NT <b style="color:blueviolet">10.0</b>; Win64; x64; rv:96.0) Gecko/20100101 <b style="color:green">Firefox</b>/<b style="color:darkolivegreen">96.0</b>
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: fr,en-US;q=0.7,en;q=0.3
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: none
Sec-Fetch-User: ?1
Pragma: no-cache
Cache-Control: no-cache
</pre>
</div>
</ul></a>