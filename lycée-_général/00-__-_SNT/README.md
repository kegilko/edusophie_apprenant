# README

+++sommaire

<span class="d-none d-print-block">Cette fiche doit se trouver en toute première page dans votre classeur</span>

## Matériel demandé

### À la maison

- Un classeur au format A4 :
    - avec des grands anneaux (2 ou 4)
    - contenant des pochettes plastifiées
    - contenant 8 intercalaires avec une étiquette sur chacun, le nom de chaque étiquette doit être : 

```python
intercalaires = {
   0 : "photographie numérique",
   1 : "données structurées et traitement",
   2 : "internet",
   3 : "web",
   4 : "réseaux sociaux",
   5 : "informatique embarqué, objets connectés",
   6 : "localisation, cartographie, mobilité",
   7 : "programmation"
}
```

### En classe

- Un porte-vue contenant au moins 40 pages 
- Des feuilles A4 perforées (grand carreau ou petit carreau)
- Une clé usb (>= 3 Go), attention à ne pas la perdre !

Lors nous terminons un chapitre, les feuilles et documents du porte-vue doivent être transférés dans le classeur de cours qui reste à la maison.

**Pendant l'année je ramasserai les classeurs.**

## Planning

- Séance d'une heure en demi-groupe toutes les deux semaines le lundi de 13h à 14h en salle PRO3
- Séance d'une heures le lundi de 14h à 15h en salle PRO2

## Évaluations notées

**En fin de chapitre**, un contrôle :
  - contenu : l'ensemble du chapitre qui vient d'être vu. Il peut également y avoir des questions sur les chapitres précédents 

## Progression de l'année scolaire

- La plupart des chapitres s'étalent sur environ 4 semaines
- Entre aide encouragée (voir **charte d'entraide** ci-dessous)
- Formation de groupes aléatoires, ou non, dans les TPs

## Charte d'entraide +++emoji-warning 
- En classe entière, lors d'une activité en binôme ou en groupe : **chuchoter** 
- Ne **jamais** donner la réponse directement à ses camarades
- Ne **jamais** écrire du code à la place de ses camarades
- Poser des questions
- Orienter vers les parties du cours concernées
- Orienter vers les exercices en lien avec la problématique rencontrée