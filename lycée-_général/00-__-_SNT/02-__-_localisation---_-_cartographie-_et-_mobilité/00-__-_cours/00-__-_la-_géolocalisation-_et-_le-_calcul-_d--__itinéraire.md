# Introduction à la géolocalisation et calcul d'itinéraire

+++programme
Décrire le principe de fonctionnement de la géolocalisation
Utiliser un logiciel pour calculer un itinéraire
Représenter un calcul d’itinéraire comme un problème sur un graphe

Expliquer comment fonctionne la géolocalisation
Utiliser un site Internet pour déterminer un itinéraire
Utiliser un algorithme pour déterminer le plus court chemin entre un point de départ et un point d'arrivée
+++

+++sommaire

## Fonctionnement de la géolocalisation

### Analyse de vidéo

+++video "https://www.youtube.com/watch?v=iTfNhcC2vBA" "MOOC SNT / Géolocalisation, comment s'y retrouver ?"

+++question write "<b>[1:28]</b> Quelles sont les informations que reçoit le récepteur GPS de la part des satellites ?"

+++spoil
  La position du satellite et l'heure d'émission du signal
spoil+++

+++question write "<b>[1:42]</b> Comment estime-t-on la distance entre le récepteur et le satellite ?"

+++spoil
  Le récepteur GPS calcule la durée du trajet. Il suffit ensuite de multiplier cette durée par la vitesse de propagation de l'onde
spoil+++

+++question write "<b>[2:01]</b> Combien de satellites sont nécessaires pour repérer une position ? pour quelle raison en faut il autant ?"

+++spoil
  Il faut 4 satellites. Un seul satellite n'est pas suffisant pour déterminer la position d'un récepteur GPS. Trois satellites permettent de localiser un récepteur (latitude, longitude, altitude). Le 4ième satellite synchronise l'heure du récepteur par rapport aux satellites.
spoil+++

+++question write "<b>[3:46]</b> Quelles données sont rassemblées sur OpenStreetMap ?"

+++spoil
  Des données topographiques, industrielles, touristiques et même fournies par les utilisateurs.
spoil+++

### Résumons

- La géolocalisation permet de calculer la position d'un récepteur placé sur Terre (comme un téléphone portable par exemple)
- La position GPS est obtenue : 
    - en utilisant le décalage entre l'heure d'émission et l'heure de réception d'un message par le récepteur. Cela permet de calculer les distances entre le récepteur et chacun des **trois satellites**
    - puis en calculant les coordonnées (latitude, longitude, altitude) du récepteur par **trilatération**, c'est à dire en repérant le point sur Terre correspondant aux distances calculées
    - un quatrième satellite est utilisé pour corriger d'éventuelles erreurs d'horloge côté récepteur

<hr>

## Application de la géolocalisation : le calcul d'itinéraire

Une application courante des technologies liées à la géolocalisation est le calcul d'itinéraire entre un point de départ et un point d'arrivée. Le meilleur itinéraire peut être choisi via différents critères : la distance, le coût du trajet, l'impact écologique, etc...

La géolocalisation permet aussi de collecter en continue des données en temps réels permettant de calculer l'itinéraire le plus optimal. (par exemple pour éviter un bouchon en voiture)

Dans ce thème introductif, nous nous focalisons sur le calcul de **la distance la plus courte** entre un point de départ et un point d'arrivée. 

### Utilisation d'outil pour calculer l'itinéraire le plus court

Il existe une multitude d'outils permettant de déterminer l'itinéraire le plus court entre deux points. Vous allez utilisez un des outils listés ci-dessous. 

0. +++lien "https://www.google.com/maps/dir/" "Google map"
1. +++lien "https://fr.mappy.com/itineraire" "Mappy"
2. +++lien "https://www.openstreetmap.org/directions" "OpenStreetMap"
3. +++lien "https://www.bing.com/maps/" "Bing Map"

+++question write "Quel site Web avez vous choisi pour calculer le trajet ?"

<input type="text" class="w-100">

+++bulle r3do
en partant du lycée Beauséjour (lieu de départ), dans quelle ville en Europe souhaiteriez vous voyager ? (lieu d'arrivée)
+++

+++question write "Quelle est l'adresse exacte du lycée  ?"

<input type="text" class="w-100">

+++question write "Quelle destination a été choisie ?"

<input type="text" class="w-100">

+++bulle r3do droite
Modifiez les options de l'outil que vous utilisez pour essayer de trouver le trajet le plus court (en temps) possible ! 
+++

+++question write "En utilisant le site Web choisi, quel est le temps du trajet le plus court (en temps) que vous avez trouvé ?"

<input type="text" class="w-100">

<hr>

+++bulle r3do
Rassemblons ensemble les résultats dans ce tableau ci-dessous
+++

<table class="table table-sm">
  <thead>
    <tr>
      <th></th>
      <th class="col-2 text-center">Google map</th>
      <th class="col-2 text-center">Mappy</th>
      <th class="col-2 text-center">OpenStreetMap</th>
      <th class="col-2 text-center">Bing Map</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="align-middle">temps relevé n°1 (hh:mm)</td>
      <td><input type="text" class="w-100"></td>
      <td><input type="text" class="w-100"></td>
      <td><input type="text" class="w-100"></td>
      <td><input type="text" class="w-100"></td>
    </tr>
    <tr>
      <td class="align-middle">temps relevé n°2 (hh:mm)</td>
      <td><input type="text" class="w-100"></td>
      <td><input type="text" class="w-100"></td>
      <td><input type="text" class="w-100"></td>
      <td><input type="text" class="w-100"></td>
    </tr>
    <tr>
      <td class="align-middle">temps relevé n°3 (hh:mm)</td>
      <td><input type="text" class="w-100"></td>
      <td><input type="text" class="w-100"></td>
      <td><input type="text" class="w-100"></td>
      <td><input type="text" class="w-100"></td>
    </tr>
    <tr>
      <th class="align-middle">temps moyen (hh:mm)</th>
      <th><input type="text" class="w-100"></th>
      <th><input type="text" class="w-100"></th>
      <th><input type="text" class="w-100"></th>
      <th><input type="text" class="w-100"></th>
    </tr>
    <tr>
      <th class="align-middle">temps minimal (hh:mm)</th>
      <th><input type="text" class="w-100"></th>
      <th><input type="text" class="w-100"></th>
      <th><input type="text" class="w-100"></th>
      <th><input type="text" class="w-100"></th>
    </tr>
  </tbody>
</table>
  
+++bulle r3do
La personne qui a trouvé le temps le plus court vient nous montrer comment elle l'a trouvé au tableau !
+++

### Calcul du plus court chemin à l'aide d'un algorithme glouton

Un réseaux entre différents points peut être représenté sous la forme d'un graphe. Un graphe est un diagramme contenant des nœuds reliés par des arrêtes.

+++image "/images/md/G522BEBH3A7D6638B98H3A44A44DD8" col-6 mx-auto

Pour calculer la plus courte distance, plusieurs approches algorithmiques sont possibles.

+++question write "Rappelez ce qu'est un algorithme"

+++spoil
  Un algorithme est une suite d'instructions à suivre afin d'obtenir un résultat à un problème. Un algorithme peut être comparé à une recette de cuisine par exemple.
spoil+++

+++imageFloat /images/md/GB9383995DDBDGEE7BC5AHC65E87E9
Les algorithmes peuvent être rangés dans différentes catégories. Certains algorithmes sont qualifiés de <b>glouton</b>. Ces algorithmes ont la particularité suivante : à chaque étape d'exécution de l'algorithme, celui-ci cherche la meilleure solution au problème dans l'espoir d'obtenir à la fin la meilleure solution. 
+++



Voici ci-dessous la représentation d'un réseau routier entre différentes villes.

+++image "/images/md/93BDC2H9832E9E257G62H94EAD94C8" col-10 mx-auto

Et voici un exemple d'algorithme glouton : 

<pre class="p-2"><code class="markdown">Se placer dans la ville de départ
Tant que la ville d'arrivée n'est pas atteinte
  Depuis la ville actuelle, aller dans la ville voisine (non visités encore) la plus proche
</code></pre>

À chaque étape, l'algorithme choisit la solution qui semble être la meilleure, il s'agit bien d'un algorithme glouton. 

+++question write "Si je souhaite aller de Montpellier à Valence en utilisant l'algorithme glouton, par quelle(s) ville(s) vais-je passer ? combien de kilomètres au total je vais parcourir ?"

+++spoil
  Montpellier > Avignon > Valence, pour un total de 71 + 77 = 148 kilomètres
spoil+++

+++question write "Si je souhaite aller de Montpellier à Saint-Étienne en utilisant l'algorithme glouton, par quelle(s) ville(s) vais-je passer ? combien de kilomètres au total je vais parcourir ?"

+++spoil
  Montpellier > Avignon > Valence > Lyon > Saint-Étienne, pour un total de 71 + 77 + 68 + 54 = 270 kilomètres
spoil+++

+++question write "Est-ce que le trajet trouvé dans la question précédente est le plus court ? Conclure sur l'efficacité de l'algorithme glouton proposé"

+++spoil
  Montpellier > Saint-Étienne, pour un total de 188 kilomètres. L'algorithme est efficace pour certains trajets mais pas d'autres, cet algorithme n'est donc pas très fiable.
spoil+++

### Résumons

- Trouver un itinéraire pour aller, par exemple, d'une ville à une autre revient à déterminer le **meilleur chemin** pour aller d'un point A vers un point B sur un graphe.
- Un graphe est composé de nœuds (ici les villes) et d'arêtes (ici les routes)
- Les critères qualifiant si le meilleur chemin sont variés : la distance parcourue, le temps du trajet, le coût en énergie (fossile et/ou électrique), le coût écologique, ...
- Les outils permettant de calculer un itinéraire utilisent des données préalablement collectées et des algorithmes. 
- Un algorithme **glouton** est un algorithme qui cherche la meilleure solution à chaque étape dans l'espoir d'obtenir à la fin le meilleur résultat pour un problème donné. Ces algorithmes ne trouvent pas toujours la solution la plus optimale à un problème donné.