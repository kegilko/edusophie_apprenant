# Données de géolocalisation : les trames NMEA

+++programme
  Décrire le principe de fonctionnement de la géolocalisation
  Décoder une trame NMEA pour trouver des coordonnées géographiques.
  
  Différencier les systèmes de navigation par satellites
  Extraire les coordonnées géographiques de la plupart des appareils : smartphones, voitures, drones, ...
+++

+++prof prévu pour deux séances d'une heure

+++sommaire

## Constellations de satellite et géolocalisation

+++video "https://www.youtube.com/watch?v=4sht2CDVQ2w" "Galileo, qu’est-ce que c’est ?"

+++question write "[00:22] Donner un exemple d'utilisation de la technologie GPS pour les domaines suivants :"

- Le domaine de l'agriculture
- Le domaine de la recherche scientifique 

+++spoil
  <ul>
    <li><b>Agriculture</b> : contrôler le taux d'humidité des sols à l'aide de drones. Le signal émis par le satellite est reçu par le drone directement et indirectement peu de <b>temps</b> après (reflet du signal sur le sol). Selon la différence de <b>temps</b>, on peut déduire certaines propriétés du sol (comme le taux d'humidité).</li>
    
    +++image "/images/md/G83G4BBD69F6BAFD3C8A244EHCFAB8" col-4 mx-auto
    
    <li><b>Recherche scientifique</b> : le suivis par satellite d'espèces animales (domaines de l'environnement, de la biodiversité, ...) </li>
  </ul>
spoil+++

<hr>

+++bulle r3do
  le système satellite GPS 'Navstar' a été lancé dans l'espace le 22 février 1978.
+++

+++bulle r3do droite
  aujourd'hui on dénombre une trentaine de satellite pour chacune des constellations GPS ( +++flag-us ) , Galileo ( +++flag-eu ) et Glonass ( +++flag-ru )
+++

+++question write "[01:39] Quels sont les principales forces du système Galileo ?"

+++spoil
  <ul>
    <li>Les satellites Galileo embarquent les dernières innovations technologiques. Cela permet par exemple d'obtenir de meilleures mesures de localisation</li>
    <li>Le projet Galileo est un projet européen fédérateur, autonome et strictement réservé à un usage civil</li>
    <li>Le système Galileo n'est pas exclusif et se complète avec les autres systèmes de navigation par satellites pour améliorer la précision des mesures</li>
  </ul>
spoil+++

<hr>

+++question write "De quel célèbre scientifique s'inspire le nom Galiléo ? Que peut on dire sur ce scientifique ?"

+++spoil
   Galilée est un mathématicien, géomètre, physicien et astronome du XVII<sup>e</sup> siècle. Il fut entre autre le défendeur du système héliocentrisme 
   
   +++spoil "héliocentrisme ?" nolabel
-> L'héliocentrisme est le système cosmologique qui place le Soleil au centre de l'univers (et non la Terre)
   spoil+++
   
spoil+++

### Résumons

- Il existe plusieurs systèmes de navigation par satellites : GPS ( américain ) , Galileo ( européen ) et Glonass ( russe ) 
- Galileo est un projet européen utilisant sa propre constellation de satellites dont l'usage est strictement civil

## Transmissions de données de localisation : la trame NMEA

### Introduction

Les différents composants d’un appareil électronique (ordinateur, serveur, smartphone, tablette, drones, ...) communiquent par des **protocoles** normalisés. Ainsi, les puces électroniques qui effectuent les calculs de positionnement envoient leurs résultats présentés suivant une trame normalisée. 

+++question write "Qu'est-ce qu'un protocole ?"

+++spoil
  Un protocole informatique est un ensemble de règles sur lesquelles s'accordent différents appareils pour pouvoir communiquer entre eux
spoil+++

Le développeur d’un dispositif souhaitant connaître la géolocalisation d'un appareil sait qu’il pourra exploiter cette trame pour obtenir ces informations. Il existe plusieurs versions de la trame NMEA, nous étudions ci-dessous la version NMEA 0183.

Imaginons une application sur smartphone qui permet de connaître en temps réel la position d'un ensemble de drones autonomes. Un bouton dans l'application du smartphone permet de récupérer les dernières coordonnées de chaque drone stockées dans le serveur : 

+++image "/images/md/99H3FD49FBH9FG45FC7E9BC679BCDC" col-6 mx-auto

+++question write "Qu'est-ce qu'un serveur ?"

+++spoil
  Un serveur est un ordinateur contenant une application qui attends continuellement de recevoir des données. Lorsque le serveur reçoit des données, il les traite 
spoil+++

Notre serveur va donc recevoir périodiquement les trames envoyées par les drones. Voici à quoi ressemble le contenu d'une trame NMEA 0183 : 

<div class="text-center mb-3"><span class="border border-dark p-2">$GPGGA,064036.289,4836.538,N,0740.937,E,1,04,3.2,200.2,M,,,,0000*0E</span></div>

- les différentes données dans la trame sont séparées par des virgules. Certaines données peuvent ne pas être renseignées ( contenu vide entre deux virgules )
- les coordonnées sont représentées par la latitude et la longitude. Ces mesures sont exprimées en degrés et minutes d'arc 

+++image "/images/md/667CA47EA6H4GDCDHH846H493BEBEH" col-8 mx-auto

### Analyse de trames NMEA

Intéressons nous plus précisément aux données en lien avec la géolocalisation : 

<div class="text-center mb-3"><span class="p-2">$GPGGA<b style="font-size:1.5rem">,</b><b class="text-red">064036.289</b><b style="font-size:1.5rem">,</b><b class="text-green">4836.538</b><b style="font-size:1.5rem">,</b><b class="text-green">N</b><b style="font-size:1.5rem">,</b><b class="text-blue">0740.937</b><b style="font-size:1.5rem">,</b><b class="text-blue">E</b><b style="font-size:1.5rem">,</b>1<b style="font-size:1.5rem">,</b>04<b style="font-size:1.5rem">,</b>3.2<b style="font-size:1.5rem">,</b><b class="text-violet">200.2</b><b style="font-size:1.5rem">,</b><b class="text-violet">M</b><b style="font-size:1.5rem">,,,,</b>0000*0E</span></div>

- <span class="text-red">donnée n<sup>o</sup>1, <b>la date de la création de la trame</b></span>
    - la trame a été créée à <b class="text-red">06</b> heure, <b class="text-red">40</b> minutes et <b class="text-red">36.289</b> secondes
- <span class="text-green">données n<sup>o</sup>2 et 3, <b>la latitude Nord en degrés et minutes d'arc: </b></span>
    - les deux premier chiffres de gauche correspondent aux degrés
    - les autres chiffres correspondent aux minutes d'arc
    - la donnée N signifie Nord et est l'indicateur de la latitude (valeurs possibles : N ou S)
      - <b class="text-green">4836.538 N</b> signifie donc : <b class="text-green">48</b> degrés et <b class="text-green">36.538</b> minutes d'arc <b class="text-green">N</b>ord

- <span class="text-blue">données n<sup>o</sup>4 et 5, <b>la longitude Est en degrés et minutes d'arc</b></span>
    - les deux premier chiffres de gauche correspondent aux degrés
    - les autres chiffres correspondent aux minutes d'arc
    - la donnée E signifie Est et est l'indicateur du type de longitude (valeurs possibles : E ou W)
      - <b class="text-blue">0740.937 E</b> signifie donc : <b class="text-blue">07</b> degrés et <b class="text-blue">40.937</b> minutes d'arc <b class="text-blue">E</b>st
- <span class="text-violet">données n<sup>o</sup>9 et 10, <b>l'altitude en Mètres</b></span>


Voici les dernières trames reçues par notre serveur : 

<table class="table table-sm">
  <thead>
    <tr>
      <th class="text-center">n<sup>o</sup> drone</th>
      <th>Contenu de la trame</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-center">0</td>
      <td>$GPGGA,104927.017,4319.439,N,0313.639,E,1,12,1.0,124.3,M,,,,*6C</td>
    </tr>
    <tr>
      <td class="text-center">1</td>
      <td>$GPGGA,105528.227,4340.305,N,0356.079,E,1,12,1.0,101.7,M,,,,*64</td>
    </tr>
    <tr>
      <td class="text-center">2</td>
      <td>$GPGGA,102729.112,4836.538,N,0740.937,E,1,12,1.0,170.2,M,,,,*6F</td>
    </tr>
  </tbody>
</table>
  
<div class="text-center mb-3">les trames se trouvent +++lien "/fichiers/F4B6B89CE7HDH8FDC3D6EE73CFBG84" "dans ce fichier"</div>  
  
+++question write "Analysez les trames et complétez le tableau ci-dessous"

<table class="table table-sm">
  <thead>
    <tr>
      <th class="text-center">n<sup>o</sup> drone</th>
      <th>date de création de la trame</th>
      <th>latitude (en degrés-minutes d'arc)</th>
      <th>longitude (en degrés-minutes d'arc)</th>
      <th>altitude (en mètre)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-center">0</td>
      <td class="text-center">10 h 49 mn et 27.017 s</td>
      <td class="text-center">43 degrés 19.439 minutes d'arc N</td>
      <td class="text-center">03 degrés 13.639 minutes d'arc E</td>
      <td class="text-center">124.3 mètres</td>
    </tr>
    <tr>
      <td class="text-center">1</td>
      <td>+++spoil text-center nolabel
        10 h 55 mn et 28.227 s
      spoil+++ </td>
      <td>+++spoil text-center nolabel
        43 degrés 40.305 minutes d'arc N
      spoil+++ </td>
      <td>+++spoil text-center nolabel
        03 degrés 56.079 minutes d'arc E
      spoil+++ </td>
      <td>+++spoil text-center nolabel
        101.7 mètres
      spoil+++ </td>
    </tr>
    <tr>
      <td class="text-center">2</td>
      <td>+++spoil text-center nolabel
        10 h 27 mn et 29.112 s
      spoil+++ </td>
      <td>+++spoil text-center nolabel
        48 degrés 36.538 minutes d'arc N
      spoil+++ </td>
      <td>+++spoil text-center nolabel
        07 degrés 40.937 minutes d'arc E
      spoil+++ </td>
      <td>+++spoil text-center nolabel
        170.2 mètres
      spoil+++ </td>
    </tr>
  </tbody>
</table>

<hr>

+++bulle r3do
  si je veux savoir où se trouve le drone n°3, comment je peux m'y prendre ? 
+++

+++question write "lisez le texte ci-dessous puis complétez le <b>schéma récapitulatif</b>"

Le serveur de notre application reçoit la trame d'un drone. Comme on l'a vu, les données dans la trame contiennent la latitude et la longitude. Cependant, notre serveur ne sait pas faire la correspondance entre une latitude/longitude et le nom de la ville où se trouve ces coordonnées.

Notre serveur va utiliser un service extérieur qui permet d'obtenir le nom de la ville correspondant aux coordonnées soumises. Un tel service est proposé par de nombreux serveurs spécialisés, nous utiliserons ici le serveur nominatim.openstreetmap.org.

Pour pouvoir utiliser le service proposé par nominatim.openstreetmap.org, un programme dans le serveur va devoir convertir la latitude et la longitude de la trame en degrés décimaux.

Une fois les données converties, elles sont transmises au serveur nominatim.openstreetmap.org. Ce dernier rassemble les informations liées aux coordonnées fournies en entrée. Il envoie ensuite le résultat de ses recherches au serveur de notre application.

**Schéma récapitulatif** : 

+++image "/images/md/C86E868G2BHB9967CH8E5G2F5ECHH7" col-11 mx-auto

+++spoil d-print-none
  +++image "/images/md/F658945D4GCH4D7DHF2DC3AG3B4BFE" col-11 mx-auto
spoil+++

### Analyse et exploitation automatique d'une trame à l'aide d'un programme python

Jusqu'à la fin de cette activité, nous nous concentrerons seulement sur le programme chargé de convertir et d'envoyer les données converties au serveur nominatim.openstreetmap.org. Le but est d'analyser un code python pour continuer à se familiariser avec ce langage.

Voici l'algorithme qui décrit dans les grandes lignes le comportement de notre programme : 

```markdown
# Étape 0 : 
  => Extraire d'une trame fournie la latitude et la longitude 
# Étape 1 :
  => Convertir la latitude en degrés décimaux 
  => Convertir la longitude en degrés décimaux 
# Étape 2 :
  => Envoyez les données converties au service de recherche d'informations du serveur nominatim.openstreetmap.org
  => Stocker dans une variable le résultat renvoyé par le serveur nominatim.openstreetmap.org
# Étapes 3 : 
  => Affichez le nom de la ville dans laquelle se trouve le drone de la trame fournie
```

Vous trouverez +++lien "/fichiers/AH9936HEE75H78B5AD24A8B694329C" "dans cette page" le code du programme python qui détermine dans quelle ville se trouve le drone qui a envoyé sa trame.

+++question "Recopiez le code dans un IDE et testez le en utilisant la trame du drone n°0"

<hr>

Lorsque vous exécutez avec succès le programme avec une trame, vous pouvez observer qu'une URL est affichée dans la console de l'IDE. Cette url est l'url utilisée par le programme pour solliciter le service du serveur nominatim.openstreetmap.org

+++question write "Quelle est la ligne dans le programme qui permet d'afficher l'url dans la console ?"

+++spoil
  Il s'agit de la ligne 40 qui utilise la fonction print et qui affiche le contenu de la variable 'url'
spoil+++

+++question write "D'après le code, en fonction de quoi varie l'url qui est générée ?"

+++spoil
  À la ligne 39, nous voyons que la variable url utilise deux autres variables : 'latitude_convertie' et 'longitude_convertie'. En fonction de la trame utilisée au début du programme, ces variables vont varier. Donc l'url adressée au serveur nominatim.openstreetmap.org varie en fonction de la trame utilisée
spoil+++

+++question "Copiez l'url affichée dans la console dans la barre d'url du <b>navigateur firefox</b> et validez"

Vous devriez voir une page ressemblant à ceci : 

+++image "/images/md/H58EH45A8H8GB5829B665345CDG9FA" col-8 mx-auto

+++question "Rappelez ce qu'est le json"

+++spoil
  Le json est un format léger de données structurées. Les données structurées sous ce format sont faciles à analyser et modifier pour un ordinateur. (tout comme le format csv vu aussi au chapitre précédent)
spoil+++

<hr>

Actuellement. le programme affiche la ville où se trouve le drone qui a généré la trame. 

+++question write "Quelles lignes doit on ajouter à la fin du programme pour afficher dans la console la région et le code postal du lieux localisé par la trame ?"

+++spoil

  ```python
print("region : " + response['address']['state'])
print("code postal : " + response['address']['postal'])
  ```

spoil+++

+++question write "À l'aide du programme et des trames envoyées par les 3 drones, complétez le tableau ci-dessous"

<div class="text-center mb-3">pour rappel, les trames sont disponibles +++lien "/fichiers/F4B6B89CE7HDH8FDC3D6EE73CFBG84" "dans ce fichier"</div>

<table class="table table-sm">
  <thead>
    <tr>
      <th class="text-center">n<sup>o</sup> drone</th>
      <th>Contenu de la trame</th>
      <th>ville</th>
      <th>region</th>
      <th>code postal</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-center">0</td>
      <td>$GPGGA,104927.017,4319.439,N,<br>0313.639,E,1,12,1.0,124.3,M,,,,*6C</td>
      <td class="col-3">+++spoil text-center nolabel
        Béziers
      spoil+++ </td>
      <td class="col-3">+++spoil text-center nolabel
        Occitanie
      spoil+++ </td>
      <td class="col-3">+++spoil text-center nolabel
        34500
      spoil+++ </td>
    </tr>
    <tr>
      <td class="text-center">1</td>
      <td>$GPGGA,105528.227,4340.305,N,<br>0356.079,E,1,12,1.0,101.7,M,,,,*64</td>
      <td>+++spoil text-center nolabel
        Teyran
      spoil+++ </td>
      <td>+++spoil text-center nolabel
        Occitanie
      spoil+++ </td>
      <td>+++spoil text-center nolabel
        34820
      spoil+++ </td>
    </tr>
    <tr>
      <td class="text-center">2</td>
      <td>$GPGGA,102729.112,4836.538,N,<br>0740.937,E,1,12,1.0,170.2,M,,,,*6F</td>
      <td>+++spoil text-center nolabel
        Oberhausbergen
      spoil+++ </td>
      <td>+++spoil text-center nolabel
        Grand Est
      spoil+++ </td>
      <td>+++spoil text-center nolabel
        67205
      spoil+++ </td>
    </tr>
  </tbody>
</table>

<br>

+++question write "Après quelques recherches sur Internet, expliquez ce que permet de faire la fonction python 'split' ? (utilisée à la ligne 23)"

+++spoil
  La fonction split sert à découper une chaîne de caractère ( +++flag-us <code>string</code>) selon un motif. Cela permet de séparer les données contenues dans une chaîne de caractère et de pouvoir les manipuler facilement.
  
  Exemple d'utilisation : 
widget python
phrase = "le chat mange la souris"
mots = phrase.split(' ') # le découpe la phrase à chaque espaces
print(mots[0]) # j'affiche le premier mot de la phrase
print(mots[3]) # j'affiche le quatrième mot de la phrase
widget
spoil+++

### Résumons

- un protocole informatique est un ensemble de règles sur lesquelles s'accordent différents appareils pour pouvoir communiquer entre eux 
- un certains nombre d'appareils de notre quotidien et/ou dans l'industrie (smartphone, voiture, avion, drone, ...) contiennent une puce de géolocalisation
- une puce de géolocalisation peut, à l'aide par exemple d'un systèmes de navigation par satellites, générer une trame NMEA
- une trame NMEA est un message composé de plusieurs champs qui contiennent des informations de géolocalisation spécifiques. Les principales informations sont : 
    - l'heure de la création de la trame
    - la latitude de l'appareil 
    - la longitude de l'appareil 
    - l'altitude de l'appareil
