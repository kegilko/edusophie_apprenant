# Les données personnelles et le cloud

+++programme
Définir une donnée personnelle
Utiliser un support de stockage dans le nuage
Partager des fichiers

Identifier une donnée personnelle
Définir ce qu'est le cloud
Justifier votre point de vue à l’aide d’arguments
Apprendre à écouter les arguments opposés et à changer de position le cas échéant
+++

+++sommaire 1

## Correction de la fiche de préparation de débat

+++question "Qu'est-ce qu'une donnée personnelle ?"

+++spoil
  Une donnée personnelle est une donnée qui permet d'identifier directement ou indirectement une personne physique
spoil+++

+++bulle r3do
  une définition plus détaillée se trouve à la toute fin de ce document
+++

+++question "Qu'est-ce que le cloud ?"

+++spoil
  Le cloud désigne l'accès à des ressources informatiques (stockage, applications, puissance de calcul, données, ..) situées dans des serveurs informatiques distants
spoil+++

+++bulle r3do
  la démocratisation d'Internet en très haut débit a permis aux technologies du cloud de se développer
+++

+++bulle r3do droite
  le cloud permet de stocker massivement des données, y compris des données personnelles
+++

## Débat sur les données personnelles

Il existe plusieurs manières d'organiser un débat. Dans cette séance, nous animons **un débat mouvant**. 

Pour organiser ce type de débat :

- Nous avons bien sur besoin d'un ou plusieurs sujets sur lesquels débattre. De préférence des sujets clivants où il est difficile d'obtenir un consensus

+++bulle r3do
le but du débat n'est pas de trancher sur un sujet mais de construire une réflexion collective sur des sujets sociétaux complexes
+++

- Nous débattons en étant tous debout ! dans un **débat mouvant**, vous êtes amené à changer (souvent) de place
- Nous avons besoin d'organisateur(rice)s : 
    - Un rapporteur qui se charge de noter les informations importantes qui émergent du débat
    - Un arbitre qui distribue la parole auprès des participants qui lèvent la main
    - Un maître du temps qui s'assure que le débat  

+++bulle r3do
en vous appuyant sur votre préparation au débat et au déroulement du débat, vous devrez ensuite rédigez une synthèse sur un des sujets du débat
+++

## À vos marques, prêts... partez !

<div class="d-none d-print-block">
  Les sujets du débat sont disponibles sur le cours en ligne ( lien : <a href="https://edusophie.com/s/2F2GF2" target="_blank">https://edusophie.com/s/2F2GF2</a> )
</div>

<div class="d-print-none">

  <div class="text-center">
  +++spoil "sujet n°0" nolabel
    <p class="h4">Sujet n°0</p>
    <p class="rounded h3 border border-2 border-dark p-2 text-blue"><b>Si vous n'avez rien à vous reprocher...<br>vous n'avez rien à cacher !</b></p>
  spoil+++
  </div>
  
  <div class="text-center">
  +++spoil "sujet n°1" nolabel
    <p class="h4">Sujet n°1</p>
    <p class="rounded h3 border border-2 border-dark p-2 text-blue"><b>L’anonymat sur Internet favorise les formes de violences</b></p>
  spoil+++
  </div>
  
  <div class="text-center">
  +++spoil "sujet n°2" nolabel
    <p class="h4">Sujet n°2</p>
    <p class="rounded h3 border border-2 border-dark p-2 text-blue"><b>Il faut interdire l’anonymat sur Internet</b></p>
  spoil+++
  </div>

<hr>

</div>

+++consignes
<ol start="0">
  <li>Un sujet est choisi</li>
  <li>Un élève est désigné pour être le <b class="text-yellow">maître du temps</b></li>
  <li><b class="text-yellow">(<i class="fas fa-stopwatch"></i> 30 secondes)</b> Réflexion individuelle sur le sujet : êtes vous <b class="text-green">plutôt contre</b> ou <b class="text-blue">plutôt pour</b> ?</li>
  <li>Temps de réflexion terminé ! <ul>
    <li>Les personnes qui sont <b class="text-green">plutôt contre</b> vont à <b class="text-green">gauche de l'enseignant</b></li>
    <li>Les personnes qui sont <b class="text-blue">plutôt pour</b> vont à <b class="text-blue">droite de l'enseignant</b></li>
  </ul></li>
  <li><b class="text-yellow">(<i class="fas fa-stopwatch"></i> 7 minutes max)</b> le but du jeu : <b>amener plus de personnes dans votre groupe</b>, pour cela : <ul>
    <li>Levez la main</li>
    <li>Attendez que l'arbitre vous donne la parole</li>
    <li>Une fois que vous avez la parole, donner un argument en faveur de votre groupe</li>
  </ul></li>
</ol>

<div class="bulleGenerator r3do col-10 mb-2 mx-auto " style="break-inside: avoid;"><div class="before d-lg-none d-print-none-2 d-flex justify-content-start"><img src="/images/avatars/r3do.png" class="w-25"></div><div class="rounded p-2 in d-flex justify-content-center align-items-center"><img src="/images/avatars/r3do.png" class="d-none d-lg-block d-print-block" style="width: 15%;"><div class="espace d-none d-lg-block d-print-block"></div><p class="mb-0"><span class="name">R3do : </span>l'argument présenté par ton camarade te semble pertinent ? <b>change de groupe</b> :]</p></div></div>
+++

## Fin du débat

+++todo Partie non exploitée finalement pendant le débat car les élèves ne changeaient pas de groupe. Réfléchir à comment améliorer ce point. En attendant, le code python est caché

<div class="d-none">

### Quelques données relatives au débat

<div class="d-none d-print-block">
  Une mini-synthèse est disponible sur le cours en ligne ( lien : <a href="https://edusophie.com/s/2F2GF2" target="_blank">https://edusophie.com/s/2F2GF2</a> )
</div>

<div class="d-print-none">

widget python
class Sujet:
  def __init__(self, nomDuSujet:str):
    self.nomDuSujet = nomDuSujet
    self.arguments = {}
  def ajouterArgument(self, argument:str, nombreElevesConvaincus:int):
    self.arguments[argument] = str(nombreElevesConvaincus)
    return self
  def __str__(self):
    return self.nomDuSujet

# Sujet 0
sujet0 = Sujet("Si vous n'avez à vous reprocher, vous n'aviez rien à cacher")
sujet0.ajouterArgument("", 1)
sujet0.ajouterArgument("", 3)

# Sujet 1
sujet1 = Sujet("L’anonymat sur Internet favorise les formes de violences")
sujet1.ajouterArgument("", 3)
sujet1.ajouterArgument("", 2)

# Sujet 2
sujet2 = Sujet("Donc il faut interdire l’anonymat sur Internet")
sujet2.ajouterArgument("", 2)
sujet2.ajouterArgument("", 4)

sujets = [sujet0, sujet1, sujet2]

print("### Synthèse ###")

csv = "nom du sujet;argument;nombre de personnes convaincues\n"
for sujet in sujets:
  print("\n## Sujet : '",sujet,"'\n")
  print("Les 3 meilleurs arguments :")
  arguments = sorted(sujet.arguments.items(), key=lambda x: x[1], reverse=True)
  max = 0
  for argument in arguments:
    if(max < 3):
      print(argument[1] + " : " + argument[0])
    csv += sujet.nomDuSujet.replace(";", ":") + ";" + argument[0].replace(";", ":") + ";" + argument[1] + "\n"
    max += 1

print("\n\n### données des argument en format CSV ###\n")
print(csv)
widget

</div>
</div>

### Rédiger sa synthèse

Vous devrez rédiger chez vous une synthèse sur un des sujets du débat. Voici +++link "https://www.edusophie.com/apprenant/00-__-_SNT/01-__-_les-_donnees-_structurees-_et-_leur-_traitement/02-__-_divers/fiche-_de-_syntese-_de-_debat.md" "le lien" pour accéder à cette fiche.

+++todo bulle retirée suite au todo ci-dessus (code python non exploité)

+++bulle r3do droite
pour la rédaction, n'hésitez pas à vous aider de la fiche de préparation de débat ainsi qu'à la synthèse générée par le programme python ci-dessus !
+++d-none


## Résumons

- Une donnée est un élément se rapportant à un objet, une personne ou encore un événement.
- Une **donnée personnelle** est une donnée identifiant directement ou indirectement une personne physique. Voici quelques exemples :
    - données **directement** identifiantes : nom, prénom, photo où je suis identifiable, vidéo où je suis identifiable, ...
    - données **indirectement** identifiantes : numéro de téléphone, adresse postale, ...
        - derrière une adresse postale se trouve un ensemble de personnes (les personnes de ma famille par exemple). Je fais partie de la famille, donc cette donnée m'identifie indirectement
- Le **cloud** désigne l'accès à des ressources informatiques (stockage, applications, puissance de calcul, données, ..) situées dans des serveurs informatiques distants accessibles à travers un réseau (comme Internet par exemple)