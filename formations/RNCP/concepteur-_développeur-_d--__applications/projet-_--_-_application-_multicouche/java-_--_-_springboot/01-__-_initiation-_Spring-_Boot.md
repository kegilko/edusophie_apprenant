# Initiation à Spring Boot

+++programme
  développer la partie back-end d’une interface utilisateur web
  développer des composants d'accès aux données
  développer des composants métiers
  
  développer une plateforme Web en utilisant des approches et des outils récents
+++

+++sommaire

## Introduction 

Le but de ce support est de se familiariser avec le framework **Spring Boot** en développant un site de photographe. Le site proposera une page d'accueil qui présente le photographe, quelques photos, et la possibilité d'afficher le détaillé d'une photo (date, lieu, ...) en cliquant dessus.

---

+++bulle matthieu
  les frameworks applicatifs suivent des <b>logiques de conceptions différentes</b> (spring boot, angular, symfony, react, flutter...)
+++

+++bulle matthieu droite
  mais ! ils partagent un même socle de fonctionnalités 'de base', donc lorsque vous manipulez pour la première fois un framework applicatif, posez les vous les questions suivantes
+++

- **Comment créer des accès entre le client et le serveur ?** nous parlons ici de système de routage 
- **Comment véhiculer les données entre le frontend et le backend ?** ici nous avons choisi de mettre en place une API REST
- **Comment persister des données côté serveur ?** autrement dit, comment communiquer entre le serveur applicatif et un SGBD ? (qui sera MySQL dans notre cas) 

Nous allons traiter ces sujets un à un

## Système de routage

Dans le cadre d'une application Web, **une route** représente le chemin d'accès vers une **ressource serveur**. Une **ressource serveur** peut contenir un ensemble d'éléments tels que : le code d'une page Web, des images, des musiques, etc...

### Créer une route statique

Voici ci-dessous un exemple présentant la création d'une **route statique**, c'est à dire une route qui ne varie pas : 

```java
// fichier src/main/java/com/formation/sandbox/SandboxApplication.java

package com.formation.sandbox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SandboxApplication {

	public static void main(String[] args) {
		SpringApplication.run(SandboxApplication.class, args);
	}

	@GetMapping(value = {"/home"})
	public String home(@PathVariable String message) {
		return String.format("%s", message);
	}

}
```

Le framework utilise ce qu'on appelle un **système d'annotation** (système utilisé par plusieurs framework), Ce système permet d'ajouter des **meta-données** à des classes ou méthodes Java, ces **méta-données** peuvent être utilisées par des programmes externes au code. Les annotations en Java commencent par `@`.

+++bulle matthieu
  prenez quelques instants pour analyser le code
+++

+++bulle matthieu droite
  quelle <b>route</b> a été définie dans le code ? quelle <b>ressource serveur</b> est générée et renvoyée au client ?
+++

+++spoil
<p>Ici l'annotation de la méthode <code>home</code> est : <code>@GetMapping(value = {"/home"})</code>. Cette '<b>méta-donnée</b>' permet de créer une <b>route</b> dont le chemin est '<code>/home</code>'. Lorsqu'un client tente d'accéder à la <b>ressource</b> '<code>/home</code>' <b>du serveur</b>, le serveur exécute la méthode java qui suit l'annotation, à savoir, la méthode <code>home</code></p>

<p>Par convention, on dit que la méthode '<code>home</code>' est un <b>contrôleur</b>.</p>
spoil+++


### Créer une route dynamique

Le site de photos va contenir <i>in fine</i> un nombre variable de photos, et ce nombre peut être assez important. Le client pourra accéder aux différentes pages détaillées des photos à travers les routes '`/photo/1`', '`/photo/2`', '`/photo/3`', etc...

+++bulle matthieu
  à vous de jouer !
+++

+++question "Remplacer le contenu de la classe <code>SandboxApplication</code> par +++lien "/fichiers/3DD5C53553B8D56C64B46F4B7H4FF6" "le contenu suivant" puis tester la nouvelle version du site. Prenez quelques minutes pour analyser le code, l'exécuter et tester le résultat qu'il produit"

+++question "Quel problème présente le code actuel si dans le futur le site devra gérer des centaines de photos ?"

+++spoil
  <p>Actuellement, un contrôleur est créé pour chaque photo, donc si dans le futur le site doit gérer un nombre important de photos, nous allons alors devoir ajouter autant de contrôleurs qu'il y aura de photos.</p>
  
  <p>Dupliquer plusieurs fois un bloc de code en changeant une seule information d'un bloc à l'autre <b>doit vous alerter</b>, il existe <b><u>très certainement</u></b> une manière de factoriser le code. La duplication de code influe négativement la lisibilité de votre programme, son évolutivité et sa résistance aux bugs</p>
spoil+++

+++question "En vous aidant de recherches sur Internet, trouvez le moyen de factorisez les contrôleurs dédiés aux photos en un seul contrôleur"

+++spoil "Indice" nolabel
  <p>Les routes définies par les méthodes <code>photo1()</code>, <code>photo2()</code> et <code>photo3()</code> sont des routes statiques, vous devez créer un contrôleur généroque dont la route, <b>dynamique</b>, permettra de s'adapter en fonction du numéro de la photo demandée</p>
spoil+++

+++spoil
  <p>Il faut retirer les contrôleurs <code>photo1()</code>, <code>photo2()</code> et <code>photo3()</code> et créer la méthode générique <code>photo()</code> avec le bon routage dynamique et adapter le contenu de la méthode : </p>
  
  +++fichier "/fichiers/GGBH4F6D4HG97C3BE7E7B5F6DEFG89" "Le lien vers le code corrigé"
spoil+++

+++bulle matthieu
  pour les personnes en avance, voici ci-dessous quelques idées d'améliorations que vous pouvez apporter a votre programme
+++

+++bulle matthieu droite
  vous pouvez aussi aller épauler vos collègues qui n'ont pas terminés
+++

**Idées d'améliorations**

- ajouter un lien dans chaque page 'photo' permettant de revenir dans la page principale
- implémentez la fonctionnalité suivante : si un utilisateur veut accéder à une photo qui n'existe pas (si il saisit en url par exemple 'http://127.0.0.1:8080/photo/12312313245987'), il doit alors être automatiquement redirigé vers la page d'accueil et cela sans déclencher d'erreur. Quelques recherchez sur Internet sont nécessaire pour savoir comment déclencher une redirection avec spring boot

## Création d'une API REST

+++bulle matthieu
  la partie qui suit est une mise en pratique à travers une succession d'étapes, vous pouvez travailler de votre côté ou en binôme
+++

+++bulle matthieu droite
  les échanges et entre aides sont vivement encouragés, n'hésitez pas à vous lever et aller voir vos collègues si vous bloquez ou si vous voulez montrer vos avancées !
+++

### Restructuration du projet

Un des principaux avantages d'une architecture multicouche est d'offrir une application facilement modulable. La mise en place d'une API REST participe à cette modularité. 

Afin de marquer nettement la frontière entre backend et frontend, nous allons scinder le projet en deux dossiers distincts.

+++question "Renommez le dossier principal 'sandbox' par 'backend'"

+++question "Créez un dossier vide nommé 'sandbox' et placez dedans le dossier 'backend'"

+++question "Dans le dossier 'sandbox', créez un nouveau dossier nommé 'frontend'"

+++bulle matthieu
  vous devriez alors avoir une structure de dossier ressemblant à ceci
+++

+++image "/images/md/2G6E3E472DADG2CB5A7D83D3GBDF7C" mx-auto col-3

### Travail côté backend

Dans cette partie, vous devez mettre en place une **API REST** minimaliste. Une **API REST** fournit un ensemble de méthodes qui permettent au frontend de solliciter des ressources du côté du backend. <u>Les ressources qui circulent à travers ce type d'API sont en format **JSON**</u>.

Le JSON est un format permettant de structurer des informations dans un contenu textuel. Il existe différents types de formats de structuration de données tels que le CSV ou encore le XML, chacun ayant leurs <b style="color:blue">avantages</b> et leurs <b style="color:red">inconvénients</b>. Le JSON est aujourd'hui très utilisé dans l'écosystème technique Web, en raison de sa <b style="color:blue">légèreté</b> (<i class="far fa-thumbs-up"></i> flux réseaux) et de sa <b style="color:blue">simplicité</b> (<i class="far fa-thumbs-up"></i> lisibilité).

Ci-dessous un comparatif de données structurées d'une bilbiothèque sous les différents formats JSON, CSV et XML :


<div class="d-flex flex-wrap justify-content-evenly">
  <div class="border border-3 rounded p-2 align-self-start mb-3">
    <pre class="mb-0">[
  {
    "titre":"Machiavel, Le Prince",
    "editeur": "GF Flammarion",
    "annee": 1980
  },
  {
    "titre":"Les furtifs",
    "editeur": "LA VOLTE",
    "annee": 2019
  },
  {
    "titre":"Eduku geniulon !",
    "editeur": "Polgár László",
    "annee": 2004
  },
]</pre>
  <div class="spoilOpenGenerator mt-1 p-1 nolabel edu-ds text-center pt-0"><button class="d-print-none btn btn-primary">Format</button><div class="content text-left invisible d-print-block" style="height: 0"><b><span>[Réponse] </span></b><b>fichier JSON | nbr chars : 270</b></div></div>
  <div class="spoilOpenGenerator mt-1 p-1 nolabel edu-ds text-center pt-0"><button class="d-print-none btn btn-primary">Première apparition</button><div class="content text-left invisible d-print-block" style="height: 0"><b><span>[Réponse] </span></b><b>2001</b></div></div>
  </div>
  <div class="border border-3 rounded p-2 align-self-start mb-3">
    <pre class="mb-0">titre;editeur;annee
Machiavel, Le Prince;GF Flammarion;1980
Les furtifs;LA VOLTE;2019
Eduku geniulon !;Polgár László;2004</pre>
  <div class="spoilOpenGenerator mt-1 p-1 nolabel edu-ds text-center pt-0"><button class="d-print-none btn btn-primary">Format</button><div class="content text-left invisible d-print-block" style="height: 0"><b><span>[Réponse] </span></b><b>fichier CSV | nbr chars : 121</b></div></div>
  <div class="spoilOpenGenerator mt-1 p-1 nolabel edu-ds text-center pt-0"><button class="d-print-none btn btn-primary">Première apparition</button><div class="content text-left invisible d-print-block" style="height: 0"><b><span>[Réponse] </span></b><b>1972</b></div></div>
  </div>
  <div class="border border-3 rounded p-2 align-self-start mb-3">
    <pre class="mb-0">\<livres>
  \<row>
    \<titre>Machiavel, Le Prince\</titre>
    \<editeur>GF Flammarion\</editeur>
    \<annee>1980\</annee>
  \</row>
  \<row>
    \<titre>Les furtifs\</titre>
    \<editeur>LA VOLTE\</editeur>
    \<annee>2019\</annee>
  \</row>
  \<row>
    \<titre>Eduku geniulon !\</titre>
    \<editeur>Polgár László\</editeur>
    \<annee>2004\</annee>
  \</row>
\</livres></pre>
  <div class="spoilOpenGenerator mt-1 p-1 nolabel edu-ds text-center pt-0"><button class="d-print-none btn btn-primary">Format</button><div class="content text-left invisible d-print-block" style="height: 0"><b><span>[Réponse] </span></b><b>fichier XML | nbr chars : 354</b></div></div>
  <div class="spoilOpenGenerator mt-1 p-1 nolabel edu-ds text-center pt-0"><button class="d-print-none btn btn-primary">Première apparition</button><div class="content text-left invisible d-print-block" style="height: 0"><b><span>[Réponse] </span></b><b>1998</b></div></div>
  </div>
</div>

---

En vous appuyant sur des recherches Internet, suivez les étapes suivantes : 

+++question "Modifiez le contrôleur qui gère l'affichage des pages de photo détaillée afin que ce dernier ne renvoit seulement que l'url et la description de la photo sollicitée sous format JSON"

--- 

+++bulle matthieu
  il existe une multitude d'outils vous aidant à tester une API REST, un des plus connus est Postman
+++

+++question "Installez +++lien "https://www.postman.com/downloads/" "Postman" sur votre OS"

+++question "Testez la route de votre contrôleur (question 6) et vérifiez que ce dernier envoie bien une réponse sous forme de JSON en fonction de l'identifiant de la photo passé en paramètre"

+++spoil
  <p>Plusieurs approches sont possibles, dans la correction ci-dessous, j'ai pris le parti de ne pas utiliser de packages extérieurs pour mener les étapes à bien.</p> 
  
  <p>Par exemple pour la manipulation de données en JSON, il aurai été tout à fait possible d'utiliser un package externe tel que +++lien "https://github.com/FasterXML/jackson" "Jackson" . Cette approche doit être privilégiée si la structure de données en JSON devient complexe (contient des tableaux, des tableaux de tableaux, ...), ce qui n'est pas le cas pour l'instant</p>
  
  +++lien "/fichiers/H68A8C9828E7FE9A5FH6G7AGHA5CHB" "Le lien vers le code corrigé"
spoil+++

### Travail côté frontend

Votre backend offre, à travers l'API REST, un **endpoint** permettant d'obtenir les données détaillées d'une photo. Maintenant, vous allez développer une application frontend (pour commencer...) minimaliste. 

+++question "Dans le dossier frontend, créez les fichiers suivants :"

- index.html (contiendra le code HTML et, éventuellement, CSS de la page d'accueil du site)
- photo.html (affichera les informations détaillées de la photo numéro d'une photo)
- requetesAPI.js (fichier javascript qui contiendra le code permettant de requêter l'API du backend)

+++question "Dans le fichier index.html : ajoutez du code HTML qui affiche un titre et une photo, cliquer sur la photo doit rediriger le visiteur vers le lien 'photo.html?id=1'"

+++question "Incluez dans le fichier 'photo.html' (dans la balise head) le fichier javascript 'requetesAPI.js', vérifiez que le fichier javascript est bien chargé en utilisant dedans l'instruction <code>console.log</code>"

+++bulle matthieu
  <code>console.log</code> affiche un message visible seulement dans l'inspecteur de votre navigateur
+++

+++spoil
  En liens, les contenus des fichiers +++fichier "/fichiers/A5H8F4EGC327322DHCAD75936537F3" "frontend/index.html", +++fichier "/fichiers/H96C9HFFG67C95G5ADD3AH3C4FDC3C" "frontend/photo.html" et +++fichier "/fichiers/76H87A258B78A62A82H7G2G2CEE3B5" "frontend/requetesAPI.js"
spoil+++

---

+++question "Ajoutez une fonction javascript nommée 'infophoto' dans le fichier 'requetesAPI.js'. Cette fonction doit, à l'aide d'une requête AJAX, récupérer les données d'une photo et les afficher dans la page Web"

+++bulle matthieu
  pour vous aider si besoin, vous trouverez ci-dessous les différentes étapes que doit suivre le contenu de la fonction 'infophoto'
+++

+++spoil "Étape 1"
  Récupérer dans une variable le paramètre id qui se trouve dans l'URL
  
  +++bulle matthieu
    N'oubliez pas de vérifier si votre variable contient bien le contenu prévu à l'aide de l'instruction <code>console.log</code>
  +++
spoil+++

+++spoil "Étape 2"
  Exécutez une requête AJAX en javascript qui va solliciter le <b>endpoint</b> 'http://127.0.0.1:8080/photo/{id}' où {id} correspond à la valeur de la variable stockée dans l'étape 1. Attention, il s'agit d'une requête de type GET. (et non POST)
spoil+++

+++spoil "Étape 3"
  À l'aide d'instructions javascript, ajouter dans le 'body' de la page 'photo.html' deux balises HTML 'img' puis 'p'. Ces balises devront contenir respectivement le lien vers l'image de la photo et la description de la photo qui sont les informations qui ont été récupérées suite à l'appel ajax (étape 2)
spoil+++

+++spoil "Étape 3 - erreur Cross-Origin Request"
  Votre requête AJAX semble fonctionner, mais une erreur parlant de "Cross-Origin Request" apparaît dans la console de votre navigateur ? C'est normal, afin d'éviter les éventuels abus/hack, les navigateurs Web bloquent automatiquement les appels AJAX effectués depuis un fichier html directement ouvert dans un navigateur
  
  +++bulle matthieu
    comme la sécurité n'est pas le principal sujet de cette formation, je ne rentre pas plus dans les détails ici
  +++
  
  +++bulle matthieu droite
    mais n'hésitez pas à me solliciter directement si vous voulez en savoir plus !
  +++
  
  Les navigateurs permettent par des moyens plus ou moins faciles de désactiver les protection liées aux CORS (<b class="text-red">à ne faire bien sur que lorsqu'on est dans un environnement de développement !</b>). Sur Chrome par exemple, cela peut se faire facilement +++lien "https://stackoverflow.com/a/42024918" "comme suit". Firefox lui ne permet pas de modifier ce type de sécurité, et il faut alors se rabatre sur un plugin de la communauté.
  
spoil+++

---

+++spoil

  Voici un exemple de contenu possible pour le fichier requeteAPI.js : 

  <pre>    <code class="language-javascript">function infophoto(){

    // étape 1 : récupérer dans une variable le paramètre id qui se trouve dans l'URL
    const urlParams = new URLSearchParams(window.location.search);
    const idPhoto = urlParams.get('id')
    console.log("id de la photo : " + idPhoto)

    // Étape 2 : exécutez une requête AJAX en javascript qui va solliciter le endpoint 'http://127.0.0.1:8080/photo/{id}'
    const HttpRequest = new XMLHttpRequest();
    const url='http://127.0.0.1:8080/photo/' + idPhoto; // attention, le numero du port '8080' doit correctpondre au port utilisé par votre serveur springboot

    HttpRequest.onreadystatechange = function(e){
        if(HttpRequest.readyState == XMLHttpRequest.DONE){ // https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/readyState
            const reponseJSON = JSON.parse(HttpRequest.responseText);
            console.log(reponseJSON)
            reponseJSON_to_DOMelements(reponseJSON) // Étape 3
        }
    }

    HttpRequest.open("GET", url);
    HttpRequest.send();
}

// fonction dont le contenu répond à l'étape 3
function reponseJSON_to_DOMelements(reponseJSON){
    // Étape 3.0 : nous créons 'virtuellement' deux balises 'img' et 'p'
    var newIMG = document.createElement("img");
    var newP = document.createElement("p");

    // Étape 3.1 : nous personnalisons les balises 'img' et 'p'
    newIMG.setAttribute('src', reponseJSON.url)
    newP.innerHTML = reponseJSON.description

    // Étape 3.1 : enfin, nous ajoutons les deux balises dans le body
    document.body.appendChild(newIMG)
    document.body.appendChild(newP)
}

// on exécute la fonction !
infophoto()</code>
  </pre>
spoil+++

###Améliorations possibles

+++bulle matthieu
  vous êtes en avance ? voici quelques pistes pour améliorer le site et prendre de l'avance sur le module
+++

- complexifiez votre API en ajoutant de nouveaux endpoints (informations diverses sur le photographe, liens vers ses réseaux sociaux...)
    - puis, améliorez le code frontend afin d'afficher les informations des nouveaux endpoints de l'API REST
- modifier votre application afin qu'elle utilise des photos stockées sur votre serveur (au lieu d'utiliser des url externes)
- penchez vous sur la persistance des données avec Spring Boot et le SGBD MySQL. Les données dans notre applications sont : les description des photos, les urls vers les photos, les informations du photographe, ...)
    - pour les plus téméraires d'entre vous, développez un formulaire HTML côté frontend permettant d'uploader vers le backend une nouvelle photo