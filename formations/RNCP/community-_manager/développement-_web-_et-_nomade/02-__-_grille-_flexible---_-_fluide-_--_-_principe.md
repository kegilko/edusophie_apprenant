# Grille flexible, fluide : principe

+++programme
  Conception classique vs conception selon une grille
  Importance des blocs
  Points de rupture
  
  Créer facilement des interfaces graphiques Web et responsives
  Utiliser le framework CSS Bootstrap
+++

+++sommaire

## Introduction 

**Face à la multiplication des navigateurs Web et à la variété des tailles d'écrans, développer la partie graphique d'un site Web de A à Z est devenu très chronophage.** 

Des outils existent et permettent de faciliter la construction d'interface graphique Web cross-browser et responsives. Dans cette séance, nous apprenons à utiliser un des frameworks les plus connus, il s'agit de **Bootstrap**.

### Bootstrap : vue d'ensemble 

Bootstrap est un framework CSS développé par Twitter et placé sous licence open source en 2011.

+++bulle matthieu
  qu'est-ce qu'un framework déjà ?
+++

En 2022, Bootstrap en est à sa **5ième version**. Il est téléchargeable depuis +++lien "https://getbootstrap.com/" "son site officiel" et il existe plusieurs moyen de l'intégrer dans un projet. 

Bootstrap utilise un système de grille et permet de placer facilement des éléments responsives à travers 12 (par défaut) colonnes invisibles.

+++image "/images/md/AG743ECC3A4B88958CDDH2G59ECA4H" col-8 mx-auto

## Place à la pratique !

Pour la suite de cette session, je vous invite à télécharger et à décompresser le projet squelette que je vous ai préparé +++lien "/d/formations/website_sandbox.zip" "dans ce lien". 

+++bulle matthieu
  le projet contient une page Web blanche mais contenant déjà les lignes nécessaires pour utiliser bootstrap
+++

+++bulle matthieu droite
  prenons quelques instants pour analyser la structure du projet
+++

+++image "/images/md/BDFF8276868B6HBEH3379E2267B838" col-6 mx-auto

<ul>
  <li>Par convention, la première page d'un site est contenue dans un fichier nommé <b class="isFile"><i class="fas fa-file"></i> index.html</b></li>
  <li>Le dossier <b class="isDirectory"><i class="fas fa-folder"></i> assets</b> contient le code CSS et le code JS (javascript) du site
    <ul>
      <li>Le sous-dossier <b class="isDirectory"><i class="fas fa-folder"></i> contrib</b> contient les librairies et framework externes au projet (CSS et JS), il ne faut pas modifier le code qui se trouve dans ce sous dossier</li>
      <li>Le sous-dossier <b class="isDirectory"><i class="fas fa-folder"></i> custom</b> contient le code CSS et JS du projet</li>
    </ul>
  </li>
  <li>Pour terminer, les dossiers <b class="isDirectory"><i class="fas fa-folder"></i> images</b> et <b class="isDirectory"><i class="fas fa-folder"></i> pages</b> contiennent respectivement les images et les sous-pages html utililées dans le site</li>
</ul>

### Courte initiation en CSS

+++bulle matthieu
  à quoi sert le langage CSS déjà ?
+++

+++question "Ajouter 3 paragraphes dans le fichier <code>index.html</code>"

+++question "En vous aidant de recherches sur Internet, ajouter une règle CSS dans le fichier <code>assets/custom/style.css</code> afin de colorer le texte contenu dans les paragraphes en vert"

### Les premiers pas avec Bootstrap

+++bulle matthieu
  nous traitons cette partie ensemble, le but <i>in fine</i> est de développer un site CV présentant vos créations, vos expériences, etc...
+++

+++question "Dessinons ensemble un squelette du site sous deux formats : le format mobile et le format desktop (ordinateur)"

---

+++bulle matthieu
  nous allons maintenant ajouter du code HTML ainsi que des classes CSS provenant de bootstrap afin de poser nos premiers éléments sur la page principale
+++

La documentation Bootstrap est très complète et fourmille d'exemples. Faîtes bien attention lorsque vous la consulter, il faut choisir la bonne version de la documentation par rapport à la version de Bootstrap utilisée. Nous utilisons dans le projet bootstrap en **version 5.0.2**, voici donc le lien vers +++lien "https://getbootstrap.com/docs/5.0/getting-started/introduction/" "la documentation de cette version".

+++bulle matthieu
  prenons quelques instants pour parcourir la documentation
+++

+++bulle matthieu droite
  comment peut on tester l'affichage d'un site sous différents formats avec son navigateur ?
+++

+++question "Programmons ensuite la structure principale du site (format mobile, on développe en mobile first souvenez vous !) en utilisant les classes CSS fournies par Bootstrap"

---

+++bulle matthieu
  nous souhaitons maintenant que l'interface du site s'adapte automatiquement lorsque 
+++

+++bulle matthieu droite
  nous allons alors utiliser le mécanisme de <b>points de rupture</b> ( +++flag-us <b>breakpoints</b> ), que nous raconte la documentation bootstrap sur ce sujet ?
+++

+++question "Ajoutons des classes pour que l'interface s'adapte au format desktop"

### À vous de jouer ! 

+++question "Améliorez votre CV en vous appuyant sur la documentation de Bootstrap, voici ci-dessous quelques idées d'améliorations (par ordre de difficulté)"

<ul>
  <li>En utilisant le système des 12 colonnes de Bootstrap, ajoutez sur votre page principale une photo (ou un avatar), un bloc de présentation et un bloc listant vos expériences professionnelles. Assurez vous que l'affichage entre mobile et desktop soit propre.</li>
  <li>Transformez votre bloc d'expériences professionnelles en un bloc 'accordéon' (voir documentation Bootstrap...)</li>
  <li>Ajoutez une barre de navigation responsive dans le site permettant de naviguer de la page principale aux pages des articles de votre site (voir documentation Bootstrap...)</li>
  <li>Mettez en place un carousel d'images sur la page principale (voir documentation Bootstrap...)</li>
</ul>

+++bulle matthieu
  n'hésitez pas à vous lever et à aller voir le travail des autres, échangez/partagez sur vos travaux !
+++

--- 

**Pour les personnes en avance :**

Sortons un peu du cadre Bootstrap et intéressez vous à la décoration et animation de votre site. Voici quelques liens intéressants :

<ul>
  <li>+++lien "https://devdojo.com/souptikdn/12-best-css-animation-libraries" "Une page listant des librairies d'animations CSS et/ou JS", pour installer une des librairies proposées demandez un coup de main à votre formateur</li>
  <li>+++lien "https://codepen.io/" "Un site dans lequel" les personnes partagent à la communauté leurs créations CSS et/ou JS que vous pouvez reprendre sur votre site</li>
  <li>+++lien "https://openclassrooms.com/forum/sujet/heberger-votre-site-web-gratuitement-64210" "Un post dans un forum" (parmis tant d'autres...) qui explique comment mettre en ligne un site simple et cela gratuitement</li>
</ul>

