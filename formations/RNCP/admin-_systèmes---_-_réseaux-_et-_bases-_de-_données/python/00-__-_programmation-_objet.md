# Programmation objet

<fieldset class="col-10 mx-auto px-3 py-2 bg-white border border-2 mx-2 mb-2 programmeGenerator"><legend class="text-center mb-0 border border-3 bg-white rounded-3 py-0 mx-auto"><b style="font-size: 2rem">Au programme</b></legend><p class="h3"></p><ul><li>  Particularités du modèle objet</li><li>  Classes et instanciation Constructeurs</li><li>  Les méthodes spéciales</li><li>  Protection d’accès des attributs et des méthodes Héritage : simple, multiple, le polymorphisme</li></ul><p class="h3">Vous apprenez à : </p><ul><li> Développer des programmes facilement maintenables</li><li> Modéliser des concepts et à les implémenter en Python</li></ul></fieldset>


+++sommaire

## Introduction

+++question "Qu'est-ce qu'un langage de programmation et à quel besoin initial cela répondait il ?"

+++spoil nolabel
  <p>En informatique moderne, on peut définir un langage de programmation de la manière suivante : il s'agit d'un langage proposant toute une <b>grammaire</b> et un <b>vocabulaire</b> permettant d'effectuer des opérations au sens large (calculs, traitement de texte, ...) à l'aide d'instructions et avec un système de gestion de mémoire (vive et/ou morte).</p>
  <p>Initialement, nous avons eu besoin de créer des langages de programmations pour effectuer des calculs mathématiques variés.</p>
spoil+++

+++question "Qu'est-ce qu'un paradigme de programmation en informatique ?"

+++spoil nolabel
  <p>Un paradigme de programmation est une manière de classifier les langages de programmations en fonction de leurs fonctionnalités. Nous trouverons par exemple des langages de programmations spécialisés pour les calculs mathématiques, d'autres spécialisés pour des domaines industriels spécifiques etc etc..</p>
spoil+++

+++bulle matthieu
  Quel(s) paradigme(s) de programmation connaissez vous ?
+++

+++question "Quand est-ce que la première version de python a été publiée ? sur quel paradigme de programmation repose ce langage ?"

+++spoil nolabel
  La première version de Python a été publiée le 20 février 1991. C'est un langage de programmation <b>multi-paradigme</b>, il est <b>entre autres</b> : 
  <ul>
    <li>Imperatif : nous pouvons enchainer l'exécution d'instructions les unes après les autres</li>
    <li>Procedural : nous pouvons utiliser découper le code dans des fonctions</li>
    <li>Orienté Objet ( POO ou +++flag-us OOP ): l'objet de ce cours ! </li>
  </ul>
spoil+++

+++bulle matthieu
  aujourd'hui, Python est un langage extrêmement utilisé dans l'industrie de l'intelligence artificielle (deep learning, machin learning, ...) et dans la recherche
+++

### Paradigmes Impératif VS Procédural VS orienté objet

<p class="text-center"><b>Programmation Impérative</b></p>
widget python
age = 20
print(f"j'ai {age} ans !")
widget

---

<p class="text-center"><b>Programmation Procédurale</b></p>
widget python
def afficherAge(age):
  print(f"j'ai {age} ans !")
  
afficherAge(20)
widget

+++bulle matthieu
  pourquoi créer des fonctions dans un programme ?
+++

---

<p class="text-center"><b>Programmation Objet</b></p>
widget python
class Personnage:
  def __init__(self, age):
    self.age = age

  def afficherAge(self):
    print(f"j'ai {self.age} ans !")

claude = Personnage(20)
claude.afficherAge()
widget

+++bulle matthieu
  quel est l'intêret de la programmation orientée objet ?
+++

---

+++bulle matthieu droite
  avec le développement fulgurant de l'industrie informatique dans les années 1970~1990, la <b>maintenabilité</b> des programmes informatiques qui sont de plus en plus conséquents devient un <b>sujet majeur</b>
+++

+++bulle matthieu
  quels critères permettent de rendre un programme informatique résilient au temps ?
+++

+++spoil
  <ul>
    <li><b>La modularité</b> : la capacité de mettre à jour ou de modifier des parties du programmes sans compromettre le reste de l'application (introduction de bugs dus à des effets de bord entre autres)</li>
    <li><b>La lisibilité</b> : la capacité à rester compréhensible malgré le temps qui s'écoule et le nombre de personnes qui sont intervenues dans le programme</li>
    <li><b>La sécurité</b> : la capacité à résister aux intrusions malveillantes</li>
  </ul>
spoil+++

--- 

La programmation orientée objet permet de donc de construire des programmes modulables et plus résistants aux bugs. Cette modularité ainsi que le concept de boites noires (que nous abordons plus bas) participent à une meilleure sécurité de l'application.

## POO : initiation

<div style="" class="d-flex align-self-stretch col-10 offset-1">

<div class="p-2 d-flex align-items-center border border-black rounded col-5">
  <div><i class="far fa-clock fa-4x text-orange"></i></div>
  <div class="ms-2 w-100 text-center">Observer et comprendre les avantages de la POO demande du temps</div>
</div>

<div class="col-2"></div>

<div class="p-2 d-flex align-items-center border border-black rounded col-5">
  <div><i class="far fa-smile fa-4x text-green"></i></div>
  <div class="ms-2 w-100 text-center">Concevoir des programmes en suivant l'approche POO est un exercice assez amusant</div>
</div>

</div>

---

widget python
class Chien:
  def __init__(self, nom):
    self.nom = nom
  
  def avance(self, distance):
    print(f"{self.nom} marche {distance} mètre(s) droit devant")
    
  def cri(self):
    print(f"{self.nom} aboie !")

chien1 = Chien('ondine')
chien2 = Chien('sylphide')

# Interaction entre les animaux
chien1.cri()
chien1.avance(10)
chien2.cri()
chien2.avance(5)
chien1.avance(2)
widget

+++bulle matthieu
  tout d'abord, intéressons nous à la syntaxe de base introduite par la POO
+++

+++bulle matthieu droite
  pendant quelques minutes exécutez le programme ci-dessus, analysez le, modifiez le... puis répondez aux questions ci-dessous (aidez vous de recherches sur Internet si besoin)
+++

+++question "À quoi semble servir l'instruction <code>class</code> ? (à la ligne 1)"

+++spoil
  <p><code>class</code> permet de construire une variable 'complexe', qui est appellée 'objet' en programmation orientée objet. Jusqu'à maintenant, vous avez surtout appris à manipuler des variables de types simples (les variables de type 'integer' qui contiennent un nombre entier, les variables de type 'string' qui contiennent une chaine de caractère, ...). À la différence d'une variable de type 'simple', une <code>class</code>e <b>peut</b> contenir : </p>
  
  <ul>
    <li>des <b>propriétés</b> : ici la <code>class</code>e "chien" possède la propriété "nom" (le nom du chien)</li>
    <li>des <b>méthodes</b> : ici, la <code>class</code>e "chien" possède deux méthodes qui sont <code>avance</code> et <code>cri</code></li>
  </ul>
spoil+++

+++question "Que semblent faire les lignes 11 et 12 ?"

+++spoil
  <p>À partir de la classe chien, nous créons un objet nommé 'chien1' qui contient les informations d'un chien qui s'appelle 'ondine' ainsi qu'un autre objet nommé 'chien2' qui contient les informations d'un autre chien qui s'appelle 'sylphide'. On dit alors que chien1 <b>est une instance</b> de la classe 'Chien'.</p>
  
  <p>Lors de la création de l'objet 'chien1', la méthode <code>__init__</code> de la classe 'Chien' est automatiquement exécutée. Il s'agit d'une méthode générique qui peut être utilisée, ou non, dans n'importe quelle classe en Python. En programmation orientée objet, on appelle cette méthode le constructeur de la classe.</p>
spoil+++

+++question "À quoi semble servir l'instruction <code>self</code> ? (à ligne 8 par exemple)"

+++spoil
  <p><code>self</code> fait référence à l'objet qui est en train d'être manipulé. Analysons par exemple l'exécution de la ligne 15 (<code>chien1.cri()</code>) : </p>
  <ul>
    <li>On appelle <b style="color:orange">l'objet 'chien1'</b></li>
    <li>Avec cet objet, nous exécutons la méthode <code>cri</code> qui est définie dans la classe 'Chien' à la ligne 8</li>
    <li>La méthode <code>cri</code> utilise en paramètre <b style="color:orange">l'objet courant.</b></li>
    <li>Dans la méthode <code>cri</code>, nous affichons le nom rattaché à <b style="color:orange">l'objet courant.</b></li>
    <li>Lors de la création de l'objet 'chien1' à la ligne 11, nous avons précisé que son prénom est 'ondine'</li>
    <li>Et donc, <code>print(f"{self.nom} aboie !")</code> signifie : affiche le nom de <b style="color:orange">l'objet 'chien1'</b> suivi de "aboie !"</li>
  </ul>
spoil+++

+++question "Ajoutez une nouvelle classe <code>chat</code>, un chat doit lui aussi pouvoir <code>avancer</code> et <code>crier</code>. Faîtes ensuite interagir un nouveau chat avec Ondine et Sylphide"

+++spoil
widget python
class Chien:
  def __init__(self, nom):
    self.nom = nom
  
  def avance(self, distance):
    print(f"{self.nom} marche {distance} mètre(s) droit devant")
    
  def cri(self):
    print(f"{self.nom} aboie !")

class Chat:
  def __init__(self, nom):
    self.nom = nom
  
  def avance(self, distance):
    print(f"{self.nom} marche {distance} mètre(s) droit devant")
    
  def cri(self):
    print(f"{self.nom} miaule !")

chien1 = Chien('ondine')
chien2 = Chien('sylphide')
luna = Chat('luna')

# Interaction entre les animaux
chien1.cri()
chien1.avance(10)
luna.cri()
chien2.cri()
chien2.avance(5)
luna.avance(15)
chien1.avance(2)
widget
spoil+++

---

**Pour les personnes en avance, choisissez :**
<ul><li>aidez vos collègues</li></ul>
ou
<ul><li>améliorez le programme (voir ci-dessous)</li></ul>

Vous remarquez que la seule différence entre la classe Chien et la classe Chat est le verbe employé pour le cri (aboie ou miaule). Dans le futur, il est possible que nous ajoutions de nouveaux animaux, le soucis alors est de se retrouver avec un programme conséquent comportant beaucoup de parties dupliquées. 

Il existe un mecanisme en POO qui permet de factoriser le code dans ce type de cas de figure. 

+++question "À l'aide de recherches sur Internet, recherchez et trouvez le moyen de factoriser la classe Chien et la classe Chat afin de limiter la redondance de code"

+++bulle matthieu
  c'est mathématique, plus il y a de code dans un programme, plus il y a de chances d'y introduire des bugs lors de sa modification d'éventuelles modifications
+++

+++bulle matthieu droite
  la factorisation dans un programme est une étape très importante !
+++

### Exercices de modélisation et d'implémentations

La POO permet de calquer très facilement des observations que nous pouvons faire depuis le monde réel vers un programme informatique.

+++bulle matthieu
  modélisons ce qui se trouve autour de nous ! nous avons des personnes, des chaises , et tout cela dans une salle
+++

+++bulle matthieu droite
  pour chacune de ses entités, posons nous les questions suivantes
+++

0. Quelle est la nature de l'entité que j'observe ? quelles sont ses caractéristiques ? 
1. Quelles sont les fonctionnalités de l'entité que j'observe ? qu'est elle capable de faire ?

+++bulle matthieu
  modélisons une personne ensemble
+++

+++diagramme col-3 mx-auto
 classDiagram
      class Personne
      Personne : +string prenom
      Personne : +integer age
      Personne : +parle()
      Personne : +ecoute()
+++

+++bulle matthieu
  puis retranscrivons cette modélisation telle quelle dans une classe python
+++

widget python
class Personne:
  def __init__(self, prenom, age):
    self.prenom = prenom
    self.age = age
  
  def parle(self, phrase):
    print(f"{self.prenom} : {phrase}")

  def ecoute(self, Personne):
    print(f"{self.prenom} écoute attentivement {Personne.prenom}")
widget

À vous de jouer pour la suite ! 

+++question "Instanciez deux personnes après la class Personne : la première personne parle puis la seconde personne écoute attentivement la première. Lorsque le programme est exécuté, il doit donc afficher : "

<div class="col-8 mx-auto p-2 border border-black rounded mb-2"><code>Matthieu : Bonjour à toutes et à tous ! je vais vous expliquer comment fonctionne la POO
Mathilde écoute attentivement Matthieu</code></div>

+++spoil
  
widget python
class Personne:
  def __init__(self, prenom, age):
    self.prenom = prenom
    self.age = age
  
  def parle(self, phrase):
    print(f"{self.prenom} : {phrase}")

  def ecoute(self, Personne):
    print(f"{self.prenom} écoute attentivement {Personne.prenom}")

mathilde = Personne('Mathilde', '20')
matthieu = Personne('Matthieu', '20')

matthieu.parle('Bonjour à toutes et à tous ! je vais vous expliquer comment fonctionne la POO')
mathilde.ecoute(matthieu)
widget
  
spoil+++

+++question "Créez en python le contenu de la classe Chaise qui doit suivre la modélisation affichée ci-dessous. Par défault, une chaise est inoccupée."

+++diagramme col-3 mx-auto
classDiagram
    class Chaise
    Chaise : +integer numero
    Chaise : +string materiaux
    Chaise : +Personne estOccupee
+++

+++spoil

widget python
class Chaise:
  def __init__(self, numero, materiaux):
    self.numero = numero
    self.materiaux = materiaux
    self.estOccupee = null
widget

+++bulle matthieu
  comme vous pouvez le remarquer, une classe peut ne pas contenir de méthodes
+++

+++bulle matthieu droite
  et inversement, une classe peut contenir seulement des méthodes et aucune propriété
+++

spoil+++

--- 

Dans notre programme nous souhaitons qu'une personne puisse s'assoir sur une chaise. 

+++question "Modifiez la classe Personne afin qu'une personne puisse occuper une chaise. Voici l'algorithme du programme :"

<ul>
  <li>Création de l'objet 'nicolas' de type Personne</li>
  <li>Création d'un tableau contenant trois chaises en bois dont les numéros respectifs sont 1, 2 et 3</li>
  <li>'nicolas' s'assoit sur la chaine numéro 2, le programme affiche cette information</li>
  <li>Le programme affiche dans une boucle les informations de toutes les chaises, lorsqu'une chaise est occupée, le prénom de la personne qui l'occupe est affichée</li>
</ul>

Le programme pourrait afficher par exemple : 

<div class="col-8 mx-auto p-2 border border-black rounded mb-2"><code>Nicolas : je m'assoie sur la chaise numéro 2
Informations sur les chaises : 
  - chaise numéro 1 : innocupée
  - chaise numéro 2 : occupée par Nicolas
  - chaise numéro 3 : innocupée</code></div>

+++spoil

widget python
class Personne:
  def __init__(self, prenom, age):
    self.prenom = prenom
    self.age = age
  
  def parle(self, phrase):
    print(f"{self.prenom} : {phrase}")

  def ecoute(self, Personne):
    print(f"{self.prenom} écoute attentivement {Personne.prenom}")
    
  def sassoit(self, Chaise):
    print(f"{self.prenom} : je m'assoie sur la chaise numéro {Chaise.numero}")
    Chaise.estOccupee = self

class Chaise:
  def __init__(self, numero, materiaux):
    self.numero = numero
    self.materiaux = materiaux
    self.estOccupee = None
    
nicolas = Personne('Nicolas', '20')

chaises = [
  Chaise(1, 'bois'),
  Chaise(2, 'bois'),
  Chaise(3, 'bois')
]

nicolas.sassoit(chaises[1])

print("Informations sur les chaises :")
for chaise in chaises:
  if(chaise.estOccupee != None):
    print(f"\t- chaise numéro {chaise.numero} : occupée par {nicolas.prenom}")
  else:
    print(f"\t- chaise numéro {chaise.numero} : innocupée")
widget

spoil+++

---

**Pour les personnes en avance, choisissez :**
<ul><li>aidez vos collègues</li></ul>
ou
<ul><li>améliorez le programme (voir ci-dessous)</li></ul>

+++question "Modélisez une salle et créez la classe Salle correspondante en code python. Le programme doit être capable : "

<ul>
  <li>De créer plusieurs salles</li>
  <li>D'afficher le nombre de chaises dans une salle</li>
  <li>D'afficher le prénom de toutes les personnes qui se trouve dans une salle (une salle peut bien sur être vide)</li>
  <li>D'afficher si oui ou non, une salle contient assez de chaises par rapport au nombre de personnes qui occupent actuellement la salle</li>
</ul>

---

+++bulle matthieu
  continuez à vous exercer dans ces exercices de modélisation ! 
+++

+++bulle matthieu droite
  la couleur correspond à la difficulté
+++

<div class="d-flex flex-wrap justify-content-evenly">
  
  <div class="card border border-3 rounded col-3 align-self-start" style="border-color: green !important;">
    <div class="card-body">
      <div class="text-center"><b class="card-title text-center">Modélisez et implémentez un arbre en python</b></div>
      <hr>
      <p class="card-text">Un arbre contient des branches qui peuvent contenir des feuilles</p>
      <p class="card-text">Un arbre peut grandir indéfiniment et peut peut être un jour atteindre la lune</p>
    </div>
  </div>
  
  <div class="card border border-3 rounded col-3 align-self-start" style="border-color: orange !important;">
    <div class="card-body">
      <div class="text-center"><b class="card-title text-center">Modélisez et implémentez un véhicule de transport personnel en python</b></div>
      <hr>
      <p class="card-text">Une véhicule peut avoir un nombre varié de roues et de sièges de passagers</p>
      <p class="card-text">Un véhicule peut être à l'arret ou alors être en mouvement à une certaine vitesse</p>
      <p class="card-text">Un véhicule peut percuter un mur si il roule trop vite (selon la distance du mur)</p>
    </div>
  </div>
  
  <div class="card border border-3 rounded col-3 align-self-start" style="border-color: darkorange !important; background-image:url('/images/md/H67B6EFC968E3853H6A2BC43EF23H7'); background-position: bottom;background-repeat: no-repeat;">
    <div class="card-body">
      <div class="text-center"><b class="card-title text-center">Modélisez et implémentez des émotions en python</b></div>
      <hr>
      <p class="card-text">Une émotion peut être du rire, de la tristesse, ... </p>
      <p class="card-text">Une émotion a différents degrès d'intensités </p>
      <p class="card-text">Une émotion de forte intensité peut en atténuer une autre</p>
      <p class="card-text">Une émotion peut en entraîner une autre après un certain temps : la peur mène à la colère, la colère mène à la haine, la haine… mène à la souffrance</p>
    </div>
  </div>
  
</div>

## POO : concepts avancés

+++bulle matthieu
  dans cette partie du cours, vous allez rechercher et vous documentez pendant une heure sur 2 concepts importants liés à la programmation objets avancée
+++

+++bulle matthieu droite
  les résultats de vos recherches seront présentés face au reste de la salle
+++

Les deux concepts sont : 

- Les **méthodes spéciales** en POO python (<code>__lt__</code>, <code>__le__</code>, <code>__eq__</code>, ...) qui permettent de comparer des objets entre eux
- Le concept **d'héritage** en POO permettant de partager des propriétés et méthodes d'un objet à l'autre. Cette mécanique permettra par exemple de factoriser très proprement le problème de code redondant soulevé dans l'exercice sur les chiens et les chats

+++bulle matthieu
  préparez des exemples simples pour illustrer vos présentations face à vos autres collègues
+++

## Projet du module 

Comme explicité plus haut, il faut du temps pour pouvoir apprécier les avantages d'un concept abstrait tel que la programmation objet. Travailler sur un projet étalé sur le temps et dans lequel plusieurs personnes interviennent reste un des meilleurs moyens de monter en compétence sur la POO. Je vous propose alors de concevoir un projet en binôme ou trinôme, projet qui sera donc développé en utilisant le paradigme de programmation orientée objet. 

La suite du module vous introduira de nouvelles techniques telle que l'exploitation de bases de données. Vous pourrez donc enrichir votre projet au fur et à mesure du module. 

**Voici le sujet du projet :**

Vous allez créer un mini jeux vidéo, il s'agit d'un jeu de rôle dans lequel l'utilisateur peut se créer un personnage. Le personnage peut avoir une classe et par effet d'héritages POO, la classe pourra profiter ou non de certaines armes pour attaquer et de certaines magies. Il faut également créer des monstres et pouvoir faire combattre son personnage contre ces monstres et ainsi gagner des points d'expériences. 

Le contenu, la structure et les intéractions des classes entre elles est libre, servez vous de ce que vous avez vu en cours ! échangez entre vous ! inspirez vous de codes trouvés sur Internet ! 

---

+++bulle matthieu
  vous êtes aussi libre de proposer votre propre projet dont vous devrez me faire part avant la fin de cette séance ;)
+++

+++bulle matthieu droite
  faites moi s'en part avant la fin de cette séance ;)
+++