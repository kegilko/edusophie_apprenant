# Gérer une base de données

+++programme
  utiliser une base de données avec python et la DB API
  
  concevoir une base de données
  créer une base de données, créer des tables, créer des colonnes
  insérer/modifier/supprimer des valeurs depuis la base de données
+++

+++sommaire

## Introduction 

Dans la vie quotidienne, il est fréquent d'utiliser des informations provenant de bases de données : 
- lorsque nous consultons le catalogue d'une bibliothèque
- lorsque nous effectuons une réservation dans un hotel
- lorsque nous écoutons notre playliste de musique favorite
- ...

+++bulle matthieu
  comment définir ce qu'est une base de données ? quelle(s) différences y-a-t'il entre une base de données et un fichier texte ?
+++

+++spoil
  L'objectif premier d'une base de données est d'organiser un ensemble de données selon une structure qui va optimiser le temps nécessaire pour les stocker et pour les extraire
spoil+++

Nous utilisons aujourd'hui des **SGBD** (**S**ystème de **G**estion de **B**ase de **D**onnées) qui permettent d'interfacer la base de données et les utilisateurs qui ont besoin d;utiliser cette dernière.

+++bulle matthieu
  connaissez vous des SGBD ?
+++

Il existe plusieurs types de bases de données qui répondent à des besoins différents (souvent liés aux structures des données à conserver). Nous nous focalisons ici sur les bases de données relationnelles qui sont majoritairement utilisées aujourd'hui.

## Modélisation de base de données 

+++image "/images/md/E3FDH667994H4E862C2BB9C4A4CE47" col-6 mx-auto

<p></p>

Les bases de données sont composées d'un ensemble de tables qui peuvent contenir un ensemble d'entités **partageant les mêmes propriétés**. Chaque entité dans une table **doit être identifiable de manière unique**. Des relations **peuvent lier** certaines tables entre elles. 

+++bulle matthieu
  cette organisation ressemble furieusement aux exercices de modélisation POO ! 
+++

La modélisation des bases de données se fait en trois principales étapes qui s'effectuent dans cet ordre : 
0. la modélisation **conceptuelle** : représentée par un schéma nommé **MCD** (**M**odèle de **C**onception de **D**onnées)
1. **Puis**, la modélisation **logique** : représentée par un schéma **MLD** (**M**odèle de **L**ogique de **D**onnées)
2. **Puis**, la modélisation **physique** : représentée par un schéma **MPD** (**M**odèle **P**hysique de **D**onnées)

+++bulle matthieu
  tous ensemble, modélisons puis créons une base de données utilisée par un système de gestion simple d'emprunt de livre dans une bibliothèque française
+++

### Créer un dictionnaire de données

+++bulle matthieu 
  déterminons les principales fonctionnalités du système de gestion d'emprunt 
+++

+++spoil
  <ul>
    <li>Une personne inscrite dans la bibliothèque peut emprunter un livre</li>
    <li>Chaque livre peut être emprunté pendant 30 jours</li>
    <li>Si une personne ne rend pas un livre avant sa date de fin d'emprunt, nous la relançons par téléphone</li>
  </ul>
spoil+++

+++bulle matthieu droite
  ensuite, quelles données doit conserver la bibliothèque pour pouvoir fonctionner ?
+++

<div class="col-8 mx-auto">
  <table class="table table-sm">
    <thead>
      <tr class="text-center" style="background-color:lightblue">
        <th>Donnée</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Donnée 1</td>
        <td>description de la donnée 1</td>
      </tr>
      <tr>
        <td>Donnée 2</td>
        <td>description de la donnée 2</td>
      </tr>
      <tr>
        <td>...</td>
        <td>...</td>
      </tr>
    </tbody>
  </table>
</div>

+++spoil
  <div class="col-8 mx-auto">
  <table class="table table-sm">
    <thead>
      <tr class="text-center" style="background-color:lightblue">
        <th>Donnée</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><b>numéro de sécurité sociale NIR</b></td>
        <td>Permet d'identifier de manière unique une personne</td>
      </tr>
      <tr>
        <td><b>nom</b></td>
        <td>nom d'une personne</td>
      </tr>
      <tr>
        <td><b>prénom</b></td>
        <td>prénom d'une personne</td>
      </tr>
      <tr>
        <td><b>numéro de téléphone</b></td>
        <td>numéro de téléphone d'une personne</td>
      </tr>
      <tr>
        <td><b>ISBN-13</b></td>
        <td>Permet d'identifier de manière unique un livre (<b>I</b>nternational <b>S</b>tandard <b>B</b>ook <b>N</b>umber)</td>
      </tr>
      <tr>
        <td><b>date d'emprunt</b></td>
        <td>La date où le livre a été emprunté</td>
      </tr>
    </tbody>
  </table>
</div>
spoil+++

+++bulle matthieu droite
  pour terminer, précisons la nature des données et regroupons les en groupe
+++

<div class="col-8 mx-auto">
  <table class="table table-sm">
    <thead>
      <tr class="text-center" style="background-color:lightblue">
        <th>Donnée</th>
        <th>Description</th>
        <th>Type de donnée</th>
        <th>taille min et max de la donnée</th>
        <th>Groupe</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Donnée 1</td>
        <td>description de la donnée 1</td>
        <td>type de la donnée 1 (booléen, entier, chaîne de caractère, date, ...)</td>
        <td>de X à Y</td>
        <td>groupe A</td>
      </tr>
      <tr>
        <td>Donnée 2</td>
        <td>description de la donnée 2</td>
        <td>type de la donnée 2 (booléen, entier, chaîne de caractère, date, ...)</td>
        <td>de X à Y</td>
        <td>groupe B</td>
      </tr>
      <tr>
        <td>...</td>
        <td>...</td>
      </tr>
    </tbody>
  </table>
</div>

+++spoil
  <div class="col-8 mx-auto">
  <table class="table table-sm">
    <thead>
      <tr class="text-center" style="background-color:lightblue">
        <th>Donnée</th>
        <th>Description</th>
        <th>Type de donnée</th>
        <th>taille min et max de la donnée</th>
        <th>Groupe</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><b>numéro de sécurité sociale NIR</b></td>
        <td>Permet d'identifier de manière unique une personne</td>
        <td>entier</td>
        <td>15 chiffres</td>
        <td>Personne</td>
      </tr>
      <tr>
        <td><b>nom</b></td>
        <td>nom d'une personne</td>
        <td>chaîne de caractères</td>
        <td>de 3 à 20 caractères</td>
        <td>Personne</td>
      </tr>
      <tr>
        <td><b>prénom</b></td>
        <td>prénom d'une personne</td>
        <td>chaîne de caractères</td>
        <td>de 3 à 20 caractères</td>
        <td>Personne</td>
      </tr>
      <tr>
        <td><b>numéro de téléphone</b></td>
        <td>numéro de téléphone d'une personne</td>
        <td>chaîne de caractères</td>
        <td>10 chiffres</td>
        <td>Personne</td>
      </tr>
      <tr>
        <td><b>ISBN-13</b></td>
        <td>Permet d'identifier de manière unique un livre (<b>I</b>nternational <b>S</b>tandard <b>B</b>ook <b>N</b>umber)</td>
        <td>chaîne de caractères</td>
        <td>13 caractères</td>
        <td>Livre</td>
      </tr>
      <tr>
        <td><b>date d'emprunt</b></td>
        <td>La date où le livre a été emprunté</td>
        <td>chaîne de caractères</td>
        <td>10 caractères au format YYYY-MM-DD</td>
        <td>Livre</td>
      </tr>
    </tbody>
  </table>
</div>
spoil+++

### Créer un MCD

Un **MCD** suit les règles principales suivantes :
- Le schéma ne tient pas aucunément compte des technologies informatiques qui seront utilisées pour mettre en place la base de données
- Le schéma représente de manière abstraite à travers des entités et des relations
    - Une entité est seulement représentée par son nom
    - Une relation est symbolisée par <b class="text-red">un sens</b> (pour faciliter la lecture), <b class="text-green">une description</b> et un <b class="text-blue">couple de cardinalitées (x,y)</b> 
    - Une <b class="text-blue">cardinalité</b> peut avoir comme valeur :
        - <b class="text-blue">(0,1)</b> : par exemple, un produit a été commandé par <b class="text-blue">0</b> à <b class="text-blue">1</b> personne
        - <b class="text-blue">(1,1)</b> : par exemple, une personne est originaire de <b class="text-blue">1</b> et <b class="text-blue">1</b> seul pays
        - <b class="text-blue">(0,n)</b> : par exemple, une résidence est habitée par <b class="text-blue">0</b> à <b class="text-blue">n</b> habitant(s)
        - <b class="text-blue">(1,n)</b> : par exemple, un livre a été écrit par <b class="text-blue">1</b> à <b class="text-blue">n</b> auteur(s)

+++bulle matthieu
  rédigés dans les toutes premières phases d'un projet, un MCD doit pouvoir être autant compris par l'équipe MOA (Maitrîse d'ouvrage) que par l'équipe MOE (Maitrîse d'oeuvre) 
+++

#### Exercez vous ! 

Voici un exemple de MCD entre les deux entités 'livre' et 'éditeur'

+++image "/images/md/H428843AF42G8DF5B6A7AF3ABG3E5C" col-10 mx-auto

<p></p>

+++question "Dessinez les MCD pour les entités suivantes"

- L'oeuf et la poule
- Une maison et des pièces
- Un citoyen et un maire (un citoyen peut être maire et un maire correspond est un citoyen)
- Un article Web et un tag (exemples de tag : cuisine, jeuxvideo, sortie, danse, échecs, ...)
- Un acteur et un film
- Un train, un wagon, un passager
- Un voyageur, un pays, une ville

+++bulle matthieu
  revenons-en à notre bibliothèque, comment représenter en MCD les entités personne et livre ?
+++

+++spoil
  +++image "/images/md/8349E25HE3BAG9BA35AGFDG7GBB372" col-10 mx-auto
spoil+++

### Créer un MLD

Le **MLD** fait suite au **MCD**, il suit les règles principales suivantes : 
- Les entités deviennent des **tables** : un table possède le nom de l'entité et les noms des attributs propres à l'entité
    - Les attributs qui permettent d'identifier de manière unique chaque instance d'une table sont appelés **clé primaire** (+++flag-us **primary key**) et sont <u>surlignés</u>
- Les relations, décrites de manières abstraites dans le MCD sont **transformées** pour se rapprocher de l'état final de l'implémentation informatique de la base de données

+++bulle matthieu
  les transformations lors de cette étape sont une opération délicate qu'il faut suivre avec rigueur, vous vous rendrez compte un peu plus loin dans ce support de l'importance de ces opérations de transformations
+++

+++bulle matthieu droite
  voici les principales transformations à connaître et à savoir appliquer
+++

<br>

<p class=" mb-0 h4 text-center text-cyan">Règle de transformation MCD->MLD <b><u>n°1</u></b></p>
<div class="h4 mt-4 mb-4 bg-white text-center"><p class="border border-2 rounded d-inline p-3"><span class="p-2 border border-dark rounded-3" style="background-color:#cccccc;">entité 1</span> <sub><b class="text-blue">(0,1)</b> ou <b class="text-blue">(1,1)</b></sub> <b class="text-red"><=== </b><b class="bg-dark p-1 rounded" style="color:#00e400">association</b><b class="text-red"> ===></b> <sub><b class="text-blue">(0,n)</b> ou <b class="text-blue">(1,n)</b></sub> <span class="p-2 border border-dark rounded-3" style="background-color:#cccccc;">entité 2</span></p></div>

- Dans la liste des attributs de la table représentant l'entité 1, il faut ajouter la clé primaire de la table représentant l'entité 2

<p class=" mb-0 h4 text-center text-cyan">Règle de transformation MCD->MLD <b><u>n°2</u></b></p>
<div class="h4 mt-4 mb-4 bg-white text-center"><p class="border border-2 rounded d-inline p-3"><span class="p-2 border border-dark rounded-3" style="background-color:#cccccc;">entité 1</span> <sub><b class="text-blue">(0,1)</b> ou <b class="text-blue">(1,1)</b></sub> <b class="text-red"><=== </b><b class="bg-dark p-1 rounded" style="color:#00e400">association</b><b class="text-red"> ===></b> <sub><b class="text-blue">(0,1)</b> ou <b class="text-blue">(1,1)</b></sub> <span class="p-2 border border-dark rounded-3" style="background-color:#cccccc;">entité 2</span></p></div>

- On ajoute dans la table représentant l'entité 1 la clé primaire de l'entité 2
- On ajoute dans la table représentant l'entité 2 la clé primaire de l'entité 1

<p class=" mb-0 h4 text-center text-cyan">Règle de transformation MCD->MLD <b><u>n°3</u></b></p>
<div class="h4 mt-4 mb-4 bg-white text-center"><p class="border border-2 rounded d-inline p-3"><span class="p-2 border border-dark rounded-3" style="background-color:#cccccc;">entité 1</span> <sub><b class="text-blue">(0,n)</b> ou <b class="text-blue">(1,n)</b></sub> <b class="text-red"><=== </b><b class="bg-dark p-1 rounded" style="color:#00e400">association</b><b class="text-red"> ===></b> <sub><b class="text-blue">(0,n)</b> ou <b class="text-blue">(1,n)</b></sub> <span class="p-2 border border-dark rounded-3" style="background-color:#cccccc;">entité 2</span></p></div>

- Une **table de correspondance** doit être ajoutée entre la table représentant l'entité 1 et la table de l'entité 2
- La table de correspondance doit contenir les clés primaires des tables des entités 1 et 2 
- Le nom de la table de correspondance peut être la concaténation entre le nom de la table de l'entité 1 et le nom de la table de l'entité 2



---

#### Entraînons nous ensemble pour passer d'un MCD à un MLD !

+++bulle matthieu droite
  reprenons le <b>MCD</b> des entités voyageur, pays et ville
+++

+++image "/images/md/8BA7H5C9DG28HCG3CH7CF2G6D46DB2" col-6 mx-auto

+++bulle matthieu
  il y a deux relations en tout, et la relation la plus facile à transformer est la relation qui lie les entités Pays et Ville
+++

+++bulle matthieu droite
  nous devons appliquer la <span class="text-cyan">règle de transformation <b><u>n°1</u></b></span>, la relation <b>MCD</b>...
+++

+++image "/images/md/6GA89HHC6C8GC25G2DE2DAF7CAEBF4" col-8 mx-auto

+++bulle matthieu
  ...devient donc en <b>MLD</b>...
+++

+++image "/images/md/5D426ED976BFGFG6GGGCAAD9475DH9" col-8 mx-auto bg-white

+++bulle matthieu
  occupons nous maintenant de la relation entre voyageur et ville, nous devons appliquer la <span class="text-cyan">règle de transformation <b><u>n°3</u></b></span>, la relation <b>MCD</b>...
+++

+++image "/images/md/29B756G24HB7GD38622F3464GG73EE" col-8 mx-auto

+++bulle matthieu
  ...devient donc en <b>MLD</b>...
+++

+++image "/images/md/DC8AGB89AAG9H34C3H55AH7BBCB232" col-8 mx-auto bg-white

--- 

Voici donc le passage de la représentation de la base de données depuis le **MCD** vers le **MLD**

<div class="d-flex justify-content-between align-items-end bg-white">
  <div class="col-5 text-center">
    +++image "/images/md/8BA7H5C9DG28HCG3CH7CF2G6D46DB2"
    <p><b>M</b>odèle <b>C</b>onceptuel des <b>D</b>onnées</p>
  </div>
  <div class="col-6 text-center">
    +++image "/images/md/3FDEA67438B7F5BA5D3C9DDFBG5F4C"
    <p><b>M</b>odèle <b>L</b>ogique des <b>D</b>onnées</p>
  </div>
</div>

+++bulle matthieu
  dans la table Ville se trouve la propriété <b class="text-cyan">nomPays</b> qui fait référence à la <b>clé primaire</b> d'une autre table (nomPays), on dit alors que la propriété <b class="text-cyan">nomPays</b> de la table 'Ville' est <b>une clé étrangère</b>
+++

+++bulle matthieu droite
  ce qui identifie de manière unique chaque instance de la <b>table de correspondance</b> <b class="text-cyan">VoyageurVille</b> est la combinaison des <b>deux clés étrangères</b> <b class="text-cyan">idVoyageur</b> et <b class="text-cyan">nomVille</b>
+++

#### Exercez vous !

+++question "Dessinez les représentations MLD des MCD suivants"

- L'oeuf et la poule
- Une maison et des pièces
- Un citoyen et un maire (un citoyen peut être maire et un maire correspond est un citoyen)
- Un article Web et un tag (exemples de tag : cuisine, jeuxvideo, sortie, danse, échecs, ...)
- Un train, un wagon, un passager

+++bulle matthieu
  revenons denouveau à notre bibliothèque, comment représenter en MLD la base de données de notre bibliothèque ?
+++

+++spoil
  +++image "/images/md/9ECCCDCB396473A7CBC2E3H5795B84" col-8 mx-auto bg-white
spoil+++

### Créer un MPD

Le **MPD** est la dernière étape pour schématiser la base de donnée. Le but ici est de compléter le **MLD** afin de s'approcher le plus possible de l'état réel / physique de la base de données, nous prenons alors en compte la stack technique que nous utiliserons côté BDD/SGBD. Le **MPD** suit les principales suivantes : 

- Une table n'est plus composée de propriétés mais de champs
- Les noms des tables et des champs sont normalisés / informatisés : on ne garde des majuscules que lorsque c'est justifié/utile, on enlève les accents, on enlève les espaces, on utilise que des caractères alphabétiques (avec, plus rarement, des caractères numériques)
- Nous précisons pour chacun des champs leur type (utilisé en fonction du SGBD choisi) et (bonus) si ils peuvent être NULL

+++bulle matthieu
  le <b>MLD</b> représentant la base de données d'emprunts de livre...
+++

+++image "/images/md/9ECCCDCB396473A7CBC2E3H5795B84" col-8 mx-auto bg-white m-3

+++bulle matthieu
  ...devient alors en <b>MPD</b> (sachant que nous utilisons le <b>SGBD</b> MySQL)
+++

+++image "/images/md/8C352GE2B4G754B43GAF8CED8BC5G5" col-8 mx-auto bg-white

#### Exercez vous !

+++question "Reprenez le <b>MLD</b> sur les tables Voyageur, VoyageurVille, Ville, Pays et transcrivez le en <b>MPD</b>"

---

+++bulle matthieu
  vous savez donc maintenant comment définir les données nécessaires au bon fonctionnement d'une application (voir, d'un <b>S</b>ystème d'<b>I</b>nformations)
+++

+++bulle matthieu droite
  et à partir de là, comment obtenir un schéma représentant l'état le plus proche de l'implémentation réelle de la base de données.
+++

## Python et la manipulation de base de données

Certaines bibliothèques et frameworks mettent à disposition des **fonctions facilitant la manipulation de bases de données**. Mais selon le degrés de facilité, cela peut présenter **deux inconvénients importants** : 

- celui de nous rendre **dépendant de la logique** de la-dîte bibliothèque
- ainsi que **des limites** de la-dîte bibliothèque aussi complète soit-elle

+++bulle matthieu
  cependant, la quasi-totalité des bibliothèques permettent d'exécuter des requêtes SQL arbitraires
+++

+++bulle matthieu droite
  mon conseil pour les débutants alors, s'assurer de maîtriser un minimum les bases en SQL avant d'apprendre à utiliser des outils plus 'haut niveau'
+++

Nous allons alors par la suite créer et manipuler des bases de données à l'aide de commandes SQL.

Afin de ne pas alourdir le travail, nous utiliserons un SGBD léger, ainsi qu'un logiciel nous permettant de visualiser les évolutions de la base de données : 

- le SGBD sqlite3 qui est déjà intégré dans python depuis python 2.5
- le logiciel opensource +++lien "https://sqlitebrowser.org/" "dbbrowser" qui est léger, dispose d'une UI et fournit toutes les fonctionnalités qu'il faut pour développer dans de bonnes conditions

+++question "Installer le logiciel 'dbbrowser' sur votre ordinateur"

### Création et/ou ouverture d'une base de données avec sqlite

La création et l'ouverture d'une base de données s'effectuent avec la même opération : 

```python
import sqlite3  
  
oConnection = sqlite3.connect('mabasededonnees.db')

oConnection.close() # fermeture explicite (et propre) du flux de connexion entre le script et la BDD
```

Ainsi : 
- Si le fichier `mabasededonnees.db` n'existe pas, il est créé dans le même dossier que le script python 
- Sinon/Puis, le fichier `mabasededonnees.db` est ouvert et géré à travers l'objet `oConnection`

+++bulle matthieu
  si je veux créer ou ouvrir une base de données dans un dossier enfant, comment pourrais-je faire ?
+++

+++bulle matthieu droite
  même question mais pour une base de données situées dans le dossier parent 
+++

+++spoil
  <pre><code class="language-python">import sqlite3  
  
oConnection1 = sqlite3.connect('dossierEnfant/mabasededonnees1.db')
oConnection2 = sqlite3.connect('../mabasededonnees2.db')

# ...

oConnection2.close()

# ...

oConnection1.close()
  </code></pre>
spoil+++

### Création de tables

Pour rappel, voici le **MPD** de la relation personne-livre qui permet de gérer l'opération d'emprunt de livre : 

+++image "/images/md/8C352GE2B4G754B43GAF8CED8BC5G5" col-8 mx-auto bg-white

L'objet `oConnection` est un objet généraliste qui permet, entre autre, d'exécuter arbitrairement des requêtes SQL. Voici un exemple : 

```python
import sqlite3  
oConnection = sqlite3.connect('mabasededonnees.db')

oConnection.execute('''CREATE TABLE personne(
   /* définition des champs */
   nir INTEGER,
   nom VARCHAR(20) NOT NULL,
   prenom VARCHAR(20) NOT NULL,
   tel VARCHAR(10) NOT NULL,
   /* déclaration des clés étrangères et primaires */
   PRIMARY KEY(nir)
)''')

# le commit est facultatif avec sqlite3 dans le cadre de la création d'une table
# mais il est obligatoire pour d'autres opérations telles que l'insertion de données dans la BDD
# donc 'commitez' manuellement tout le temps pour commencer ;)
oConnection.commit()

oConnection.close()
```

Cependant, cette approche n'est pas conseillée car elle ne respecte pas +++lien "https://peps.python.org/pep-0249/" "les standards de spécifications définit par Python" en rapport avec les manipulations de bases de données.

+++bulle matthieu
  s'orienter vers les approches 'standardisées' suit, entre autres, la même logique qui consiste à maitriser le SQL avant d'apprendre à utiliser des approches plus 'haut niveau'
+++

La bibliothèque sqlite3 mets à disposition un objet de type +++lien "https://fr.wikipedia.org/wiki/Curseur_(base_de_donn%C3%A9es)" "'curseur'" qui permet de manipuler la base de données. Et bonne nouvelle, la mise à disposition de cet objet respecte les standards définis par Python. Voici comment on utilise un curseur avec sqlite3 : 

```python
import sqlite3  
oConnection = sqlite3.connect('mabasededonnees.db')
oCurseur = oConnection.cursor()

oCurseur.execute('''CREATE TABLE personne(
   /* définition des champs */
   nir INTEGER,
   nom VARCHAR(20) NOT NULL,
   prenom VARCHAR(20) NOT NULL,
   tel VARCHAR(10) NOT NULL,
   /* déclaration des clés étrangères et primaires */
   PRIMARY KEY(nir)
)''')

oCurseur.close()
oConnection.close()
```

+++bulle matthieu
  selon vous, est-ce qu'il manque des informations à la ligne 7 ?
+++

+++spoil nolabel
  +++bulle matthieu
    la champs 'nir' est <b>le seul champ</b> qui compose la clé primaire, or, une clé primaire est par définition <b>non nulle</b>, <b>unique</b> et <b>indexé</b>
  +++
  +++bulle matthieu droite
    dans ce cas, il n'est pas nécessaire de préciser à la ligne 7 que le champs doit être non nul et unique
  +++
spoil+++

+++question "En vous aidant d'éventuelles recherches sur Internet, créer une table nommée 'livre'. N'oubliez pas que vous pouvez vérifier visuellement le résultat avec le logiciel dbbrowser"

+++spoil
<pre><code class="language-python">import sqlite3  
oConnection = sqlite3.connect('mabasededonnees.db')
oCurseur = oConnection.cursor()

oConnection.execute('''CREATE TABLE personne(
   /* définition des champs */
   nir INTEGER NOT NULL UNIQUE,
   nom VARCHAR(20),
   prenom VARCHAR(20),
   tel VARCHAR(10),
   /* déclaration des clés étrangères et primaires */
   PRIMARY KEY(nir)
)''')

oConnection.execute('''CREATE TABLE livre(
   /* définition des champs */
   isbn13 VARCHAR(13),
   nir INTEGER,
   dateEmprunt DATE,
   /* déclaration des clés étrangères et primaires */
   PRIMARY KEY(isbn13),
   FOREIGN KEY(nir) REFERENCES personne(nir)
)''')

oCurseur.close()
oConnection.close()</code></pre>  
spoil+++

+++bulle matthieu
  vous êtes en avance ? créez la base de données (contenant tables et champs) en reprenant la liaison 'voyageur<==>ville<==pays' vu plus tôt dans ce support
+++

### Insertion de données 

Créer, insérer et extraire des données sont trois opérations qui interviennent généralement dans des phases distinctes dans le cycle de vie d'une application. Ainsi, il semble opportun de créer pour la suite : 

0. un script python qui se charge de créer la base de données (ce qui a été fait auparavant)
1. un script python qui se charge de **peupler** ( +++flag-us populate ) la base de données, c'est à dire d'ajouter des données dans cette dernière. C'est ce que nous allons voir.
2. un script python qui se charge de rassembler les fonctions de requêtages des données, ce que nous verrons plus bas

L'insertion des données en SQL se fait à travers l'instruction `INSERT INTO`, en voici uh exemple d'utilisation : 

```sql
import sqlite3  

oConnection = sqlite3.connect('mabasededonnees.db')
oCurseur = oConnection.cursor()

oCurseur.execute('''INSERT INTO personne(nir, nom, prenom, tel)
VALUES
("815-25-1095","wayne","bruce","7351857301"),
("673-63-0554","gale","dorothy","0123456789")
''')

oCurseur.close()
oConnection.close()
```

+++question "Modifiez le code ci-dessus afin d'insérez dans votre base de données des livres empruntés et des livres non empruntés"

+++spoil
<pre><code class="language-python">import sqlite3  

oConnection = sqlite3.connect('mabasededonnees.db')
oCurseur = oConnection.cursor()

oCurseur.execute('''INSERT INTO personne(nir, nom, prenom, tel)
VALUES
(815251095,"wayne","bruce","7351857301"),
(673630554,"gale","dorothy","0123456789")
''')

oCurseur.execute('''INSERT INTO livre(isbn13, nir, dateEmprunt)
VALUES
("9782080703170",815251095,"2008:05:28"),
("9789638653109",null,null),
("9782370490742",815251095,"2008:05:30")
''')

oConnection.commit()
oCurseur.close()
oConnection.close()</code></pre>
spoil+++

### Extraire des données

L'extraction de données est une opération aussi courante qu'importante. La difficulté se situe surtout au niveau de maîtrise du langage SQL. 

Sans plus attendre, un exemple simple d'extraction de données : 

```python
import sqlite3  

oConnection = sqlite3.connect('mabasededonnees.db')
oCurseur = oConnection.cursor()

requete="""SELECT *
FROM livre
WHERE nir IS NOT NULL
"""

oCurseur.execute(requete)
records = oCurseur.fetchall()

print(records)

oConnection.commit()
oCurseur.close()
oConnection.close()
```

+++bulle matthieu
  exécutez le code ci-dessus
+++

+++bulle matthieu droite
  quel est le type de données retourné par la méthode <code>fetchall</code> à la ligne 12 ?
+++

+++spoil
  <code>fetchall</code> retourne un tableau à deux dimensions: 
  
  <ul>
    <li>La première dimension contient l'ensemble des entrées trouvées (ici, les livres) et renvoyées par la base de données suite à la requête</li>
    <li>La deuxième dimension contient les valeurs des champs pour une entrée donnée</li>
  </ul> 
  
  +++bulle matthieu
    ainsi, si vous souhaitez par exemple afficher la date d'emprunt du premier livre, vous pouvez le faire simplement via <code>print(records[0][2])</code>
  +++
spoil+++

<hr>

Pour vous entraîner, vous allez créer à travers plusieurs étapes une fonction permettant de lister les livres empruntés par un utilisateur donné. C'est une fonction qui peut se retrouver typiquement dans une **API**.

+++question "Dans le logiciel dbbrowser, rédigez et testez la requête SQL qui permet de lister les codes 'isbn13' de tous les livres empruntés par Bruce Wayne"

+++question "Créez un nouveau fichier nommé API.py, ajoutez dedans quelques lignes python permettant d'exécuter à travers python la requête de la question précédente"

<hr>

Le fichier 'API.py' contiendra à terme un ensemble de fonctions permettant d'interroger la base de données. Nous allons rendre la requête plus générique puis l'encapsuler dans une fonction facilement ré-utilisable.

+++question "Au début du fichier 'API.py', créez la variable 'nir' et affectez lui une des valeurs qui se trouve en base de données. Modifiez ensuite le code python gérant la requête SQL afin que cette dernière varie en fonction de la valeur de la variable 'nir'"

+++spoil "Aide"
  Vous aurez probablement besoin de trouver le moyen, en python, d'utiliser une variable dans une chaine de caractères qui fait plusieurs lignes. Une recherche sur Internet vous permettra de trouver rapidement une des approches possibles.
spoil+++

+++question "Pour terminer, refactorez le fichiez 'API.py' avec l'ajout d'une fonction nommée 'getBooks' et qui prend en paramètre un identifiant 'nir' et qui retourne les identifiants 'isbn13' des livres empruntés par le lecteur dont l'identifiant est 'nir'"

+++question "(bonus) refactorez le code en utilisant le paradigme de programmation orientée object"

+++spoil

<br><b>Version procédurale</b>

<pre><code class="language-python">import sqlite3  

oConnection = sqlite3.connect('mabasededonnees.db')
oCurseur = oConnection.cursor()

def getBooks(nir):
    requete=f"""SELECT isbn13
    FROM livre
    JOIN personne ON livre.nir = personne.nir
    WHERE livre.nir = {nir}
    """

    oCurseur.execute(requete)
    return oCurseur.fetchall()    

print(getBooks('815251095'))

oCurseur.close()
oConnection.close()</code></pre>

<b>Version POO 'possible'</b>

<pre><code class="language-python">import sqlite3  

class Database:
    def __init__(self, dbname):
        self.oConnection = sqlite3.connect(dbname)
        self.oCurseur = self.oConnection.cursor()
        
    def __del__(self):
        self.oCurseur.close()
        self.oConnection.close()

class Api:
    @staticmethod
    def getBooks(db, nir):
        requete=f"""SELECT isbn13
        FROM livre
        JOIN personne ON livre.nir = personne.nir
        WHERE livre.nir = {nir}
        """
        
        db.oCurseur.execute(requete)
        return db.oCurseur.fetchall()

# exemple d'utilisation des classes définies
Db = Database('mabasededonnees.db')

print(Api.getBooks(Db, '815251095'))

del Db</code></pre>

spoil+++

<hr>

+++bulle matthieu
  Pour les personnes en avances, plusieurs choix s'offrent à vous
+++

- vous pouvez améliorer le fichier API.py en y ajoutant d'autres fonctions courantes : getNirFromFirstAndLastName(firstName, lastName), getAvailablesBooks(), ...
- vous pouvez vous intéresser à d'autres opérations de BDD : modification de données, suppressions de données, modification de tables,...
- vous pouvez vous entraîner en continuant l'implémentation de la base de données "'voyageur<==>ville<==pays'"