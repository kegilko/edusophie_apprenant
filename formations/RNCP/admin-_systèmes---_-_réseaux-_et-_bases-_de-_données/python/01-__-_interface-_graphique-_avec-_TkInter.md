# Interface graphique avec TkInter

<fieldset class="col-10 mx-auto px-3 py-2 bg-white border border-2 mx-2 mb-2 programmeGenerator"><legend class="text-center mb-0 border border-3 bg-white rounded-3 py-0 mx-auto"><b style="font-size: 2rem">Au programme</b></legend><p class="h3"></p><ul><li>  Présentation des widgets disponibles</li><li>  Présentation des approches d'agencement : pack, grid et place</li><li>  Les principaux conteneurs</li><li>  La gestion des évènements</li><li>  Les applications multifenêtres</li></ul><p class="h3">Vous apprenez à : </p><ul><li>  Développer une interface graphique à l'aide d'une bibliothèque spécialisée</li><li>  Faire de la <b>P</b>rogrammation <b>O</b>rientée <b>O</b>bjet et également de la <b>programmation événementielle</b></li></ul></fieldset>

+++sommaire

## Les librairies graphiques

+++bulle matthieu
  qu'est-ce qu'une librairie en programmation ?
+++

+++bulle matthieu droite
  et donc, une librairie graphique ?
+++

+++spoil
  En programmation, une librairie regroupe un ou plusieurs fichiers dans lesquels se trouvent des fonctions prêtes à l'emploi pour répondre à des besoins spécifiques. Une librairie graphique est alors une librairie spécialisée dans la création d'interfaces graphiques (+++flag-us <b>GUI</b> : <b>G</b>raphical <b>U</b>ser <b>I</b>nterface).
spoil+++

---

Avec Python, une multitude de librairies graphiques existent. En voici quelques unes connues : 

- Tkinter : une librairie graphique légère généraliste pré-installée et adaptée pour des petits projets
- PyQt5 : une librairie graphique généraliste plus complète mais plus difficile à prendre en main
- PyForms : une librairie graphique spécialisée dans la création de formulaires (web, client lourd et même terminal)


+++bulle matthieu
  si je veux utiliser une librairie avec Python, comment je fais ?
+++

+++spoil
  Si la librairie n'est pas installée par défaut, il faut alors l'installer et deux options sont possibles : 
  
  <ul>
    <li>Selon votre IDE (c'est le cas pour Thonny), ce dernier propose le téléchagement et l'installation de librairies externes</li>
    <li>Sinon, vous pouvez utiliser le gestionnaire de package python nommé <code>pip</code></li>
  </ul>
  
  Ensuite, un bon réflexe à avoir est d'aller consulter la documentation de la librairie (site web...) et de suivre les premiers exemples avec votre IDE pour comprendre la logique de la librairie
spoil+++

+++bulle matthieu
  dans la suite de cette séance, nous allons nous familiariser avec la librairie <b>Tkinter</b>
+++

## Tour d'horizon de la librairie tkinter

### Quelques exemples

+++imageFloat /images/md/35GFBE4HB4CGGAHD6DDB788E85AAD5 gauche
  un exemple d'utilisation courante de tkinter
+++https://python-course.eu/tkinter/entry-widgets-in-tkinter.php

+++imageFloat /images/md/C8H2F6AB269G44D75327A45G9G75A2 droite
  mais il est tout a fait possible de créer des jeux avec tkinter tel que le démineur...
+++https://github.com/ripexz/python-tkinter-minesweeper

+++imageFloat /images/md/3DF9DGBB552939E88BB6GA465358DD gauche
  ...ou encore un arkanoid-like
+++https://github.com/rjstyles/Bounce-Game

### Petit historique

En 1988, une équipe universitaire et américaine menée par John Ousterhout conçut le langage de programmation **Tcl** (**T**ool **C**ommand **L**anguage) et en 1990 une bibliothèque est ajoutée, il s'agit de Tk (**T**ool**K**it). Cette bibliothèque a pour but de faciliter la création d'interfaces graphiques.

De part sa simplicité et son efficacité (en terme de performance), le tandem **Tcl**-**Tk** fut très populaire à l'époque. De plus, La conception de **Tk** a été faite de telle sorte qu'il est possible de l'interfacer avec d'autres langages (Perl, Python, Ruby, ... ), on parle alors de <i>language binding</i>.

Dans le cas de python, c'est l'interface Tkinter (Tk-interface) qui fut développée et qui a vu le jour en 1991 sous licence **BSD** (+++flag-us **B**erkeley **S**oftware **D**istribution License).

+++bulle matthieu
  étant donné que tkinter est seulement une interface du langage Tk , la documentation officielle se trouve sur le site <a href="https://tkdocs.com/index.html">Tk</a>
+++

+++bulle matthieu droite
  <a href="https://tkdocs.com/tutorial/index.html">ici se trouve</a> une documentation introductive de la bibliothèque Tk, et <a href="https://tkdocs.com/pyref/">par là</a> une documentation tkinter plus exhaustive.
+++

### Analyse d'un Hello world

Voici un exemple de 'hello world' de tkinter : 

```python
import tkinter as tk
from tkinter import ttk
 
root = tk.Tk()

message = ttk.Label(root, text="Hello, World!")
message.pack()

root.mainloop()
```

<div class="d-flex justify-content-evenly">
  <div class="col-5">
    <p class="text-center">lignes 1 et 2 : <code>tk</code> vs <code>ttk</code></p>
+++bulle matthieu
  <code>ttk</code> est une extension de la bibliothèque <code>tkinter</code> et offre aux widgets de meilleurs look et certaines fonctionnalités supplementaires
+++col-12

+++bulle matthieu droite
  certains widgets n'ont pas été ré-implémentés dans <code>ttk</code>, la documentation officielle précise lorsqu'un widget n'a pas été ré-implémenté dans <code>ttk</code>
+++col-12
  </div>
  <div style="background-color:black;width:1px;"></div>
  <div class="col-5">
    <p class="text-center">ligne 7 : la méthode <code>pack()</code></p>
+++bulle matthieu
  la méthode <code>pack</code> permet d'ajouter très sommairement des widgets les uns en dessous des autres
+++col-12

+++bulle matthieu droite
  pour la découverte des principaux widgets nous utiliserons la méthode <code>pack</code>
+++col-12
  </div>
</div>



### les principaux widgets

Dans tkinter, Les widgets désignent les blocs qui constituent l'interface graphique que vous êtes en train de développer. Nous pouvons les classer dans 4 grands groupes : 

- les boutons 
- les zones de texte
- les conteneurs
- le reste, les éléments interactifs et/ou graphiques : scrollbar, image, canvas, menu, ...

#### les boutons

<div class="col-10 mx-auto mb-3">
  <table class="table table-sm">
    <tbody>
      <tr>
        <td><code><a href="https://tkdocs.com/tutorial/widgets.html#button">Button</a></code></td>
        <td class="w-100"><input type="button" value="un bouton classique"></td>
      </tr>
      <tr>
        <td><code><a href="https://tkdocs.com/tutorial/widgets.html#checkbutton">Checkbutton</a></code></td>
        <td class="w-100"><input type="checkbox" name="check1" id="check1"> une case cochable</td>
      </tr>
      <tr>
        <td><code><a href="https://tkdocs.com/tutorial/widgets.html#radiobutton">Radiobutton</a></code></td>
        <td class="w-100">une série de boutons type 'radio' dont un seul est sélectionnable : <br>
          <input type="radio" name="choix"> oui<br>
          <input type="radio" name="choix"> non
        </td>
      </tr>
    </tbody>
  </table>
</div>

+++bulle matthieu
  créez une interface contenant des boutons, des checkbox et des radio buttons
+++

+++bulle matthieu droite
  explorez/testez les options utilisables pour ces trois widgets
+++

---

+++bulle matthieu
  en vous aidant de la documentation sur Internet, enrichissez votre interface en y ajoutant des widgets suivants : Label, Text, Entry, ListBox et Scale
+++

+++bulle matthieu droite
  explorez/testez les options utilisables pour ces widgets
+++

#### les zones de texte

<div class="col-10 mx-auto mb-3">
  <table class="table table-sm">
    <tbody>
      <tr>
        <td><code style="color:#0d6efd;">Label</code></td>
        <td class="w-100">+++spoil nolabel
          Le widget <a href="https://tkdocs.com/tutorial/widgets.html#label"><code>Label</code></a> est utilisé pour afficher du texte. Il peut également contenir des images.
        spoil+++ </td>
      </tr>
      <tr>
        <td><code style="color:#0d6efd;">Text</code></td>
        <td class="w-100">+++spoil nolabel
          Le widget <a href="https://tkdocs.com/tutorial/text.html"><code>Text</code></a> est utilisé pour afficher une zone de texte riche et éditable
        spoil+++ </td>
      </tr>
    </tbody>
  </table>
</div>

#### les éléments interactifs/graphiques

<div class="col-10 mx-auto mb-3">
  <table class="table table-sm">
    <tbody>
      <tr>
        <td><code style="color:#0d6efd;">Entry</code></td>
        <td class="w-100">+++spoil nolabel
          Le widget <a href="https://tkdocs.com/tutorial/widgets.html#entry"><code>Entry</code></a> permet à un utilisateur de saisir une valeur.<br>
          <input type="text" placeholder="exemple...">
        spoil+++ </td>
      </tr>
      <tr>
        <td><code style="color:#0d6efd;">ListBox</code></td>
        <td class="w-100">+++spoil nolabel
          Le widget <a href="https://tkdocs.com/tutorial/morewidgets.html#listbox"><code>ListBox</code></a> permet à l'utilisateur de sélectionner une valeur parmis un ensemble de valeurs pré-définies.<br>
          <select name="" id="">
            <option value="pomme">pomme</option>
            <option value="poire">poire</option>
            <option value="cerise">cerise</option>
            <option value="mangue">mangue</option>
          </select>
        spoil+++ </td>
      </tr>
      <tr>
        <td><code style="color:#0d6efd;">Scale</code></td>
        <td class="w-100">+++spoil nolabel
          Le widget <a href="https://tkdocs.com/tutorial/morewidgets.html#scale"><code>Scale</code></a> permet de sélectionner une valeur en slidant un point sur une barre.<br>
          <input type="range" name="" id="" value="">
        spoil+++ </td>
      </tr>
    </tbody>
  </table>
</div>

---

+++spoil "Réponse : exemple de code avec les différents widgets" nolabel text-center
  <a href="/d/formations/tkinter_exemples_widgets.py">lien vers le code</a>
spoil+++

### Structurer/agencer les widgets les uns par rapport aux autres

Tkinter propose de créer des interfaces graphiques à travers 3 approches (+++flag-us geometric manager) : 

+++image "/images/md/EA353B3G9CGGB7CAA7DD8FG9E5HD8G" col-10 mx-auto

- **Pack** : les élements dans l'interface graphique sont placés les uns à côté des autres. Il s'agit d'un mode facile à utiliser et qui permet de placer basiquement des éléments.
- **Grid** : les élements dans l'interface graphique sont placés à travers une grille invisible. Il s'agit du mode le plus complet (et donc le plus difficile à prendre en main) qui permet de créer des interfaces qui s'adaptent à la taille de la fenêtre et facilement maintenables.
- **Place** : les élements dans l'interface graphique sont placés via des coordonnées X et Y. Ce mode est plus simple à prendre en main que **Grid** mais le programme sera plus difficile à maintenir et ne s'adaptera pas par défaut avec la taille de la fenêtre.

+++bulle matthieu
  le placement par grille est l'approche la plus souvent utilisée en entreprise
+++



#### les widgets de type 'conteneur'

<div class="col-10 mx-auto mb-3">
  <table class="table table-sm">
    <tbody>
      <tr>
        <td><code><a href="https://tkdocs.com/tutorial/widgets.html#frame">Frame</a></code></td>
        <td class="w-100">Permet de séparer dans des blocs distincts contenant un ensemble de widget. <b>Il est aussi possible d'utiliser un geometric manager différent entre chaque Frame</b></td>
      </tr>
      <tr>
        <td><code><a href="https://tkdocs.com/tutorial/complex.html#labelframe">labelframe</a></code></td>
        <td class="w-100">Fonctionne de la même manière que que le widget <code>Frame</code> mais permet en plus de placer un label et une bordure autour du bloc</td>
      </tr>
    </tbody>
  </table>
</div>

+++bulle matthieu
  l'exercice suivant consiste à <a href="/d/formations/tkinter_exemples_grid_newuser.py">analyser ce code</a> et à commenter les principales lignes pour expliquer leur fonctionnement
+++

+++bulle matthieu droite
  une fois que vous avez saisi comment fonctionne le code dans les grandes lignes, apportez-y les améliorations ci-dessous
+++

0. Modifiez les champs 'mot de passe' afin que le mot de passe saisit par l'utilisateur soit caché ( = affichage en \*\*\*\*\*\*\*)
1. Ajoutez un bouton de validation, ce bouton devra être contenu hors des deux <code>labelFrame</code> '<code>lfBlocInfosPersos</code>' et '<code>lfBlocMotsDePasse</code>'. De plus, ce bouton devra être 'collé' à gauche.
2. Modifiez l'interface afin que les 'blocs' '<code>lfBlocInfosPersos</code>' et '<code>lfBlocMotsDePasse</code>' soient affichés l'un à côté de l'autre (et non l'un en dessous de l'autre comme c'est actuellement le cas)

Voici le résultat attendu : 

+++image "/images/md/HH38GBF7B8G3GD7EB59G59H25G6GE7" mx-auto col-8

+++spoil "Correction" nolabel text-center
  <a href="/d/formations/tkinter_exemples_grid_newuser2.py">lien vers le code</a>
spoil+++

### la gestion des évènements

Les événements représentent les actions qui sont exécutées lorsque l'utilisateur interagit avec l'interface graphique : clic sur un bouton, déplacement de la souris, saisie de valeur dans un champ texte, etc...

La création d'un évènement avec tkinter est relativement simple (+++lien "https://tkdocs.com/tutorial/concepts.html#events" "documentation"). Modifions par exemple le contenu d'un label lorsque l'utilisateur coche une checkbox. Voici notre code de départ : 

<pre>  <code class="python language-python hljs">import tkinter as tk
from tkinter import ttk

root = tk.Tk()

valueCheckbox = tk.IntVar() # nous précisons que la checkbox doit stoquer la valeur de son état dans un entier (qui sera automatiquement soit 0 soit 1)
check = ttk.Checkbutton(root, variable=valueCheckbox)
label = ttk.Label(root, text='Checkbox non cochée !',width=20)

check.pack()
label.pack()

root.mainloop()
  </code>
</pre>

#### Et voici les étapes à suivre : 

0. Configurons le widget '<code>check</code>' afin que ce dernier exécute une fonction nommée '<code>modifieLabel</code>' lorsque l'utilisateur clic sur la checkbox

<pre><code class="python language-python hljs">import tkinter as tk
from tkinter import ttk

root = tk.Tk()

valueCheckbox = tk.IntVar() # nous précisons que la checkbox doit stoquer la valeur de son état dans un entier (qui sera automatiquement soit 0 soit 1)
check = ttk.Checkbutton(root, variable=valueCheckbox)
label = ttk.Label(root, text='Checkbox non cochée !',width=20)

check.pack()
label.pack()

check.bind("<​Button-1>", modifieLabel) #<​Button-1> représente le clic gauche de la souris

root.mainloop()</code></pre>

1. Créons la fonction 'modifieLabel' et récupérons dedans les variables qui contiennent le label et la valeur stoquée par la checkbox

<pre><code class="python language-python hljs">import tkinter as tk
from tkinter import ttk

root = tk.Tk()

valueCheckbox = tk.IntVar() # nous précisons que la checkbox doit stoquer la valeur de son état dans un entier (qui sera automatiquement soit 0 soit 1)
check = ttk.Checkbutton(root, variable=valueCheckbox)
label = ttk.Label(root, text='Checkbox non cochée !',width=20)

check.pack()
label.pack()

def modifieLabel(event):
    global valueCheckbox
    global label

check.bind("<​Button-1>", modifieLabel) #<​Button-1> représente le clic gauche de la souris

root.mainloop()
  </code>
</pre>

1. Et il ne reste plus qu'à programmer la partie logique de notre fonction <code>modifieLabel</code>

<pre>  <code class="python language-python hljs">import tkinter as tk
from tkinter import ttk

root = tk.Tk()

valueCheckbox = tk.IntVar() # nous précisons que la checkbox doit stoquer la valeur de son état dans un entier (qui sera automatiquement soit 0 soit 1)
check = ttk.Checkbutton(root, variable=valueCheckbox)
label = ttk.Label(root, text='Checkbox non cochée !',width=20)

check.pack()
label.pack()

def modifieLabel(event):
    global valueCheckbox
    global label
    
    if(valueCheckbox.get() == 0):
        label.config(text = 'Checkbox cochée !')
    else:
        label.config(text = 'Checkbox non cochée !')

check.bind("<​Button-1>", modifieLabel) #<​Button-1> représente le clic gauche de la souris

root.mainloop()
  </code>
</pre>

+++bulle matthieu
  et le tour est joué ! <b>attention</b> par contre, l'utilisation de variables globales est une pratique <b><u>très déconseillée</u></b>
+++

+++bulle matthieu droite
  la <b>P</b>rogrammation <b>O</b>rientée <b>O</b>bjet est une bonne réponse pour éviter cette mauvaise pratique
+++

<pre>  <code class="python language-python hljs"># Le même code qu'au dessus, mais codé en POO
import tkinter as tk
from tkinter import ttk

class FenetrePrincipale(tk.Tk):
    
    def __init__(self):
        super().__init__()
        
        self.valueCheckbox = tk.IntVar()
        self.check = ttk.Checkbutton(self, variable=self.valueCheckbox)
        self.label = ttk.Label(self, text='Checkbox non cochée !',width=20)
        
        self.__place()
        self.__bind()

    def __place(self):
        self.check.pack()
        self.label.pack()

    def __bind(self):
        self.check.bind("<​Button-1>", self.__modifieLabel)

    def __modifieLabel(self, event=None):
        if(self.valueCheckbox.get() == 0):
            self.label.config(text = 'Checkbox cochée !')
        else:
            self.label.config(text = 'Checkbox non cochée !')    


app = FenetrePrincipale()
app.mainloop()
  </code>
</pre>

---

+++bulle matthieu
  reprenez le programme consacré à la saisie des informations d'un utilisateur et apportez y les modifications suivantes
+++

0. Le programme doit être re-codé en suivant le paradigme de **P**rogrammation **O**rienté **O**bjet. Une seule classe suffira. 
1. Ajoutez les événements suivants dans le programme : 
    - le champs texte '<code>ePseudo</code>' ne doit pas contenir plus de 10 caractères. Si il en contient plus de 10, une popup s'affiche à l'écran pour signaler à l'utilisateur que le champ ne doit pas contenir plus de 10 caractères
    +++spoil "indice : comment afficher un message dans une popup" text-green
      une petite recherche sur Internet 'tkinter alert message popup' devrait vous mener vers la bonne piste
    spoil+++
    
    - lorsque l'utilisateur valide le formulaire, une popup indiquant une erreur doit s'afficher si : 
        - un des champs '<code>ePseudo</code>', '<code>eEmail</code>', '<code>eMotDePasse1</code>' ou '<code>eMotDePasse2</code>' est vide
        - les champs '<code>eMotDePasse1</code>' et '<code>eMotDePasse2</code>' ne contiennent pas le même mot de passe
    - sinon, si toutes les données du formulaire sont correctes, alors une popup doit s'afficher et signaler que le formulaire a bien été saisi

### les applications multifenêtres

Jusqu'à maintenant, nous n'avons créé que sur des programmes proposant une seule interface graphique. Il est bien sur possible de créer plusieurs 'fenêtres' au sein d'une même application. La documentation relative à cela +++lien "https://tkdocs.com/tutorial/windows.html" "se trouve ici".

+++bulle matthieu
  en vous aidant de la documentation officielle (pouvant être complétée par des recherches sur Internet), créez un programme simple gérant deux interfaces graphiques et avec la possibilité de passer de l'une vers l'autre
+++

## À vos marques, prêt... pratiquez !

Pour vous entraîner avec tout ce qui a été vu auparavant, vous avez le choix entre deux sujets : 

0. Créer deux calculatrices : une calculatrice pour les élèves de primaire et une calculatrice pour les élèves de collège
1. Améliorez le jeu de rôle programmé en POO en y ajoutant une interface graphique, au choix : la gestion d'un combat, la gestion d'un inventaire, ...

### Sujet 0 : les calculatrices

- un écran principal devra proposer le choix entre la calculatrice de primaire ou la calculatrice de collège
- les deux calculatrices ne peuvent effectuer qu'une seule opération à la fois
- les deux calculatrices doivent présenter un ensemble de boutons rangés dans une grille
- la différence entre la calculatrice de primaire et de collège est que celle du collège propose trois boutons supplémentaires : cos, sin et tan
- une fois arrivé sur une calculatrice, un menu doit permettre de switcher vers l'autre calculatrice

+++bulle matthieu
  pour les personnes en avance, améliorez les calculatrices afin de pouvoir effectuer deux opérations à la fois
+++

+++bulle matthieu droite
  les calculatrices doivent pouvoir par exemple effectuer des calculs comme '<code>7 + 2 - 8</code>' ou encore '<code>10 / 3 * 4</code>'
+++

### Sujet 1 : POO-RPG enhanced edition

Si vous souhaitez choisir ce projet mais que votre code actuel n'est pas très aboutit, vous pouvez utiliser +++lien "/d/formations/python_poo_rpg.py" "la base de code suivante". Si vous avez des questions sur le fonctionnement du code, n'hésitez pas poser des questions !

- un écran principal doit proposer de créer une nouvelle partie ou de charger la dernière sauvegarde
    - ne vous pré-occupez pas pour l'instant de la sauvegarde ou du chargement d'une sauvegarde si elle n'est pas encore implémentée (cliquer sur le bouton de chargement n'aura par exemple aucun effet)
- lors d'une nouvelle partie, un joueur doit renseigner les informations de son personnage à travers une interface graphique (saisir le nom, sélectionner la classe de personnage, ...)
- le reste de la partie peut se faire depuis le terminal python (donc sans interfaces graphiques)

+++bulle matthieu
  pour les personnes en avance, ajoutez dans votre programme les classes <code>Item</code> et <code>Sac</code>, un sac pourra contenir un ensemble d'items récoltés pendant l'avanture
+++

+++bulle matthieu droite
  puis créez une interface graphique permettant au joueur de consulter visuellement et à tout moment le contenu de son sac
+++