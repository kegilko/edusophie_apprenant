# Premiers pas sur Debian

![https://upload.wikimedia.org/wikipedia/commons/0/04/Debian_logo.png](https://upload.wikimedia.org/wikipedia/commons/0/04/Debian_logo.png)

Nous pouvons naviguer entre les fichiers et les dossiers avec la souris. Vous allez apprendre à le faire en utilisant des commandes à la place de la souris. Pour exécuter des commandes il nous faut un terminal. Vous pouvez ouvrir un terminal de deux manières différentes : 

1. Soit en activant l'overview et en saisissant au clavier 'terminal' puis en validant avec la touche entrée
2. Soit avec le raccourcis 'ctrl + alt + t' 

La fenêtre ci-dessous s'ouvre : 

![terminal](https://i.ibb.co/DLDVj3g/Screenshot-from-2021-09-12-15-01-08.png)
<div class="text-center">Le contenu varie par rapport à vos ordinateurs en classe</div>
<br>

Depuis le terminal, saisissez et validez la commande '`pwd`' (sans les ' '). Cette commande affiche le chemin du dossier dans lequel se trouve actuellement votre terminal. La commande '`cd`' (**c**hange **d**irectory) vous permet de changer le dossier où se trouve votre terminal, voici deux exemples d'utilisation de cette commande : (testez les !)

- '`cd /`' : votre terminal se retrouve maintenant dans la racine de Debian 
- '`cd /home/???`' : (??? correspond au nom de votre utilisateur) votre terminal se retrouve maintenant dans la home de votre utilisateur (son espace personnel)

+++question write "Depuis le terminal, quelle commande doit on exécuter pour aller dans le dossier '/tmp' ? exécutez cette commande"

Le dossier '`/tmp`' est automatiquement vidé lorsque l'ordinateur est éteint. Ce dossier est pratique pour faire des manipulations temporaires que l'on ne souhaite pas sauvegarder. Vous allez mieux comprendre cette utilité dans l'activité suivante

+++question write "Sans prendre en compte le terminal, listez quelques fonctionnalités de Debian qui sont semblables à celles de l'OS que vous utilisez habituellement"

## Résultats

<ol start="0">
    <li>
        +++spoil
            Depuis un terminal, il faut saisir et valider la commande '`cd /tmp`' pour aller dans le dossier '/tmp'
        spoil+++
    </li>
    <li>
        +++spoil
            <ul>
              <li>Le fait de pouvoir naviguer entre les dossiers</li>
              <li>Le fait de pouvoir utiliser des périphériques externes comme le clavier ou la souris</li>
              <li>Le fait de pouvoir utiliser des logiciels courants : navigateur Internet, éditeur de texte (libre office par exemple), écouter de la musique, ...</li>
            </ul>
        spoil+++
    </li>

</ol>
