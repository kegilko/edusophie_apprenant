# Gestion des permissions sur Linux

Le but de cette activité est de se familiariser sur la gestion des permissions dans un OS type Linux. 

Commencez par ouvrir un terminal et déplacez vous dans le dossier '/home'. Une fois dans ce dossier, créez un fichier nommé 'test' avec la commande '`touch`'. Une erreur va apparaître sur votre terminal : 

<style>
  .term{
    background: black;
    color: #6ab72a;
    border-radius: .25rem !important;
    padding: .1rem .5rem !important;
  }
  
  .sampleOutput{
    display:inline-block;
  	position: relative;
  	left: 50%;
  	transform: translateX(-50%);
  	margin-bottom: 1rem;
  }
  .mtable td{
    border: none !important;
    padding-left: .2rem !important;
    padding-right: .2rem !important;
    margin-left: .2rem !important;
    margin-right: .2rem !important;
  }
  
  .mtable{
    border-collapse: separate;
    border-spacing: 0;
  }
  
  .mtable .mborder{border: 2px solid !important;}
  .mtable .nobb{border-bottom: none !important;}
  .mtable .noub{border-bottom: none !important;border-top: none !important;}
  .mtable .nou{border-top: none !important;}
  
  .mtable .mb-r{border-color: red !important;}
  .mtable .mb-g{border-color: green !important;}
  .mtable .mb-b{border-color: blue !important;}
  .mtable .mb-w{border-color: orange !important;}
  
  .tbor-orange{border-color:orange !important;background-color:black;color:#6ab72a}
  .tbor-red{border-color:red !important;background-color:black;color:#6ab72a}
  .tbor-blue{border-color:blue !important;background-color:black;color:#6ab72a}
  .tbor-green{border-color:green !important;background-color:black;color:#6ab72a}
</style>

<div class="term sampleOutput">
touch: impossible de faire un touch 'test': Permission non accordée
</div>

Pour visualiser les permissions du dossier dans lequel se trouve votre terminal, utilisez la commande '`ls -la`' : 
  - '`ls`' : liste les fichiers et dossiers du dossier ciblé (par défaut, le dossier dans lequel le terminal est)
  - '`-l`' : (option) affiche une liste avec plus d'information
  - '`-a`' : (option) affiche tous les fichiers et dossiers cachés, **ainsi que le dossier courant et le dossier parent**

Le résultat de la commande doit donner : 

<div class="term sampleOutput bg-" style="background-color:black">
  <table class="term mtable">
    <tbody>
      <tr><td>drwxr-xr-x</td><td>5</td><td>root</td><td>root</td><td>4096</td><td>3</td><td>sept.</td><td>13:34</td><td class="isDirectory">.</td></tr>
      <tr><td>drwxr-xr-x</td><td>19</td><td>root</td><td>root</td><td>4096</td><td>3</td><td>sept.</td><td>11:54</td><td class="isDirectory">..</td></tr>
      <tr><td>drwxr-xr-x</td><td>16</td><td>nomDeVotreUtilisateur</td><td>nomDeVotreUtilisateur</td><td>4096</td><td>6</td><td>sept.</td><td>00:03</td><td class="isDirectory">nomDeVotreUtilisateur</td></tr>
    </tbody>
  </table>
</div>

Décortiquons ensemble les informations qui nous intéressent : 

<div class="term sampleOutput bg-" style="background-color:black">
  <table class="term mtable">
    <tbody>
      <tr><td class="mborder mb-r nobb">drwxr-xr-x</td><td>5</td><td class="mborder mb-g nobb">root</td><td class="mborder mb-b nobb">root</td><td>4096</td><td>3</td><td>sept.</td><td>13:34</td><td class="isDirectory mborder mb-w nobb">.</td></tr>
      <tr><td class="mborder mb-r noub">drwxr-xr-x</td><td>19</td><td class="mborder mb-g noub">root</td><td class="mborder mb-b noub">root</td><td>4096</td><td>3</td><td>sept.</td><td>11:54</td><td class="isDirectory mborder mb-w noub">..</td></tr>
      <tr><td class="mborder mb-r nou">drwxr-xr-x</td><td>16</td><td class="mborder mb-g nou">nomDeVotreUtilisateur</td><td class="mborder mb-b nou">nomDeVotreUtilisateur</td><td>4096</td><td>6</td><td>sept.</td><td>00:03</td><td class="isDirectory mborder mb-w nou">nomDeVotreUtilisateur</td></tr>
    </tbody>
  </table>
</div>


<div class="col-8 d-flex mx-auto flex-wrap">
  <div class="p-2 col-6 text-center"><div class="zoomOnHover card item-card card-block border border-dark">
    <div class="border border-dark border-5" style="background-color:orange;"><b>0</b></div>
    <div class="px-1 mb-2 mt-auto text-center">
      '<b class="isDirectory display-6" style="line-height:0">.</b>' qui signifie 'le dossier dans lequel le terminal est' (ici : '/home')<br><b>ou</b><br>
      '<b class="isDirectory display-6" style="line-height:0">..</b>' qui signifie 'le dossier parent dans lequel le terminal est'  (ici : '/', la racine de notre OS)<br><b>ou</b><br>
      Nom du <b class="isDirectory">dossier</b> ou <b class="isFile">fichier</b>
    </div> 
  </div></div>
  <div class="p-2 col-6 text-center"><div class="zoomOnHover card item-card card-block border border-dark">
    <div class="border border-dark border-5" style="background-color:green;"><b>1</b></div>
    <div class="px-1 mb-2 mt-auto text-center">
      Nom de l'utilisateur propriétaire du <b class="isDirectory">dossier</b> ou du <b class="isFile">fichier</b>
    </div> 
  </div></div>
  <div class="p-2 col-6 text-center"><div class="zoomOnHover card item-card card-block border border-dark">
    <div class="border border-dark border-5" style="background-color:blue;"><b>2</b></div>
    <div class="px-1 mb-2 mt-auto text-center">
      Nom du groupe auquel appartient le <b class="isDirectory">dossier</b> ou <b class="isFile">fichier</b>
    </div> 
  </div></div>
  <div class="p-2 col-6 text-center"><div class="zoomOnHover card item-card card-block border border-dark">
    <div class="border border-dark border-5" style="background-color:red;"><b>3</b></div>
    <div class="px-1 mb-2 mt-auto text-center">
      La première lettre correspond au type d'élément ( <span class="term">d</span> comme directory). Les autres lettres représentent les droits définis pour le <b class="isDirectory">dossier</b> ou le <b class="isFile">fichier</b>. Plus de détail par la suite !
    </div> 
  </div></div>
</div>

Par exemple : 
<ul>
  <li><span class="px-1 border border-2 isDirectory tbor-orange">nomDeVotreUtilisateur</span> : le propriétaire de ce <b class="isDirectory">dossier</b> est l'utilisateur avec lequel vous êtes connecté, le <b class="isDirectory">dossier</b> appartient au groupe ayant le même nom que votre utilisateur et la configuration des droits du <b class="isDirectory">dossier</b> est <span class="px-1 border border-2 tbor-red">rwxr-xr-x</span></li>
  <li><b class="isDirectory">Dossier</b> <span class="px-1 border border-2 isDirectory tbor-orange">.</span> : le propriétaire de ce <b class="isDirectory">dossier</b> (qui ici est '/home' car notre terminal est dans '/home') est l'utilisateur <span class="px-1 border border-2 tbor-green">root</span>, le <b class="isDirectory">dossier</b> appartient au groupe <span class="px-1 border border-2 tbor-blue">root</span> et la configuration des droits du <b class="isDirectory">dossier</b> est <span class="px-1 border border-2 tbor-red">rwxr-xr-x</span></li>
</ul>

+++bulle matthieu
  lors de l'installation d'un OS type Linux, par défaut, l'utilisateur 'root' est créé. Cet utilisateur a tous les droits sur l'ordinateur. Un compte à ne pas laisser entre de mauvaises mains !
+++

<br>
Intéressons nous aux informations <span class="px-1 border border-2 tbor-red">encadrées en rouge</span>.

Ces informations définissent les droits relatifs au <b class="isDirectory">dossiers</b> ou <b class="isFile">fichiers</b> concerné. Ces informations sont de la forme <span class="term">---------</span> où <span class="term">-</span> est une lettre pouvant prendre la valeur <span class="term">r</span>, <span class="term">w</span> ou <span class="term">x</span>. (d'autres valeurs plus rares sont possibles mais nous ne nous y intéresserons pas)

+++question "En vous aidant <a href="https://chmod-calculator.com/" target="_blank">de ce site</a>, déterminez sommairement à quoi correspondent en anglais les lettres <span class="term">r</span>, <span class="term">w</span> et <span class="term">x</span>"

+++question "En vous aidant toujours du même site, déterminez à quoi correspondent les trois premières lettre dans <span class="term"><b>---</b>------</span>, les trois lettres du milieu dans <span class="term">---<b>---</b>---</span> puis les trois dernières lettres dans  <span class="term">------<b>---</b></span>"

<br>

+++bulle matthieu
  Entre les dossiers et les fichiers, les règles des droits accordés varient légèrement. Ci-dessous un tableau récapitulatif des différences
+++

<div class="col-8 mx-auto">
    <table class="table">
      <thead>
        <tr class="text-center">
          <th></th>
          <th>Contexte 'dossier'</th>
          <th>Contexte 'fichier'</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th><span class="term">r</span></th>
          <td>- Droit de lister les dossiers et fichiers dans le dossier concerné</td>
          <td>- Droit de lire le contenu du fichier concerné</td>
        </tr>
        <tr>
          <th><span class="term">w</span></th>
          <td>
            - Droit de renommer/déplacer/supprimer le dossier concerné<br>
            - Droit de créer/supprimer les fichiers et dossiers dans le dossier concerné
          </td>
          <td>
            - Droit de renommer/déplacer/supprimer le fichier concerné<br>
            - Droit d'écrire dans le fichier concerné (ajout ou suppression de contenu)
          </td>
        </tr>
        <tr>
          <th><span class="term">x</span></th>
          <td>- Droit d'entrer dans le dossier concerné<br></td>
          <td>- Droit d'exécuter le fichier concerné<br></td>
        </tr>
      </tbody>
    </table>
</div>

Voici quelques exemples de configuration de droit de fichier possibles : 

<div class="col-8 mx-auto border border-dark mb-2">
  <div class="term d-block" style="border-radius:0 !important">
    -rwxr----- 5 paul visiteur 804 1 sept. 13:34 <span class="isFile">toto.txt</span>
  </div>
  <div class="p-2 text-center">
    - Le propriétaire 'pierre' a tous les droits sur le fichier toto.txt ( <span class="term">-<b>rwx</b>r-----</span> )<br>
    - Les utilisateurs appartenant au groupe 'visiteur' peuvent seulement  lire le fichier toto.txt ( <span class="term">-rwx<b>r--</b>---</span> )<br>
    - Les autres utilisateurs n'ont aucun droit sur le fichier ( <span class="term">-rwxr--<b>---</b></span> )
  </div>
</div>

<div class="col-8 mx-auto border border-dark mb-2">
  <div class="term d-block" style="border-radius:0 !important">
    -rwxr-xr-- 5 root paul 424 1 sept. 13:34 <span class="isFile">scriptPython.py</span>
  </div>
  <div class="p-2 text-center">
    - Le propriétaire 'root' a tous les droits sur le fichier 'scriptPython.py' ( <span class="term">-<b>rwx</b>r-xr--</span> )<br>
    - Les utilisateurs appartenant au groupe 'paul' peuvent lire ou exécuter le fichier 'scriptPython.py' ( <span class="term">-rwx<b>r-x</b>r--</span> )<br>
    - Les autres utilisateurs ont seulement le droit de lire le contenu du fichier 'scriptPython.py' ( <span class="term">-rwxr-x<b>r--</b></span> )
  </div>
</div>

<div class="col-8 mx-auto border border-dark mb-2">
  <div class="term d-block" style="border-radius:0 !important">
    drwxr-xr-x 5 root jack 4096 3 sept. 13:34 <span class="isDirectory">Images</span>
  </div>
  <div class="p-2 text-center">
    - Le propriétaire 'root' a tous les droits sur le dossier 'Images' ( <span class="term">d<b>rwx</b>r-xr-x</span> )<br>
    - Tous les autres utilisateurs peuvent entrer et lister le contenu du dossier 'Images'  ( <span class="term">drwx<b>r-xr-x</b></span> )
  </div>
</div>
<br>

+++question "Entraînez vous ! explicitez les droits du dossier <b class="isDirectory">tata</b> "

<div class="term mx-auto d-table mb-4" style="border-radius:0 !important">
    dr-xr-x--- 5 root toto 4096 3 sept. 13:34 <span class="isDirectory">tata</span>
  </div>

+++question "Quelle serait la combinaison <b class="term"><à compléter></b> du dossier <b class="isDirectory">Images</b> si tous les utilisateurs avaient tous les droits sur ce dossier ?"

<div class="term mx-auto d-table" style="border-radius:0 !important">
    <b><à compléter></b> 5 root toto 4096 3 sept. 13:34 <span class="isDirectory">Images</span>
  </div>

---

+++bulle matthieu
<b>Attention ! </b> ouvrir tous les droits sur un dossier ou un fichier peut être très <b>très</b> <u><b class="text-red" style="font-size:1.4rem">très</b></u> dangereux !
+++

+++bulle matthieu droite
car votre dossier ou fichier devient alors manipulable par n'importe quel utilisateur. Y compris des utilisateurs profitant d'une faille pour se connecter à distance sur votre ordinateur
+++

---

<br>
Revenons en au problème initial, votre terminal se trouve toujours normalement dans le <b class="isDirectory">dossier</b> '/home'. La commande '`ls -la`' nous donne les informations suivantes pour le dossier '/home'

<div class="term sampleOutput">
drwxr-xr-x  5 root       root       4096  3 sept. 13:34 <span class="isDirectory">.</span>
</div>

Pour rappel, lorsque vous essayez de créer un fichier dans le dossier '/home', le terminal vous affiche cette erreur : 

<div class="term sampleOutput">
touch: impossible de faire un touch 'test': Permission non accordée
</div>

+++question write "Expliquez pourquoi vous recevez ce message d'erreur lorsque vous essayez de créer un fichier dans le dossier '/home'"

+++question write "Affichez les informations liées au dossier personnel de votre compte (situé dans '/home/leNomDeVotreUtilisateur'). Répondez aux questions suivantes :"
0. Quel est le propriétaire du dossier '/home/leNomDeVotreUtilisateur' ? 
1. À quel groupe appartient le dossier '/home/leNomDeVotreUtilisateur' ? 
2. Quels droits sont configurés pour le dossier '/home/leNomDeVotreUtilisateur' ? 
3. Expliquer pourquoi le compte sur lequel vous êtes connecté vous permet de modifier les fichiers et dossiers contenus dans le dossier '/home/leNomDeVotreUtilisateur'

<br>
Dans le 'dossier du jour' que vous avez créé au début du cours, vous avez également créé le fichier 'gestionDesDroits.txt'. **Ouvrez l'explorateur de fichier** (en utilisant l'overview de Debian) et **déplacez vous manuellement** dans le dossier qui contient le fichier 'gestionDesDroitsUtilisateursGroupes.txt'. Dans l'explorer, **faites un clic droit** 'dans le vide' puis **cliquez sur** 'Ouvrir dans un terminal'

![](https://i.ibb.co/HNdD8yQ/Capture-d-cran-de-2021-09-18-22-37-47.png)

Un terminal s'ouvre et est directement situé dans votre 'dossier du jour'. 

+++question "À l'aide de votre terminal, affichez les informations liées au fichier 'gestionDesDroitsUtilisateursGroupes.txt'. Répondez aux questions suivantes :"
0. Quel est le propriétaire du fichier 'gestionDesDroitsUtilisateursGroupes.txt' ? 
1. À quel groupe appartient le fichier 'gestionDesDroitsUtilisateursGroupes.txt' ? 
2. Quels droits sont configurés pour le fichier 'gestionDesDroitsUtilisateursGroupes.txt' ? 
3. Pour quelle raison un autre utilisateur que le votre ne peut pas modifier le fichier 'gestionDesDroitsUtilisateursGroupes.txt' ?

Afin de vérifier qu;un autre utilisateur ne peut effectivement pas modifier le fichier 'gestionDesDroitsUtilisateursGroupes.txt', vous allez créér et changer d'utilisateur.

+++question "En vous aidant de recherches sur Internet, trouvez les commandes à effectuer dans un terminal pour créer un nouvel utilisateur et pour vous connecter dessus"

---

Depuis le terminal, essayez de supprimer, de déplacez ou de renommer le fichier 'gestionDesDroitsUtilisateursGroupes.txt' depuis votre nouvel utilisateur. Vous verrez sans surprise (d'après les réponses à la question 5) une erreur de permission apparaître.

Revenez sur votre compte utilisateur initial en exécutant par la commande '`exit`' ou '`su leNomDeVotreUtilisateurInitial`'. Vous allez modifier les droits du fichier 'gestionDesDroitsUtilisateursGroupes.txt' afin de permettre à votre nouvel utilisateur de modifier le fichier. Pour se faire, vous allez utiliser la très connue commande '`chmod`'.

Pour pouvoir utiliser la commande '`chmod`', vous devez avoir les droits d'écriture sur le dossier ou fichier concerné. 

+++bulle matthieu
  <b>Attention ! </b> cette commande peut être dangereuse. il faut donc bien réfléchir avant de l'utiliser
+++

La commande '`chmod`' s'utilise comme suit : 
- '`chmod XXX leDossierOuLeFichier`', où X est un nombre compris entre 0 et 7 (on dit que X est en octal, car en base 8)

'`XXX`' correspond à la valeur en octal des droits qu'on souhaite attribuer au dossier ou fichier. Les chiffres peuvent se calculer assez facilement, mais je ne vais pas vous demander de le faire en cette fin d'activité ;) 

À la place, vous allez de nouveau vous aider <a href="https://chmod-calculator.com/" target="_blank">du site donné en lien plus haut</a>. Vous remarquerez qu'en jouant avec les configurations 'Read'/'Write'/'Execute', un nombre composé de 3 digits est automatiquement généré. C'est le nombre qui vous intéresse ! 

Ainsi, par exemple, si vous souhaitez accorder tous les droits à un fichier à son propriétaire et n'accorder aucun droit à tous les autres utilisateurs, votre fichier doit suivre la règle : <span class="term">-rwx------</span>

En configurant cette règle dans le site 'chmod-calculator', vous obtenez le nombre 700 

![](https://i.ibb.co/S6fFPhH/Screenshot-from-2021-09-18-21-58-35.png)

+++bulle matthieu
il existe de nombreux sites Internet qui mettent à disposition un '<code>chmod</code>' interactif
+++

À l'aide du site et de la commande '<code>chmod</code>', modifiez les droits du fichier 'gestionDesDroitsUtilisateursGroupes.txt' afin de permettre à vos deux utilisateurs de renommer le fichier. Une fois la commande '<code>chmod</code>' exécutée, vérifiez que vous pouvez renommer le fichier 'gestionDesDroitsUtilisateursGroupes.txt' avec vos deux utilisateur.

+++question "Quels arguments, passés à la commande '<code>chmod</code>', avez vous du utiliser ?"

+++spoil "Aide"
  Pour rappel, la commande pour renommer un fichier ou un dossier est '<code>mv</code>' (comme <b>m</b>o<b>v</b>e)
spoil+++

<style>
.clignote {
  animation: clignote 0.4s linear infinite;
}
@keyframes clignote {  
  20% { opacity: 50%; }
}
</style>

+++bulle matthieu
<b>Attention ! </b> soyez avares sur les droits que vous accordez pour un dossier ou un fichier. N'accordez que ce qui est nécessaire, pas plus !
+++

## Réponses

<ol start="0">
  <li>
    +++spoil
        La lettre 'r' correspond à '<b>r</b>ead', le lettre 'w' correspond à '<b>w</b>rite' et la lettre 'x' correspond à 'e<b>x</b>ecute'
    spoil+++
  </li>
  <li>
    +++spoil
        Les trois premières lettres correspondent aux droits configurés pour le propriétaire du dossier ou du fichier. Les trois lettres du milieu correspondent aux droits configurés pour le groupe auquel appartient du dossier ou du fichier. Les trois dernières lettre correspondent aux droits de tous les autres utilisateurs qui ne sont ni propriétaires ni appartenant au même groupe que le dossier ou le fichier.
    spoil+++
  </li>
  <li>
    +++spoil
        <div class="col-6 mx-auto border border-dark mb-2">
          <div class="term d-block" style="border-radius:0 !important">
            dr-xr-x--- 5 root toto 4096 3 sept. 13:34 <span class="isDirectory">tata</span>
          </div>
          <div class="p-2 text-center">
            - L'utilisateur propriétaire 'root' et les utilisateurs appartenant au groupe 'toto' peuvent seulement lire et exécuter le fichier tata<br>
            - Tous les autres utilisateurs n'ont aucun droit sur le fichier 'tata'
          </div>
        </div>
    spoil+++    
  </li>
  <li>
    +++spoil
        <div class="col-6 mx-auto border border-dark mb-2">
          <div class="term d-block" style="border-radius:0 !important">
            drwxrwxrwx 5 root toto 4096 3 sept. 13:34 <span class="isDirectory">Images</span>
          </div>
          <div class="p-1 text-center">
            - Tous les utilisateurs ont tous les droits sur le dossier Images<br>
          </div>
        </div>
    spoil+++  
  </li>
  <li>
    +++spoil
        D'après les droits du dossier '/home', les utilisateurs autre que le compte 'root' et qui n'appartiennent pas au groupe 'root' ont seulement le droit d'entrer et de lister les dossiers et fichiers du dossier '/root'. Comme ces utilisateurs n'ont pas le droit de créer des fichiers dans le dossier '/home', c'est la raison pour laquelle mon utilisateur reçoit une erreur de permission du terminal lorsque j'essaie de créer un fichier dans le dossier '/home'
    spoil+++
  </li>
  <li>
    +++spoil
        <ol>
          <li>Le propriétaire du dossier est l'utilisateur avec lequel je suis connecté</li>
          <li>Le dossier appartient au groupe portant le nom de l'utilisateur avec lequel je suis connecté</li>
          <li></li>
          <li></li>
        </ol>
    spoil+++
  </li>
  <li>
    +++spoil
        <ol>
          <li>Le propriétaire du fichier est l'utilisateur avec lequel je suis connecté</li>
          <li>Le fichier appartient au groupe portant le nom de l'utilisateur avec lequel je suis connecté</li>
          <li></li>
          <li></li>
        </ol>
    spoil+++
  </li>
  <li>
    +++spoil
        Plusieurs commandes sont possibles. La plus propre (c'est à dire celle qui octroie juste les droits qu'il faut) est '<code>chmod 666 gestionDesDroitsUtilisateursGroupes.txt</code>'. La commande '<code>chmod 646 gestionDesDroitsUtilisateursGroupes.txt</code>' est aussi valable, cependant, il n'est pas très cohérent que 'tout le reste des utilisateurs' (les 3 dernières lettres) aient plus de droit que 'les utilisateurs appartenant au même groupe que le fichier' (les trois lettre du milieu). 
        
        +++bulle matthieu
        Faites bien attention à ne pas donner plus de droit que nécessaire. Des droits trop permissifs sont les premières portes d'entrées pour les utilisateurs malveillants
        +++
        
        +++bulle matthieu droite
        Notez aussi que le dossier qui contient le fichier doit avoir les droits adéquats pour pouvoir modifier les fichiers qu'il contient
        +++
    spoil+++
  </li>
</ol>
