# Se familiariser avec le terminal Linux

Cette activité consiste à utiliser des commandes usuelles qu'offre un système d'exploitation (créer un fichier, supprimer un dossier, etc...) qui est ici Debian. Dans un premier temps, vous allez préparer votre environnement pour travailler dans de bonnes conditions :

## Préparer son environnement de travail

Pour préparer votre environnement de travail, suivez les étapes suivantes : 

0. Ouvrez un terminal
1. Depuis le terminal, déplacez vous dans le dossier '/tmp'
2. Ouvrez l'explorer
3. Depuis l'explorer, déplacez vous dans le dossier '/tmp'
4. En utilisant le raccourcis 'windows + flèche droite' et 'windows + flèche gauche' faites en sorte que votre écran contienne le terminal à gauche et la fenêtre explorer à droite. Vous devriez avoir un écran ressemblant á ceci 

<div class="text-center col-6 mx-auto">
  <img class="rounded" src="https://i.ibb.co/DrJddSM/tablet.jpg" style="width:100%">
  <p>Donc à gauche, un terminal qui se trouve dans le dossier '/tmp' et à droite, l'explorer Debian qui se trouve lui aussi dans le dossier '/tmp'</p>
</div>

Pour rappel, le dossier '/tmp' est automatiquement vidé lorsque l'ordinateur est éteint. 

## Apprendre à créer un dossier

Vous allez créer un dossier en ligne de commande, pour se faire, on utilise la commande '`mkdir`'. Il faut bien évidemment préciser à l'ordinateur le nom du dossier que l'on souhaite créer. Ainsi, vous devez ajouter un **argument** après la commande '`mkdir`'.

Depuis votre terminal, saisissez et exécutez la commande '`mkdir os_activite_1`'

+++bulle matthieu
  Remarquez qu'il n'y a pas d'accent à 'activite' dans la commande '<code>mkdir os_activite_1</code>'. En informatique, <b class="text-red">nous n'utilisons jamais d'accent</b>, sous peine de provoquer des erreurs qui ne sont pas forcément faciles à corriger
+++

+++question write "Après avoir exécuté la commande de création de dossier depuis le terminal, que remarquez-vous dans l'explorer à la droite de votre écran ?"

Depuis le terminal, déplacez vous dans le dossier 'os\_activite\_2'. Depuis l'explorer, déplacez vous dans le dossier 'os\_activite\_2'. Jusqu'à la fin de cette activité, assurez vous de rester <b class="text-red">toujours</b> dans le dossier '/tmp/os\_activite\_2' (et les sous dossiers que vous allez créer) dans votre terminal et votre explorateur. Si vous avez un doute, utilisez la commande '`pwd`' pour voir où se situe votre terminal.

Pour la suite, vous allez découvrir de nouvelles commandes pour effectuer des manipulations basiques sur l'ordinateur. Chaque fois que vous exécutez une commande sur le terminal à droite, vérifiez à gauche dans l'explorer si vous obtenez le résultat que vous désiriez. 

## Apprendre à créer un fichier

Pour créer un fichier de n'importe quel type, il faut utiliser la commande '`touch`' en précisant en **argument** (comme pour la commande '`mkdir`') le nom du fichier que vous voulez créer. Avec le terminal, créez quelques fichiers dans le dossier 'os\_activite\_2' et constatez leur apparition dans l'explorer.

+++question write "Quelle est la commande que vous avez utilisée pour créer un fichier ?"

En vous assurant d'être toujours dans le dossier '/tmp/os\_activite\_2', créez un nouveau dossier nommé 'toto'. Vous pouvez afficher depuis votre terminal la liste des fichiers et dossiers du dossier courant (actuellement, le dossier courant doit être '/tmp/os\_activite\_2'). Saisissez et exécutez la commande  '`ls`' pour visualiser le contenu du dossier courant dans lequel se trouve votre terminal.

## Apprendre à se déplacer de manière relative et non de manière absolue

Depuis votre terminal, vous allez vous déplacer dans le dossier 'toto' que vous venez de créer. Vous remarquerez qu'il devient pénible de saisir tout le chemin complet pour entrer dans le dossier 'toto' dans la commande '`cd /tmp/os_activite_1/toto`'. '/tmp/os\_activite\_1/toto' représente le **chemin absolu** du dossier 'toto', **absolu** car nous partons de la racine ('**/**') dans le chemin '**/**tmp/os\_activite\_1/toto'. En vous assurant d'être dans le dossier '/tmp/os\_activite\_1', utilisez plutôt la commande <code>cd</code> de la manière suivante en utilisant cette fois-ci le **chemin relatif** : '`cd toto`'. 

Ensuite, avec la commande '`pwd`' vérifiez que vous êtes bien dans le dossier '/tmp/os\_activite\_1/toto'. Pour terminer, créez deux fichiers et deux dossiers dans le dossier 'toto', choisissez les noms que vous voulez. 

+++bulle matthieu
  Dans le terminal, si vous souhaitez vous déplacer dans le dossier 'parent', utilisez la commande '<code>cd</code>' comme ceci : '<code>cd ..</code>'
+++

+++bulle matthieu droite
  '<code>..</code>' signifie 'dossier parent'. Donc la commande '<code>cd ..</code>' signifie 'Terminal ! déplace toi dans le dossier parent'
+++

## Apprendre à renommer un fichier ou un dossier

Recherchez sur Internet la commande qui permet de modifier le nom d'un fichier ou d'un dossier depuis un terminal Linux et entraînez vous sur les fichiers et dossiers qui se trouvent dans '/tmp/os\_activite\_2/' et ses sous-dossiers. 

+++question write "Quelle est la recherche que vous avez effectuée sur Internet et qui vous a permis de trouver la commande permettant de modifier le nom d'un fichier ou d'un dossier ?"

+++question write "Quelle est la commande permettant de modifier le nom d'un fichier"

+++question write "Cette commande a une autre utilité...qu'elle est-elle ?"

## Apprendre à supprimer un fichier ou un dossier

+++bulle matthieu
  <b class="text-red">Attention !</b> dans cette partie vous devez redoubler de vigilance lorsque vous utilisez les commandes de suppression de fichier ou de dossier
+++
+++bulle matthieu droite
  Assurez vous <b>toujours</b> dans cette activité que votre terminal se trouve dans le dossier '/tmp/os_activite_1' ou dans un de ses sous-dossiers
+++

Pour supprimer un fichier ou un dossier, il faut utiliser la commande '`rm`'. Dans le dossier '/tmp/os\_activite\_1', créez un fichier nommé 'asupprimer'. Puis supprimez le avec la commande '`rm -i asupprimer`'. La commande `rm` supprime complètement le fichier, il est impossible de le récupérer (ni même dans la corbeille). Pour plus de sécurité, le paramètre '`-i`' est ajouté après '`rm`' afin que le terminal demande votre validation pour supprimer le fichier.

+++bulle matthieu
  Pour consulter les paramètres disponibles pour une commande basique de Linux, vous pouvez utiliser Internet et aussi le manuel. Pour afficher la manuel d'une commande, il suffit de saisir et d'exécuter la commande précédée de <code>man</code>
+++
+++bulle matthieu droite
  Par exemple, exécuter la commande '<code>man rm</code>' affiche sur le terminal le manuel de la commande '<code>rm</code>'. Pour quitter le terminal, il suffit d'appuyer sur la touche 'q' (q comme <b>q</b>uit) du clavier
+++

Utilisez cette même commande pour supprimer un dossier qui se trouve dans '/tmp/os\_activite\_2'. Un message d'erreur va apparaître, cherchez sur Internet comment utiliser la commande '`rm`' pour supprimer un dossier. **Attention !** lorsque vous l'avez trouvé, **ne l'exécutez pas**, appelez votre formateur et montrez lui la commande pour vérifier que c'est bon ;) 

+++question "Quelle est la commande qui permet de supprimer un dossier ?"

## Quelques commandes supplémentaires

Dans cette partie se trouvent quelques commandes énoncées en vrac. Elles ne sont pas dangereuse, donc je vous invite à les essayer et de vous renseigner, si besoin, sur Internet pour voir des exemples d'utilisation de ses commandes.

### Des commandes très pratiques
  
- la commande `tree` qui permet d'afficher l'arborescence d'un dossier et de ses sous-dossiers
- la commande `find .` qui permet d'afficher tous les fichiers et dossiers à partir de là où se situe le terminal 
- la combinaison de commandes `find . | grep "nomDuFichierOuDOssier"` qui permet de retrouver un fichier ou dossier dans le disque dur
- la commande `htop` qui permet d'afficher l'état en temps réel de l'ordinateur (RAM et processeur en cours d'utilisation, processus en cours, ...). Appuyez sur la touche 'q' pour quitter 'htop'
- la commande `history` qui permet de lister toutes les dernières commandes effectuées par l'utilisateur courant
- la commande `dh -h /` qui permet d'afficher l'espace restant sur la partition du disque dur occupée par Linux

### Des commandes inutiles (et donc indispensables)

- la commande `fortune` qui affiche un adage aléatoire
- la commande `cowsay` qui permet d'afficher un message énoncé par une vache en ASCII. Exemple : `cowsay 'Bonjour !'
- les commandes `fortune` et `cowsay` peuvent être combinées. Exemple `fortune | cowsay`
- la commande `lolcat` qui permet de rendre moins morose l'affichage des résultats des commandes sur le terminal. Exemples : `fortune | lolcat`, ou encore `fortune | cowsay | lolcat`, ou encore `htop | lolcat`, ou encore `history | lolcat`, etc etc...
- la commande `toilet` qui permet d'afficher des titres en ascii super stylés ! exemples : `toilet 'Bonjour !'``
    - Les parmètres de la commande `toilet` sont très variés, vous pouvez trouver beaucoup d'exemple d'utilisation
    - La commande `toilet` est évidemment combinable avec la commande `lolcat` ;) je vous laisse trouver comment faire !

+++bulle matthieu
  Vous pouvez continuer à rechercher des commandes insolites en fouillant dans Internet (avec une recherche de type 'funny command linux')
+++

## Résultats

<ol start="0">
  <li>
  +++spoil
    Le terminal se trouve dans le dossier '/tmp'. Après avoir créé le dossier 'os_activite_1' depuis le terminal, le nouveau dossier s'affiche dans l'explorer.
  spoil+++
  </li>
  <li>
  +++spoil
    Pour créer un fichier, il faut utiliser la commande '<code>touch</code>'. Si on souhaite que le fichier soit nommé 'faa', il faut donc utiliser la commande '<code>touch faa</code>'
    <br><br>
    +++bulle matthieu
      Tout comme pour la commande '<code>mkdir</code>', il est possible de créer plusieurs fichiers d'un coup comme ceci : '<code>touch fichier1 fichier2.txt fichier3.png</code>'
    +++
  spoil+++
  </li>
  <li>
  +++spoil
    Il suffit de saisir dans son moteur de recherche préféré 'renommer fichier linux terminal' ou en anglais 'rename file terminal linux'.
  spoil+++
  </li>
  <li>
  +++spoil
    Pour renommer un fichier, il faut utiliser la commande '<code>mv</code>' (mv comme <b>m</b>o<b>v</b>e). Si on souhaite que le fichier 'faa' soit renommé par 'fii', il faut donc utiliser la commande '<code>mv faa fii</code>'
  spoil+++
  </li>
  <li>
  +++spoil
    Comme son nom l'indique, cette commande permet aussi de déplacer fichiers et dossiers. Voici un exemple d'utilisation de la commande '<code>mv</code>' qui permet de déplacer le fichier fii dans le dossier parent : '<code>mv fii ..</code>'
  spoil+++
  </li>
  <li>
  +++spoil
    Pour supprimer un dossier, il faut ajouter à la commande '<code>rm</code>' l'option '<code>r</code>'. Donc, par exemple, pour supprimer le dossier 'dossierasupprimer', il faut utiliser la commande '<code>rm -r dossierasupprimer</code>'. Pour plus de sécurité, et si le dossier contient peu de fichiers, utilisez aussi l'option '<code>i</code>' comme ceci : '<code>rm -ri dossierasupprimer</code>'. 
    <br><br>
    +++bulle matthieu
      L'option '<code>i</code>' vous oblige à confirmer la suppression de chacun des fichiers et/ou dossiers qui vont être supprimés
    +++
    +++bulle matthieu droite
      Les options peuvent se cumuler et être ajoutées dans n'importe quel ordre, ainsi, la commande '<code>rm -ri dossierasupprimer</code>' est strictement équivalente à '<code>rm -ir dossierasupprimer</code>'
    +++
  spoil+++
  </li>
</ol>
