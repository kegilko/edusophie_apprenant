# Gestion des droits, des utilisateurs et des goupes

+++programme
Créer de nouveau utilisateurs
Gérer les droits et permissions d’accès aux fichiers

Comprendre quels dossiers et fichiers vous avez le droit de manipuler
Sécuriser les fichiers d'un utilisateur
+++

+++sommaire

## Introduction

La plupart des systèmes d'exploitations offrent la possibilité de gérer plusieurs types d'utilisateurs : un utilisateur administrateur, un utilisateur courant, un visiteur, etc. 

Les systèmes d'exploitation basés sur UNIX (Mac OS) ou s'inspirant d'UNIX (OSs de type Linux) utilisent un système de gestion et de droit simple et puissant. 

## Préparation de son environnement de travail

Tout d'abord, vous allez préparer proprement un coin dans l'ordinateur dans lequel vous travaillerez par la suite à travers ce module : 

<div class="d-flex justify-content-center align-items-center">
  <div>
    <div class="p-3 border border-dark d-inline-block mx-auto rounded text-white bg-dark">
    ...<br>
    /<span class="isDirectory">etc</span>/<br>
    /<span class="isDirectory">home</span>/<br>
     <span style="color:#A46534"><b>nom d'utilisateur</b></span>/<br>
      <span class="isDirectory">Bureau</span>/<br>
      <span class="isDirectory">Documents</span>/<br>
      <span class="isDirectory">Images</span>/<br>
      <span class="isDirectory">Modèles</span>/<br>
      <span class="isDirectory">Musique</span>/<br>
      <span class="isDirectory">Public</span>/<br>
      <span class="isDirectory">Téléchargement</span>/<br>
      <span class="isDirectory">Vidéos</span>/<br>
    ...<br>
    /<span class="isDirectory">tmp</span>/<br>
    ...<br>
    </div>
    <div class="text-center"><b>Avant</b></div>
  </div>
  
  <div class="border border-dark border-3 rounded p-3 mx-3"><b>=></b> Ajout de 2 dossiers et d'un fichier</div>
  
  <div>
    <div class="p-3 border border-dark d-inline-block mx-auto rounded text-white bg-dark">
    ...<br>
    /<span class="isDirectory">etc</span>/<br>
    /<span class="isDirectory">home</span>/<br>
     <span style="color:#A46534"><b>nom d'utilisateur</b></span>/<br>
  
      <span class="isDirectory">Bureau</span>/<br>
      <span class="isDirectory">Documents</span>/<br>
    <div class="border rounded border-danger pe-4" style="width:max-content">  <span class="isDirectory">Module_linux</span>/<br>
       <span style="color:#A46534"><b>dd/mm/yyyy</b></span>/<br>
        <span class="isFile">gestionDesDroitsUtilisateursGroupes.txt</span><br></div>
      ...<br>
     <span class="isDirectory">terminale</span>/<br>
    ...<br>
    /<span class="isDirectory">tmp</span>/<br>
    ...<br>
    </div>
    <div class="text-center"><b>Après</b></div>
  </div>
</div>
<br>
Pour se faire, ouvrez un terminal (**raccourcis 'ctrl + alt + t'**) et exécutez les commandes suivantes : 

```bash
# bash
mkdir -p /home/${USER}/Module_linux/$(date '+%d_%m_%Y')
```

Ensuite, créez dans votre 'dossier du jour' un fichier texte nommé 'gestionDesDroitsUtilisateursGroupes.txt' : 

```bash
# bash
cd /home/${USER}/Module_linux/$(date '+%d_%m_%Y') && touch gestionDesDroitsUtilisateursGroupes.txt
```

+++bulle matthieu
  le fichier 'gestionDesDroitsUtilisateursGroupes.txt' vous servira à prendre des notes si vous le souhaitez
+++

## Gestion des droits sur un OS de type Linux

### Des utilisateurs et des groupes

Les systèmes d'exploitation type UNIX ou LINUX gèrent les droits des dossiers et fichiers à travers des utilisateurs et des groupes. Les **utilisateurs** appartiennent à un ensemble de **groupe(s)** et un **groupe** contient un ensemble d'**utilisateur(s)**. Cette mécanique de **groupe** permet d'affiner les droits auxquels ont accès les **utilisateurs**. 

Lorsqu'un utilisateur est créé, un **groupe** portant le même nom que l'**utilisateur** est créé. L'**utilisateur** est automatiquement ajouté dans ce **groupe**. Pour voir dans quel(s) est votre utilisateur, exécutez dans votre terminal la commande '`groups`'.

Pour comprendre un peu mieux l'utilité des groupes, voici un exemple de configuration qu'on pourrait mettre en place sur un OS type Linux à la maison : 

+++diagramme col-8 mx-auto
flowchart LR
  A["Utilisateur 'visiteur'"]--> G1["appartient au(x) groupe(s)"]
  B["Utilisateur fas:fa-lock 'famille' fas:fa-lock"]--> G2["appartient au(x) groupe(s)"]
  G2-->H["jeuxvideo"]
  C["Utilisateur fas:fa-lock 'parent' fas:fa-lock"]--> G3["appartient au(x) groupe(s)"]
  G3-->H["jeuxvideo"]
  G3-->I["papiersAdministratif"]
+++

+++activité binome 35 activites/gestion-_des-_permissions-_sur-_Linux.md "Gestion des permissions sur Linux"

## Résumons +++emoji-writing_hand 

- Les systèmes d'exploitations type UNIX ou LINUX offrent un système de gestion de droits géré avec des utilisateurs et des groupes
- Un utilisateur peut appartenir à un ensemble de groupe(s), et un groupe peut contenir un ensemble d'utilisateur(s)
- Un dossier ou fichier ne possède qu'un seul utilisateur propriétaire et n'appartient qu'à un seul groupe
- La configuration des droits est en format : <span class="text-red">xxx</span><span class="text-green">xxx</span><span class="text-blue">xxx</span>, où x peut avoir comme valeur soit '**r**', soit '**w**', soit '**x**'
    - <span class="text-red">les trois premiers caractères</span> correspondent aux droits du propriétaire du dossier ou du fichier
    - <span class="text-green">les trois caractères du milieu</span> correspondent aux droits des utilisateurs partageant le même groupe que le dossier ou fichier
    - <span class="text-blue">les trois derniers caractères</span> correspondent aux droits de tous les autres utilisateurs qui ne rentrent ni dans <span class="text-red">la première catégorie</span>, ni dans <span class="text-green">la deuxième</span>
- La commande '`chmod`' permet de modifier les droits d'un dossier ou d'un fichier


## Allez plus loin

Il est possible de modifier le propriétaire et le groupe d'un fichier ou dossier grâce à la commande '`chown`'. Cette commande demande, par défaut, d'avoir les droits administrateurs (accessibles grâce à '`sudo`').

<div class="d-none">

- utilisateur 
- groupe



> act 0 => faire toucher des fichiers avec des droits restreints

// cours théorique sur les droits Linux : chmod

> act 1 => application a trouver

// cours théorique sur les droits Linux : chown

</div>