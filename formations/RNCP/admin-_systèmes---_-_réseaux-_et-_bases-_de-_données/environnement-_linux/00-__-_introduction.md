# Introduction

+++programme
Identifier les fonctions d’un système d’exploitation
Utiliser les commandes de base en ligne de commande

Utiliser un environnement Debian graphique (utilisation niveau bureautique)
Utiliser quelques commandes Linux essentielles
+++

+++sommaire 1

## Introduction

+++bulle matthieu 
  Petite devinette : quels logos reconnaissez vous ci-dessous ? 
+++

<div class="d-flex justify-content-center flex-wrap">
  <img class="m-2" style="max-height:80px" src="https://e7.pngegg.com/pngimages/532/642/png-clipart-blue-microsoft-windows-logo-windows-8-1-microsoft-windows-computer-software-system-windows-icon-blue-angle-thumbnail.png">
  <img class="m-2" style="max-height:80px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Ubuntu_logoib.svg/240px-Ubuntu_logoib.svg.png">
  <img class="m-2" style="max-height:80px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Android_robot.svg/204px-Android_robot.svg.png">
  <img class="m-2" style="max-height:80px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Google_Chrome_icon_%28September_2014%29.svg/240px-Google_Chrome_icon_%28September_2014%29.svg.png">
  <img class="m-2" style="max-height:80px" src="https://upload.wikimedia.org/wikipedia/fr/thumb/3/3b/Raspberry_Pi_logo.svg/190px-Raspberry_Pi_logo.svg.png">
  <img class="m-2" style="max-height:80px" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLr0m3qe14Cwi385GoJm8TdB5ienJb7zwmMQ&usqp=CAU">
</div>


### Qu'est-ce qu'un système d'exploitation ?

+++spoil "Contenu" nolabel

+++citation
  An operating system (OS) is system software that manages computer hardware, software resources, and provides common services for computer programs. 
+++ wikipedia https://en.wikipedia.org/wiki/Operating_system

spoil+++

Par exemple, lorsque je veux utiliser mon navigateur internet sur le système d'exploitation **windows**, voici ce qu'il se passe : 

<div class="mb-3 d-flex border border-dark p-3 rounded-3 justify-content-around">
  <div class="align-self-center">
  <ol>
    <li>Je double clique sur l'icône du navigateur internet</li>
    <li><b>Windows</b> cherche dans le disque dur le navigateur et le démarre</li>
    <li>Le navigateur internet se lance</li>
    <li>Pendant son démarrage, le navigateur demande à <b>Windows</b> si il peut allouer certaines ressources de l'ordinateur (mémoire vive, carte graphique, processeur, ..)</li>
    <li><b>Windows</b> vérifie que ces ressources sont disponibles ou attends qu'elles soient libérées par un autre programme</li>
    <li>Les ressources enfin disponible, <b>Windows</b> les réserve pour que le navigateur Internet puisse finir de démarrer</li>
    <li>je clic sur un bouton pour ouvrir un nouvel onglet dans mon navigateur internet</li>
    <li><b>Windows</b> vérifie que ces ressources sont disponibles ou attends qu'elles soient libérées par un autre programme</li>
    <li>etc etc...</li>
  </ol>
  </div>
  <div class=""><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/Operating_system_placement.svg/324px-Operating_system_placement.svg.png"></div>
</div>

### Bref historique

Les premiers ordinateurs, apparus dans les années 1940, possédaient un ensemble très limite de fonctionnalités. Les avancées technologiques sur les composants électroniques ont permis de créer des ordinateurs beaucoup plus polyvalents. Les premiers systèmes d'exploitations sont apparus à partir des années 1950.

Le premier système d'exploitation à marquer de son empreinte l’histoire de l’informatique est apparu en 1969 sous le nom d'**UNIX**. De nombreux systèmes d'exploitations aujourd'hui connus se basent ou s'inspirent d'**UNIX**. Les années 1980 ont vu émerger les premiers systèmes d'exploitations 'grand publique' tel que : 

<div class="col-12 col-lg-6 col-print-8 col-xxl-6 mx-auto mb-4">
<table class="table align-middle text-center">
  <tbody>
    <tr>
      <td style="max-width:80px"><img class="w-100" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/MS_DOS.JPG/96px-MS_DOS.JPG"></td>
      <td class="text-nowrap"><b>MS-DOS</b></td>
      <td>Premier système d'exploitation de l'entreprise Microsoft</td>
      <td>1981</td>
    </tr>
    <tr>
      <td style="max-width:80px"><img class="w-100" src="https://i.pinimg.com/originals/52/b7/fa/52b7fa129a290570dc111b13a5e3bc77.jpg"></td>
      <td style="width:0"><b>Mac OS</b></td>
      <td>Système d'exploitation de l'entreprise Apple</td>
      <td>1984</td>
    </tr>
    <tr>
      <td style="max-width:80px"><img class="w-100" src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Tux.svg/201px-Tux.svg.png"></td>
      <td style="width:0"><b>Linux</b></td>
      <td>Premier système d'exploitation open-source</td>
      <td>1991</td>
    </tr>
  </tbody>
</table>
</div>

### Résumons +++emoji-writing_hand 

- Un système d'exploitation (**OS** en anglais pour **O**perating **S**ystem) est un logiciel qui fait la passerelle entre les applications utilisées par un utilisateur (software) et le matériel de la machine hôte (hardware). 
- Un système d'exploitation offre à l'utilisateur un environnement graphique (ou non) facilitant l'utilisation de l'ordinateur
- Les logiciels open source sont des logiciels dont le code source est accessible
- Il est possible d'installer plusieurs systèmes d'exploitations sur un même ordinateur

## Linux : état des lieux

+++imageFloat https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Heckert_GNU_white.svg/246px-Heckert_GNU_white.svg.png
Linux à sa création est distribué sous une licence open source en suivant la philosophie des logiciels libres, mouvement initié dans les années 1980. Des communautés de passionnés, et également de grandes entreprises, ont repris Linux et l'on amélioré pour créer de nouveau systèmes d'exploitations adaptés à leurs besoins. <b>Il existe aujourd'hui des centaines de systèmes d'exploitations de type Linux.</b>
+++ wikipedia https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Linux_Distribution_Timeline.svg/3020px-Linux_Distribution_Timeline.svg.png

+++bulle matthieu
  nous utiliserons ici le système d'exploitation Debian, un OS dont la première version est parue en 1993
+++

+++bulle matthieu droite
  des dizaines de systèmes d'exploitations dérivés de Debian ont vu le jour depuis, donc prendre en main Debian permet de prendre indirectement en main des dizaines de systèmes d'exploitation Linux
+++

+++imageFloat https://upload.wikimedia.org/wikipedia/commons/thumb/3/30/Operating_systems_used_on_top_500_supercomputers_de.svg/1920px-Operating_systems_used_on_top_500_supercomputers_de.svg.png droite
Depuis 2017, <b>Linux équipe la totalité des 500 super-ordinateurs les plus puissants au monde</b>
+++ wikipédia https://fr.wikipedia.org/wiki/Superordinateur#Syst%C3%A8mes_d'exploitation_pour_superordinateurs



<ul>
  <li><a href="https://www.developpez.com/actu/226592/Machines-virtuelles-Azure-Linux-met-fin-au-regne-de-Windows-Server-et-est-desormais-par-moment-l-OS-le-plus-utilise-sur-le-cloud-de-Microsoft/" target="_blank">La grande partie des serveurs détenus par windows tournent sous des systèmes d'exploitation Linux</a></li>
  <li><a href="https://gs.statcounter.com/os-market-share/mobile/worldwide" target="_blank">Le système d'exploitation Android, basé sur Linux, domine de loin le marché du mobile</a></li>
  <li><a href="https://myamend.com/linux-takes-lead-in-iot-market-keeping-80-market-share/" target="_blank">La plupart des objets connectés utilisent les systèmes d'exploitation Linux</a></li>
  <li><a href="https://www.tecmint.com/big-companies-and-devices-running-on-gnulinux/" target="_blank">Les grands groupes et secteurs industriels utilisent des systèmes d'exploitations Linux</a></li>
  <li><a href="https://peacocksoftware.com/blog/top-websites-running-linux-2018" target="_blank">La majeur partie des sites les plus consultés dans le monde utilisent des systèmes d'exploitation Linux</a></li>
  <li>...</li>
  
</ul>

## Premiers pas sur Linux

+++activité individuel 5 ../01-__-_activites/00-__-_applications-_directes/premiers-_pas-_sur-_Debian.md "Premiers pas sur Debian"

### Résumons

- Tous les systèmes d'exploitations (Debian, Windows 10, iOS, Android, chromeOS, ...) offrent des fonctionnalités communes : 
    - pouvoir manipuler des fichiers et des dossiers (créer / supprimer / renommer / déplacer)
    - pouvoir installer des applications, qui peuvent être mises à jour par la suite
    - pouvoir mettre à jour le système d'exploitation (certains forcent la mise à jour comme windows)
    - pouvoir connecter des périphériques externes : souris, clavier, écran, clé usb, ...
- Sur Linux : 
    - les dossiers personnels se trouvent dans le dossier '/home/nom\_de\_lutilisateur'
    - le dossier '/tmp' est automatiquement vidé lorsque l'ordinateur est éteint. ce dossier est pratique pour des manipulations temporaires.

## Linux et le langage bash

Bash est le langage utilisé par défaut par Linux et son terminal. Ce langage permet d'interagir plus directement avec le système d'exploitation. La plupart des systèmes d'exploitation sur ordinateur mettent à disposition un terminal dans lequel nous pouvons exécuter des commandes.

+++citation 
Il est parfois plus simple de taper une commande que d'effectuer des manipulations demandant beaucoup de clics de souris dans une interface graphique. C'est aussi un moyen plus simple pour expliquer comment faire quelque chose à quelqu'un (sur un forum par exemple), puisqu'il suffit d'indiquer la commande et non la suite de clics à effectuer sur l'interface graphique. Cependant, même si le terminal peut être beaucoup plus efficace qu'une interface graphique sous les doigts d'un utilisateur avancé, il est moins abordable que les interfaces graphiques. 
+++ site ubuntu français https://doc.ubuntu-fr.org/terminal

+++activité binome 30 activites/se-_familiariser-_avec-_le-_terminal-_Linux.md "Se familiariser avec le terminal Linux"

### Résumons
 
- Bash est le langage de programmation natif sur Linux, il permet d'effectuer des opérations courantes d'utilisateur à travers des commandes
- Des commandes ba**sh** peuvent être exécutées directement depuis un terminal ou alors depuis un fichier .**sh**
- Pour consulter le **man**uel d'une commande '`X`', il suffit d'exécuter la commande '`man X`' (ou parfois '`X -h`' ou encore '`X -help`').
- Quelques commandes bash essentielles : 
    - '`mkdir`' (**m**a**k**e **dir**ectory) permet de créer un dossier (exemple : '`mkdir mon_nouveau_dossier`')
    - '`cd`' (**c**hange **d**irectory) permet de se déplacer un dossier (exemple : '`cd mon_nouveau_dossier`')
    - '`touch`' permet de créer un fichier (exemple : '`touch un_nouveau_fichier.txt`')
    - '`cat`' permet d'afficher le contenu d'un fichier (exemple : '`cat un_nouveau_fichier.txt`')
    - '`mv`' (**m**o**v**e) permet de renommer un fichier ou un dossier (exemple : '`mv un_nouveau_fichier.txt toto.txt`'). Cette commande permet aussi de déplacer un fichier ou un dossier (exemple : '`mv toto.txt dossierA`' pour déplacer le fichier 'toto.txt' dans le dossier 'dossierA')
    - '`rm`' (**r**e**m**ove) permet de supprimer un fichier (exemple : '`rm toto.txt`') ou un dossier (exemple : '`rm toto.txt`')
        - **Attention à cette commande !** elle efface définitivement le fichier ou dossier du disque dur